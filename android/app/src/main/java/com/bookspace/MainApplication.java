package com.bookspace;

import android.app.Application;

import com.facebook.react.ReactApplication;
import org.wonday.pdf.RCTPdfView;
import com.RNFetchBlob.RNFetchBlobPackage;
import cn.qiuxiang.react.baidumap.BaiduMapPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.bolan9999.SpringScrollViewPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.bookspace.android_upgrade.UpgradePackage;
import cn.jiguang.plugins.push.JPushModule;
import com.tencent.bugly.crashreport.CrashReport;


import java.util.Arrays;
import java.util.List;
public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
              new UpgradePackage(),
          new MainReactPackage(),
            new RCTPdfView(),
            new RNFetchBlobPackage(),
            new BaiduMapPackage(),
            new RNCWebViewPackage(),
            new SpringScrollViewPackage(),
            new RNDeviceInfo(),
            new AsyncStoragePackage(),
            new SplashScreenReactPackage(),
            new VectorIconsPackage(),
            new RNGestureHandlerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
//    JPushModule.registerActivityLifecycle(this);
    CrashReport.initCrashReport(getApplicationContext(), "9ce7d3cc9a", true);
  }
}
