/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import  setup  from './js/Setup'

// AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appName, setup);
