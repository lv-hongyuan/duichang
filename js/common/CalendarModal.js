/**
 * 日历modal
 */
import React, { Component } from 'react';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, StyleSheet } from 'react-native';
import GlobalStyles from '../common/GlobalStyles';
import { Calendar, CalendarList, LocaleConfig } from 'react-native-calendars';
import DateUtil from '../common/DateUtil'
import SafeAreaViewPlus from '../common/SafeAreaViewPlus'
import NavigationBar from '../common/NavigationBar'
import ViewUtil from '../common/ViewUtil'
import NavigationUtil from '../Navigator/NavigationUtil'


export default class CalendarModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    leftButtonClick(){
        NavigationUtil.goBack(this.props.navigation)
    }

    renderCalendar() {

        LocaleConfig.locales['fr'] = {
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            monthNamesShort:['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
            dayNamesShort: ['日', '一', '二', '三', '四', '五', '六']
        };
        LocaleConfig.defaultLocale = 'fr';

        
        return <View>
            
            <CalendarList
                theme={{
                    todayTextColor:GlobalStyles.nav_bar_color,
                }}
                onDayPress={(day) => {
                    this.props.navigation.state.params.backData(day);
                    this.props.navigation.goBack();
                }}
            />
        </View>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'日期选择'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;
        return (
            <SafeAreaViewPlus style={{ flex: 1 }} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.renderCalendar()}
            </SafeAreaViewPlus>
        )
    }
}