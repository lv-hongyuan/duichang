import {Dimensions,Platform,DeviceInfo} from "react-native";

const {height, width} = Dimensions.get('window');
const NAV_BAR_COLOR ='#BC1920';
const BACKGROUND_COLOR = '#F8F8F8';
const SEPARATE_LINE_COLOR = '#EEEEEE';  //分割线
const IS_IOS = Platform.OS === 'ios';
const IS_IPHONEX = DeviceInfo.isIPhoneX_deprecated ? true : false;
const GRAY_TEXT_COLOR = '#666666';
const TEXTINPUT_BORDER_COLOR = '#DEDEDE';

export default {
    isIos:IS_IOS,
    nav_bar_height_ios: 44,
    nav_bar_height_android: 50,
    nav_bar_color:NAV_BAR_COLOR,
    screen_height: height,
    screen_width:width,
    backgroundColor:BACKGROUND_COLOR,
    screenHeight:height,
    screenWidth:width,
    separate_line_color:SEPARATE_LINE_COLOR,
    is_iphoneX:IS_IPHONEX,
    gray_text_color:GRAY_TEXT_COLOR,
    textInput_borderColor:TEXTINPUT_BORDER_COLOR,
}