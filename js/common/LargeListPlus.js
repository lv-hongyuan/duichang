import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
import NoDataView from './NoDataView';

export default class LargeListPlus extends Component {
  //属性检查
  static propTypes = {
    NoDataViewheight: PropTypes.number,      //背景图位置
    itemHeight: PropTypes.number,             //每列数据的高度
    data: PropTypes.array,       //数据源
    renderItem: PropTypes.func,            //每条数据
    onRefresh: PropTypes.func,               //刷新事件
    renderHeader: PropTypes.func,             //列表头部
    onLoading: PropTypes.func,
    showIndicator: PropTypes.bool,            //是否显示垂直滑动条
    showBackgroundImage: PropTypes.bool,        //是否显示背景图   
    style: PropTypes.object,                //列表style
    scrollEnabled: PropTypes.bool,           //列表是否可滚动
  }

  //默认属性
  static defaultProps = {
    NoDataViewheight: 100,
    itemHeight: 110,
    data: [],
    renderHeader: () => { return null },
    showIndicator: true,
    showBackgroundImage: true,
    style: {},
    scrollEnabled: true,
  }

  constructor(props) {
    super(props);
    this.state={
      showText:''
    }
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  componentWillReceiveProps(nextProps) {
    let istoTop = nextProps.toTop
    let isendRefresh = nextProps.endRefresh
    if (isendRefresh == 'Loading') {
      this._largelist.endLoading()
    }
    if (isendRefresh == 'Refresh') {
      this._largelist.endRefresh();
      isendRefresh = ''
    }
    if (istoTop) {
      this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
    }
  }

  content() {
    let item = this.props
    return <View style={{ height: '100%' }}>
      {item.showBackgroundImage ? NoDataView.renderContent(item.NoDataViewheight, item.data.length > 0 ? false : true) : null}
      {item.data.length == 0 || !item.onLoading ? <LargeList
        style={item.style}
        ref={ref => (this._largelist = ref)}
        refreshHeader={ChineseWithLastDateHeader}
        loadingFooter={ChineseWithLastDateFooter}
        heightForIndexPath={() => item.itemHeight}
        data={this.handleLargeListData(item.data)}
        renderIndexPath={item.renderItem}
        onRefresh={() => { item.onRefresh() }}
        renderHeader={item.renderHeader}
        showsVerticalScrollIndicator={item.showIndicator}
        scrollEnabled={item.scrollEnabled}
      /> :
        <LargeList
          style={item.style}
          ref={ref => (this._largelist = ref)}
          refreshHeader={ChineseWithLastDateHeader}
          loadingFooter={ChineseWithLastDateFooter}
          heightForIndexPath={() => item.itemHeight}
          data={this.handleLargeListData(item.data)}
          renderIndexPath={item.renderItem}
          onRefresh={() => { item.onRefresh() }}
          renderHeader={item.renderHeader}
          onLoading={() => { item.onLoading() }}
          showsVerticalScrollIndicator={item.showIndicator}
          scrollEnabled={item.scrollEnabled}
        />}
    </View>
  }

  render() {
    return <View style={{ flex: 1 }}>
      {this.content()}
    </View>
  }


}