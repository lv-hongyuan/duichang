/**
 * 没有数据弹出的modal
 */
import React, { Component } from 'react';
import { Text, View, Modal, Dimensions, Platform, TouchableOpacity, StyleSheet, Image } from 'react-native';
import GlobalStyles from '../common/GlobalStyles';
import SafeAreaViewPlus from '../common/SafeAreaViewPlus'


export default class NoDataModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        }
    }

    show() {
        this.setState({
            modalVisible: true,
        })
    }

    hidden() {
        this.setState({
            modalVisible: false,
        })
    }

    renderContent() {
        return <View style={{marginTop:100,width:GlobalStyles.screenWidth,height:GlobalStyles.screenHeight-100 ,justifyContent: 'center', alignItems: 'center', backgroundColor:GlobalStyles.backgroundColor }}>
            <Image style={{ width: 100, height: 94 }} source={(require('../../resource/none-data.png'))} />
            <Text style={{ width: 300, height: 30, lineHeight: 30, textAlign: 'center', color: '#606266' }}>{'抱歉,无数据'}</Text>
        </View>
    }

    render() {
        return (
            <SafeAreaViewPlus style={{flex:1,justifyContent:'center',alignContent: 'center',backgroundColor:GlobalStyles.backgroundColor}}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderContent()}
                </Modal>
            </SafeAreaViewPlus>
        )
    }

}
