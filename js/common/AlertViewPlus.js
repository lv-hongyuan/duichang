import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, Modal, TouchableOpacity } from 'react-native';
import GlobalStyles from './GlobalStyles';

//!created by wanghy on 2020/05/28



export default class AlertViewPlus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    }
  }

  static defaultProps = {
    ViewLevel: 10,                 //? 窗口大小显示等级  0 — 12 数字越大窗口越大 默认为10
    CancelBGA: '#aaa',             //? 默认取消键背景颜色
    ConFirmBGA: GlobalStyles.nav_bar_color,  //?默认确定键背景颜色
    CancelText: '取消',             //? 默认左按钮文字
    ConFirmText: '确定',            //? 默认右按钮文字
    ShowCancelButton: true,        //? 默认显示取消按钮  不显示取消按钮的时候确定键背景颜色为红色字体为白色
    title: null,                   //? 默认标题
    message: null,                 //? 默认详细信息
    ButtonfontSize:null,           //? 按钮文字大小     默认为ViewLevel 1.7倍
    titleFontSize:null,            //? 标题文字大小     默认为ViewLevel 1.7倍
    mesFontSize:null,              //? 详细信息文字大小  默认为ViewLevel 1.5倍
  };

  showAlert() {
    this.setState({ modalVisible: true })
  }

  //点击了取消按钮
  closeAlert() {
    this.setState({ modalVisible: false })
    // this.props.alertCancelDown();
  }
  //点击了确定按钮
  alertSureDown() {
    this.setState({ modalVisible: false })
    this.props.alertSureDown();     //? 点击确认按钮事件
  }

  render() {
    return (
      <Modal
        animationType={"none"}
        visible={this.state.modalVisible}
        transparent={true}
      >
        <View style={styles.contain}>
          <View style={[styles.viewPage, {
            width: this.props.ViewLevel * 30,
            height: this.props.ViewLevel * 20
          }]}>
            <View
              style={{
                paddingHorizontal: 5, flex: 1, borderBottomColor: '#eee', borderBottomWidth: 1,
                justifyContent:'center',alignItems:'center'
              }}>
              <View style={{
                height:'30%',justifyContent:'center',alignItems:'center'
              }}>
                <Text style={{
                  fontSize:this.props.titleFontSize ? this.props.titleFontSize : this.props.ViewLevel * 1.7
                }}>
                  {this.props.title}
                </Text>
              </View>
              <View>
                <Text style={{
                  fontSize:this.props.mesFontSize ? this.props.mesFontSize : this.props.ViewLevel * 1.5
                }}>
                  {this.props.message}
                </Text>
              </View>
            </View>
            <View style={{
              flexDirection: 'row',
              height: '27%'
            }}>
              {this.props.ShowCancelButton ? <TouchableOpacity
                style={[styles.button, { borderRightWidth: 1, borderColor: '#EEE' }]}
                onPress={() => {this.closeAlert()}}
              >
                <Text style={{
                  color: this.props.CancelBGA,
                  fontSize: this.props.ButtonfontSize ? this.props.ButtonfontSize : this.props.ViewLevel * 1.7,
                }}>
                  {this.props.CancelText}
                </Text>
              </TouchableOpacity> : null}

              <TouchableOpacity
                style={[styles.button, {
                  backgroundColor: this.props.ShowCancelButton ? null : GlobalStyles.nav_bar_color,
                }]}
                onPress={() => this.alertSureDown()}
              >
                <Text style={{
                  color: this.props.ShowCancelButton ? this.props.ConFirmBGA : '#fff',
                  fontSize: this.props.ButtonfontSize ? this.props.ButtonfontSize : this.props.ViewLevel * 1.7,
                }}>
                  {this.props.ConFirmText}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewPage: {
    borderRadius: 5,
    backgroundColor: '#fff',
    overflow:'hidden'
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {

  }
})