/**
 * 带背景的菊花
 */
import React, { Component } from 'react';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity,ActivityIndicator, StyleSheet } from 'react-native';
import GlobalStyles from './GlobalStyles';
export default class ActivityIndicatorPlus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            indicatorAnimating: false, //菊花
        }
    }

    show() {
        this.setState({
            modalVisible: true,
            indicatorAnimating: true, 
        })
    }

    dismiss(){
        this.setState({
            modalVisible: false,
            indicatorAnimating: false, 
        })
    }

    render() {
        return (
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                   {this.state.indicatorAnimating && (
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 + 100, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='gray'
                        />)} 
                </Modal>
        )
    }
}

const styles = StyleSheet.create({
    indicatorStyle: {
        zIndex: 119,
    }
});


