import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Image, Dimensions, Platform, Keyboard, TouchableOpacity, TextInput, KeyboardAvoidingView, } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class NoDismissAlertView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            placeViewHeight: 0,
            // checkIndex: 1,
            modalVisible: false,
        }
    }

    static propTypes = {
        /****************是否显示************************/
        ModalVisible: PropTypes.bool,//是否显示对话框
        subTitleVisible: PropTypes.bool,//是否显示审核按钮

        /********文字****************/
        TitleText: PropTypes.any,//标题文字
        DesText: PropTypes.any,//描述文字
        CancelText: PropTypes.any,//取消文字
        OkText: PropTypes.any,//确定文字

        /***********宽高距离*************/
        HeightModal: PropTypes.any,//这个弹窗的高度
        WidthModal: PropTypes.any,//这个弹窗的宽度
        TitleHeight: PropTypes.any,//这个弹窗的标题高度
        TitleWidth: PropTypes.any,//这个弹窗的标题宽度
        TitleMarginTop: PropTypes.any,//标题顶部距离
        TitleMarginBottom: PropTypes.any,//标题底部距离
        BottomHeight: PropTypes.any,//这个弹窗的底部高度
        BottomWidth: PropTypes.any,//这个弹窗的底部宽度

        /********字体****************/
        TitleFontSize: PropTypes.number,//标题的文字大小
        TitleFontColor: PropTypes.any,//标题的文字颜色
        oneBtnSubtitleFontSize: PropTypes.number,//第一个按钮文字大小
        oneBtnSubtitleFontColor: PropTypes.any,//第一个按钮文字颜色
        TwoBtnSubtitleFontSize: PropTypes.number,//第二个按钮文字大小
        TwoBtnSubtitleFontColor: PropTypes.any,//第二个按钮文字颜色
        DescriptionFontSize: PropTypes.number,//描述的文字大小
        DescriptionFontColor: PropTypes.any,//描述的文字颜色
        BottomLeftFontSize: PropTypes.number,//下面取消确定的文字大小
        BottomLeftFontColor: PropTypes.any,//下面取消确定的文字的颜色
        BottomRightFontSize: PropTypes.number,//下面取消确定的文字大小
        BottomRightFontColor: PropTypes.any,//下面取消确定的文字的颜色


    }
    //显示alert
    showAlert() {
        this.setState({ modalVisible: true })
        
    }
    //点击了取消按钮
    closeAlert() {
        
    }
    //点击了确定按钮
    alertSureDown() {
        this.props.alertSureDown();
    }

    render() {
        return (
            <View>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                    // onRequestClose={() => this.setState({ modalVisible: false })}
                >
                    {this.renderContent()}
                </Modal>
            </View>
        );
    }

    renderContent() {
        return (
            <View style={styles.ViewPage}>
                <View style={{
                    // height: this.props.HeightModal ? this.props.HeightModal : (this.props.TextInputVisible?300:150),
                    height: this.props.HeightModal ? this.props.HeightModal : 150,
                    width: this.props.WidthModal ? this.props.WidthModal : 303,
                    backgroundColor: 'white',
                    borderRadius: 4,
                    display: 'flex',
                    marginBottom: this.state.placeViewHeight,
                }}>
                    <View style={{ flex: 1, justifyContent: 'space-around', marginTop: 10 }}>
                        {
                            /********title**********/
                            <View style={{
                                height: this.props.TitleHeight ? this.props.TitleHeight : (this.props.HeightModal ? (this.props.HeightModal - 50) : 100),
                                width: this.props.TitleWidth ? this.props.TitleWidth : 303,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                                <Text style={{
                                    fontSize: this.props.TitleFontSize ? this.props.TitleFontSize : 18,
                                    color: this.props.TitleFontColor ? this.props.TitleFontColor : '#243047',
                                    // marginTop: this.props.TitleMarginTop ? this.props.TitleMarginTop : 18,
                                    marginBottom: this.props.TitleMarginBottom ? this.props.TitleMarginBottom : 10,
                                    marginLeft: 15,
                                    marginRight: 15,
                                }}>{this.props.TitleText}</Text>
                                {
                                    this.props.DesText &&
                                    <Text style={{
                                        fontSize: this.props.TitleFontSize ? this.props.TitleFontSize : 18,
                                        color: this.props.TitleFontColor ? this.props.TitleFontColor : '#243047',
                                        // marginTop: this.props.TitleMarginTop ? this.props.TitleMarginTop : 18,
                                        marginBottom: this.props.TitleMarginBottom ? this.props.TitleMarginBottom : 10,
                                        marginLeft: 15,
                                        marginRight: 15,
                                    }}>{this.props.DesText}</Text>
                                }
                            </View>
                        }

                        <View style={{ flex: 1 }} />

                        <View style={{
                            /***取消确定**/
                            height: this.props.BottomHeight ? this.props.BottomHeight : 50,
                            width: this.props.BottomWidth ? this.props.BottomWidth : 303,
                            flexDirection: 'row',
                            borderTopWidth: 0.5,
                            borderColor: '#E5E5E5',
                        }}>
                            {/* <TouchableOpacity style={{ flex: 1 }}
                                // onPress={() => this.setState({ modalVisible: false })}
                                onPress={()=>this.closeAlert()}
                            >
                                <View style={{
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderRightWidth: 0.5,
                                    borderColor: '#E5E5E5'
                                }}>
                                    <Text style={{
                                        fontSize: this.props.BottomLeftFontSize ? this.props.BottomLeftFontSize : 18
                                        , color: this.props.BottomLeftFontColor ? this.props.BottomLeftFontColor : '#9EA3AD'
                                    }}>{this.props.CancelText ? this.props.CancelText : '取消'}</Text>
                                </View>
                            </TouchableOpacity> */}
                            <TouchableOpacity style={{ flex: 1 }}
                                onPress={() => this.alertSureDown()
                                }>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', borderBottomRightRadius: 8 }}>
                                    <Text style={{
                                        fontSize: this.props.BottomRightFontSize ? this.props.BottomRightFontSize : 18
                                        , color: this.props.BottomRightFontColor ? this.props.BottomRightFontColor : '#3576F0'
                                    }}>{this.props.OkText ? this.props.OkText : '确定'}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </View>
        )
    }

}
const styles =
{
    ViewPage: {
        width: deviceWidth,
        height: deviceHeight,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
};
