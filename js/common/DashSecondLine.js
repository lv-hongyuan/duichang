import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
} from 'react-native';
/*水平方向的虚线
 * len 虚线个数
 * width 总长度
 * backgroundColor 背景颜色
 * */
export default class DashSecondLine extends Component {
    render() {
        var len = this.props.len;
        var arr = [];
        for (let i = 0; i < len; i++) {
            arr.push(i);
        }
        return <View style={[styles.dashLine, { width: this.props.width,marginLeft:this.props.marginL,marginTop:this.props.marginT }]}>
            {
                arr.map((item, index) => {
                    return <Text style={{ color: this.props.bgColor, fontSize: 10 }} key={'dash' + index}>-</Text>;
                })
            }
        </View>
    }
}
const styles = StyleSheet.create({
    dashLine: {
        flexDirection: 'row',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    dashItem: {
        height: 0.5,
        width: 2,
        marginRight: 2,
        flex: 1,
    }
})