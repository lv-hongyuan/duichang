
// *********  数字校验

import React from 'react';

export default class NumberCheck {

  //判断正整数或者正小数（包含0）
  // val  传入的数字
  static validFloat(val) {
    if (val == '0' || val == 0) {
      return true
    }
    // let floatP = /^([1-9]+[0-9]*)(\.[0-9]{1,4})?$/
    let floatP = /^[1-9]\d*.\d*|0\.\d*[1-9]\d*$/
    let intP = /^[1-9]\d*$/
    if (floatP.test(val)) {
      return true
    } else if (intP.test(val)) {
      return true
    } else {
      return false
    }
  }


  //判断是小数或整数(包含正负)
  // val  传入的数字
  static validFloatNum(val) {
    let P1 = /^-(0|[1-9]+[0-9]*)(\.[0-9]{1,4})?$/
    let P2 = /^([1-9]+[0-9]*)(\.[0-9]{1,4})?$/
    let P3 = /^[1-9]\d*$/
    if (P1.test(val)) {
      return true
    } else if (P2.test(val)) {
      return true
    } else if (P3.test(val)) {
      return true
    } else {
      return false
    }
  }



  //判断正整数或者正小数(不包含0)
  // val  传入的数字
  static validFloatAndInt(val) {
    let floatP = /^([1-9]+[0-9]*)(\.[0-9]{1,4})?$/
    let intP = /^[1-9]\d*$/
    if (floatP.test(val)) {
      return true
    } else if (intP.test(val)) {
      return true
    } else {
      return false
    }
  }


  //电话号码校验
  // val  传入的数字
  static validatePhone(val) {
    let isPhone = /^1[3|4|5|6|7|8|9][0-9]{9}$|^[0-9]{3}-[0-9]{8}$|^[0-9]{4}-[0-9]{7,8}$|[0-9]{3}[0-9]{8}$|^[0-9]{4}[0-9]{7,8}$/;
    if (isPhone.test(val)) {
      if (val.indexOf("-") != -1) {
        if (val.split("-")[0].length === 3) {
          if (val.split("-")[1].length !== 8) {
            return false;
          }
        }
        if (val.split("-")[0].length === 4) {
          if (val.split("-")[1].length !== 7 && val.split("-")[1].length !== 8) {
            return false;
          }
        }
      } else {
        if (val.length != 11) {
          return false;
        }
      }
      return true
    } else {
      return false
    }
  }

  //校验字符串是否是空
  // val  传入的字符串
  stringIsEmpty(obj) {
    if (typeof obj == "undefined" || obj == null || obj == '' || obj == '请选择') {
      return true;
    } else {
      return false;
    }
  }

}