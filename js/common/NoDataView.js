/**
 * 没有数据弹出的modal
 */
import React, { Component } from 'react';
import { Text, View,Image } from 'react-native';
import GlobalStyles from '../common/GlobalStyles';


export default class NoDataView {
    static renderContent(topMargin = 0,showItem = true) {
        return <View style={{
            width: '100%',
            alignItems: 'center',
            position: 'absolute',
            top: GlobalStyles.screen_height / 2 - topMargin
        }}
        >
            {showItem ? 
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{ width: 100, height: 94 }} source={(require('../../resource/none-data.png'))} />
                    <Text style={{ width: 300, height: 30, lineHeight: 30, textAlign: 'center', color: '#606266' }}>抱歉,无数据</Text>
                </View> : null
            }


        </View>
    }
    // renderContent() {
    //     return <TouchableOpacity
    //         style={{ width: GlobalStyles.screenWidth, height: GlobalStyles.screenWidth, justifyContent: 'center', alignItems: 'center', backgroundColor: GlobalStyles.backgroundColor }}
    //         activeOpacity={1}
    //         onPressIn={(event)=>{
    //             // console.log('开始触摸',event.nativeEvent.locationY)
    //             this.startT = event.nativeEvent.locationY
    //         }}
    //         onPressOut={(event)=>{
    //             // console.log('结束触摸',event.nativeEvent.locationY)
    //             if((event.nativeEvent.locationY - this.startT)>60){
    //                 if(this.props.noDataViewCallBack){
    //                     this.props.noDataViewCallBack()
    //                 }   
    //             }
    //         }}
    //     >
    //         <Image style={{ width: 100, height: 94 }} source={(require('../../resource/none-data.png'))} />
    //         <Text style={{ width: 300, height: 30, lineHeight: 30, textAlign: 'center', color: '#606266' }}>{'抱歉,无数据'}</Text>
    //     </TouchableOpacity>
    // }
    // render() {
    //     return (
    //         <View
    //             style={{
    //                 width: '100%',
    //                 alignItems: 'center',
    //                 position:'absolute',
    //                 top:GlobalStyles.screen_height/2-100
    //             }}
    //         >
    //             {/* {this.renderContent()} */}
    //             <Image style={{ width: 100, height: 94 }} source={(require('../../resource/none-data.png'))} />
    //             <Text style={{ width: 300, height: 30, lineHeight: 30, textAlign: 'center', color: '#606266' }}>{'抱歉,无数据'}</Text>
    //         </View>
    //     )
    // }

}
