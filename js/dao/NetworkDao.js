import RNFetchBlob from 'rn-fetch-blob'
import {Platform} from "react-native";
import publicDao from './publicDao';

// import RNFetchBlodownload from 'rn-fetch-blob'
var BASE_URL = '172.16.1.61:8110';
// var BASE_URL = 'dingcang.zhonggu56.com';
var BASE_DOMAIN = 'http://' + BASE_URL + '/application/'

export default class NetworkDao {
    static fetchPostNet(path, param) {
        let net_url = BASE_DOMAIN + path
        console.log('net_url is :', net_url)
        return new Promise((resolve, reject) => {
            fetch(net_url, {
                method: 'POST',
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'application/json',
                },
                body: param
            }).then((response) => {
                if (response.ok) {
                    response.text().then(text => {
                        //数据解析
                        let jsonData = JSON.parse(text)
                        console.log('接口名：', path, '可用数据:', jsonData)
                        resolve(jsonData)

                    })
                }
            }).catch((error) => {
                console.log('error:', error)
                reject(error);
            })
        })
    }

    static fetchGetNet(path) {
        let net_url = BASE_DOMAIN + path
        console.log('net_url is :', net_url)
        return new Promise((resolve, reject) => {
            fetch(net_url, {
                method: 'GET',
            }).then((response) => {
                if (response.ok) {
                    response.text().then(text => {
                        resolve(text)
                    })
                }
            }).catch((error) => {
                console.log('error:', error)
                reject(error);
            })
        })
    }

    static downloadFile(path, isPDF,fileName) {
        let net_url = BASE_DOMAIN + path
        console.log('下载地址',net_url)
        // let fileName = 'abc.pdf'
        let dirs =  RNFetchBlob.fs.dirs
        return new Promise((resolve, reject) => {
            RNFetchBlob
                .config({
                    // fileCache: true,
                    path: publicDao.DOWNLOAD_FILE + '/' + fileName,
                })
                .fetch('GET', net_url, {
                    //some headers ..
                })
                .then((res) => {
                    // the temp file path
                    console.log(res.path())
                    resolve(res)
                }).catch((error) => {
                    console.log('error:', error)
                    reject(error);
                })
        })
    }

}