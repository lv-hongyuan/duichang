import AsyncStorage from '@react-native-community/async-storage';
const USERINFO_KEY = 'userInfoKey'

export default class DataDao {
    //保存当前登陆用户的信息
    saveCurrentUserInfo(info) {
        AsyncStorage.setItem(USERINFO_KEY, JSON.stringify(info), (error) => {
            if (error) {
                console.log('保存用户信息失败')
            } else {
                console.log('保存用户信息成功')
            }
        })
    }

    //获取当前登陆用户的信息
    getCurrentUserInfo(callback){
        AsyncStorage.getItem(USERINFO_KEY, (error, result) => {
            if (error) {
                console.log('获取用户信息失败')
            } else {
                console.log('当前用户信息:', result)
                if (result === null) {
                    callback('{}')
                } else {
                    callback(result)
                }
            }
        })
    }

    //清除当前用户信息
    clearCurrentUserInfo(callback) {
        AsyncStorage.removeItem(USERINFO_KEY, (error) => {
            if (error) {
                console.log('删除失败:', error)
                callback('0')
            } else {
                console.log('删除成功')
                callback('1')
            }
        })
    }

    //保存下载文件的信息
    saveDownloadFile(fileKey,info) {
        AsyncStorage.setItem(fileKey, JSON.stringify(info), (error) => {
            if (error) {
                console.log('保存下载文件信息失败')
            } else {
                console.log('保存下载文件信息成功')
            }
        })

    }

    //获取下载文件的信息
    getDownloadFileInfo(fileKey,callback){
        AsyncStorage.getItem(fileKey, (error, result) => {
            if (error) {
                console.log('获取下载文件信息失败')
            } else {
                console.log('当前文件信息:', result)
                if (result === null) {
                    callback('{}')
                } else {
                    callback(result)
                }
            }
        })
    }

    //清除当前用户信息
    clearDownloadFileInfo(fileKey,callback) {
        AsyncStorage.removeItem(fileKey, (error) => {
            if (error) {
                console.log('删除失败:', error)
                callback('0')
            } else {
                console.log('删除成功')
                callback('1')
            }
        })
    }

}
