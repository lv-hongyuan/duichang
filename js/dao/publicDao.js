/**
 * 全局变量
 */

export default {
    CURRENT_TOKEN: '',
    COMPANY:'',
    USERNAME:'',
    IS_LOGOUT:'1',
    BoxStateView_NavigationKey:'',
    YG_FLAG:'',
    DOOR_TOKEN:'',
    DOWNLOAD_FILE:'',
    PHONE_NUMBER:'',      //用户手机号
    PHONE_PWD:'',         //手机号登陆时的密码
    IS_PHONE_LOGIN:'0',   //是否是手机号登陆
    IS_IDENTIFY_LOGIN:'0',  //是否是手机验证码登陆
    IS_ONLINE:'0'
}