
/**
 * 登陆
 */
import React, { Component } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image, ImageBackground, ShadowPropTypesIOS, TextInput, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../Navigator/NavigationUtil';
import GlobalStyles from '../common/GlobalStyles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LoginInputView from './LoginInputView'
import DataDao from '../dao/DataDao'
import NetworkDao from '../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import publicDao from '../dao/publicDao'
import DeviceInfo from 'react-native-device-info'
import ViewUtil from '../common/ViewUtil'
import { EMITTER_LOGIN_SUCESS, EMITTER_CREATECOSTBILLSUCCESS_TYPE, EMITTER_CREATETRANSBILLSUCESS_TYPE, EMITTER_TRANSCOSTSEARCHSUCESS_TYPE } from '../common/EmitterTypes'
import RNFetchBlob from 'rn-fetch-blob';
import ActivityIndicatorPlus from '../common/ActivityIndicatorPlus'

let dataDao = new DataDao();
let countDown = 60
let _this = null
export default class LoginView extends Component {

  constructor(props) {
    super(props);
    _this = this
    this.params = this.props.navigation.state.params
    if (this.params == null) {
      this.params = {}
    }
    this.isShowAlertUser = true
    this.canTimer = true
    this.state = {
      indicatorAnimating: false, //菊花
      companyName: '',             //公司名
      userName: '',                //用户名
      mainPWD: '',                 //主用户名
      secondPWD: '',             //副密码
      loginWay: true,             //登录方式
      phoneNumber: '',            //手机号
      passWordWay: true,          //手机号登录模式下的密码输入方式(true是手机号密码登陆)
      sendAutoNumber: true,        //发送验证码按钮是否可点击
      numbers: countDown,          //倒计时显示数字
      autoNumber: '',               //验证码
      phonePwd: '',                //手机号密码
    }
    if (this.params.isMine == null) {
      this.params.isMine = '0'
    }
    if (this.params.isMine && this.params.isMine == '1') {
      this.isShowAlertUser = false      //不显示：‘此信息需用户登录后,才可以查看详情’
    }
    if (this.params.isMineMsg && this.params.isMineMsg == '1') {
      this.isShowAlertUser = true        //显示：‘此信息需用户登录后,才可以查看详情’
    }
  }

  componentDidMount() {
    dataDao.getCurrentUserInfo(data => {
      let jsonData = JSON.parse(data)

      if (jsonData.isPhoneLogin == null) {      //记录登录状态：账号密码登录
        this.setState({ loginWay: false, passWordWay: false })
      } else if (jsonData.isPhoneLogin == '0') {
        this.setState({ loginWay: true })
      }
      else {
        if (jsonData.isIdentifyLogin && jsonData.isIdentifyLogin == '0') {
          this.setState({ loginWay: false, passWordWay: true })       //记录登录状态：手机号+密码登录
        } else {
          this.setState({ loginWay: false, passWordWay: false })       //记录登录状态：手机号+验证码登录
        }
      }

      if (jsonData.userName && jsonData.userName.length > 0) {
        if (publicDao.IS_LOGOUT == '1') {
          if (jsonData.phoneNum && jsonData.phoneNum.length > 0) {     //输入过手机号：退出登录时的账号密码输入框默认值
            this.setState({
              companyName: jsonData.companyName,
              userName: jsonData.userName,
              mainPWD: '',
              secondPWD: '',
              autoNumber: '',
              phoneNumber: jsonData.phoneNum,
              phonePwd: ''
            })
          } else {
            this.setState({                                      //未输入过手机号：退出登录时的账号密码默认值
              companyName: jsonData.companyName,
              userName: jsonData.userName,
              mainPWD: '',
              secondPWD: '',
              autoNumber: '',
              phonePwd: ''
            })
          }

        } else {                          //已经登录且输入过手机号：由于其他原因返回登录界面时的各个输入框默认值
          if (jsonData.phoneNum && jsonData.phoneNum.length > 0) {
            let phonePWD = null
            if (jsonData.phonePWD && jsonData.phonePWD.length > 0) {
              phonePWD = jsonData.phonePWD
            }
            if (this.state.passWordWay) {      //已经登录：账号密码登录输入框默认值
              this.setState({
                companyName: jsonData.companyName,
                userName: jsonData.userName,
                mainPWD: jsonData.mainPWD,
                secondPWD: jsonData.secondPWD,
                phoneNumber: jsonData.phoneNum,
                phonePwd: phonePWD
              })
            } else {
              this.setState({
                companyName: jsonData.companyName,
                userName: jsonData.userName,
                mainPWD: jsonData.mainPWD,
                secondPWD: jsonData.secondPWD,
                phoneNumber: jsonData.phoneNum
              })
            }
          } else {
            this.setState({
              companyName: jsonData.companyName,
              userName: jsonData.userName,
              mainPWD: jsonData.mainPWD,
              secondPWD: jsonData.secondPWD
            })
          }
        }
      }
    })
  }

  //创建单证下载文件夹
  createDownloadFile(dir) {
    RNFetchBlob.fs.exists(dir)
      .then((res) => {
        if (res) {
          console.log('文件夹已存在')
        } else {
          RNFetchBlob.fs.mkdir(dir)
            .then((res) => {
              if (res) {
                console.log('创建文件夹成功')
                publicDao.DOWNLOAD_FILE = dir
              }
            })
            .catch((error) => {
              console.log('创建文件夹失败', error)
            })
        }
      })
      .catch((error) => {
        console.log('检测文件夹失败')
      })

  }

  //使用账号密码登录
  loginBtnClick() {
    // let _this = this
    // console.log('登陆按钮点击事件')
    let currentInfo = {}
    currentInfo.companyName = this.state.companyName.trim();
    currentInfo.userName = this.state.userName.trim();
    currentInfo.mainPWD = this.state.mainPWD.trim();
    currentInfo.secondPWD = this.state.secondPWD.trim();
    dataDao.saveCurrentUserInfo(currentInfo)

    if (this.state.companyName && this.state.companyName.length > 0 && this.state.userName && this.state.userName.length > 0 && this.state.mainPWD && this.state.mainPWD.length > 0 && this.state.secondPWD && this.state.secondPWD.length > 0) {
      _this.refs.activityIndicatorPlus.show()
      let params = {}
      params.uniqueCode = DeviceInfo.getUniqueID()
      params.userName = this.state.userName
      params.masterPassword = this.state.mainPWD
      params.subPassword = this.state.secondPWD
      params.companyName = this.state.companyName
      let paramsStr = JSON.stringify(params)
      NetworkDao.fetchPostNet('login.json', paramsStr)
        .then(data => {
          _this.refs.activityIndicatorPlus.dismiss()
          if (data._backcode == '200') {
            publicDao.CURRENT_TOKEN = data.token
            publicDao.YG_FLAG = data.yjflag
            publicDao.DOOR_TOKEN = data.doorToken
            publicDao.IS_LOGOUT = "0"
            publicDao.COMPANY = this.state.companyName
            publicDao.USERNAME = this.state.userName
            publicDao.IS_PHONE_LOGIN = '0'
            publicDao.IS_IDENTIFY_LOGIN = '0'
            let currentInfo = {}
            currentInfo.uniqueCode = DeviceInfo.getUniqueID()
            currentInfo.userName = this.state.userName
            currentInfo.mainPWD = this.state.mainPWD
            currentInfo.secondPWD = this.state.secondPWD
            currentInfo.companyName = this.state.companyName
            currentInfo.token = data.token
            currentInfo.yjflag = data.yjflag
            currentInfo.doorToken = data.doorToken
            currentInfo.isPhoneLogin = '0'
            currentInfo.isIdentifyLogin = '0'
            let dirs = RNFetchBlob.fs.dirs
            let dir = Platform.OS == 'ios' ? dirs.DocumentDir + '/' + this.state.companyName : dirs.DownloadDir + '/' + this.state.companyName
            currentInfo.downloadFile = dir
            dataDao.saveCurrentUserInfo(currentInfo)
            NavigationUtil.navigation = this.props.navigation;
            // NavigationUtil.goPage({}, 'Main')
            // console.log('===',this.params.routeName)
            publicDao.DOWNLOAD_FILE = dir
            this.createDownloadFile(dir)
            this.timer && clearInterval(this.timer)
            this.setState({
              sendAutoNumber: true,
              numbers: countDown
            })
            if (this.params == null || this.params.routeName == null || this.params.routeName == 'Main') {
              NavigationUtil.goPage({}, 'HomeView')
            } else {
              NavigationUtil.goPage({}, this.params.routeName)
            }
            DeviceEventEmitter.emit(EMITTER_LOGIN_SUCESS);
            DeviceEventEmitter.emit(EMITTER_CREATETRANSBILLSUCESS_TYPE);
            DeviceEventEmitter.emit(EMITTER_CREATECOSTBILLSUCCESS_TYPE);
            DeviceEventEmitter.emit(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE);
          } else {
            this.refs.toast.show(data._backmes, 5000)
          }
        })
        .catch(error => {
          this.refs.toast.show('登录失败', 800)
          _this.refs.activityIndicatorPlus.dismiss()
        })
    } else {
      this.refs.toast.show('请填写完整数据', 1200)
    }


  }
  leftButtonClick() {
    // console.log('===',this.params,this.params.isMine)
    if (this.params.isMine == null) {
      this.params.isMine = '0'
    }
    if (this.params && this.params.isMine && this.params.isMine == '0') {
      NavigationUtil.goPage({}, 'HomeView')
    } else {
      NavigationUtil.goBack(this.props.navigation)
    }
  }

  //电话号码校验
  validatePhone(val) {
    let isPhone = /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/;
    if (isPhone.test(val)) {
      return true
    } else {
      return false
    }
  }

  //验证码登录和手机号密码登陆
  autoNumberLogin() {
    // let _this = this
    let phoneNum = this.state.phoneNumber
    let phoneCode = this.state.passWordWay ? this.state.phonePwd : this.state.autoNumber
    let phoneLoginType = this.state.passWordWay ? '10' : '20'
    let passWordWay = this.state.passWordWay
    //输入框校验：
    if (phoneNum && phoneNum.length > 0 && phoneCode && phoneCode.length > 0) {
      let isY = this.validatePhone(phoneNum)
      if (!isY) {
        this.refs.toast.show('请输入正确的电话号码', 800)
        return
      }
      if (!passWordWay && phoneCode.length != 6) {
        this.refs.toast.show('验证码错误', 800)
        return
      }

      //网络请求：
      _this.refs.activityIndicatorPlus.show()
      let params = {}
      params.phoneNum = phoneNum
      params.phoneCode = phoneCode
      params.phoneLoginType = phoneLoginType
      params.uniqueCode = DeviceInfo.getUniqueID()
      let paramsStr = JSON.stringify(params)
      NetworkDao.fetchPostNet('appLoginByPhone.json', paramsStr)
        .then(data => {
          _this.refs.activityIndicatorPlus.dismiss()
          if (data._backcode == '200') {
            //手机号登录成功
            publicDao.CURRENT_TOKEN = data.token
            publicDao.YG_FLAG = data.yjflag
            publicDao.DOOR_TOKEN = data.doorToken
            publicDao.IS_LOGOUT = "0"
            publicDao.COMPANY = data.companyName
            publicDao.USERNAME = data.userName
            publicDao.PHONE_NUMBER = phoneNum
            publicDao.PHONE_PWD = passWordWay ? phoneCode : null
            publicDao.IS_PHONE_LOGIN = '1'
            publicDao.IS_IDENTIFY_LOGIN = this.state.passWordWay ? '0' : '1'
            let currentInfo = {}
            currentInfo.uniqueCode = DeviceInfo.getUniqueID()
            currentInfo.userName = data.userName
            currentInfo.mainPWD = ''
            currentInfo.secondPWD = ''
            currentInfo.companyName = data.companyName
            currentInfo.token = data.token
            currentInfo.yjflag = data.yjflag
            currentInfo.doorToken = data.doorToken
            currentInfo.phoneNum = phoneNum
            currentInfo.phonePWD = passWordWay ? phoneCode : null
            currentInfo.isPhoneLogin = '1'
            currentInfo.isIdentifyLogin = this.state.passWordWay ? '0' : '1'

            let dirs = RNFetchBlob.fs.dirs
            let dir = Platform.OS == 'ios' ? dirs.DocumentDir + '/' + data.companyName : dirs.DownloadDir + '/' + data.companyName
            currentInfo.downloadFile = dir
            publicDao.DOWNLOAD_FILE = dir
            this.createDownloadFile(dir)
            dataDao.saveCurrentUserInfo(currentInfo)

            NavigationUtil.navigation = this.props.navigation;
            this.timer && clearInterval(this.timer)
            this.setState({
              sendAutoNumber: true,
              numbers: countDown
            })
            if (this.params == null || this.params.routeName == null || this.params.routeName == 'Main') {
              NavigationUtil.goPage({}, 'HomeView')
            } else {
              NavigationUtil.goPage({}, this.params.routeName)
            }
            DeviceEventEmitter.emit(EMITTER_LOGIN_SUCESS);
            DeviceEventEmitter.emit(EMITTER_CREATETRANSBILLSUCESS_TYPE);
            DeviceEventEmitter.emit(EMITTER_CREATECOSTBILLSUCCESS_TYPE);
            DeviceEventEmitter.emit(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE);

          } else {
            this.refs.toast.show(data._backmes, 5000)
          }
        })
        .catch(error => {
          console.log(error);
          this.refs.toast.show('登录失败', 800)
          _this.refs.activityIndicatorPlus.dismiss()
        })

    } else {
      this.refs.toast.show('请填写完整数据', 800)
    }
  }

  //获取验证码
  getAutoNumber() {
    let phoneNum = this.state.phoneNumber
    //网络请求
    this.setState({ indicatorAnimating: true })
    let params = {}
    params.phoneNum = phoneNum
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getLoginVerificationCode.json', paramsStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        if (data._backcode == '200') {
          let aa = countDown
          this.setState({
            sendAutoNumber: false
          })
          //开始倒计时
          this.timer = setInterval(() => {
            aa--
            this.setState({ numbers: aa })
            if (aa == 0) {
              clearInterval(this.timer)
              this.setState({
                sendAutoNumber: true,
                numbers: countDown
              })
              aa = countDown
            }
          }, 1000);
          if (this.state.numbers == 0) {
            this.setState({
              sendAutoNumber: true
            })
          }
          this.refs.toast.show('验证码发送成功', 800)
        } else {
          this.refs.toast.show(data._backmes, 800)
        }
      })
      .catch(error => {
        this.refs.toast.show('验证码发送失败', 800)
        console.log(error);
        this.setState({ indicatorAnimating: false })
      })
  }

  //密码登录
  passWordLogin() {
    return <View style={{ flex: 1 }}>
      <View style={{ paddingHorizontal: 20 }}>
        {LoginInputView.renderInputCell((text) => {
          this.setState({ companyName: text })
        }, require('../../resource/login-ico1.png'), '请输入公司名', this.state.companyName, 'default', 45, 25, false)}
        {LoginInputView.renderInputCell((text) => {
          this.setState({ userName: text })
        }, require('../../resource/login-ico2.png'), '请输入用户名', this.state.userName, 'default', 45, 10, false)}
        {LoginInputView.renderInputCell((text) => {
          this.setState({ mainPWD: text })
        }, require('../../resource/login-ico3.png'), '请输入主密码', this.state.mainPWD, 'default', 45, 10, true)}
        {LoginInputView.renderInputCell((text) => {
          this.setState({ secondPWD: text })
        }, require('../../resource/login-ico3.png'), '请输入副密码', this.state.secondPWD, 'default', 45, 10, true)}
      </View>
    </View>
  }

  //手机号登录
  phoneNumberLogin() {
    return <View style={{ flex: 1, }}>
      <View style={{ paddingHorizontal: 20 }}>
        {LoginInputView.renderInputCell(
          (text) => {
            this.setState({ phoneNumber: text })
          },
          require('../../resource/phone.png'),
          '请输手机号',
          this.state.phoneNumber,
          'numeric',
          45, 25, false
        )}
        <View style={{ marginTop: 10,borderRadius:3,overflow:'hidden'}}>
          {
            this.state.passWordWay ?
              LoginInputView.renderInputCell(
                (text) => {this.setState({ phonePwd: text })},
                require('../../resource/login-ico3.png'), '请输入密码', this.state.phonePwd,
                'default', 45, 0, true
              ) :
              LoginInputView.renderInputCell(
                (text) => {this.setState({ autoNumber: text })},
                require('../../resource/yanzhengma.png'), '请输入验证码', this.state.autoNumber,
                'numeric', 45, 0, false)
          }
          {
            !this.state.passWordWay ?
              <TouchableOpacity
                style={{
                  backgroundColor: this.state.sendAutoNumber ? GlobalStyles.nav_bar_color : '#ccc',
                  justifyContent: 'center', alignItems: 'center', height: 45, width: 105,
                  position:'absolute',right: 0,
                }}
                activeOpacity={1}

                onPress={() => {   //点击获取验证码
                  if (this.state.numbers == countDown) {
                    let phoneNum = this.state.phoneNumber
                    if (phoneNum && phoneNum.length > 0) {
                      let isY = this.validatePhone(phoneNum)
                      if (!isY) {
                        this.refs.toast.show('请输入正确的电话号码', 800)
                        return
                      }
                      this.getAutoNumber()
                    } else {
                      this.refs.toast.show('请输入手机号码', 800)
                      return
                    }
                  }
                }}
              >
                <Text style={{ fontSize: 14, color: '#fff' }}>
                  {this.state.sendAutoNumber ? '获取验证码' : '获取验证码(' + this.state.numbers + ')'}
                </Text>
              </TouchableOpacity> : null}
        </View>
      </View>
      <TouchableOpacity
        style={{
          width: 120, height: 30,
          justifyContent: 'center', alignItems: 'flex-end', borderRadius: 3,
          alignSelf: 'flex-end', marginRight: 20, marginTop: 10
        }}
        activeOpacity={1}
        onPress={() => {
          this.setState({ passWordWay: !this.state.passWordWay, autoNumber: '', phonePwd: '' })
        }}
      >
        <Text style={{ color: '#5af', fontSize: 15, width: 200, textAlign: 'right' }}>{this.state.passWordWay ? '切换为验证码登录' : '切换为密码登录'}</Text>
      </TouchableOpacity>
    </View >
  }


  render() {
    // let _this = this
    return (
      <View style={styles.container}>
        {Platform.OS === 'android' ? <StatusBar barStyle={'light-content'} translucent backgroundColor={'transparent'} /> : null}
        <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>

          <Toast ref="toast" position="center" />
          <View>
            <View style={{ position: 'absolute', top: 20, left: 5, zIndex: 999, width: 50 }}>
              {ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            </View>

            <Image
              style={{ width: GlobalStyles.screenWidth, height: GlobalStyles.screenHeight }}
              source={require('../../resource/login-bg1.png')}
            />

          </View>

          <View style={[styles.loginBackView, { height: this.isShowAlertUser ? 395 : 360 }]}>
            {this.isShowAlertUser ? <View style={{ backgroundColor: 'rgb(255,243,216)', height: 35, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: 'rgb(244,170,67)' }}>此信息需用户登录后,才可以查看详情</Text>
            </View> : null}
            <View style={{
              marginHorizontal: 20, height: 40, marginBottom: -10,
              flexDirection: 'row', alignItems: 'center'
            }}>
              <TouchableOpacity
                style={{
                  alignItems: 'center', height: 38,
                  flexDirection: 'row', width: '50%',
                  justifyContent: 'center', borderBottomColor: this.state.loginWay ? GlobalStyles.nav_bar_color : '#DEDEDE',
                  borderBottomWidth: 2
                }}
                activeOpacity={1}
                onPress={() => {
                  this.setState({ loginWay: true })
                }}
              >
                <Text style={{ color: this.state.loginWay ? GlobalStyles.nav_bar_color : '#000', fontSize: 16 }}>账号密码登录</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  alignItems: 'center', height: 38,
                  flexDirection: 'row', width: '50%',
                  justifyContent: 'center', borderBottomColor: this.state.loginWay ? '#DEDEDE' : GlobalStyles.nav_bar_color,
                  borderBottomWidth: 2
                }}
                activeOpacity={1}
                onPress={() => {
                  this.setState({ loginWay: false })
                }}
              >
                <Text style={{ color: this.state.loginWay ? '#000' : GlobalStyles.nav_bar_color, fontSize: 16 }}>手机号登录</Text>
              </TouchableOpacity>
            </View>

            {this.state.loginWay ? this.passWordLogin() : this.phoneNumberLogin()}
            <TouchableOpacity
              style={styles.loginBtnImg}
              activeOpacity={0.7}
              onPress={() => {
                if (this.state.loginWay) {
                  this.loginBtnClick()  //使用密码登录
                } else {
                  this.autoNumberLogin()    //使用验证码登录和手机密码登陆
                }
              }}>
              <Text style={styles.loginBtnText}>登  录</Text>
            </TouchableOpacity>
          </View>
          {this.state.indicatorAnimating && (
            <ActivityIndicator
              style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
              size={GlobalStyles.isIos ? 'large' : 40}
              color='gray'
            />)}
          <ActivityIndicatorPlus
           ref={(r)=>_this.refs.activityIndicatorPlus = r}
          />
        </KeyboardAwareScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  loginBackView: {
    marginLeft: 15,
    backgroundColor: 'white',
    width: GlobalStyles.screenWidth - 30,
    marginTop: 240 - GlobalStyles.screenHeight,
    borderRadius: 5,
    overflow: 'hidden',
  },
  loginBtnImg: {
    borderRadius: 3,
    height: 50,
    backgroundColor: GlobalStyles.nav_bar_color,
    margin: 20,
  },
  loginBtnText: {
    marginLeft: 0,
    marginRight: 0,
    lineHeight: 50,
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
    fontWeight: '500'
  },
});