/**
 * 登陆输入框
 */
import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text, TextInput, Image } from 'react-native'
import GlobalStyles from '../common/GlobalStyles'

export default class LoginInputView {

    /**
     * 
     * @param {*} callBack          输入的字符
     * @param {*} leftImg           左边的图标
     * @param {*} placeHolder       占位文字
     * @param {*} defaultValue      默认文字
     * @param {*} inputType         样式
     * @param {*} cellHeight        cell高度
     * @param {*} marginTop         整个控件上边距
     * @param {*} isSec             是否是密文输入
     */
    static renderInputCell(callBack, leftImg, placeHolder, defaultValue, inputType, cellHeight, marginTop,isSec) {
        return <View style={[styles.backView, { marginTop: marginTop }]}>
            <Image
                style={{ width: 25, height: 25, marginTop: 10 }}
                source={leftImg} />
            <TextInput
                style={{ paddingTop: 0, paddingBottom: 0,marginLeft:10, height: cellHeight, width:GlobalStyles.screenWidth-30-40-25-20 }}
                placeholder={placeHolder}
                fontSize={16}
                keyboardType={inputType}
                defaultValue={defaultValue}
                clearButtonMode='while-editing'
                secureTextEntry={isSec}
                onChangeText={(text) => {
                    callBack(text)
                }}
            />
        </View>
    }

}

const styles = StyleSheet.create({
    backView: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 1,
        borderColor: GlobalStyles.textInput_borderColor,
        borderRadius: 3,
        height: 45,
    }
});