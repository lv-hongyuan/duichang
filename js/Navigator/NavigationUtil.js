/**
 * 导航工具类
 */
export default class NavigationUtil {
    /**
     * 跳转到指定页面
     * @param params 要传递的参数
     * @param page 要跳转的页面名
     **/
    static goPage(params, page) {
        const navigation = NavigationUtil.navigation;   //获取到外部导航器
        if (!navigation) {
            console.log('NavigationUtil.navigation can not be null')
            return;
        }
        navigation.navigate(
            page,
            {
                ...params
            }
        )
    }

    /**
     * 返回上一页
     * @param navigation
     */
    static goBack(navigation) {
        navigation.goBack();
    }

    //回到登录页
    static resetToLoginView(){
        const navigation = NavigationUtil.navigation; 
        navigation.navigate('Auth')
    }

    static resetToHomeView(){
        const navigation = NavigationUtil.navigation; 
        navigation.navigate('Main')
    }

}