import React from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import { createBottomTabNavigator, createStackNavigator, createSwitchNavigator, createAppContainer } from "react-navigation";
import LoginView from '../Login/LoginView'
import HomeView from '../View/Home/HomeView'
import WayBill from '../View/Waybill/WaybillView'
import Promotion from '../View/Promotion/PromotionView'
import TransCost from '../View/TransCost/TransCostView'
import MineView from '../View/Mine/MineView'
import App from '../../App'
import TransCostSearhResultView from '../View/TransCost/TransCostSearchResultView'
import TransCostPortSearchView from '../View/TransCost/TransCostPortSearchView'
import TransBillMoreSearchView from '../View/Waybill/TransBillMoreSearchView'
import CostBillMoreSearchView from '../View/Waybill/CostBillMoreSearchView'
import GoodsTracking from '../View/Home/Follow/GoodsTracking'
import TransBillDetail from '../View/Home/Follow/TransBillDetail'
import ShipDurationDynamic from '../View/Home/ShipDuration/ShipDurationDynamic'
import ShipDuirationSearchView from '../View/Home/ShipDuration/ShipDurationSearchView'
import ExcreteOrderApply from '../View/Waybill/ExcreteOrderApply'
import BookSpaceInfo from '../View/Home/Book/BookSpaceInfo'
import ShipPromotion from '../View/Home/ShipPromotion/ShipPromotion'
import ImmediateBookSpace from '../View/BookSpaceUtils/ImmediateBookSpace'
import ChangeOrderApply from '../View/Waybill/ChangeOrderApply'
import ExcreteBookSpaceInfo from '../View/Waybill/ExcreteBookSpaceInfo'
import BookSpaceGoodsName from '../View/BookSpaceUtils/BookSpaceGoodsName'
import TransGoodsTrack from '../View/Waybill/TransGoodsTrack'
import DeclareProvision from '../View/BookSpaceUtils/DeclareProvision'
import BookSpaceGoodsInfo from '../View/BookSpaceUtils/BookSpaceGoodsInfo'
import CreateOrderAgain from '../View/Waybill/CreateOrderAgain'
import WaybillDetailVeiw from '../View/Waybill/WaybillDetailVeiw'
import CalendarModal from '../common/CalendarModal';
import ContactOfficeView from '../View/Waybill/ContactOfficeView';
import ContactOfficeSearchView from '../View/Waybill/ContactOfficeSearchView';
import TrustOrder from '../View/Home/trustOrder/TrustOrder'
import TemplateBookSpace from '../View/Home/templateBookSpace/TemplateBookSpace';
import TrustOrderSearchView from '../View/Home/trustOrder/TrustOrderSearchView';
import myMessage from '../View/Mine/myMessage';
import BookSpaceCommonContact from '../View/BookSpaceUtils/BookSpaceCommonContact';
import IntoCartonNoView from '../View/Home/IntoCartonNo/IntoCartonNoView';
import IntoCartonNoSeachView from '../View/Home/IntoCartonNo/IntoCartonNoSeachView';
import IntoCartonNoDetailView from '../View/Home/IntoCartonNo/IntoCartonNoDetailView';
import SouthChinaSelectView from '../View/Home/trustOrder/SouthChinaSelectView'
import ShowPDFView from '../View/Home/trustOrder/ShowPDFView'
import publicDao from '../dao/publicDao'
import NavigationUtil from '../Navigator/NavigationUtil';
import DownLoadFireView from '../View/Mine/DownLoadFireView';
import ShowZIPView from '../View/Home/trustOrder/ShowZIPView';
import TrackMapDeail from '../View/Home/Follow/TrackMapDetail';
import BaiDuMapDetail from '../View/BookSpaceUtils/BaiDuMapDetail';

//登陆验证
const AuthLoading = createStackNavigator({
    App: {
        screen: App,
        navigationOptions: {
            header: null,
        }
    }
});

const AuthStack = createStackNavigator({
    LoginView: {
        screen: LoginView,
        navigationOptions: {
            header: null,
        }
    }
});

const bottomTabNav = createBottomTabNavigator({
    HomeView: {
        screen: HomeView,
        navigationOptions: {
            tabBarLabel: "首页",
            tabBarIcon: ({ focused, tintColor }) => (
                <Image
                    source={focused ? require('../../resource/label-ico1-s.png') : require('../../resource/label-ico1.png')}
                    style={styles.tabBarIconStyle}
                />
            ),
            headerTitleAllowFontScaling:false,//禁用字体跟随系统字体大小变化
        },
    },
    WayBill: {
        screen: WayBill,
        navigationOptions: {
            tabBarLabel: "运单",
            tabBarIcon: ({ focused, tintColor }) => (
                <Image
                    source={focused ? require('../../resource/label-ico2-s.png') : require('../../resource/label-ico2.png')}
                    style={styles.tabBarIconStyle}
                />
            ),
            tabBarOnPress: ({navigation, defaultHandler}) => {
                if (publicDao.IS_LOGOUT == '1') {
                    let loginJ = {}
                    loginJ.isMine = '0'
                    loginJ.routeName = 'WayBill'
                    loginJ.isMineMsg = '0'
                    NavigationUtil.goPage(loginJ, 'LoginView')
                } else {
                    defaultHandler();                  
                }
            }
        }
    },
    Promotion: {
        screen: Promotion,
        navigationOptions: {
            tabBarLabel: "促销",
            tabBarIcon: ({ focused, tintColor }) => (
                <Image
                    source={focused ? require('../../resource/label-ico3-s.png') : require('../../resource/label-ico3.png')}
                    style={styles.tabBarIconStyle}
                />
            )
        }
    },
    TransCost: {
        screen: TransCost,
        navigationOptions: {
            tabBarLabel: "运价",
            tabBarIcon: ({ focused, tintColor }) => (
                <Image
                    source={focused ? require('../../resource/label-ico4-s.png') : require('../../resource/label-ico4.png')}
                    style={styles.tabBarIconStyle}
                />
            ),
            tabBarOnPress: ({navigation, defaultHandler}) => {
                if (publicDao.IS_LOGOUT == '1') {
                    let loginJ = {}
                    loginJ.isMine = '0'
                    loginJ.routeName = 'TransCost'
                    loginJ.isMineMsg = '0'
                    NavigationUtil.goPage(loginJ, 'LoginView')
                } else {
                    defaultHandler()
                }
            }
        }
    },
    MineView: {
        screen: MineView,
        navigationOptions: {
            tabBarLabel: "我的",
            tabBarIcon: ({ focused, tintColor }) => (
                <Image
                    source={focused ? require('../../resource/label-ico5-s.png') : require('../../resource/label-ico5.png')}
                    style={styles.tabBarIconStyle}
                />
            )
        }
    },
}, {
        initialRouteName: "HomeView",
        lazy: true,
        tabBarOptions: {
            allowFontScaling:false,
            inactiveTintColor: "#8F8F8F",
            // activeTintColor: "#ED5601",
            activeTintColor: '#BC1920',
            labelStyle: {
                fontSize: 13
            }
        }
    }
)

const mainStack = createStackNavigator({
    bottomTabNav: {
        screen: bottomTabNav,
        navigationOptions: {
            header: null,
        }
    },
    TransCostSearhResultView: {
        screen: TransCostSearhResultView,
        navigationOptions: {
            header: null,
        }
    },
    TransCostPortSearchView: {
        screen: TransCostPortSearchView,
        navigationOptions: {
            header: null,
        }
    },
    TransBillMoreSearchView: {
        screen: TransBillMoreSearchView,
        navigationOptions: {
            header: null,
        }
    },
    CostBillMoreSearchView: {
        screen: CostBillMoreSearchView,
        navigationOptions: {
            header: null,
        }
    },
    GoodsTracking: {
        screen: GoodsTracking,
        navigationOptions: {
            header: null,
        }
    },
    TransBillDetail: {
        screen: TransBillDetail,
        navigationOptions: {
            header: null,
        }
    },
    ShipDurationDynamic: {
        screen: ShipDurationDynamic,
        navigationOptions: {
            header: null,
        }
    },
    ShipDuirationSearchView: {
        screen: ShipDuirationSearchView,
        navigationOptions: {
            header: null,
        }
    },
    ExcreteOrderApply: {
        screen: ExcreteOrderApply,
        navigationOptions: {
            header: null,
        }
    },
    BookSpaceInfo: {
        screen: BookSpaceInfo,
        navigationOptions: {
            header: null,
        }
    },
    ShipPromotion: {
        screen: ShipPromotion,
        navigationOptions: {
            header: null,
        }
    },
    ImmediateBookSpace: {
        screen: ImmediateBookSpace,
        navigationOptions: {
            header: null,
        }
    },
    ChangeOrderApply: {
        screen: ChangeOrderApply,
        navigationOptions: {
            header: null,
        }
    },
    ExcreteBookSpaceInfo: {
        screen: ExcreteBookSpaceInfo,
        navigationOptions: {
            header: null,
        }
    },
    BookSpaceGoodsName: {
        screen: BookSpaceGoodsName,
        navigationOptions: {
            header: null,
        }
    },
    TransGoodsTrack: {
        screen: TransGoodsTrack,
        navigationOptions: {
            header: null,
        }
    },
    DeclareProvision: {
        screen: DeclareProvision,
        navigationOptions: {
            header: null,
        }
    },
    BookSpaceGoodsInfo: {
        screen: BookSpaceGoodsInfo,
        navigationOptions: {
            header: null,
        }
    },
    CreateOrderAgain: {
        screen: CreateOrderAgain,
        navigationOptions: {
            header: null,
        }
    },
    WaybillDetailVeiw: {
        screen: WaybillDetailVeiw,
        navigationOptions: {
            header: null,
        }
    },
    CalendarModal: {
        screen: CalendarModal,
        navigationOptions: {
            header: null,
        }
    },
    ContactOfficeView: {
        screen: ContactOfficeView,
        navigationOptions: {
            header: null,
        }
    },
    ContactOfficeSearchView: {
        screen: ContactOfficeSearchView,
        navigationOptions: {
            header: null
        }
    },
    TrustOrder: {
        screen: TrustOrder,
        navigationOptions: {
            header: null
        }
    },
    TemplateBookSpace: {
        screen: TemplateBookSpace,
        navigationOptions: {
            header: null
        }
    },
    TrustOrderSearchView: {
        screen: TrustOrderSearchView,
        navigationOptions: {
            header: null
        }
    },
    myMessage: {
        screen: myMessage,
        navigationOptions: {
            header: null
        }
    },
    BookSpaceCommonContact: {
        screen: BookSpaceCommonContact,
        navigationOptions: {
            header: null
        }
    },
    IntoCartonNoView: {
        screen: IntoCartonNoView,
        navigationOptions: {
            header: null
        }
    },
    IntoCartonNoSeachView: {
        screen: IntoCartonNoSeachView,
        navigationOptions: {
            header: null
        }
    },
    IntoCartonNoDetailView: {
        screen: IntoCartonNoDetailView,
        navigationOptions: {
            header: null
        }
    },
    SouthChinaSelectView: {
        screen: SouthChinaSelectView,
        navigationOptions: {
            header: null
        }
    },
    ShowPDFView: {
        screen: ShowPDFView,
        navigationOptions: {
            header: null
        }
    },
    LoginView: {
        screen: LoginView,
        navigationOptions: {
            header: null,
        }
    },
    HomeView: {
        screen: HomeView,
        navigationOptions: {
            header: null,
        }
    },
    MineView:{
        screen:MineView,
        navigationOptions:{
            header:null,
        }
    },
    DownLoadFireView:{
        screen:DownLoadFireView,
        navigationOptions:{
            header:null
        }
    },
    ShowZIPView:{
        screen:ShowZIPView,
        navigationOptions:{
            header:null
        }
    },
    TrackMapDeail:{
        screen:TrackMapDeail,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    BaiDuMapDetail:{
        screen:BaiDuMapDetail,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    }
})

export default createAppContainer(createSwitchNavigator({
    AuthLoading: AuthLoading,
    Main: mainStack,
    Auth: AuthStack,
}, {
        defaultNavigationOptions: {
            initialRouteName: AuthLoading
        }
    }));


const styles = StyleSheet.create({
    tabBarIconStyle: {
        width: 20,
        height: 20,
    },
});
