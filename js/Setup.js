import React, { Component } from 'react';

import App from '../App';
import AppNav from '../js/Navigator/AppNavigator'
import { Provider } from '@ant-design/react-native';


export default function setup() {
  class Root extends Component {
    render() {
      return (
        <Provider>
          <AppNav />
        </Provider>
      );
    }
  }

  return Root;
}