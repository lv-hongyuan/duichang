/**
 * 点击货物跟踪进来
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import NavigationUtil from '../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../common/NoDataView'


export default class TransGoodsTrack extends Component {
    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
        console.log('==',this.params)
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],
            isNoData: false
        }
    }

    componentDidMount() {
        this.searchGoods()
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    searchGoods() {
        this.setState({ indicatorAnimating: true })
        let dcdh = this.params.dcdh
        if(this.params.dcdhfp){
           dcdh =  dcdh + '_' + this.params.dcdhfp
        }
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.waybillNum = dcdh
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('waybillSearch.json', paramsStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this.setState({ dataArr: data.list, isNoData: false })
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '404') {
                    this.setState({ dataArr: [], isNoData: true })
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                this.refs.toast.show('搜索失败', 800)
            })
    }


    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    renderItem(data) {
        let item = data.item
        return <TouchableOpacity
            style={{ width: GlobalStyles.screenWidth, height: 60, backgroundColor: 'white', flexDirection: 'row', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            onPress={() => {
                //cell点击事件
                NavigationUtil.goPage(item, 'TransBillDetail')
            }}
        >
            <Text style={{ height: 60, lineHeight: 60, fontSize: 17, color: '#0877DE' }}>{item.boxNum}</Text>
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ height: 60, lineHeight: 60, fontSize: 15, color: '#778087' }}>{item.xxxl + item.boxState}</Text>
                <Ionicons
                    name={'ios-arrow-forward'}
                    size={20}
                    style={{
                        marginLeft: 10,
                        alignSelf: 'center',
                        color: '#CCCCCC',
                    }} />
            </View>

        </TouchableOpacity>
    }
    //flatlist
    renderFlatList() {
        return <View style={{ flex: 1, marginTop: 10, backgroundColor: GlobalStyles.backgroundColor }}>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => this.separatorView()}
                data={this.state.dataArr}
                renderItem={data => this.renderItem(data)}
            />
        </View>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'货运跟踪'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast" position="center"/>
                {this.renderFlatList()}
                {this.state.isNoData ? NoDataView.renderContent(40) : null}
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    }
});

