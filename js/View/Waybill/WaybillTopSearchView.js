import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, Image, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'


export default class WaybillTopSearchView extends Component {
    static renderSearchView(placeHolder, defaultV, textCallback, endCallBack, btnMoreCallback,btnSearchCallback) {
        return <View style={{ width: GlobalStyles.screenWidth, height: 70, backgroundColor: 'white',flexDirection:'row',alignItems:'center'}}>
            <Image style={{ height: 70, width: 67 }} source={require('../../../resource/rudder.png')} />
            <View style={styles.inputBackView}>
                <Image style={{ marginLeft: 10,  width: 20, height: 20 }}
                    source={require('../../../resource/zoomer-g.png')}
                />
                <TextInput
                    ref="textinput"
                    style={{ marginLeft: 10, height: 35, paddingTop: 0, paddingBottom: 0, flex: 1 }}
                    placeholder={placeHolder}
                    clearButtonMode={'while-editing'}
                    returnKeyType={'search'}
                    value={defaultV}
                    fontSize={16}
                    onChangeText={(text) => {
                        textCallback(text)
                    }}
                    // onEndEditing={(event) => {
                    //     Keyboard.dismiss()
                    //     // endCallBack(event)
                    // }}
                    onSubmitEditing={(event) => {
                        // Keyboard.dismiss()
                        endCallBack(event)
                    }}
                />
                <TouchableOpacity
                    style={{
                        width: 59, height: 35, borderTopRightRadius: 3, borderBottomRightRadius: 3,
                        backgroundColor: GlobalStyles.nav_bar_color,
                    }}
                    onPress={(event) => {
                        Keyboard.dismiss()
                        btnSearchCallback()
                    }}
                >
                    <Text style={{ fontSize: 15, color: 'white', lineHeight: 35, textAlign: 'center' }}>{'搜索'}</Text>
                </TouchableOpacity>

            </View>
            <TouchableOpacity
                style={{ marginHorizontal:10, width: 60, height: 35, borderRadius: 3, backgroundColor: GlobalStyles.nav_bar_color }}
                onPress={() => {
                    btnMoreCallback()
                }}
            >
                <Text style={{ fontSize: 15, color: 'white', lineHeight: 35, textAlign: 'center' }}>{'更多'}</Text>
            </TouchableOpacity>
        </View>
    }




}

const styles = StyleSheet.create({
    inputBackView: {
        flex:1,
        marginLeft:-50,
        borderRadius: 3,
        borderColor: '#ddd',
        borderWidth: 1,
        // marginTop: -55,
        flexDirection: 'row',
        alignItems: 'center',
    }
});