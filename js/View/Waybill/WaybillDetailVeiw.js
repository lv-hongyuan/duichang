/**
 * 我的运单和我的订单详情页面
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import BookSpaceShowCellsUtil from '../BookSpaceUtils/BookSpaceShowCellsUtil'
import ViewUtil from '../../common/ViewUtil'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BookSpaceBottomModal from '../BookSpaceUtils/BookSpaceBottomModal'
import DatePicker from 'react-native-datepicker'
import DateUril from '../../common/DateUtil'
import BookSpaceAreaLinkage from '../BookSpaceUtils/BookSpaceAreaLinkage'
import area2 from '../../../resource/area2.json'
import TransCostPortSearchView from '../TransCost/TransCostPortSearchView'
import CostDetailModal from '../BookSpaceUtils/CostDetailModal'
import Picker from '@ant-design/react-native/lib/picker';
import find from 'lodash/find'
import publicDao from '../../dao/publicDao'
import NetworkDao from '../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import BookInfoDao from '../../dao/BookInfoDao'
import ActivityIndicatorPlus from '../../common/ActivityIndicatorPlus'
import _ from 'lodash'
import WaybillDetailCostDetail from './WaybillDetailCostDetail'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class WaybillDetailVeiw extends Component {

    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
        console.log(this.params)
        let currentDate = DateUril.getCurrentDate()     //获取当前日期
        this.bottomModalSelect = ''                   //bottomModal选了哪一个
        this.portSelect = 0                             //选择起运港还是目的港
        this.isTransBill = this.params.isTransBill      //是否是运单
        this.hyf = 0
        let orderNum = ''
        if (this.params.dcdh && this.params.dcdh.length > 0) {
            orderNum = this.params.dcdh
            if (this.params.dcdhfp && this.params.dcdhfp.length > 0) {
                orderNum = orderNum + '_' + this.params.dcdhfp
            }
        }

        this.boxInfoArr = this.isTransBill ? this.params.contDTOList : null  //箱信息数据
        let xt = this.params.xt
        if (xt) {
            if (xt == 'F') {
                xt = '重箱'
            } else if (xt == 'E') {
                xt = '空箱'
            }
        } else {
            xt = '重箱'
        }

        let tsyqmq = this.params.tsyqmq     //装货特殊要求
        let tsyqdm = this.params.tsyqdm     //卸货特殊要求

        this.state = {
            showBaseInfo: true,               //展开基本信息
            showEntrustInfo: false,           //展开委托信息
            showGoodsInfo: false,             //展开货物信息
            showBoxInfo: false,                //展开箱信息
            showCostInfo: false,              //展开费用信息
            showDeclareProvision: false,      //展开声明条款

            orderNum: orderNum,                //运单号                     
            startPortName: this.params.startPort ? this.params.startPort : '请选择',           //起运港
            endPortName: this.params.endPort ? this.params.endPort : '请选择',             //目的港
            advanceLoadDate: this.params.bookZxsj ? this.params.bookZxsj : this.params.bookYjzhsj,          //预计装货日期
            payMode: this.params.fkfs ? this.params.fkfs : '请选择',                  //付款方式
            transProvision: this.params.ystk ? this.params.ystk : '请选择',            //运输条款
            collectTicket: this.params.spf,                                            //受票方
            boxReceipt: this.params.xnqsd ? this.params.xnqsd : false,                    //箱内签收单
            bookPersonDetention: this.params.dcrZk ? this.params.dcrZk : false,           //订舱人扣货
            baseInfoMark: this.params.bz ? this.params.bz : '',                     //基本信息的备注

            delivePersonName: this.params.fhrqc ? this.params.fhrqc : '',                //发货人全称
            deliverPersonContact: this.params.bookZxdd ? this.params.bookZxdd : this.params.fhrlxr,           //发货人联系人
            deliverPersonPhone: this.params.fhrdh ? this.params.fhrdh : '',              //发货人电话
            deliverGatePoint: this.params.fhrmd ? this.params.fhrmd : '发货门点',                //发货门点
            deliverDetailAddress: this.params.fhrdz ? this.params.fhrdz : '发货详细地址',            //发货详细地址
            receivePersonName: this.params.shr ? this.params.shr : this.params.shrqc,             //收货人全称
            receivePersonContact: this.params.shrlxr ? this.params.shrlxr : '',          //收货人联系人
            receivePersonPhone: this.params.shrdh ? this.params.shrdh : '',            //收货人电话
            receiveGatePoint: this.params.shrmd ? this.params.shrmd : '收货门点',            //收货门点
            receiveDetailAddress: this.params.shrdz ? this.params.shrdz : '收货详细地址',                //收货详细地址

            goodsName: this.params.hm ? this.params.hm : '请选择',                       //货名
            goodsNameArr: [],                        //货名数组
            boxState: xt,                               //箱态
            goodsMode: this.params.hl ? this.params.hl : '',                       //货类
            packingMode: this.params.bzlx ? this.params.bzlx : '请选择',                    //包装类型
            boxMode: this.params.xxxl ? this.params.xxxl : this.params.xx,                             //箱型
            boxWeight: this.params.sl ? this.params.sl + '' : '',                           //箱量
            singleBoxWeight: this.params.zl ? this.params.zl + '' : '0',                     //单箱重量
            uploadSpecialRequire: this.convertSpecialCodeToName(tsyqmq),                 //装货特殊要求
            downloadSpecialRequire: this.convertSpecialCodeToName(tsyqdm),               //卸货特殊要求
            boxMark: this.params.bookBz2 ? this.params.bookBz2 : this.params.xbz,                             //箱备注
            dangerGoods: false,                         //危险品
            selfBox: this.params.soc ? this.params.soc : false,                             //自备柜
            dangerIMDG: '',                          //危险品的IMDG
            dangerMode: '',                          //危险品类别
            dangerUNCode: '',                        //危险品联合国编号
            dangerMark: '',                          //危险品备注

            seaServeCost: '',                        //海运服务费
            uploadCarCost: this.params.zgtcfysbj ? this.params.zgtcfysbj + '' : '0',                       //装港拖车服务费
            downloadCarCost: this.params.xgtcfysbj ? this.params.xgtcfysbj + '' : '0',                     //卸港拖车服务费
            uploadRailCost: this.params.zgtlfw ? this.params.zgtlfw + '' : '0',                      //装港铁路服务费
            downloadRailCost: this.params.xgtlfw ? this.params.xgtlfw + '' : '0',                    //卸港铁路服务费
            uploadCarAdvanceCost: this.params.zgtcddf ? this.params.zgtcddf : '',                        //装港拖车代垫费
            uploadCarAdvanceCostMark: this.params.zgtcddfbz ? this.params.zgtcddfbz : '',                 //装港拖车代垫费备注
            downloadCarAdvanceCost: this.params.xgtcddf ? this.params.xgtcddf : '',                      //卸港拖车代垫费
            downloadcarAdvanceCostMark: this.params.xgtcddfbz ? this.params.xgtcddfbz : '',              //卸港拖车代垫费备注
            premiumPayMode: this.params.bxfzf,                                                       //保险费支付方式
            premiumWorth: this.params.bxhz ? this.params.bxhz + '' : '0',                        //保险货值
            totalCost: '',                           //总价

            currentDate: currentDate,                //当前日期
            // areaArray: area2,                         //省市区门点数据
            isShowAreaModal: false,                    //是否显示省市区门点modal
            collectTicketArr: [{ 'name': '订舱人', 'code': 0 }, { 'name': '发货人', 'code': 1 }, { 'name': '收货人', 'code': 2 }],   //受票方数据
            sendGateArray: [],                       //发货门点数据
            receiveGateArray: [],                    //收货门点数据
            transProvisionArr: [{ 'name': 'CY-CY' }, { 'name': 'CY-DO' }, { 'name': 'DO-CY' }, { 'name': 'DO-DO' }, { 'name': 'CY-CFS' }, { 'name': 'CFS-CFS' }, { 'name': 'DO-CFS' }, { 'name': 'CFS-CY' }, { 'name': 'CFS-DO' }, { 'name': 'FI-CY' }, { 'name': 'CY-FO' }, { 'name': 'FI-DO' }, { 'name': 'DO-FO' }],   //运输条款数据
            payModeArr: [{ 'name': '赎单' }, { 'name': '到付' }],        //付款方式数据
            packingModeArr: [{ 'name': '木箱', 'code': '100' }, { 'name': '散装', 'code': '200' }, { 'name': '卷装', 'code': '300' }, { 'name': '液袋', 'code': '400' }, { 'name': '普通袋装', 'code': '500' }, { 'name': '托盘', 'code': '600' }, { 'name': '纸箱', 'code': '700' }, { 'name': '灌装', 'code': '800' }, { 'name': '桶装', 'code': '900' }, { 'name': '空箱', 'code': '1000' }],        //包装类型数据
            premiumPayModeArr: [{ 'name': '无保险', 'code': 0 }, { 'name': '车队提箱时支付', 'code': 1 }, { 'name': '含运费', 'code': 2 }],        //保险费支付方式数据
            boxModeArr: [{ 'name': '20GP' }, { 'name': '40GP' }, { 'name': '40HC' }, { 'name': '20TK' }, { 'name': '40RH' }], //箱型数据
            boxStateArr: [{ 'name': '重箱' }, { 'name': '空箱' }],  //箱态数据
            costDetailArr: [],       //总价明细数据
            isShowCalcuBtn: false,           //是否显示计费按钮
            isShowDetailBtn: true,      //是否显示明细按钮
            goodsTemperature: this.params.szwd ? this.params.szwd : '',        //设置温度
            goodsWind: this.params.fmkd ? this.params.fmkd : '',               //峰门开度
            advanceBoxDate: this.params.yjtxsj ? this.params.yjtxsj : '请选择',          //预计提箱时间
            goodsHumidity: this.params.sdsz ? this.params.sdsz : '',           //湿度湿度
            goodsCooling: this.params.ylyq == 0 ? false : true,            //预冷要求
        }
    }

    convertSpecialCodeToName(code) {     //根据装卸货特殊要求的code获取name
        if (code == 0) {
            return '无'
        } else if (code == 1) {
            return '短板车'
        } else if (code == 2) {
            return '自卸车'
        } else if (code == 4) {
            return '不打冷'
        }
    }

    getFeeData() {
        let tempArr = []
        tempArr.push({ 'feeName': '总费用', 'fee': this.params.fyzj })
                    tempArr.push({ 'feeName': '单箱合计', 'fee': this.params.dxfyzj })
                    tempArr.push({ 'feeName': '杂费', 'fee': this.params.dlfbz })
                    let j3 = {}
                    let tempArr2 = []
                   
                    tempArr2.push({ 'feeName': '海运服务费', 'fee': this.hyf })
                    tempArr2.push({ 'feeName': 'THC代收', 'fee': this.params.thc })
                    tempArr2.push({ 'feeName': '燃油附加费', 'fee': this.params.ryfjf })
                    if(this.state.transProvision.indexOf('CY-') != -1){
                        if(parseFloat(this.params.dlckzfhj) != 0){
                            tempArr2.push({ 'feeName': '代理出口杂费合计', 'fee': this.params.dlckzfhj })
                        }
                    }else{
                        tempArr2.push({ 'feeName': '代理出口杂费合计', 'fee': this.params.dlckzfhj })
                    }

                    if(this.state.transProvision.indexOf('-CY') != -1){
                        if(parseFloat(this.params.dljkzfhj) != 0){
                            tempArr2.push({ 'feeName': '代理进口杂费合计', 'fee': this.params.dljkzfhj })
                        }
                    }else{
                        tempArr2.push({ 'feeName': '代理进口杂费合计', 'fee': this.params.dljkzfhj})
                    }

                    if(this.state.transProvision.indexOf('DO-') != -1 || this.state.transProvision.indexOf('CFS-') != -1){
                        if (this.params.zgtcfysbj != null) {
                            tempArr2.push({ 'feeName': '装港拖车服务费', 'fee': this.params.zgtcfysbj })
                        }
                    }
                    if(this.state.transProvision.indexOf('DO-') != -1 ){
                        if (this.params.zgtcddf != null) {
                            tempArr2.push({ 'feeName': '装港拖车代垫费', 'fee': this.params.zgtcddf })
                        }
                    }
                    
                    if(this.state.transProvision.indexOf('-DO') != -1 || this.state.transProvision.indexOf('-CFS') != -1){
                        if (this.params.xgtcfysbj != null) {
                            tempArr2.push({ 'feeName': '卸港拖车服务费', 'fee': this.params.xgtcfysbj })
                        }
                    }
                    if(this.state.transProvision.indexOf('-DO') != -1){
                        if (this.params.xgtcddf != null) {
                            tempArr2.push({ 'feeName': '卸港拖车代垫费', 'fee': this.params.xgtcddf })
                        }
                    }
                    if (this.params.zgtlfw != null) {
                        tempArr2.push({ 'feeName': '装港铁路服务费', 'fee': this.params.zgtlfw })
                    }
                    if (this.params.xgtlfw != null) {
                        tempArr2.push({ 'feeName': '卸港铁路服务费', 'fee': this.params.xgtlfw })
                    }
                    if (this.params.bxf != null) {
                        tempArr2.push({ 'feeName': '保险费', 'fee': this.params.bxf })
                    }
                    j3.feeName = '明细'
                    j3.feeArr = tempArr2
                    tempArr.push(j3)
                    console.log(tempArr);

        this.setState({ costDetailArr: tempArr, totalCost: this.params.fyzj, isShowDetailBtn: true })

    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //找到对应的数据
    filterNameFromCode(arr, code, mode) {
        let newArr = _.filter(arr, ['code', code])
        let obj = newArr[0]
        if (mode == 'collectTicket') {
            this.setState({ collectTicket: obj.name })
        } else if (mode == 'premiumPayMode') {
            this.setState({ premiumPayMode: obj.name })
        }
    }

    //获取海运服务费
    getSeaserverData() {
        let cy20 = this.params.cy20
        let cy40 = this.params.cy40
        let cy40h = this.params.cy40h
        let seaCost = 0
        if (cy20 != 0) {
            seaCost = cy20
        } else if (cy40 != 0) {
            seaCost = cy40
        } else if (cy40h != 0) {
            seaCost = cy40h
        }
        if (this.params.dcf) {
            seaCost = this.params.dcf
        }
        seaCost = seaCost + ''
        this.setState({ seaServeCost: seaCost })
        this.hyf = seaCost
    }

    componentDidMount() {

        if (this.state.collectTicket != '请选择') {
            this.filterNameFromCode(this.state.collectTicketArr, this.state.collectTicket, 'collectTicket')
        }
        if (this.state.boxReceipt != false) {
            if (this.state.boxReceipt == 0) {
                this.setState({ boxReceipt: false })
            } else if (this.state.boxReceipt == 1) {
                this.setState({ boxReceipt: true })
            }
        }
        if (this.state.bookPersonDetention != false) {
            if (this.state.bookPersonDetention == 0) {
                this.setState({ bookPersonDetention: false })
            } else if (this.state.bookPersonDetention == 1) {
                this.setState({ bookPersonDetention: true })
            }
        }
        if (this.state.selfBox != false) {
            if (this.state.selfBox == 0) {
                this.setState({ selfBox: false })
            } else if (this.state.selfBox == 1) {
                this.setState({ selfBox: true })
            }
        }
        this.getSeaserverData()
        if (this.state.premiumPayMode != '请选择') {
            this.filterNameFromCode(this.state.premiumPayModeArr, this.state.premiumPayMode, 'premiumPayMode')
        }

        this.getFeeData()
    }

    renderLine() {
        return <View style={{ width: GlobalStyles.screenWidth, height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //基本信息下面的cell
    renderBaseInfoDetail() {
        return <View>
            {this.isTransBill ? BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '运单号', this.state.orderNum, '#354953',true) : null}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '起运港', this.state.startPortName, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '目的港', this.state.endPortName, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '预计装货日期', this.state.advanceLoadDate, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '付款方式', this.state.payMode, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '运输条款', this.state.transProvision, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderTextChooseCell('#FFFBFB', '受票方', this.state.collectTicket, '#354953',false)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderSwitchCell('#FFFBFB', '箱内签收单', this.state.boxReceipt)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderSwitchCell('#FFFBFB', '订舱人扣货', this.state.bookPersonDetention)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderDoubleTextCell('#FFFBFB', '备注', this.state.baseInfoMark)}
        </View>
    }

    //委托信息下面的cell
    renderEnTrustDetail() {

        let deliverGatePointColor = this.state.deliverGatePoint == '发货门点' ? '#999999' : '#354953'
        let deliverDetailAddressColor = this.state.deliverDetailAddress == '发货详细地址' ? '#999999' : '#354953'
        let receiveGatePointColor = this.state.receiveGatePoint == '收货门点' ? '#999999' : '#354953'
        let receiveDetailAddressColor = this.state.receiveDetailAddress == '收货详细地址' ? '#999999' : '#354953'

        return <View>
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人全称', 120, this.state.delivePersonName)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人联系人', 120, this.state.deliverPersonContact)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人电话', 120, this.state.deliverPersonPhone)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.deliverDetailAddress, deliverDetailAddressColor)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人全称', 120, this.state.receivePersonName)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人联系人', 120, this.state.receivePersonContact)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人电话', 120, this.state.receivePersonPhone)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.receiveDetailAddress, receiveDetailAddressColor)}
        </View>
    }

    //货物信息的cell
    renderGoodsInfoDetail() {
        let is40RH = this.state.boxMode == '40RH' ? true : false
        if(this.state.boxMode.indexOf('RH') != -1 || this.state.boxMode.indexOf('RF') != -1){
            is40RH = true
        }else{
            is40RH = false
        }
        return <View>
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '货名', 120, this.state.goodsName)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '货类', 120, this.state.goodsMode)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '包装类型', 120, this.state.packingMode)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '箱型', 120, this.state.boxMode)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '箱型', 120, this.state.boxState)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '箱量', 120, this.state.boxWeight)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '单箱重量(吨)', 120, this.state.singleBoxWeight)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '装货特殊要求', 120, this.state.uploadSpecialRequire)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸货特殊要求', 120, this.state.downloadSpecialRequire)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderDoubleTextCell('#FFFBFB', '箱备注', this.state.boxMark)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderSwitchCell('#FFF5F5', '危险品', this.state.dangerGoods)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderSwitchCell('#FFFBFB', '自备柜', this.state.selfBox)}
            {is40RH ? this.renderLine() : null}
            {is40RH ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '设置温度(℃)', 120, this.state.goodsTemperature) : null}
            {is40RH ? this.renderLine() : null}
            {is40RH ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '风门开度', 120, this.state.goodsWind) : null}
            {is40RH ? this.renderLine() : null}
            {is40RH ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '预计提箱时间', 120, this.state.advanceBoxDate) : null}
            {is40RH ? this.renderLine() : null}
            {is40RH ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '湿度设置(%)', 120, this.state.goodsHumidity) : null}
            {is40RH ? this.renderLine() : null}
            {/* {is40RH ? BookSpaceShowCellsUtil.renderCheckBoxCell('#FFFBFB', '预冷要求', 120, this.state.goodsCooling) : null} */}
            {is40RH ? this.goodsCoolingView() : null}

        </View>
    }
    goodsCoolingView(){
        return <View
        style={{
            flexDirection: 'row', height: 50, backgroundColor: '#FFFBFB',
            paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between'
        }}
    >
        <Text style={{
            height: 50,
            lineHeight: 50, fontSize: 17, width: 120,
            color: '#666'
        }}>预冷要求</Text>
        <View style={styles.Ionicons}>
            <Ionicons
                name={this.state.goodsCooling ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={24}
                color={GlobalStyles.nav_bar_color} >
            </Ionicons>
        </View>
    </View>
    }

    //箱信息的cell
    renderBoxInfoDetail() {
        if (this.boxInfoArr && this.boxInfoArr.length > 0) {
            return this.boxInfoArr.map((item) => {
                return (
                    <View key={item.id} style={{ height: 50, paddingLeft: 15, paddingRight: 15, backgroundColor: '#FFFBFB' }}>
                        <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#354953' }}>{'箱号:' + item.xh + '    铅封号:' + item.qfh}</Text>
                        {this.renderLine()}
                    </View>
                )
            })
        } else {
            return null
        }
    }

    //费用信息的cell
    renderCostDetail() {

        let isUploadCarAdvanceCost = false
        if ((this.state.transProvision.indexOf('DO-') != -1) && (this.state.uploadCarAdvanceCost.toString().length > 0)) { //装港拖车代垫费是否显示
            isUploadCarAdvanceCost = true
        } else {
            isUploadCarAdvanceCost = false
        }
        let isUploadCarAdvanceCostMark = (this.state.uploadCarAdvanceCost && this.state.uploadCarAdvanceCost.toString().length > 0) ? true : false   //装港拖车代垫费是否必填

        let isDownloadCarAdvanceCost = false
        if ((this.state.transProvision.indexOf('-DO') != -1) && (this.state.downloadCarAdvanceCost.toString().length > 0)) { //卸港拖车服务费是否显示
            isDownloadCarAdvanceCost = true
        } else {
            isDownloadCarAdvanceCost = false
        }
        let isDownloadCarAdvanceCostMark = (this.state.downloadCarAdvanceCost && this.state.downloadCarAdvanceCost.toString().length > 0) ? true : false //卸港拖车服务费是否是必填


        return <View>
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '海运服务费', 160, this.state.seaServeCost)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港拖车服务费', 160, this.state.uploadCarCost)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港拖车服务费', 160, this.state.downloadCarCost)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港铁路服务费', 160, this.state.uploadRailCost)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港铁路服务费', 160, this.state.downloadRailCost)}
            {this.renderLine()}
            {isUploadCarAdvanceCost ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港拖车代垫费', 160, this.state.uploadCarAdvanceCost) : null}
            {isUploadCarAdvanceCost ? this.renderLine() : null}
            {isUploadCarAdvanceCostMark ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港拖车代理费备注', 160, this.state.uploadCarAdvanceCostMark) : null}
            {isUploadCarAdvanceCostMark ? this.renderLine() : null}
            {isDownloadCarAdvanceCost ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港拖车代垫费', 160, this.state.downloadCarAdvanceCost) : null}
            {isDownloadCarAdvanceCost ? this.renderLine() : null}
            {isDownloadCarAdvanceCostMark ? BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港拖车代垫费备注', 160, this.state.downloadcarAdvanceCostMark) : null}
            {isDownloadCarAdvanceCostMark ? this.renderLine() : null}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '保险费支付方式', 160, this.state.premiumPayMode)}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderMarginleftInputCell('#FFFBFB', '保险货值', 160, this.state.premiumWorth)}
            {BookSpaceShowCellsUtil.renderTotalCostCell(this.state.totalCost, this.state.isShowCalcuBtn, () => {
                console.log('计费回调')
                if (this.state.startPortName != '请选择' && this.state.endPortName != '请选择' && this.state.transProvision != '请选择' && this.state.goodsName && this.state.goodsName.length > 0 && this.state.boxMode && this.state.boxMode.length > 0 && this.state.premiumPayMode != '请选择' && this.state.seaServeCost && this.state.seaServeCost.length > 0 && this.state.boxWeight && this.state.boxWeight.length > 0) {

                } else {
                    this.refs.toast.show('数据不完整', 500)
                    return
                }
            }, this.state.isShowDetailBtn, () => {
                console.log('明细回调')
                this.refs.waybillDetailCostDetail.show(this.state.costDetailArr)
            })}
        </View>
    }

    //模拟flatlist
    renderFlatList() {
        return <KeyboardAwareScrollView>
            {BookSpaceShowCellsUtil.renderOutCell('基本信息', this.state.showBaseInfo, () => {
                console.log('基本信息cell点击事件')
                this.setState({ showBaseInfo: !this.state.showBaseInfo })
            })}
            {this.state.showBaseInfo ? this.renderBaseInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderOutCell('委托信息', this.state.showEntrustInfo, () => {
                this.setState({ showEntrustInfo: !this.state.showEntrustInfo })
            })}
            {this.state.showEntrustInfo ? this.renderEnTrustDetail() : null}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderOutCell('货物信息', this.state.showGoodsInfo, () => {
                this.setState({ showGoodsInfo: !this.state.showGoodsInfo })
            })}
            {this.state.showGoodsInfo ? this.renderGoodsInfoDetail() : null}
            {this.renderLine()}
            {this.isTransBill ? BookSpaceShowCellsUtil.renderOutCell('箱信息', this.state.showBoxInfo, () => {
                this.setState({ showBoxInfo: !this.state.showBoxInfo })
            }) : null}
            {this.state.showBoxInfo ? this.renderBoxInfoDetail() : null}
            {this.isTransBill ? this.renderLine() : null}
            {BookSpaceShowCellsUtil.renderOutCell('费用信息', this.state.showCostInfo, () => {
                this.setState({ showCostInfo: !this.state.showCostInfo })
            })}
            {this.state.showCostInfo ? this.renderCostDetail() : null}
            {this.renderLine()}
            {BookSpaceShowCellsUtil.renderOutCell('声明条款', this.state.showDeclareProvision, () => {
                NavigationUtil.goPage({}, 'DeclareProvision')
            })}
        </KeyboardAwareScrollView>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'订舱信息'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <ScrollView>
                    {this.renderFlatList()}
                </ScrollView>
                <WaybillDetailCostDetail
                    ref="waybillDetailCostDetail"
                />
                <ActivityIndicatorPlus
                    ref="activityIndicatorPlus"
                />
                <Toast ref="toast" position="center" />
            </SafeAreaViewPlus>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    Ionicons: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
});