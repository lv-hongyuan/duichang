/**
 * 拆单的订舱信息
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, DeviceEventEmitter, NetInfo } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import BookSpaceCellsUtil from '../BookSpaceUtils/BookSpaceCellsUtil'
import ViewUtil from '../../common/ViewUtil'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BookSpaceBottomModal from '../BookSpaceUtils/BookSpaceBottomModal'
import DatePicker from 'react-native-datepicker'
import DateUril from '../../common/DateUtil'
import BookSpaceAreaLinkage from '../BookSpaceUtils/BookSpaceAreaLinkage'
import area2 from '../../../resource/area2.json'
import TransCostPortSearchView from '../TransCost/TransCostPortSearchView'
import CostDetailModal from '../BookSpaceUtils/CostDetailModal'
import Picker from '@ant-design/react-native/lib/picker';
import find from 'lodash/find'
import publicDao from '../../dao/publicDao'
import NetworkDao from '../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import BookInfoDao from '../../dao/BookInfoDao'
import ActivityIndicatorPlus from '../../common/ActivityIndicatorPlus'
import _ from 'lodash'
import { EMITTER_CHOISE_ADDRESS_SUCESS,EMITTER_EXCRETEORDERSUCESS_TYPE, EMITTER_COMMON_CONTACT_SELECT_TYPE } from '../../common/EmitterTypes'
import AlertView from '../../common/AlertView';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NumberCheck from '../../common/NumberCheck';
import { Geocode } from 'react-native-baidumap-sdk';

let _this = null


export default class ExcreteBookSpaceInfo extends Component {

    constructor(props) {
        super(props)
        _this = this
        this.params = this.props.navigation.state.params
        let tempArr = this.params.contDTOList
        let tempArr2 = _.filter(tempArr, ['isS', '1'])
        this.excreteArr = tempArr2
        let currentDate = DateUril.getCurrentDate()     //获取当前日期
        this.bottomModalSelect = ''                   //bottomModal选了哪一个
        this.portSelect = 0                             //选择起运港还是目的港
        this.dataPickerSelect = ''                      //日期选择
        this.isUploadCarAdvanceCost = false     //是否显示装港代垫费
        this.isDownloadCarAdvanceCost = false   //是否显示卸港代垫费
        this.isSingleBoxWeight = true         //单箱重量是否可编辑
        this.isNetwork = true
        if (this.params.hm && this.params.hm.indexOf('空箱') != -1) {
            this.isSingleBoxWeight = false
        }
        let xt = this.params.xt
        if (xt) {
            if (xt == 'F') {
                xt = '重箱'
                this.isSingleBoxWeight = true
            } else if (xt == 'E') {
                xt = '空箱'
                this.isSingleBoxWeight = false
            }
        } else {
            xt = '重箱'
            this.isSingleBoxWeight = true
        }

        let tsyqmq = this.params.tsyqmq     //装货特殊要求
        let tsyqdm = this.params.tsyqdm     //卸货特殊要求
        this.preStartPortName = ''                     //选择前的始发港
        this.preEndPortName = ''                        //选择前的目的港
        let isNameEdit = true
        if((this.params.hm) && (this.params.hm.indexOf('空箱') != -1)){
            isNameEdit = false
        }

        this.state = {
            showBaseInfo: true,               //展开基本信息
            showEntrustInfo: false,           //展开委托信息
            showGoodsInfo: false,             //展开货物信息
            showBoxInfo: false,                //展开箱信息
            showCostInfo: false,              //展开费用信息
            showDeclareProvision: false,      //展开声明条款

            startPortName: this.params.startPort ? this.params.startPort : '请选择',           //起运港
            endPortName: this.params.endPort ? this.params.endPort : '请选择',             //目的港
            advanceLoadDate: this.params.bookZxsj ? this.params.bookZxsj : '请选择',          //预计装货日期
            payMode: this.params.fkfs ? this.params.fkfs : '请选择',                  //付款方式
            transProvision: this.params.ystk ? this.params.ystk : '请选择',            //运输条款
            collectTicket: this.params.spf,                                            //受票方
            boxReceipt: this.params.xnqsd ? this.params.xnqsd : false,                    //箱内签收单
            bookPersonDetention: this.params.dcrZk ? this.params.dcrZk : false,           //订舱人扣货
            baseInfoMark: this.params.bz ? this.params.bz : '',                     //基本信息的备注

            delivePersonName: this.params.fhrqc ? this.params.fhrqc : '',                //发货人全称
            deliverPersonContact: this.params.bookZxdd ? this.params.bookZxdd : '',           //发货人联系人
            deliverPersonPhone: this.params.fhrdh ? this.params.fhrdh : '',              //发货人电话
            deliverGatePoint: this.params.fhrmd ? this.params.fhrmd : '发货门点',                //发货门点
            deliverDetailAddress: this.params.fhrdz ? this.params.fhrdz : '',            //发货详细地址
            receivePersonName: this.params.shr ? this.params.shr : '',             //收货人全称
            receivePersonContact: this.params.shrlxr ? this.params.shrlxr : '',          //收货人联系人
            receivePersonPhone: this.params.shrdh ? this.params.shrdh : '',            //收货人电话
            receiveGatePoint: this.params.shrmd ? this.params.shrmd : '收货门点',            //收货门点
            receiveDetailAddress: this.params.shrdz ? this.params.shrdz : '',                //收货详细地址

            goodsName: this.params.hm ? this.params.hm : '请选择',                       //货名
            goodsNameArr: [],                        //货名数组
            boxModeArr: [],
            goodsNameEdit: isNameEdit,                     //货名cell是否可点击
            goodsMode: this.params.hl ? this.params.hl : '',                       //货类
            packingMode: this.params.bzlx ? this.params.bzlx : '请选择',                    //包装类型
            boxMode: this.params.xxxl ? this.params.xxxl : '请选择',                             //箱型
            boxState: xt,                               //箱态
            boxWeight: this.excreteArr.length + '',                           //箱量
            singleBoxWeight: this.params.zl ? this.params.zl + '' : '',                     //单箱重量
            uploadSpecialRequire: this.convertSpecialCodeToName(tsyqmq),                 //装货特殊要求
            downloadSpecialRequire: this.convertSpecialCodeToName(tsyqdm),               //卸货特殊要求
            boxMark: this.params.bookBz2 ? this.params.bookBz2 : '',                             //箱备注
            dangerGoods: false,                         //危险品
            selfBox: this.params.soc ? this.params.soc : false,                             //自备柜
            dangerIMDG: '',                          //危险品的IMDG
            dangerMode: '',                          //危险品类别
            dangerUNCode: '',                        //危险品联合国编号
            dangerMark: '',                          //危险品备注

            seaServeCost: '',                        //海运服务费
            uploadCarCost: this.params.zgtcfysbj ? this.params.zgtcfysbj + '' : '',                       //装港拖车服务费
            downloadCarCost: this.params.xgtcfysbj ? this.params.xgtcfysbj + '' : '',                     //卸港拖车服务费
            uploadRailCost: this.params.zgtlfw ? this.params.zgtlfw + '' : '',                      //装港铁路服务费
            downloadRailCost: this.params.xgtlfw ? this.params.xgtlfw + '' : '',                    //卸港铁路服务费
            uploadCarAdvanceCost: this.params.zgtcddf ? this.params.zgtcddf : '0',                        //装港拖车代垫费
            uploadCarAdvanceCostMark: this.params.zgtcddfbz ? this.params.zgtcddfbz : '',                 //装港拖车代垫费备注
            downloadCarAdvanceCost: this.params.xgtcddf ? this.params.xgtcddf : '0',                      //卸港拖车代垫费
            downloadcarAdvanceCostMark: this.params.xgtcddfbz ? this.params.xgtcddfbz : '',              //卸港拖车代垫费备注
            premiumPayMode: this.params.bxfzf,                                                       //保险费支付方式
            premiumWorth: this.params.bxhz ? this.params.bxhz + '' : '',                        //保险货值
            totalCost: '',                           //总价

            currentDate: currentDate,                //当前日期
            // areaArray: area2,                         //省市区门点数据
            isShowAreaModal: false,                    //是否显示省市区门点modal
            collectTicketArr: [{ 'name': '订舱人', 'code': 0 }, { 'name': '发货人', 'code': 1 }, { 'name': '收货人', 'code': 2 }],   //受票方数据
            sendGateArray: [],                       //发货门点数据
            receiveGateArray: [],                    //收货门点数据
            transProvisionArr: [{ 'name': 'CY-CY' }, { 'name': 'CY-DO' }, { 'name': 'DO-CY' }, { 'name': 'DO-DO' }, { 'name': 'CY-CFS' }, { 'name': 'CFS-CFS' }, { 'name': 'DO-CFS' }, { 'name': 'CFS-CY' }, { 'name': 'CFS-DO' }, { 'name': 'FI-CY' }, { 'name': 'CY-FO' }, { 'name': 'FI-DO' }, { 'name': 'DO-FO' }],   //运输条款数据
            payModeArr: publicDao.YG_FLAG == 'true' ? [{ 'name': '赎单' }, { 'name': '到付' }, { 'name': '月结' }] : [{ 'name': '赎单' }, { 'name': '到付' }],        //付款方式数据
            packingModeArr: [{ 'name': '木箱', 'code': '100' }, { 'name': '散装', 'code': '200' }, { 'name': '卷装', 'code': '300' }, { 'name': '液袋', 'code': '400' }, { 'name': '普通袋装', 'code': '500' }, { 'name': '托盘', 'code': '600' }, { 'name': '纸箱', 'code': '700' }, { 'name': '灌装', 'code': '800' }, { 'name': '桶装', 'code': '900' }, { 'name': '空箱', 'code': '1000' }],        //包装类型数据
            premiumPayModeArr: [{ 'name': '无保险', 'code': 0 }, { 'name': '车队提箱时支付', 'code': 1 }, { 'name': '含运费', 'code': 2 }],        //保险费支付方式数据
            // boxModeArr: [{ 'name': '20GP' }, { 'name': '40GP' }, { 'name': '40HC' }, { 'name': '20TK' }, { 'name': '40RH' }, { 'name': '40RF' }], //箱型数据
            boxStateArr: [{ 'name': '重箱' }, { 'name': '空箱' }],  //箱态数据
            costDetailArr: [],       //总价明细数据
            excreteMark: '',          //拆单备注
            isShowDetailBtn: false,      //是否显示明细按钮
            goodsTemperature: this.params.szwd ? this.params.szwd : '',        //设置温度
            goodsWind: this.params.fmkd ? this.params.fmkd : '',               //峰门开度
            advanceBoxDate: this.params.yjtxsj ? this.params.yjtxsj : '请选择',          //预计提箱时间
            goodsHumidity: this.params.sdsz ? this.params.sdsz : '',           //湿度湿度
            goodsCooling: this.params.ylyq == 0 ? false : true,            //预冷要求
            specialRequireArr: [{ 'name': '无', 'code': 0 }, { 'name': '短板车', 'code': 1 }, { 'name': '自卸车', 'code': 2 }, { 'name': '不打冷', 'code': 4 }], //装卸货特殊要求数据

        }
    }

    convertSpecialNameToCode(name) {  //根据装卸货特殊要求的name获取code
        if (name == '无') {
            return 0
        } else if (name == '短板车') {
            return 1
        } else if (name == '自卸车') {
            return 2
        } else if (name == '不打冷') {
            return 4
        }
    }

    convertSpecialCodeToName(code) {     //根据装卸货特殊要求的code获取name
        if (code == 0) {
            return '无'
        } else if (code == 1) {
            return '短板车'
        } else if (code == 2) {
            return '自卸车'
        } else if (code == 4) {
            return '不打冷'
        }
    }

    async getAdressArray() {
        await this.getGateData(true, this.state.startPortName)
        setTimeout(() => {
            this.getGateData(false, this.state.endPortName)
        }, 500)
    }

    //常用联系人保存
    saveCommonContact(isSend) {
        // let _this = this
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        if (isSend) {
            param.lxrLx = '20'
            param.gsmc = this.state.delivePersonName
            param.lxr = this.state.deliverPersonContact
            param.dh = this.state.deliverPersonPhone
            if (this.isDeliverGatePoint) {
                param.md = this.state.deliverGatePoint
                param.dz = this.state.deliverDetailAddress
            }
        } else {
            param.lxrLx = '10'
            param.gsmc = this.state.receivePersonName
            param.lxr = this.state.receivePersonContact
            param.dh = this.state.receivePersonPhone
            if (this.isReceiveGatePoint) {
                param.md = this.state.receiveGatePoint
                param.dz = this.state.receiveDetailAddress
            }
        }
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('saveAppDcLxr.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backcode == '200') {
                    if (isSend) {
                        this.refs.toast.show('添加发货人成功', 800)
                    } else {
                        this.refs.toast.show('添加收货人成功', 800)
                    }
                } else {
                    if (isSend) {
                        this.refs.toast.show('添加发货人失败', 800)
                    } else {
                        this.refs.toast.show('添加收货人失败', 800)
                    }
                }

            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (isSend) {
                    this.refs.toast.show('添加发货人失败', 800)
                } else {
                    this.refs.toast.show('添加收货人失败', 800)
                }

            })
    }
    //收发货门点数据
    getGateData(isSend, port, missDoor = false) {
        // let _this = this
        if (isSend) {
            if (this.state.sendGateArray.length > 0) {
                if (this.preStartPortName == port) {
                    return
                }
            }
        } else {
            if (this.state.receiveGateArray.length > 0) {
                if (this.preEndPortName == port) {
                    return
                }
            }
        }
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        param.portName = port
        param.doorToken = publicDao.DOOR_TOKEN
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('getDoorsList.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backCode == '200') {
                    if (isSend) {
                        this.preStartPortName = port
                        this.setState({ sendGateArray: data.entityList })
                    } else {
                        this.preEndPortName = port
                        this.setState({ receiveGateArray: data.entityList })
                    }
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '202') {
                    this.refs.toast.show('请先选择起运港', 800)
                } else if (data._backCode == '404' && missDoor){
                    this.refs.toast.show('该港口下没有门点信息', 800)
                } else {
                    // this.refs.toast.show('获取门点失败', 800)
                    if (isSend) {
                        this.setState({ sendGateArray: [] })
                    } else {
                        this.setState({ receiveGateArray: [] })
                    }
                }
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                // this.refs.toast.show('获取门点失败', 800)
                // this.refs.toast.show('获取数据失败,请检查网络', 1000, () => {
                //     NavigationUtil.goBack(this.props.navigation)
                // });
                if (isSend) {
                    this.setState({ sendGateArray: [] })
                } else {
                    this.setState({ receiveGateArray: [] })
                }
            })
    }


    //如果条款为DO-DO 异步执行获取拖车费报价避免造成token失效的错误
    async someAsgetTruckFeeData() {
        console.log('d0-do');
        await this.getTruckFeeData(1, 1)
        setTimeout(() => {
            this.getTruckFeeData(0, 0)
        }, 500)
    }
    //根据运输条款去获取拖车费报价
    transProvisionToGetTruckFeeData() {
        if ((this.state.transProvision.indexOf('DO-') != -1) && (this.state.transProvision.indexOf('-DO') != -1)) {
            this.someAsgetTruckFeeData()
        } else {
            if (this.state.transProvision.indexOf('DO-') != -1) {
                this.getTruckFeeData(1, 0)
            }
            if (this.state.transProvision.indexOf('-DO') != -1) {
                this.getTruckFeeData(0, 0)
            }
        }
    }

    // transProvisionToGetTruckFeeData() {
    //     if (this.state.transProvision.indexOf('DO-') != -1) {
    //         this.getTruckFeeData(1)
    //     }
    //     if (this.state.transProvision.indexOf('-DO') != -1) {
    //         this.getTruckFeeData(0)
    //     }
    // }

    //获取拖车费报价
    getTruckFeeData(isExport, showIndicatorPlus) {
        // let _this = this
        // getTruckFeeData(isExport) {
        let param = {}
        if (this.stringIsEmpty(this.state.boxState)) {  //箱态
            return
        }
        if (this.stringIsEmpty(this.state.boxMode)) {   //箱型
            return
        }
        if (isExport == 0) {
            if (this.stringIsEmpty(this.state.endPortName)) {  //目的港
                if (showIndicatorPlus == 0) { _this.refs.activityIndicatorPlus.dismiss() }
                return
            }
            if (this.state.receiveGatePoint == '收货门点') {    //收货门点
                if (showIndicatorPlus == 0) { _this.refs.activityIndicatorPlus.dismiss() }
                return
            }

            param.appDjxdz = this.state.receiveGatePoint
            param.appPortName = this.state.endPortName
            param.appTsyq = this.convertSpecialNameToCode(this.state.downloadSpecialRequire)

        } else if (isExport == 1) {
            if (this.stringIsEmpty(this.state.startPortName)) { //起运港
                return
            }
            if (this.state.deliverGatePoint == '发货门点') {    //发货门点
                return
            }

            param.appDjxdz = this.state.deliverGatePoint
            param.appPortName = this.state.startPortName
            param.appTsyq = this.convertSpecialNameToCode(this.state.uploadSpecialRequire)
        }

        param.isExport = isExport
        param.xt = this.state.boxState == '重箱' ? 'F' : 'E'
        param.xx = this.state.boxMode
        param.token = publicDao.CURRENT_TOKEN
        param.doorToken = publicDao.DOOR_TOKEN
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('getTruckFee.json', paramStr)
            .then(data => {
                if (showIndicatorPlus == 0) { _this.refs.activityIndicatorPlus.dismiss() }
                // _this.refs.activityIndicatorPlus.dismiss()
                if (data._backcode == '200') {
                    if (isExport == 0) {
                        this.setState({ downloadCarCost: data.trailPrice })
                    } else if (isExport == 1) {
                        this.setState({ uploadCarCost: data.trailPrice })
                    }
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '404') {
                    this.refs.toast.show('未获取到数据', 800)
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 1500)
                    if (isExport == 0) {
                        this.setState({ downloadCarCost: '' })
                    } else if (isExport == 1) {
                        this.setState({ uploadCarCost: '' })
                    }
                }
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                // this.refs.toast.show('获取货名数据失败', 800)
                console.log('获取拖车费报价错误', error)
            })

    }

    //货名
    getGoodsNameData() {
        // let _this = this
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('listGoodsTypeAndInfo.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backcode == '200') {
                    BookInfoDao.GoodsNameArr = data.list
                    this.setState({ goodsNameArr: data.list })
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '404') {
                    this.refs.toast.show('未获取到数据', 800)
                }
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                this.refs.toast.show('计费失败', 800)
            })
    }

    //计费
    calcuFee() {
        // let _this = this
        let pm = this.state.premiumPayMode
        let pc = '0'
        if (pm == '无保险') {
            pc = '0'
        } else if (pm == '车队提箱时支付') {
            pc = '1'
        } else if (pm == '含运费') {
            pc = '2'
        }
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        param.sfg = this.state.startPortName
        param.mdg = this.state.endPortName
        param.ystk = this.state.transProvision             //运输条款
        param.fhrmd = this.state.deliverGatePoint == '发货门点' ? '' : this.state.deliverGatePoint
        param.shrmd = this.state.receiveGatePoint == '收货门点' ? '' : this.state.receiveGatePoint
        param.hm = this.state.goodsName
        param.xx = this.state.boxMode
        param.xt = this.state.boxState == '重箱' ? 'F' : 'E'
        param.tsyqmq = this.convertSpecialNameToCode(this.state.uploadSpecialRequire)
        param.tsyqdm = this.convertSpecialNameToCode(this.state.downloadSpecialRequire)
        param.soc = this.state.selfBox == true ? '1' : '0'
        param.sl = this.state.boxWeight
        param.tsyqmq = '0'       //装货要求
        param.tsyqdm = '0'       //卸货要求
        param.dcf = this.state.seaServeCost
        param.zgtcf = this.state.uploadCarCost
        param.xgtcf = this.state.downloadCarCost
        param.zgtlfw = this.state.uploadRailCost
        param.xgtlfw = this.state.downloadRailCost
        param.bxfzf = pc
        param.bxhz = this.state.premiumWorth
        param.zgtcddf = this.state.uploadCarAdvanceCost == 0 ? null : this.state.uploadCarAdvanceCost
        param.zgtcddfbz = this.state.uploadCarAdvanceCostMark
        param.xgtcddf = this.state.downloadCarAdvanceCost == 0 ? null : this.state.downloadCarAdvanceCost
        param.xgtcddfbz = this.state.downloadcarAdvanceCostMark
        param.zgtcfysbj = this.state.uploadCarCost
        param.xgtcfysbj = this.state.downloadCarCost
        param.startLng = ''     //根据运输条款,从地图获取经纬度,这期不做
        param.startLat = ''
        param.endLng = ''
        param.endLat = ''
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('computationChargeable.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backcode == '200') {
                    let tempArr = []
                    tempArr.push({ 'feeName': '总费用', 'fee': data.fyhj })
                    tempArr.push({ 'feeName': '单箱合计', 'fee': data.dxhj })
                    tempArr.push({ 'feeName': '杂费', 'fee': data.zfbz })
                    let j3 = {}
                    let tempArr2 = []
                    if (data.hyf != null) {
                        tempArr2.push({ 'feeName': '海运服务费', 'fee': data.hyf })
                    }
                    tempArr2.push({ 'feeName': 'THC代收', 'fee': data.thc })
                    tempArr2.push({ 'feeName': '燃油附加费', 'fee': data.ryfjf })
                    if (this.state.transProvision.indexOf('CY-') != -1) {
                        if (parseFloat(data.ckdlzfhj) != 0) {
                            tempArr2.push({ 'feeName': '代理出口杂费合计', 'fee': data.ckdlzfhj })
                        }
                    } else {
                        tempArr2.push({ 'feeName': '代理出口杂费合计', 'fee': data.ckdlzfhj })
                    }
                    if (this.state.transProvision.indexOf('-CY') != -1) {
                        if (parseFloat(data.jkdlzfhj) != 0) {
                            tempArr2.push({ 'feeName': '代理进口杂费合计', 'fee': data.jkdlzfhj })
                        }
                    } else {
                        tempArr2.push({ 'feeName': '代理进口杂费合计', 'fee': data.jkdlzfhj })
                    }
                    if(this.state.transProvision.indexOf('DO-') != -1 || this.state.transProvision.indexOf('CFS-') != -1){
                        if (data.zgtcfysbj != null) {
                            tempArr2.push({ 'feeName': '装港拖车服务费', 'fee': data.zgtcfysbj })
                        }
                    }
                    if(this.state.transProvision.indexOf('DO-') != -1 ){
                        if (data.zgtcddf != null) {
                            tempArr2.push({ 'feeName': '装港拖车代垫费', 'fee': data.zgtcddf })
                        }
                    }
                    
                    if(this.state.transProvision.indexOf('-DO') != -1 || this.state.transProvision.indexOf('-CFS') != -1){
                        if (data.xgtcfysbj != null) {
                            tempArr2.push({ 'feeName': '卸港拖车服务费', 'fee': data.xgtcfysbj })
                        }
                    }
                    if(this.state.transProvision.indexOf('-DO') != -1){
                        if (data.xgtcddf != null) {
                            tempArr2.push({ 'feeName': '卸港拖车代垫费', 'fee': data.xgtcddf })
                        }
                    }
                    // if (data.zgtlfw != null) {
                    //     tempArr2.push({ 'feeName': '装港铁路服务费', 'fee': data.zgtlfw })
                    // }
                    // if (data.xgtlfw != null) {
                    //     tempArr2.push({ 'feeName': '卸港铁路服务费', 'fee': data.xgtlfw })
                    // }
                    tempArr2.push({ 'feeName': '装港铁路服务费', 'fee': data.zgtlfw ? data.zgtlfw : '0'})
                    tempArr2.push({ 'feeName': '卸港铁路服务费', 'fee': data.xgtlfw ? data.xgtlfw : '0'})
                    if (data.bxf != null) {
                        tempArr2.push({ 'feeName': '保险费', 'fee': data.bxf })
                    }
                    j3.feeName = '明细'
                    j3.feeArr = tempArr2
                    tempArr.push(j3)
                    if (this.state.uploadCarCost == '0') {
                        if (data.zgtcfwf) {
                            this.setState({ uploadCarCost: data.zgtcfwf + '' })
                        }

                    }
                    if (this.state.downloadCarCost == '0') {
                        if (data.xgtlfw) {
                            this.setState({ downloadCarCost: data.xgtlfw + '' })
                        }

                    }
                    this.setState({ costDetailArr: tempArr, totalCost: data.fyhj, isShowDetailBtn: true })
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '201') {
                    this.refs.toast.show('token错误', 500)
                } else if (data._backcode == '203') {
                    this.refs.toast.show('保险费支付方式不能是车队提箱时支付', 500)
                } else if (data._backcode == '216') {
                    this.refs.toast.show('请检查始发港口是否正确', 500)
                } else if (data._backcode == '217') {
                    this.refs.toast.show('请检查目的港口是否正确', 500)
                }
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                this.refs.toast.show('计费失败', 800)
            })
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //校验字符串是否是空
    stringIsEmpty(obj) {
        if (typeof obj == "undefined" || obj == null || obj == '' || obj == '请选择') {
            return true;
        } else {
            return false;
        }
    }

    //拆单按钮点击事件
    commitBtnClick() {
        // let _this = this
        _this.refs.activityIndicatorPlus.show()
        let contDTOList = this.excreteArr
        let xh = ''
        if (contDTOList && contDTOList.length > 0) {
            contDTOList.forEach(element => {
                let tempS = element.xh + ',' + element.qfh + ';'
                xh += tempS
            });
        }
        xh = xh.substring(0, xh.length - 1)
        console.log('===', xh)
        let cy20 = null, cy40 = null, cy40h = null
        // let tempArr = _.filter(this.state.boxModeArr, ['name', this.state.boxMode])

        // if (tempArr && tempArr.length > 0) {
        //     if (this.state.boxMode == '20GP') {
        //         cy20 = this.state.seaServeCost
        //     } else if (this.state.boxMode == '20TK') {
        //         cy20 = this.state.seaServeCost
        //     } else if (this.state.boxMode == '40GP') {
        //         cy40 = this.state.seaServeCost
        //     } else if (this.state.boxMode == '40RH') {
        //         cy40 = this.state.seaServeCost
        //     } else if (this.state.boxMode == '40HC') {
        //         cy40h = this.state.seaServeCost
        //     }
        // } else {
        //     if ((this.state.boxMode.indexOf('40') != -1) || (this.state.boxMode.indexOf('45') != -1)) {
        //         cy40 = this.state.seaServeCost
        //     } else if (this.state.boxMode.indexOf('20') != -1) {
        //         cy20 = this.state.seaServeCost
        //     }
        // }
        if ((this.state.boxMode.indexOf('40') != -1) || (this.state.boxMode.indexOf('45') != -1)) {
            cy40 = this.state.seaServeCost
        } else if (this.state.boxMode.indexOf('20') != -1) {
            cy20 = this.state.seaServeCost
        }
        if (this.state.boxMode == '40HC') {
            cy40h = this.state.seaServeCost
            cy40 = null
        }

        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        param.id = this.params.orderId
        param.xh = xh
        param.sqbz = this.state.excreteMark
        param.sfg = this.state.startPortName
        param.mdg = this.state.endPortName
        param.fkfs = this.state.payMode
        param.ystk = this.state.transProvision
        param.xnqsd = this.state.boxReceipt == false ? 0 : 1
        param.bz = this.state.baseInfoMark
        param.spf = this.filterCodeFromName(this.state.collectTicketArr, this.state.collectTicket)
        param.bookZxsj = this.state.advanceLoadDate
        param.bxf = ''                              //保险费
        param.bxfzf = this.filterCodeFromName(this.state.premiumPayModeArr, this.state.premiumPayMode)
        param.bxhz = this.state.premiumWorth
        param.bzlx = this.filterCodeFromName(this.state.packingModeArr, this.state.packingMode)
        param.cy20 = cy20
        param.cy40 = cy40
        param.cy40h = cy40h
        param.fhrdh = this.state.deliverPersonPhone
        param.fhrdz = this.state.deliverDetailAddress
        param.bookZxdd = this.state.deliverPersonContact
        param.fhrmd = this.state.deliverGatePoint == '发货门点' ? '' : this.state.deliverGatePoint
        param.fhrqc = this.state.delivePersonName
        param.hl = this.state.goodsMode
        param.hm = this.state.goodsName
        // param.imdg = ''                             //危险品imdg
        param.shrdh = this.state.receivePersonPhone
        param.shrlxr = this.state.receivePersonContact
        param.shrdz = this.state.receiveDetailAddress
        param.shrmd = this.state.receiveGatePoint == '收货门点' ? '' : this.state.receiveGatePoint
        param.shr = this.state.receivePersonName
        param.sl = this.state.boxWeight
        param.soc = this.state.selfBox == false ? 0 : 1
        param.wxpbs = this.state.dangerGoods == false ? 0 : 1
        // param.wxpbh = ''                    //为危险品时危险品编号为必填项，否则为非必填项
        // param.wxpbz = ''                    //为危险品时危险品备注为必填项，否则为非必填项
        // param.wxplb = ''                    //为危险品时危险品类别为必填项，否则为非必填项
        param.xbz = this.state.boxMark
        param.xgtcf = this.state.downloadCarCost
        param.xgtlfw = this.state.downloadRailCost
        param.xxxl = this.state.boxMode
        param.xt = this.state.boxState == '重箱' ? 'F' : 'E'
        param.tsyqmq = this.convertSpecialNameToCode(this.state.uploadSpecialRequire)
        param.tsyqdm = this.convertSpecialNameToCode(this.state.downloadSpecialRequire)
        param.zgtcf = this.state.uploadCarCost
        param.zgtlfw = this.state.uploadRailCost
        param.dcrZk = this.state.bookPersonDetention == false ? 0 : 1
        param.zl = this.state.singleBoxWeight
        param.ryfjfTime = this.state.currentDate
        param.szwd = this.state.goodsTemperature
        param.fmkd = this.state.goodsWind
        param.yjtxsj = this.state.advanceBoxDate == '请选择' ? '' : this.state.advanceBoxDate
        param.sdsz = this.state.goodsHumidity
        param.ylyq = this.state.goodsCooling == true ? '1' : '0'
        param.zgtcddf = this.state.uploadCarAdvanceCost == 0 ? null : this.state.uploadCarAdvanceCost
        param.zgtcddfbz = this.state.uploadCarAdvanceCostMark
        param.xgtcddf = this.state.downloadCarAdvanceCost == 0 ? null : this.state.downloadCarAdvanceCost
        param.xgtcddfbz = this.state.downloadcarAdvanceCostMark
        param.zgtcfysbj = this.state.uploadCarCost
        param.xgtcfysbj = this.state.downloadCarCost
        param.startLng = ''     //根据运输条款,从地图获取经纬度,这期不做
        param.startLat = ''
        param.endLng = ''
        param.endLat = ''
        console.log('param:', param)
        let paramStr = JSON.stringify(param)
        NetworkDao.fetchPostNet('splitWayBill.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backcode == '200') {
                    this.refs.toast.show('拆单成功', 500, () => {
                        DeviceEventEmitter.emit(EMITTER_EXCRETEORDERSUCESS_TYPE);
                        // NavigationUtil.goBack(this.props.navigation)
                        this.props.navigation.goBack(publicDao.BoxStateView_NavigationKey)
                    })
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '221') {
                    this.refs.toast.show('装港拖车费有误或卸港拖车费有误', 500)
                } else if (data._backcode == '411') {
                    this.refs.toast.show('获取订单流水号失败', 500)
                } else if (data._backcode == '450') {
                    this.refs.toast.show('新建失败', 500)
                } else if (data._backcode == '701') {
                    this.refs.toast.show('您被列为黑名单,请联系中谷总部营运中心业务人员', 500)
                } else if (data._backcode == '805') {
                    this.refs.toast.show('无法修改箱型', 500)
                } else if (data._backcode == '806') {
                    this.refs.toast.show('冷冻箱必须办理保险', 500)
                } else if (data._backcode == '801') {
                    this.refs.toast.show('运单已放货,禁止修改扣货状态', 500)
                } else if (data._backcode == '702') {
                    this.refs.toast.show('门点信息有误,请到网上订舱下单', 500)
                } else if (data._backcode == '686') {
                    this.refs.toast.show('您的账号需要认证,请去网页端进行认证', 500)
                }
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                this.refs.toast.show('拆单失败', 800)
            })
    }

    //找到对应的数据
    filterNameFromCode(arr, code, mode) {
        let newArr = _.filter(arr, ['code', code])
        let obj = newArr[0]
        if (mode == 'collectTicket') {
            this.setState({ collectTicket: obj.name })
        } else if (mode == 'premiumPayMode') {
            this.setState({ premiumPayMode: obj.name })
        }
    }

    //获取海运服务费
    getSeaserverData() {
        // let _this = this
        let cy20 = this.params.cy20
        let cy40 = this.params.cy40
        let cy40h = this.params.cy40h
        let seaCost = 0
        if (cy20 != 0) {
            seaCost = cy20
        } else if (cy40 != 0) {
            seaCost = cy40
        } else if (cy40h != 0) {
            seaCost = cy40h
        }
        seaCost = seaCost + ''
        this.setState({ seaServeCost: seaCost })
    }

    //根据那么找到相应的code
    filterCodeFromName(arr, name) {
        let newArr = _.filter(arr, ['name', name])
        let obj = newArr[0]
        return obj.code
    }
    //箱型数据
    getBoxModeArr() {
        // let _this = this
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        let paramStr = JSON.stringify(param)
        _this.refs.activityIndicatorPlus.show()
        NetworkDao.fetchPostNet('getAllBoxType.json', paramStr)
            .then(data => {
                _this.refs.activityIndicatorPlus.dismiss()
                if (data._backCode == "200") {
                    console.log('获取箱型：', data);
                    this.setState({ boxModeArr: data.entityList })
                } else if (data._backCode == "224") {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                    return
                } else if (data._backCode == "404") {
                    this.refs.toast.show('未获取到数据', 800)
                    return
                }
            })
            .then(() => {
                this.refs.bookSpaceBottomModal.show('name', this.state.boxModeArr)
            })
            .catch(error => {
                _this.refs.activityIndicatorPlus.dismiss()
                this.refs.toast.show('获取数据失败,请检查网络', 1000, () => {
                    // NavigationUtil.goBack(this.props.navigation)
                });
            })
    }

    componentDidMount() {
        if (BookInfoDao.GoodsNameArr && BookInfoDao.GoodsNameArr.length > 0) {
            this.setState({ goodsNameArr: BookInfoDao.GoodsNameArr })
        } else {
            this.getGoodsNameData()
        }
        if (this.state.collectTicket != '请选择') {
            this.filterNameFromCode(this.state.collectTicketArr, this.state.collectTicket, 'collectTicket')
        }
        if (this.state.boxReceipt != false) {
            if (this.state.boxReceipt == 0) {
                this.setState({ boxReceipt: false })
            } else if (this.state.boxReceipt == 1) {
                this.setState({ boxReceipt: true })
            }
        }
        if (this.state.bookPersonDetention != false) {
            if (this.state.bookPersonDetention == 0) {
                this.setState({ bookPersonDetention: false })
            } else if (this.state.bookPersonDetention == 1) {
                this.setState({ bookPersonDetention: true })
            }
        }
        if (this.state.selfBox != false) {
            if (this.state.selfBox == 0) {
                this.setState({ selfBox: false })
            } else if (this.state.selfBox == 1) {
                this.setState({ selfBox: true })
            }
        }
        this.getSeaserverData()
        if (this.state.premiumPayMode != '请选择') {
            this.filterNameFromCode(this.state.premiumPayModeArr, this.state.premiumPayMode, 'premiumPayMode')
        }
        if (this.state.startPortName == '上海') {
            let tempA = []
            if (this.state.boxMode == '40RH') {
                tempA = [{ 'name': '含运费', 'code': 2 }]
            } else {
                tempA = [{ 'name': '无保险', 'code': 0 }, { 'name': '含运费', 'code': 2 }]
            }
            this.setState({ premiumPayModeArr: tempA })
        }
        this.getAdressArray()

        //监听网络变化事件
        NetInfo.addEventListener('change', (networkType) => {
            // this.refs.toast.show('网络状态:'+networkType, 2000)
            if ((networkType == 'none') || (networkType == 'NONE')) {
                //没网
                this.isNetwork = false
            } else {
                this.isNetwork = true
                if (!this.stringIsEmpty(this.state.startPortName)) {
                    this.getGateData(true, this.state.startPortName)
                }
                if (!this.stringIsEmpty(this.state.endPortName)) {
                    this.getGateData(false, this.state.endPortName)
                }
            }
        })

        this.searchListener = DeviceEventEmitter.addListener(EMITTER_COMMON_CONTACT_SELECT_TYPE, (para) => {
            console.log('选择常用联系人返回的数据', para)
            let data = para.data
            if (para.isSend) {
                if(this.state.transProvision.indexOf('DO-') != -1){
                    this.setState({ delivePersonName: data.gsmc, deliverPersonContact: data.lxr, deliverPersonPhone: data.dh, deliverGatePoint: data.md, deliverDetailAddress: data.dz }, () => {
                        if(data.md &&data.md.length > 0 && data.dz && data.dz.length > 0 ){
                            this.transProvisionToGetTruckFeeData()
                        }
                    })
                }else{
                    this.setState({ delivePersonName: data.gsmc, deliverPersonContact: data.lxr, deliverPersonPhone: data.dh}, () => {
                    })
                }
                
            } else {
                if(this.state.transProvision.indexOf('-DO') != -1){
                    this.setState({ receivePersonName: data.gsmc, receivePersonContact: data.lxr, receivePersonPhone: data.dh, receiveGatePoint: data.md, receiveDetailAddress: data.dz }, () => {
                        if(data.md &&data.md.length > 0 && data.dz && data.dz.length > 0 ){
                            this.transProvisionToGetTruckFeeData()
                        }
                    })
                }else{
                    this.setState({ receivePersonName: data.gsmc, receivePersonContact: data.lxr, receivePersonPhone: data.dh}, () => {
                    })
                }
                
            }
        });

        if (this.state.goodsName == '商品空箱') {
            this.setState({ singleBoxWeight: '0' })
        }
        //! 详细地址地图界面选择完详细地址后的监听事件
        this.choiseAddressListener = DeviceEventEmitter.addListener(EMITTER_CHOISE_ADDRESS_SUCESS, (para) => {
            console.log(para);
            if( para.key == 'getPoint' ){
                this.setState({ receiveDetailAddress: para.address })
            }else if( para.key == 'postPoint' ){
                this.setState({ deliverDetailAddress: para.address })
            }
        })
    }

    componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
        this.choiseAddressListener && this.choiseAddressListener.remove()
    }

    alertCancelDown = () => {
        console.log('点击了alert取消按钮')
    }

    //点击了确定退出登录
    alertSureDown = () => {
        if (this.state.boxMode.indexOf('20') != -1) {
            if (parseFloat(this.state.singleBoxWeight) > 27.7) {
                this.refs.toast.show('小柜货重上限为27.7吨', 900)
                // this.timer = setTimeout(() => {
                //     this.commitBtnClick()
                // }, 1200);
                return
            } else {
                this.commitBtnClick()
            }

        } else if (this.state.boxMode.indexOf('40') != -1) {
            if (parseFloat(this.state.singleBoxWeight) > 26.2) {
                this.refs.toast.show('大柜货重上限为26.2吨', 900)
                // this.timer = setTimeout(() => {
                //     this.commitBtnClick()
                // }, 1200);
                return
            } else {
                this.commitBtnClick()
            }

        } else {
            this.commitBtnClick()
        }
    }

    renderLine() {
        return <View style={{ width: GlobalStyles.screenWidth, height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //基本信息下面的cell
    renderBaseInfoDetail() {
        let startPortNameColor = this.state.startPortName == '请选择' ? '#999999' : '#354953'
        let endPortNameColor = this.state.endPortName == '请选择' ? '#999999' : '#354953'
        let advanceLoadDateColor = this.state.advanceLoadDate == '请选择' ? '#999999' : '#354953'
        let payModeColor = this.state.payMode == '请选择' ? '#999999' : '#354953'
        let transProvisionColor = this.state.transProvision == '请选择' ? '#999999' : '#354953'
        let collectTicketColor = this.state.collectTicket == '请选择' ? '#999999' : '#354953'
        return <View>
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '起运港', this.state.startPortName, startPortNameColor, () => {
                console.log('起运港点击事件')
                Keyboard.dismiss()
                this.portSelect = 0
                this.props.navigation.navigate('TransCostPortSearchView', {
                    backData: (data) => {
                        if (data == '上海') {
                            let tempA = [{ 'name': '无保险', 'code': 0 }, { 'name': '含运费', 'code': 2 }]
                            let tempS = this.state.premiumPayMode
                            if (tempS == '车队提箱时支付') {
                                tempS = '请选择'
                            }
                            this.setState({ premiumPayModeArr: tempA, premiumPayMode: tempS })
                        } else {
                            let tempA = [{ 'name': '无保险', 'code': 0 }, { 'name': '车队提箱时支付', 'code': 1 }, { 'name': '含运费', 'code': 2 }]
                            this.setState({ premiumPayModeArr: tempA })
                        }
                        // this.setState({ startPortName: data })
                        if (data == this.state.startPortName) {
                            this.transProvisionToGetTruckFeeData()
                        } else {
                            if (this.state.deliverGatePoint != '发货门点') {
                                this.setState({
                                    startPortName: data,
                                    deliverGatePoint: '发货门点',
                                    deliverDetailAddress: ''
                                })
                            } else {
                                this.setState({ startPortName: data })
                            }
                        }
                        this.getGateData(true, data)
                    }
                })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '目的港', this.state.endPortName, endPortNameColor, () => {
                console.log('目的港点击事件')
                Keyboard.dismiss()
                this.portSelect = 1
                this.props.navigation.navigate('TransCostPortSearchView', {
                    backData: (data) => {
                        // this.setState({ endPortName: data })
                        if (data == this.state.endPortName) {
                            this.transProvisionToGetTruckFeeData()
                        } else {
                            if (this.state.receiveGatePoint != '收货门点') {
                                this.setState({
                                    endPortName: data,
                                    receiveGatePoint: '收货门点',
                                    receiveDetailAddress: ''
                                })
                            } else {
                                this.setState({ endPortName: data })
                            }
                        }
                        this.getGateData(false, data)
                    }
                })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '预计装货日期', this.state.advanceLoadDate, advanceLoadDateColor, () => {
                //预计装货日期
                this.dataPickerSelect = 'advanceLoadDate'
                this.refs.datePicker.onPressDate()
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '付款方式', this.state.payMode, payModeColor, () => {
                //付款方式
                this.bottomModalSelect = 'payMode'
                this.refs.bookSpaceBottomModal.show('name', this.state.payModeArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '运输条款', this.state.transProvision, transProvisionColor, () => {
                //运输条款
                this.bottomModalSelect = 'transProvision'
                this.refs.bookSpaceBottomModal.show('name', this.state.transProvisionArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '受票方', this.state.collectTicket, collectTicketColor, () => {
                //受票方
                this.bottomModalSelect = 'collectTicket'
                this.refs.bookSpaceBottomModal.show('name', this.state.collectTicketArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell(true, '#FFFBFB', '箱内签收单', this.state.boxReceipt, (val) => {
                console.log('箱内签收单是否选中:', val)
                this.setState({ boxReceipt: val })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell(true, '#FFFBFB', '订舱人扣货', this.state.bookPersonDetention, (val) => {
                this.setState({ bookPersonDetention: val })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextInputCell('#FFFBFB', '备注', '选填', this.state.baseInfoMark, (text) => {
                console.log('输入的文字:', text)
            }, (event) => {
                console.log('备注是:', event.nativeEvent.text)
                this.setState({ baseInfoMark: event.nativeEvent.text })
            })}
        </View>
    }

    //委托信息下面的cell
    renderEnTrustDetail() {

        let deliverGatePointColor = this.state.deliverGatePoint == '发货门点' ? '#666' : '#354953'
        let receiveGatePointColor = this.state.receiveGatePoint == '收货门点' ? '#666' : '#354953'
        let isD = false   //发货门点是否必填
        if (this.state.transProvision.indexOf('DO-') != -1) {
            isD = true
        } else {
            isD = false
        }
        this.isDeliverGatePoint = isD
        let isR = false //收货门点是否必填
        if (this.state.transProvision.indexOf('-DO') != -1) {
            isR = true
        } else {
            isR = false
        }
        this.isReceiveGatePoint = isR

        let isDd = false  //发货门点是否有数据
        if (this.state.sendGateArray && this.state.sendGateArray.length > 0) {
            isDd = true;
        } else {
            isDd = false
        }

        let isRd = false //收货门点是否有数据
        if (this.state.receiveGateArray && this.state.receiveGateArray.length > 0) {
            isRd = true;
        } else {
            isRd = false
        }

        return <View>
            {BookSpaceCellsUtil.renderMarginleftInputCellRightBtn(true, '#FFFBFB', '发货人全称', 120, '请输入', true, 'default', this.state.delivePersonName, (text) => {
                console.log('发货人全称:', text)
            }, (event) => {
                console.log('发货人全称是:', event.nativeEvent.text)
                this.setState({ delivePersonName: event.nativeEvent.text })
            }, () => {
                console.log('发货人全称右侧按钮点击事件')
                let data = {}
                data.isSend = true
                NavigationUtil.goPage(data, 'BookSpaceCommonContact')
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '发货人联系人', 120, '请输入', true, 'default', this.state.deliverPersonContact, (text) => {

            }, (event) => {
                this.setState({ deliverPersonContact: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '发货人电话', 120, '请输入', true, 'numeric', this.state.deliverPersonPhone, (text) => {
                this.setState({ deliverPersonPhone: text })
            }, (event) => {
                let txt = event.nativeEvent.text
                let isY = NumberCheck.validatePhone(txt)
                if (isY) {
                    // this.setState({ deliverPersonPhone: txt })
                } else {
                    this.refs.toast.show('电话格式不对', 800)
                }
            })}
            {this.renderLine()}
            {isD ? (this.state.startPortName == '请选择' ? BookSpaceCellsUtil.renderLeftTitleColorChooseCell(isD, '#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor, () => {
                this.refs.toast.show('请选择起运港')

            }) : (isDd ? <Picker
                data={this.state.sendGateArray}
                cols={4}
                onChange={(val) => {
                    console.log('onChnage val is ', val)
                }}
                onOk={(val) => {
                    console.log('onOk val is ', val)
                    let newArr = []
                    if (this.state.sendGateArray && this.state.sendGateArray.length > 0) {
                        newArr = this.state.sendGateArray.concat()  //源数据
                    }
                    if (newArr && newArr.length > 0) {
                        let area1 = find(newArr, (item2) => item2.value == val[0])
                        console.log('省 json :', area1)
                        let area2 = find(area1.children, (item2) => item2.value == val[1])
                        console.log('市 json :', area2)
                        let area3 = find(area2.children, (item2) => item2.value == val[2])
                        console.log('区 json :', area3)
                        let area4 = find(area3.children, (item2) => item2.value == val[3])
                        console.log('门点 json:', area4)
                        let areaFull = area1.label + ',' + area2.label + ',' + area3.label + ',' + area4.label
                        this.setState({ deliverGatePoint: areaFull }, () => {
                            this.transProvisionToGetTruckFeeData()
                        })
                    }
                }}
                okText='确定'
                dismissText='取消'
                cascade={true}
                itemStyle={{ fontSize: 16 }}
            >
                {BookSpaceCellsUtil.renderLeftTitleColorChooseCell(isD, '#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor, () => {
                    // this.getGateData(true, this.state.startPortName)
                })}
            </Picker> : BookSpaceCellsUtil.renderLeftTitleColorChooseCell(true, '#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor, () => {
                //门点没有数据
                if(this.isNetwork){
                    this.getGateData(true, this.state.startPortName,true)
                }else{
                this.refs.toast.show('获取门点失败，请检查网络', 800)
                }
            }))) : null}


            {isD ? this.renderLine() : null}
            {isD ? BookSpaceCellsUtil.renderPureTextInputCell(
                isD, 
                '#FFFBFB', 
                '发货详细地址', 
                this.state.deliverDetailAddress, () => {}, 
                (event) => {this.setState({ deliverDetailAddress: event.nativeEvent.text })},
                true,
                ()=>{
                    if (this.state.deliverGatePoint == '发货门点'|| this.state.deliverGatePoint == '') {
                        this.refs.toast.show('请选择发货门点')
                        return
                    }
                    _this.refs.activityIndicatorPlus.show()
                    // !根据发获地址和详细地址获取经纬度信息,如果有详细地址根据详细地址查询经纬度，否则按照发货门点查询
                    Geocode.search(this.state.deliverDetailAddress ? this.state.deliverGatePoint + ',' + this.state.deliverDetailAddress : this.state.deliverGatePoint )
                        .then((data)=>{
                            _this.refs.activityIndicatorPlus.dismiss()
                            NavigationUtil.goPage({
                                key:'postPoint',  // !进入详细地址地图界面的标示，判断是发货点还是收获点
                                searchAddress:this.state.deliverGatePoint,  //! 发货门点地址
                                address:this.state.deliverDetailAddress,  // !详细地址
                                lat:data.latitude,      //!纬度
                                lon:data.longitude},  //!经度
                                'BaiDuMapDetail')
                        })
                        .catch((e)=>{
                            _this.refs.activityIndicatorPlus.dismiss()
                            console.log(e);
                            this.refs.toast.show('未获取到相关地点,请检查详细地址是否填写正确',1000)
                        })
                }) 
            : null}
            {isD ? this.renderLine() : null}
            {BookSpaceCellsUtil.renderScreenWidthButton('#FFFBFB', '保存到常用联系人', () => {
                console.log('保存发货联系人到常用联系人按钮点击事件')
                if (this.stringIsEmpty(this.state.delivePersonName)) {
                    this.refs.toast.show('请填写发货人全称', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.deliverPersonContact)) {
                    this.refs.toast.show('请填写发货人联系人', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.deliverPersonPhone)) {
                    this.refs.toast.show('请填写发货人电话', 800)
                    return
                }
                let isY = NumberCheck.validatePhone(this.state.deliverPersonPhone)
                if (isY) {
                } else {
                    this.refs.toast.show('电话格式不对', 800)
                    return
                }
                if (isD) {
                    if (this.state.deliverGatePoint == '发货门点'|| this.state.deliverGatePoint == '') {
                        this.refs.toast.show('请选择发货门点', 800)
                        return
                    }
                    if (this.state.deliverDetailAddress == '') {
                        this.refs.toast.show('请填写发货详细地址', 800)
                        return
                    }
                }
                this.saveCommonContact(true)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCellRightBtn(true, '#FFFBFB', '收货人全称', 120, '请输入', true, 'default', this.state.receivePersonName, (text) => {

            }, (event) => {
                this.setState({ receivePersonName: event.nativeEvent.text })
            }, () => {
                console.log('收货人全称右侧按钮点击事件')
                let data = {}
                data.isSend = false
                NavigationUtil.goPage(data, 'BookSpaceCommonContact')
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '收货人联系人', 120, '请输入', true, 'default', this.state.receivePersonContact, (text) => {

            }, (event) => {
                this.setState({ receivePersonContact: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '收货人电话', 120, '请输入', true, 'numeric', this.state.receivePersonPhone, (text) => {
                this.setState({ receivePersonPhone: text })
            }, (event) => {
                let txt = event.nativeEvent.text
                let isY = NumberCheck.validatePhone(txt)
                if (isY) {
                    // this.setState({ receivePersonPhone: txt })
                } else {
                    this.refs.toast.show('电话格式不对', 800)
                }
            })}
            {this.renderLine()}
            {isR ? (this.state.endPortName == '请选择' ? BookSpaceCellsUtil.renderLeftTitleColorChooseCell(isR, '#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor, () => {
                this.refs.toast.show('请选择目的港')

            }) : (isRd ? <Picker
                data={this.state.receiveGateArray}
                cols={4}
                onChange={(val) => {
                    console.log('onChnage val is ', val)
                }}
                onOk={(val) => {
                    console.log('onOk val is ', val)
                    let newArr = []
                    if (this.state.receiveGateArray && this.state.receiveGateArray.length > 0) {
                        newArr = this.state.receiveGateArray.concat()  //源数据
                    }
                    if (newArr && newArr.length > 0) {
                        let area1 = find(newArr, (item2) => item2.value == val[0])
                        let area2 = find(area1.children, (item2) => item2.value == val[1])
                        let area3 = find(area2.children, (item2) => item2.value == val[2])
                        let area4 = find(area3.children, (item2) => item2.value == val[3])
                        let areaFull = area1.label + ',' + area2.label + ',' + area3.label + ',' + area4.label
                        this.setState({ receiveGatePoint: areaFull }, () => {
                            this.transProvisionToGetTruckFeeData()
                        })
                    }

                }}
                okText='确定'
                dismissText='取消'
                cascade={true}
                itemStyle={{ fontSize: 16 }}
            >
                {BookSpaceCellsUtil.renderLeftTitleColorChooseCell(isR, '#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor, () => {
                    // this.setState({ receiveGateArray: this.receiveGateArray })
                    // this.getGateData(false, this.state.endPortName)
                })}
            </Picker> : BookSpaceCellsUtil.renderLeftTitleColorChooseCell(true, '#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor, () => {
                //收货门点没有数据
                // this.getGateData(false, this.state.endPortName)
                if(this.isNetwork){
                    this.getGateData(false, this.state.endPortName,true)
                }else{
                this.refs.toast.show('获取门点失败，请检查网络', 800)
                }
            }))) : null}


            {isR ? this.renderLine() : null}
            {isR ? BookSpaceCellsUtil.renderPureTextInputCell(
                isR, 
                '#FFFBFB', 
                '收货详细地址', 
                this.state.receiveDetailAddress, () => {}, 
                (event) => {this.setState({ receiveDetailAddress: event.nativeEvent.text })},
                true,
                ()=>{
                    if (this.state.receiveGatePoint == '收货门点'|| this.state.receiveGatePoint == '') {
                        this.refs.toast.show('请选择收货门点')
                        return
                    }
                    _this.refs.activityIndicatorPlus.show()
                    // !根据收获地址和详细地址获取经纬度信息，如果添了详细地址根据详细地址查询经纬度，否则按照收获门点进行查询
                    Geocode.search(this.state.receiveDetailAddress ? this.state.receiveGatePoint + ',' + this.state.receiveDetailAddress : this.state.receiveGatePoint )
                        .then((data)=>{
                            _this.refs.activityIndicatorPlus.dismiss()
                            NavigationUtil.goPage({
                                key:'getPoint',     // !进入详细地址地图界面的标示，判断是发货点还是收获点
                                searchAddress:this.state.receiveGatePoint,  //! 收货门点地址
                                address:this.state.receiveDetailAddress,    //! 详细地址
                                lat:data.latitude,                  //!纬度
                                lon:data.longitude},                //!经度
                                'BaiDuMapDetail')
                        })
                        .catch((e)=>{
                            _this.refs.activityIndicatorPlus.dismiss()
                            console.log(e);
                            this.refs.toast.show('未获取到相关地点,请检查详细地址是否填写正确',1000)
                        })
                }) 
            : null}
            {isR ? this.renderLine() : null}
            {BookSpaceCellsUtil.renderScreenWidthButton('#FFFBFB', '保存到常用联系人', () => {
                console.log('保存收货联系人到常用联系人按钮点击事件')
                if (this.stringIsEmpty(this.state.receivePersonName)) {
                    this.refs.toast.show('请填写收货人全称', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.receivePersonContact)) {
                    this.refs.toast.show('请填写收货人联系人', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.receivePersonPhone)) {
                    this.refs.toast.show('请填写收货人电话', 800)
                    return
                }
                let isY = NumberCheck.validatePhone(this.state.receivePersonPhone)
                if (isY) {
                } else {
                    this.refs.toast.show('电话格式不对', 800)
                    return
                }
                if (isR) {
                    if (this.state.receiveGatePoint == '收货门点'|| this.state.receiveGatePoint == '') {
                        this.refs.toast.show('请选择收货门点', 800)
                        return
                    }
                    if (this.state.receiveDetailAddress == '') {
                        this.refs.toast.show('请填写收货详细地址', 800)
                        return
                    }
                }
                this.saveCommonContact(false)
            })}
        </View>
    }


    //危险品详情
    renderDanderDetail() {
        return <View>
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFF5F5', 'IMDG', 120, '请输入', 'default', true, this.state.dangerIMDG, (text) => {

            }, (event) => {
                this.setState({ dangerIMDG: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFF5F5', '类别', 120, '请输入', 'default', true, this.state.dangerMode, (text) => {

            }, (event) => {
                this.setState({ dangerMode: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFF5F5', '联合国编号', 120, '请输入', true, 'default', this.state.dangerUNCode, (text) => {

            }, (event) => {
                this.setState({ dangerUNCode: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell(false, '#FFF5F5', '危险品备注', this.state.dangerMark, (text) => {

            }, (event) => {
                this.setState({ dangerMark: event.nativeEvent.text })
            })}
        </View>
    }

    //货物信息的cell
    renderGoodsInfoDetail() {

        let packingModeColor = this.state.packingMode == '请选择' ? '#999999' : '#354953'
        let goodsNameColor = this.state.goodsName == '请选择' ? '#999999' : '#354953'
        let boxModeColor = this.state.boxMode == '请选择' ? '#999999' : '#354953'
        let boxStateColor = this.state.boxState == '请选择' ? '#999999' : '#354953'
        // let is40RH = this.state.boxMode == '40RH' ? true : false
        // let is40RF = this.state.boxMode == '40RF' ? true : false
        let advanceBoxDateColor = this.state.advanceBoxDate == '请选择' ? '#999999' : '#354953'
        let is40RH = false
        let is40RF = false
        if (this.state.boxMode.indexOf('RH') != -1) {
            is40RH = true
        }
        if (this.state.boxMode.indexOf('RF') != -1) {
            is40RF = true
        }
        let is40RHRF = false
        if (is40RH || is40RF) {
            is40RHRF = true
        } else {
            is40RHRF = false
        }

        let isUploadSpecialRequire = false
        if ((this.state.transProvision.indexOf('DO-') != -1) && (!this.stringIsEmpty(this.state.boxMode))) {
            isUploadSpecialRequire = true
        } else {
            isUploadSpecialRequire = false
        }

        let isDownloadSpecialRequire = false
        if ((this.state.transProvision.indexOf('-DO') != -1) && (!this.stringIsEmpty(this.state.boxMode))) {
            isDownloadSpecialRequire = true
        } else {
            isDownloadSpecialRequire = false
        }

        return <View>
            {/* {BookSpaceCellsUtil.renderTextInputChooseCell('#FFFBFB', '货名', '请输入', this.state.goodsName, (text) => {

            }, (event) => {

            }, () => {
                this.props.navigation.navigate('BookSpaceGoodsName', {
                    backData: (data) => {
                        console.log('货名回调:', data)
                    }
                })
            })} */}
            {this.state.goodsNameEdit ? BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '货名', 120, this.state.goodsName, goodsNameColor, true, () => {
                if (this.state.goodsNameArr && this.state.goodsNameArr.length > 0) {

                } else {
                    return
                }
                this.props.navigation.navigate('BookSpaceGoodsInfo', {
                    backData: (data) => {
                        console.log('货名回调:', data)
                        if (data.goodsName.indexOf('空箱') != -1) {
                            this.isSingleBoxWeight = false
                            this.setState({ goodsName: data.goodsName, goodsMode: data.goodsType, boxState: '空箱' })
                        } else {
                            this.isSingleBoxWeight = true
                            this.setState({ goodsName: data.goodsName, goodsMode: data.goodsType })
                        }
                    }
                })
            }) : BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '货名', 120, this.state.goodsName, goodsNameColor, false, () => {
                //不可以选择货名
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '货类', 120, this.state.goodsMode, GlobalStyles.nav_bar_color, false, () => {
                //货类选择,没有选择
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '包装类型', 120, this.state.packingMode, packingModeColor, true, () => {
                //包装类型选择
                this.bottomModalSelect = 'packingMode'
                this.refs.bookSpaceBottomModal.show('name', this.state.packingModeArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '箱型', 120, this.state.boxMode, boxModeColor, true, () => {
                this.bottomModalSelect = 'boxMode'
                this.getBoxModeArr()//获取箱型
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '箱态', 120, this.state.boxState, boxStateColor, true, () => {
                this.bottomModalSelect = 'boxState'
                this.refs.bookSpaceBottomModal.show('name', this.state.boxStateArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '箱量', 120, '请输入', false, 'numeric', this.state.boxWeight, (text) => {

            }, (event) => {
                this.setState({ boxWeight: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '单箱重量(吨)', 120, '请输入', this.isSingleBoxWeight, 'numeric', this.state.singleBoxWeight, (text) => {

            }, (event) => {
                this.setState({ singleBoxWeight: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {isUploadSpecialRequire ? BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '装货特殊要求', 120, this.state.uploadSpecialRequire, '#354953', true, () => {
                this.bottomModalSelect = 'uploadSpecialRequire'
                this.refs.bookSpaceBottomModal.show('name', this.state.specialRequireArr)
            }) : BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '装货特殊要求', 120, '无', '#354953', false, () => {
            })}
            {this.renderLine()}
            {isDownloadSpecialRequire ? BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '卸货特殊要求', 120, this.state.downloadSpecialRequire, '#354953', true, () => {
                this.bottomModalSelect = 'downloadSpecialRequire'
                this.refs.bookSpaceBottomModal.show('name', this.state.specialRequireArr)
            }) : BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '卸货特殊要求', 120, '无', '#354953', false, () => {
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell(false, '#FFFBFB', '箱备注', this.state.boxMark, (text) => {

            }, (event) => {
                this.setState({ boxMark: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell(true, '#FFF5F5', '危险品', this.state.dangerGoods, (val) => {
                // this.setState({ dangerGoods: val })
                this.refs.toast.show('暂不支持危险品选项', 800)
            })}
            {this.state.dangerGoods ? this.renderDanderDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell(true, '#FFFBFB', '自备柜', this.state.selfBox, (val) => {
                this.setState({ selfBox: val })
            })}
            {is40RHRF&& this.isSingleBoxWeight ? this.renderLine() : null}
            {is40RHRF&& this.isSingleBoxWeight ? BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '设置温度(℃)', 120, '请输入', true, 'numeric', this.state.goodsTemperature, (text) => {

            }, (event) => {
                this.setState({ goodsTemperature: event.nativeEvent.text })
            }) : null}
            {is40RHRF && this.isSingleBoxWeight? this.renderLine() : null}
            {is40RHRF&& this.isSingleBoxWeight ? BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '风门开度', 120, '请输入', true, 'default', this.state.goodsWind, (text) => {

            }, (event) => {
                this.setState({ goodsWind: event.nativeEvent.text })
            }) : null}
            {is40RHRF&& this.isSingleBoxWeight ? this.renderLine() : null}
            {is40RHRF&& this.isSingleBoxWeight ? BookSpaceCellsUtil.renderTextChooseCell(true, '#FFFBFB', '预计提箱时间', this.state.advanceBoxDate, advanceBoxDateColor, () => {
                //预计提箱时间
                this.dataPickerSelect = 'advanceBoxDate'
                this.refs.datePicker.onPressDate()
            }) : null}
            {is40RHRF&& this.isSingleBoxWeight ? this.renderLine() : null}
            {is40RHRF&& this.isSingleBoxWeight ? BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '湿度设置(%)', 120, '请输入', true, 'numeric', this.state.goodsHumidity, (text) => {

            }, (event) => {
                this.setState({ goodsHumidity: event.nativeEvent.text })
            }) : null}
            {is40RHRF&& this.isSingleBoxWeight ? this.renderLine() : null}
            {is40RHRF&& this.isSingleBoxWeight ? this.goodsCoolingView() : null}
        </View>
    }

    //预冷要求单项框
    goodsCoolingView() {
        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'row', height: 50, backgroundColor: '#FFFBFB',
                    paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between'
                }}
                onPress={() => {
                    let a = this.state.goodsCooling
                    this.setState({ goodsCooling: !a })
                }}
                activeOpacity={1}
            >
                <Text style={{
                    height: 50, marginLeft: 9,
                    lineHeight: 50, fontSize: 17, width: 120,
                    color: '#666'
                }}>预冷要求</Text>
                <View style={styles.Ionicons}>
                    <Ionicons
                        name={this.state.goodsCooling ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                        size={24}
                        color={GlobalStyles.nav_bar_color} >
                    </Ionicons>
                </View>
            </TouchableOpacity>
        )
    }
    //箱信息的cell
    renderBoxInfoDetail() {

        return this.excreteArr.map((item) => {
            return (
                <View key={item.id} style={{ height: 50, paddingLeft: 15, paddingRight: 15, backgroundColor: '#FFFBFB' }}>
                    <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#354953' }}>{'箱号:' + item.xh + '    铅封号:' + item.qfh}</Text>
                    {this.renderLine()}
                </View>
            )
        })
    }

    //费用信息的cell
    renderCostDetail() {

        let premiumPayModeColor = this.state.premiumPayMode == '请选择' ? '#999999' : '#354953'
        let isD = false   //装港拖车服务费是否必填
        if ((this.state.transProvision.indexOf('DO-') != -1) || (this.state.transProvision.indexOf('CFS-') != -1)) {
            isD = true
        } else {
            isD = false
        }
        this.isUploadCarCost = isD
        let isR = false //卸港拖车服务费是否必填
        if ((this.state.transProvision.indexOf('-DO') != -1) || (this.state.transProvision.indexOf('-CFS') != -1)) {
            isR = true
        } else {
            isR = false
        }
        this.isDownloadCarCost = isR
        let isP = false //保险货值是否必填
        if ((this.state.premiumPayMode != '请选择') && (this.state.premiumPayMode != '无保险')) {
            isP = true
        } else {
            isP = false
        }
        this.isPremiumWorth = isP

        if (this.state.transProvision.indexOf('CFS-') != -1) {  //装港拖车服务费是否可编辑
            this.uploadCarCostEdit = true
        } else {
            this.uploadCarCostEdit = false
        }

        if (this.state.transProvision.indexOf('-CFS') != -1) {  //装港拖车服务费是否可编辑
            this.downloadCarCostEdit = true
        } else {
            this.downloadCarCostEdit = false
        }

        let uploadCarCostHolder = this.uploadCarCostEdit ? '请输入' : '不可编辑'
        let downloadCarCostHolder = this.downloadCarCostEdit ? '请输入' : '不可编辑'

        // let isUploadCarAdvanceCost = false
        if (this.state.transProvision.indexOf('DO-') != -1) { //装港拖车代垫费是否显示
            this.isUploadCarAdvanceCost = true
        } else {
            this.isUploadCarAdvanceCost = false
        }
        let isUploadCarAdvanceCostMark = (this.state.uploadCarAdvanceCost && this.state.uploadCarAdvanceCost.toString().length > 0 && this.state.uploadCarAdvanceCost != 0) ? true : false   //装港拖车代垫费是否必填

        // let isDownloadCarAdvanceCost = false
        if (this.state.transProvision.indexOf('-DO') != -1) { //卸港拖车服务费是否显示
            this.isDownloadCarAdvanceCost = true
        } else {
            this.isDownloadCarAdvanceCost = false
        }
        let isDownloadCarAdvanceCostMark = (this.state.downloadCarAdvanceCost && this.state.downloadCarAdvanceCost.toString().length > 0 && this.state.downloadCarAdvanceCost != 0) ? true : false //卸港拖车服务费是否是必填


        return <View>
            {BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '海运服务费', 150, '请输入', true, 'numeric', this.state.seaServeCost, (text) => {

            }, (event) => {
                this.setState({ seaServeCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(isD, '#FFFBFB', '装港拖车服务费', 150, uploadCarCostHolder, this.uploadCarCostEdit, 'numeric', this.state.uploadCarCost, (text) => {

            }, (event) => {
                this.setState({ uploadCarCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(isR, '#FFFBFB', '卸港拖车服务费', 150, downloadCarCostHolder, this.downloadCarCostEdit, 'numeric', this.state.downloadCarCost, (text) => {

            }, (event) => {
                this.setState({ downloadCarCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '装港铁路服务费', 150, '请输入', true, 'numeric', this.state.uploadRailCost, (text) => {

            }, (event) => {
                this.setState({ uploadRailCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '卸港铁路服务费', 150, '请输入', true, 'numeric', this.state.downloadRailCost, (text) => {

            }, (event) => {
                this.setState({ downloadRailCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {this.isUploadCarAdvanceCost ? BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '装港拖车代垫费', 150, '请输入', true, 'numeric', this.state.uploadCarAdvanceCost.toString(), (text) => {

            }, (event) => {
                this.setState({ uploadCarAdvanceCost: event.nativeEvent.text })
            }) : null}
            {this.isUploadCarAdvanceCost ? this.renderLine() : null}
            {isUploadCarAdvanceCostMark ? BookSpaceCellsUtil.renderPureTextInputCell(true, '#FFFBFB', '请输入装港拖车代垫费备注', this.state.uploadCarAdvanceCostMark, (text) => {

            }, (event) => {
                this.setState({ uploadCarAdvanceCostMark: event.nativeEvent.text })
            }) : null}
            {isUploadCarAdvanceCostMark ? this.renderLine() : null}
            {this.isDownloadCarAdvanceCost ? BookSpaceCellsUtil.renderMarginleftInputCell(true, '#FFFBFB', '卸港拖车代垫费', 150, '请输入', true, 'numeric', this.state.downloadCarAdvanceCost.toString(), (text) => {

            }, (event) => {
                this.setState({ downloadCarAdvanceCost: event.nativeEvent.text })
            }) : null}
            {this.isDownloadCarAdvanceCost ? this.renderLine() : null}
            {isDownloadCarAdvanceCostMark ? BookSpaceCellsUtil.renderPureTextInputCell(true, '#FFFBFB', '请输入卸港拖车代垫费备注', this.state.downloadcarAdvanceCostMark, (text) => {

            }, (event) => {
                this.setState({ downloadcarAdvanceCostMark: event.nativeEvent.text })
            }) : null}
            {isDownloadCarAdvanceCostMark ? this.renderLine() : null}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell(true, '#FFFBFB', '保险费支付方式', 150, this.state.premiumPayMode, premiumPayModeColor, true, () => {
                //保险费支付方式
                this.bottomModalSelect = 'premiumPayMode'
                this.refs.bookSpaceBottomModal.show('name', this.state.premiumPayModeArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell(isP, '#FFFBFB', '保险货值', 150, '请输入', true, 'numeric', this.state.premiumWorth, (text) => {
                this.setState({ premiumWorth: text })
            }, (event) => {
                let text = parseFloat(this.state.premiumWorth)
                text = Math.round(text * Math.pow(10, 4)) / Math.pow(10, 4); //四舍五入
                text = Number(text).toFixed(4); //补足位数
                if(isNaN(text)){

                }else{
                    if(text< 0){
                        this.refs.toast.show('请填写正确的保险货值', 800)
                    }else{
                        this.setState({ premiumWorth: text })
                    }
                }
            })}
            {BookSpaceCellsUtil.renderTotalCostCell(this.state.totalCost, () => {
                console.log('计费回调')
                if (this.stringIsEmpty(this.state.startPortName)) {
                    this.refs.toast.show('请选择起运港', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.endPortName)) {
                    this.refs.toast.show('请选择目的港', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.transProvision)) {
                    this.refs.toast.show('请选择运输条款', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.goodsName)) {
                    this.refs.toast.show('请选择货名', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.boxMode)) {
                    this.refs.toast.show('请选择箱型', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.boxState)) {
                    this.refs.toast.show('请选择箱态', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.premiumPayMode)) {
                    this.refs.toast.show('请选择保险费支付方式', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.seaServeCost)) {
                    this.refs.toast.show('请填写海运服务费', 800)
                    return
                }
                if (!(NumberCheck.validFloatNum(this.state.seaServeCost))) {
                    this.refs.toast.show('请填写正确的海运服务费', 800)
                    return
                }
                if (this.stringIsEmpty(this.state.boxWeight)) {
                    this.refs.toast.show('请填写箱量', 800)
                    return
                }
                if (this.isDeliverGatePoint) {
                    if (this.state.deliverGatePoint == '发货门点'|| this.state.deliverGatePoint == '') {
                        this.refs.toast.show('请选择发货门点', 800)
                        return
                    }else {
                        if (this.state.uploadCarCost && this.state.uploadCarCost.length > 0) {
                            if (!(NumberCheck.validFloatAndInt(this.state.uploadCarCost))) {
                                this.refs.toast.show('请选择正确的发货门点', 800)
                                return
                            }
                        } else {
                            this.refs.toast.show('请选择正确的发货门点', 800)
                            return
                        }
                    }
                }
                if (this.isReceiveGatePoint) {
                    if (this.state.receiveGatePoint == '收货门点'|| this.state.receiveGatePoint == '') {
                        this.refs.toast.show('请选择收货门点', 800)
                        return
                    }else {
                        if (this.state.downloadCarCost && this.state.downloadCarCost.length > 0) {
                            if (!(NumberCheck.validFloatAndInt(this.state.downloadCarCost))) {
                                this.refs.toast.show('请选择正确的收货门点', 800)
                                return
                            }
                        } else {
                            this.refs.toast.show('请选择正确的收货门点', 800)
                            return
                        }
                    }
                }
                if (this.isUploadCarCost) {
                    if (this.state.uploadCarCost && this.state.uploadCarCost.length > 0) {
                        if (!(NumberCheck.validFloatAndInt(this.state.uploadCarCost))) {
                            this.refs.toast.show('请填写正确的装港拖车服务费', 800)
                            return
                        }
                    } else {
                        this.refs.toast.show('请填写装港拖车服务费', 800)
                        return
                    }
                }
                if (this.isDownloadCarCost) {
                    if (this.state.downloadCarCost && this.state.downloadCarCost.length > 0) {
                        if (!(NumberCheck.validFloatAndInt(this.state.downloadCarCost))) {
                            this.refs.toast.show('请填写正确的卸港拖车服务费', 800)
                            return
                        }
                    } else {
                        this.refs.toast.show('请填写卸港拖车服务费', 800)
                        return
                    }
                }
                if (this.isUploadCarAdvanceCost) {
                    if (this.stringIsEmpty(this.state.uploadCarAdvanceCost)) {
                        this.refs.toast.show('请填写装港拖车代垫费', 800)
                        return
                    }
                    if (!(NumberCheck.validFloat(this.state.uploadCarAdvanceCost))) {
                        this.refs.toast.show('请填写正确的装港拖车代垫费', 800)
                        return
                    }
                }
                if (this.isDownloadCarAdvanceCost) {
                    if (this.stringIsEmpty(this.state.downloadCarAdvanceCost)) {
                        this.refs.toast.show('请填写卸港拖车代垫费', 800)
                        return
                    }
                    if (!(NumberCheck.validFloat(this.state.downloadCarAdvanceCost))) {
                        this.refs.toast.show('请填写正确的卸港拖车代垫费', 800)
                        return
                    }
                }
                if ((!this.stringIsEmpty(this.state.uploadCarAdvanceCost)) && (this.state.uploadCarAdvanceCost != 0)) {
                    if (this.stringIsEmpty(this.state.uploadCarAdvanceCostMark)) {
                        this.refs.toast.show('请填写装港拖车代垫费备注', 800)
                        return
                    }
                    if (this.state.uploadCarAdvanceCostMark && !this.state.uploadCarAdvanceCostMark.trim()) {
                        this.refs.toast.show('请填写正确的装港拖车代垫费备注', 800)
                        return
                    }
                }
                if ((!this.stringIsEmpty(this.state.downloadCarAdvanceCost)) && (this.state.downloadCarAdvanceCost != 0)) {
                    if (this.stringIsEmpty(this.state.downloadcarAdvanceCostMark)) {
                        this.refs.toast.show('请填写卸港拖车代垫费备注', 800)
                        return
                    }
                    if (this.state.downloadcarAdvanceCostMark && !this.state.downloadcarAdvanceCostMark.trim()) {
                        this.refs.toast.show('请填写正确的装港拖车代垫费备注', 800)
                        return
                    }
                }
                if (this.isPremiumWorth) {
                    if (this.state.premiumWorth && this.state.premiumWorth.length > 0) {
                        if (!(NumberCheck.validFloatAndInt(this.state.premiumWorth))) {
                            this.refs.toast.show('请填写正确的保险货值', 800)
                            return
                        }
                    } else {
                        this.refs.toast.show('请填写保险货值', 800)
                        return
                    }
                }
                let intP = /^[1-9]\d*$/
                if (!(intP.test(this.state.boxWeight))) {
                    this.refs.toast.show('请填写正确的箱量', 800)
                    return
                }
                if ((!(NumberCheck.validFloatAndInt(this.state.singleBoxWeight))) && !(this.state.goodsName.indexOf('空箱') != -1)) {
                    this.refs.toast.show('请填写正确的单箱重量', 800)
                    return
                }
                this.calcuFee()

            }, this.state.isShowDetailBtn, () => {
                console.log('明细回调')
                this.refs.costDetailModal.show(this.state.costDetailArr)
            })}
        </View>
    }

    //模拟flatlist
    renderFlatList() {
        return <KeyboardAwareScrollView>
            {BookSpaceCellsUtil.renderOutCell('基本信息', this.state.showBaseInfo, () => {
                console.log('基本信息cell点击事件')
                this.setState({ showBaseInfo: !this.state.showBaseInfo })
            })}
            {this.state.showBaseInfo ? this.renderBaseInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('委托信息', this.state.showEntrustInfo, () => {
                this.setState({ showEntrustInfo: !this.state.showEntrustInfo })
            })}
            {this.state.showEntrustInfo ? this.renderEnTrustDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('货物信息', this.state.showGoodsInfo, () => {
                this.setState({ showGoodsInfo: !this.state.showGoodsInfo })
            })}
            {this.state.showGoodsInfo ? this.renderGoodsInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('箱信息', this.state.showBoxInfo, () => {
                this.setState({ showBoxInfo: !this.state.showBoxInfo })
            })}
            {this.state.showBoxInfo ? this.renderBoxInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('费用信息', this.state.showCostInfo, () => {
                this.setState({ showCostInfo: !this.state.showCostInfo })
            })}
            {this.state.showCostInfo ? this.renderCostDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('声明条款', this.state.showDeclareProvision, () => {
                // this.setState({ showDeclareProvision: !this.state.showDeclareProvision })
                NavigationUtil.goPage({}, 'DeclareProvision')
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell(true, 'white', '拆单备注', this.state.excreteMark, (text) => {

            }, (event) => {
                this.setState({ excreteMark: event.nativeEvent.text })
            })}
        </KeyboardAwareScrollView>
    }

    renderCommitBtn() {

        return <View style={{ height: 150, width: GlobalStyles.screenWidth }}>
            <TouchableOpacity
                style={{ marginTop: 50, marginLeft: 15, width: GlobalStyles.screenWidth - 30, height: 50, borderRadius: 5, backgroundColor: GlobalStyles.nav_bar_color }}
                activeOpacity={0.6}
                onPress={() => {
                    if (this.stringIsEmpty(this.state.startPortName)) {
                        this.refs.toast.show('请选择起运港', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.endPortName)) {
                        this.refs.toast.show('请选择目的港', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.advanceLoadDate)) {
                        this.refs.toast.show('请选择预计装货日期', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.payMode)) {
                        this.refs.toast.show('请选择付款方式', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.transProvision)) {
                        this.refs.toast.show('请选择运输条款', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.collectTicket)) {
                        this.refs.toast.show('请选择受票方', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.delivePersonName)) {
                        this.refs.toast.show('请填写发货人全称', 800)
                        return
                    }
                    if (this.state.delivePersonName.length > 25) {
                        this.refs.toast.show('发货人全称过长', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.deliverPersonContact)) {
                        this.refs.toast.show('请填写发货人联系人', 800)
                        return
                    }
                    if (this.state.deliverPersonContact.length > 20) {
                        this.refs.toast.show('发货人联系人过长', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.deliverPersonPhone)) {
                        this.refs.toast.show('请填写发货人电话', 800)
                        return
                    }
                    if (NumberCheck.validatePhone(this.state.deliverPersonPhone)) {

                    } else {
                        this.refs.toast.show('发货人电话格式错误', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.receivePersonName)) {
                        this.refs.toast.show('请填写收货人全称', 800)
                        return
                    }
                    if (this.state.receivePersonName.length > 25) {
                        this.refs.toast.show('收货人全称过长', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.receivePersonContact)) {
                        this.refs.toast.show('请填写收货人联系人', 800)
                        return
                    }
                    if (this.state.receivePersonContact.length > 20) {
                        this.refs.toast.show('收货人联系人过长', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.receivePersonPhone)) {
                        this.refs.toast.show('请填写收货人电话', 800)
                        return
                    }
                    if (NumberCheck.validatePhone(this.state.receivePersonPhone)) {

                    } else {
                        this.refs.toast.show('收货人电话格式错误', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.goodsName)) {
                        this.refs.toast.show('请选择货名', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.packingMode)) {
                        this.refs.toast.show('请选择包装类型', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.boxMode)) {
                        this.refs.toast.show('请选择箱型', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.boxState)) {
                        this.refs.toast.show('请选择箱态', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.boxWeight)) {
                        this.refs.toast.show('请填写箱量', 800)
                        return
                    }
                    let intP = /^[1-9]\d*$/
                    if (!(intP.test(this.state.boxWeight))) {
                        this.refs.toast.show('请填写正确的箱量', 800)
                        return
                    }
                    if (parseFloat(this.state.boxWeight) <= 0) {
                        this.refs.toast.show('请填写正确的箱量', 800)
                        return
                    }
                    if (parseInt(this.state.boxWeight) > 999) {
                        this.refs.toast.show('箱量超过999不允许订舱', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.singleBoxWeight)) {
                        this.refs.toast.show('请填写单箱重量', 800)
                        return
                    }
                    if ((!(NumberCheck.validFloatAndInt(this.state.singleBoxWeight))) && !(this.state.goodsName.indexOf('空箱') != -1)) {
                        this.refs.toast.show('请填写正确的单箱重量', 800)
                        return
                    }
                    if (parseFloat(this.state.singleBoxWeight) > 26.2) {
                        this.refs.toast.show('大柜货重上限为26.2吨', 900)
                        return
                    }
                    if (this.stringIsEmpty(this.state.seaServeCost)) {
                        this.refs.toast.show('请填写海运服务费', 800)
                        return
                    }
                    if (!(NumberCheck.validFloatNum(this.state.seaServeCost))) {
                        this.refs.toast.show('请填写正确的海运服务费', 800)
                        return
                    }
                    if (this.stringIsEmpty(this.state.premiumPayMode)) {
                        this.refs.toast.show('请选择保险费支付方式', 800)
                        return
                    }
                    if (this.state.transProvision.indexOf('DO-') == -1) {
                        this.isDeliverGatePoint = false
                    }
                    if (this.state.transProvision.indexOf('-DO') == -1) {
                        this.isReceiveGatePoint = false
                    }

                    if (this.isDeliverGatePoint) {
                        if (this.state.deliverGatePoint == '发货门点'|| this.state.deliverGatePoint == '') {
                            this.refs.toast.show('请选择发货门点', 800)
                            return
                        }else {
                            if (this.state.uploadCarCost && this.state.uploadCarCost.length > 0) {
                                if (!(NumberCheck.validFloatAndInt(this.state.uploadCarCost))) {
                                    this.refs.toast.show('请选择正确的发货门点', 800)
                                    return
                                }
                            } else {
                                this.refs.toast.show('请选择正确的发货门点', 800)
                                return
                            }
                        }
                    }
                    if (this.isReceiveGatePoint) {
                        if (this.state.receiveGatePoint == '收货门点'|| this.state.receiveGatePoint == '') {
                            this.refs.toast.show('请选择收货门点', 800)
                            return
                        }else {
                            if (this.state.downloadCarCost && this.state.downloadCarCost.length > 0) {
                                if (!(NumberCheck.validFloatAndInt(this.state.downloadCarCost))) {
                                    this.refs.toast.show('请选择正确的收货门点', 800)
                                    return
                                }
                            } else {
                                this.refs.toast.show('请选择正确的收货门点', 800)
                                return
                            }
                        }
                    }
                    if (this.isUploadCarCost) {
                        if (this.state.uploadCarCost && this.state.uploadCarCost.length > 0) {
                            if (!(NumberCheck.validFloatAndInt(this.state.uploadCarCost))) {
                                this.refs.toast.show('请填写正确的装港拖车服务费', 800)
                                return
                            }
                        } else {
                            this.refs.toast.show('请填写装港拖车服务费', 800)
                            return
                        }
                    }
                    if (this.isUploadCarAdvanceCost) {
                        if (this.stringIsEmpty(this.state.uploadCarAdvanceCost)) {
                            this.refs.toast.show('请填写装港拖车代垫费', 800)
                            return
                        }
                        if (!(NumberCheck.validFloat(this.state.uploadCarAdvanceCost))) {
                            this.refs.toast.show('请填写正确的装港拖车代垫费', 800)
                            return
                        }
                    }
                    if (this.isDownloadCarAdvanceCost) {
                        if (this.stringIsEmpty(this.state.downloadCarAdvanceCost)) {
                            this.refs.toast.show('请填写卸港拖车代垫费', 800)
                            return
                        }
                        if (!(NumberCheck.validFloat(this.state.downloadCarAdvanceCost))) {
                            this.refs.toast.show('请填写正确的卸港拖车代垫费', 800)
                            return
                        }
                    }
                    if ((!this.stringIsEmpty(this.state.uploadCarAdvanceCost)) && (this.state.uploadCarAdvanceCost != 0)) {
                        if (this.stringIsEmpty(this.state.uploadCarAdvanceCostMark)) {
                            this.refs.toast.show('请填写装港拖车代垫费备注', 800)
                            return
                        }
                        if (this.state.uploadCarAdvanceCostMark && !this.state.uploadCarAdvanceCostMark.trim()) {
                            this.refs.toast.show('请填写正确的装港拖车代垫费备注', 800)
                            return
                        }
                    }
                    if ((!this.stringIsEmpty(this.state.downloadCarAdvanceCost)) && (this.state.downloadCarAdvanceCost != 0)) {
                        if (this.stringIsEmpty(this.state.downloadcarAdvanceCostMark)) {
                            this.refs.toast.show('请填写卸港拖车代垫费备注', 800)
                            return
                        }
                        if (this.state.downloadcarAdvanceCostMark && !this.state.downloadcarAdvanceCostMark.trim()) {
                            this.refs.toast.show('请填写正确的装港拖车代垫费备注', 800)
                            return
                        }
                    }
                    if (this.isDownloadCarCost) {
                        if (this.state.downloadCarCost && this.state.downloadCarCost.length > 0) {
                            if (!(NumberCheck.validFloatAndInt(this.state.downloadCarCost))) {
                                this.refs.toast.show('请填写正确的卸港拖车服务费', 800)
                                return
                            }
                        } else {
                            this.refs.toast.show('请填写卸港拖车服务费', 800)
                            return
                        }
                    }
                    if (this.isPremiumWorth) {
                        if (this.state.premiumWorth && this.state.premiumWorth.length > 0) {
                            if (!(NumberCheck.validFloatAndInt(this.state.premiumWorth))) {
                                this.refs.toast.show('请填写正确的保险货值', 800)
                                return
                            }
                        } else {
                            this.refs.toast.show('请填写保险货值', 800)
                            return
                        }
                    }
                    // if (this.state.boxMode == '40RH' || this.state.boxMode == '40RF') {
                        if(this.state.boxMode.indexOf('RF')!= -1 || this.state.boxMode.indexOf('RH') != -1 ){
                        if (this.stringIsEmpty(this.state.goodsTemperature)) {
                            this.refs.toast.show('请设置温度', 800)
                            return
                        }
                        let temperP = /^(\-|\+)?\d+(\.\d+)?$/
                        if (!(temperP.test(this.state.goodsTemperature))) {
                            this.refs.toast.show('请设置正确的温度', 800)
                            return
                        }
                        if ((parseFloat(this.state.goodsTemperature) < -30) || (parseFloat(this.state.goodsTemperature) > 20)) {
                            this.refs.toast.show('请设置正确的温度', 800)
                            return
                        }
                        if (this.stringIsEmpty(this.state.goodsWind)) {
                            this.refs.toast.show('请填写风门开度', 800)
                            return
                        }
                        if (this.stringIsEmpty(this.state.advanceBoxDate)) {
                            this.refs.toast.show('请选择预计提箱时间', 800)
                            return
                        }
                        if (this.stringIsEmpty(this.state.goodsHumidity)) {
                            this.refs.toast.show('请填写湿度', 800)
                            return
                        }
                        if (!(NumberCheck.validFloatAndInt(this.state.goodsHumidity))) {
                            this.refs.toast.show('请填写正确的湿度', 800)
                            return
                        }
                    }
                    if (this.state.transProvision.indexOf('DO-') != -1) {
                        if (this.state.deliverGatePoint == '发货门点'|| this.state.deliverGatePoint == '') {
                            this.refs.toast.show('请选择发货门点')
                            return
                        }
                        if (this.state.deliverDetailAddress == '') {
                            this.refs.toast.show('请填写发货详细地址')
                            return
                        }
                        if (this.state.uploadCarCost == '') {
                            this.refs.toast.show('请填写装港拖车服务费')
                            return
                        }
                    }
                    if (this.state.transProvision.indexOf('-DO') != -1) {
                        if (this.state.receiveGatePoint == '收货门点'|| this.state.receiveGatePoint == '') {
                            this.refs.toast.show('请选择收货门点')
                            return
                        }
                        if (this.state.receiveDetailAddress == '') {
                            this.refs.toast.show('请填写收货详细地址')
                            return
                        }
                        if (this.state.downloadCarCost == '') {
                            this.refs.toast.show('请填写卸港拖车服务费')
                            return
                        }
                    }
                    if (this.stringIsEmpty(this.state.excreteMark)) {
                        this.refs.toast.show('请填写拆单备注')
                        return
                    }

                    if (parseFloat(this.state.boxWeight) > 200) {
                        this.refs.AlertView.showAlert();
                    } else {
                        if (this.state.boxMode.indexOf('20') != -1) {
                            if (parseFloat(this.state.singleBoxWeight) > 27.7) {
                                this.refs.toast.show('小柜货重上限为27.7吨', 900)
                                // this.timer = setTimeout(() => {
                                //     this.commitBtnClick()
                                // }, 1200);
                                return
                            } else {
                                this.commitBtnClick()
                            }

                        } else if (this.state.boxMode.indexOf('40') != -1) {
                            if (parseFloat(this.state.singleBoxWeight) > 26.2) {
                                this.refs.toast.show('大柜货重上限为26.2吨', 900)
                                // this.timer = setTimeout(() => {
                                //     this.commitBtnClick()
                                // }, 1200);
                                return
                            } else {
                                this.commitBtnClick()
                            }

                        } else {
                            this.commitBtnClick()
                        }
                    }
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth - 30, height: 50, lineHeight: 50, color: 'white', textAlign: 'center', fontSize: 18 }}>{'提交拆单'}</Text>
            </TouchableOpacity>
        </View>


    }

    render() {
        let navigationBar = <NavigationBar
            title={'订舱信息'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;
        // let _this = this
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <ScrollView>
                    {this.renderFlatList()}
                    {this.renderCommitBtn()}
                </ScrollView>
                <BookSpaceBottomModal
                    ref="bookSpaceBottomModal"
                    callBack={data => {
                        console.log('bookSpaceBottomModal data :', data)
                        if (this.bottomModalSelect == 'collectTicket') {  //受票方
                            this.setState({ collectTicket: data.name })
                        } else if (this.bottomModalSelect == 'transProvision') {     //运输条款
                            if (data.name.indexOf('DO-') != -1) {
                                this.isUploadCarCost = true
                                this.isUploadCarAdvanceCost = true
                                this.isDeliverGatePoint = true
                            } else {
                                this.isUploadCarCost = false
                                this.isUploadCarAdvanceCost = false
                                this.isDeliverGatePoint = false
                                this.setState({ deliverGatePoint: '发货门点', deliverDetailAddress: '', uploadCarCost: '', uploadCarAdvanceCost: '0', uploadCarAdvanceCostMark: '' })
                            }
                            if (data.name.indexOf('-DO') != -1) {
                                this.isDownloadCarAdvanceCost = true
                                this.isDownloadCarCost = true
                                this.isReceiveGatePoint = true
                            } else {
                                this.isDownloadCarAdvanceCost = false
                                this.isDownloadCarCost = false
                                this.isReceiveGatePoint = false
                                this.setState({ receiveGatePoint: '收货门点', receiveDetailAddress: '', downloadCarCost: '', downloadCarAdvanceCost: '0', downloadcarAdvanceCostMark: '' })
                            }
                            if (data.name.indexOf('CFS-') != -1) {
                                this.isUploadCarCost = true
                                this.uploadCarCostEdit = true
                            } else {
                                this.isUploadCarCost = false
                                this.uploadCarCostEdit = false

                            }
                            if (data.name.indexOf('-CFS') != -1) {
                                this.isDownloadCarCost = true
                                this.downloadCarCostEdit = true
                            } else {
                                this.isDownloadCarCost = false
                                this.downloadCarCostEdit = false
                            }
                            this.setState({ transProvision: data.name })
                        } else if (this.bottomModalSelect == 'payMode') {            //付款方式
                            this.setState({ payMode: data.name })
                        } else if (this.bottomModalSelect == 'packingMode') {        //包装类型
                            this.setState({ packingMode: data.name })
                        } else if (this.bottomModalSelect == 'premiumPayMode') {     //保险费支付方式
                            this.setState({ premiumPayMode: data.name })
                        } else if (this.bottomModalSelect == 'boxMode') {            //箱型
                            let tempArr = []
                            let pre = this.state.premiumPayMode
                            if (data.name.indexOf('RF') != -1 || data.name.indexOf('RH') != -1) {
                                // if (data.name == '40RH' || data.name == '40RF') {
                                if (pre == '无保险') {
                                    pre = '请选择'
                                }
                                if (this.state.startPortName == '上海') {
                                    tempArr = [{ 'name': '含运费', 'code': 2 }]
                                    if (pre == '车队提箱时支付') {
                                        pre = '请选择'
                                    }
                                } else {
                                    tempArr = [{ 'name': '车队提箱时支付', 'code': 1 }, { 'name': '含运费', 'code': 2 }]
                                }
                            } else {
                                if (this.state.startPortName == '上海') {
                                    tempArr = [{ 'name': '无保险', 'code': 0 }, { 'name': '含运费', 'code': 2 }]
                                    if (pre == '车队提箱时支付') {
                                        pre = '请选择'
                                    }
                                } else {
                                    tempArr = [{ 'name': '无保险', 'code': 0 }, { 'name': '车队提箱时支付', 'code': 1 }, { 'name': '含运费', 'code': 2 }]
                                }
                            }

                            let tempSpecialArr = []
                            if (data.name == '20GP') {
                                tempSpecialArr = [{ 'name': '无', 'code': 0 }, { 'name': '短板车', 'code': 1 }, { 'name': '自卸车', 'code': 2 }]
                            } else if (data.name == '40RF' || data.name == '40RH') {
                                tempSpecialArr = [{ 'name': '无', 'code': 0 }, { 'name': '不打冷', 'code': 4 }]
                            } else {
                                tempSpecialArr = [{ 'name': '无', 'code': 0 }]
                            }

                            this.setState({ boxMode: data.name, premiumPayModeArr: tempArr, premiumPayMode: pre, specialRequireArr: tempSpecialArr, uploadSpecialRequire: '无', downloadSpecialRequire: '无' }, () => {
                                this.transProvisionToGetTruckFeeData()
                            })
                        } else if (this.bottomModalSelect == 'boxState') {            //箱态
                            if (data.name == '空箱') {
                                this.isSingleBoxWeight = false
                                this.setState({ boxState: data.name, goodsName: '商品空箱', goodsMode: '其他货类', goodsNameEdit: false, singleBoxWeight: '0' }, () => {
                                    this.transProvisionToGetTruckFeeData()
                                })
                            } else if (data.name == '重箱') {
                                this.isSingleBoxWeight = true
                                if (this.state.goodsName == '商品空箱') {
                                    this.setState({ boxState: data.name, goodsName: '请选择', goodsMode: '', goodsNameEdit: true, singleBoxWeight:'' }, () => {
                                        this.transProvisionToGetTruckFeeData()
                                    })
                                } else {
                                    this.setState({ boxState: data.name })
                                }
                            }

                        } else if (this.bottomModalSelect == 'uploadSpecialRequire') {            //装货特殊要求
                            this.setState({ uploadSpecialRequire: data.name }, () => {
                                this.transProvisionToGetTruckFeeData()
                            })

                        } else if (this.bottomModalSelect == 'downloadSpecialRequire') {            //卸货特殊要求
                            this.setState({ downloadSpecialRequire: data.name }, () => {
                                this.transProvisionToGetTruckFeeData()
                            })

                        }
                    }}
                />
                <DatePicker
                    ref="datePicker"
                    date={this.state.currentDate}
                    style={{ width: 1, height: 1, position: 'absolute' }}
                    mode="date"
                    placeholder=""
                    format="YYYY-MM-DD"
                    // minDate="2016-05-01"
                    // maxDate="2029-06-01"
                    confirmBtnText="确定"
                    cancelBtnText="取消"
                    customStyles={{
                        btnTextConfirm: {
                            color: GlobalStyles.nav_bar_color
                        }
                    }}
                    allowFontScaling={false}
                    hideText={true}
                    showIcon={false}
                    onDateChange={(date) => {
                        let isY = DateUril.compareDate(this.state.currentDate, date)
                        if (isY) {
                            if (this.dataPickerSelect == 'advanceLoadDate') {
                                this.setState({ advanceLoadDate: date })
                            } else if (this.dataPickerSelect == 'advanceBoxDate') {
                                this.setState({ advanceBoxDate: date })
                            }
                        } else {
                            this.refs.toast.show('日期不能小于当前日期')
                        }
                    }}
                />
                <CostDetailModal
                    ref="costDetailModal"
                />
                <ActivityIndicatorPlus
                    ref={(r)=>_this.refs.activityIndicatorPlus = r}
                />
                <Toast ref="toast" position="center" />
                <AlertView
                    ref="AlertView"
                    TitleText="箱量大于200,确定提交?"
                    // DesText=" "
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDown}
                    alertCancelDown={this.alertCancelDown}
                />
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    Ionicons: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
});