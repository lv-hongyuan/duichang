/**
 * 拆单申请
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import _ from 'lodash'

export default class ExcreteOrderApply extends Component {

    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
        let tempArr = this.params.contDTOList
        tempArr.forEach(element => {
            element.isS = '0'
        });
        this.state = {
            dataArr: tempArr,
            indicatorAnimating: false, //菊花
        }
    }

    componentDidMount() {
        publicDao.BoxStateView_NavigationKey = this.props.navigation.state.key
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    renderItem(data) {
        let item = data.item
        // console.log('item is ', item)
        return <View style={{ flexDirection: 'row', height: 70, justifyContent: 'space-between', backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}>
            <View style={{ height: 70 }}>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <Text style={[styles.cellTitle, { color: '#778087' }]}>{'箱号'}</Text>
                    <Text style={[styles.cellTitle, { marginLeft: 15, color: '#0877DE' }]}>{item.xh}</Text>
                </View>
                <View style={{ flexDirection: 'row', }}>
                    <Text style={[styles.cellTitle, { color: '#778087' }]}>{'封号'}</Text>
                    <Text style={[styles.cellTitle, { marginLeft: 15, color: 'black' }]}>{item.qfh}</Text>
                </View>
            </View>
            <View style={{ height: 70, flexDirection: 'row' }}>
                <Text style={{ height: 70, lineHeight: 70, fontSize: 16, color: 'black' }}>{item.xxxl}</Text>
                <TouchableOpacity
                    style={{ marginLeft: 10, marginTop: 25, width: 20, height: 20 }}
                    onPress={() => {
                        //是否选中
                        let index = data.index
                        let tempArr = this.state.dataArr.concat()
                        let tempDic = tempArr[index]
                        if (tempDic.isS == '0') {
                            tempDic.isS = '1'
                        } else {
                            tempDic.isS = '0'
                        }
                        this.setState({ dataArr: tempArr })
                    }}
                >
                    {item.isS == '0' ? <Image style={{ width: 20, height: 20 }} source={require('../../../resource/checkbox_u.png')} /> : <Image style={{ width: 20, height: 20 }} source={require('../../../resource/checkbox.png')} />}
                </TouchableOpacity>
            </View>
        </View>
    }

    renderBottomBtn() {
        return <View style={{height:140,backgroundColor:GlobalStyles.backgroundColor}}>
            <TouchableOpacity
            style={styles.bottomBackView}
            onPress={() => {
                //提交按钮点击事件
                // console.log('--',this.params)
                let tempArr = this.params.contDTOList
                let tempArr2 = _.filter(tempArr, ['isS', '1'])
                if (tempArr2 && tempArr2.length > 0 && tempArr2.length < tempArr.length) {
                    NavigationUtil.goPage(this.params, 'ExcreteBookSpaceInfo')
                } else if (tempArr2.length == tempArr.length){
                    this.refs.toast.show('不能全选拆箱')
                }else {
                    this.refs.toast.show('没有选中要拆的箱子')
                }

            }}
        >
            <Text style={{ width: GlobalStyles.screenWidth - 30, height: 40, color: 'white',fontWeight:'600', fontSize: 17, lineHeight: 40, textAlign: 'center' }}>{'提  交'}</Text>
        </TouchableOpacity>
        </View>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'拆单申请'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast" position="center" />
                <FlatList
                style={styles.flatlistStyle}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => this.separatorView()}
                data={this.state.dataArr}
                renderItem={data => this.renderItem(data)}
                ListFooterComponent={()=>this.renderBottomBtn()}
                />
                {/* {this.renderBottomBtn()} */}
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    cellTitle: {
        height: 30,
        lineHeight: 30,
        fontSize: 15
    },
    flatlistStyle: {
        marginTop: 10,
        backgroundColor: GlobalStyles.backgroundColor,
        flex:1,
        //height: GlobalStyles.screenHeight - 230,
        //flexGrow: 0                   //flalist固定高度必须加这个
    },
    bottomBackView: {
        // position: 'absolute',
        // bottom: 50,
        // right: 15,
        width: GlobalStyles.screenWidth - 30,
        height: 40,
        backgroundColor: GlobalStyles.nav_bar_color,
        borderRadius: 5,
        marginLeft: 15,
        marginTop: 50,
    }
});