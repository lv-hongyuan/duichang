import React, { Component } from 'react';
import { DeviceInfo,Clipboard, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, DeviceEventEmitter, Linking } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import ViewUtil from '../../common/ViewUtil'
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus';
import publicDao from '../../dao/publicDao';
import NetworkDao from '../../dao/NetworkDao';
import Toast from 'react-native-easy-toast';
import { LargeList } from "react-native-largelist-v3";
import CostBillSearchClass from './CostBillSearchClass';
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
import { EMITTER_CONTACT_SEARCH_TYPE } from '../../common/EmitterTypes';
import BookSpaceBottomModal from '../BookSpaceUtils/BookSpaceBottomModal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AlertView from '../../common/AlertView'

const isBottomInset = DeviceInfo.isIPhoneX_deprecated ? true : false;

export default class ContactOfficeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataArr: [],
      largeListData: [],           //大列表数据
      isNoData: false,
      refreshing: false,
      hideLoadingMore: true,     //是否显示上拉
      indicatorAnimating: false, //菊花
      getAreaListArr: [],      //片区列表
      areaId: '1',             //片区
      blockName: '总部',
      telnum:''
    }
  }

  componentDidMount() {
    this.getAreaList()
    this.getData(this.state.areaId)
  }


  //获取片区列表数据
  getAreaList() {
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getAreaList.json', paramsStr)
      .then(data => {
        if (data._backCode == '200') {
          this.setState({ getAreaListArr: data.entityList })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          })
        } else if (data._backCode == '404') {
          this.setState({ getAreaListArr: [] })
        } else {
          this.refs.toast.show('获取数据失败', 500)
        }
      })
      .catch(error => {
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  getData(areaId) {
    this.setState({ indicatorAnimating: true, refreshing: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.areaId = areaId
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getInfoAddressList.json', paramsStr)
      .then(data => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false })
        if (data._backCode == '200') {
          let tempA = this.handleLargeListData(data.entityList)
          this.setState({ largeListData: tempA, dataArr: data.entityList, isNoData: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.setState({ dataArr: [], isNoData: true, largeListData: [] })
        }
      })
      .catch(error => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false, isNoData: false })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  //左侧返回按钮
  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  //右侧检索按钮
  rightButtonCLick() {
    this.refs.bookSpaceBottomModal.show('areaName', this.state.getAreaListArr)
  }

  alertSureDown = () => {
    let tel = 'tel:'+this.state.telnum
    console.log(tel);
    Linking.canOpenURL(tel).then((supported) => {
      if (!supported) {
        console.log('Can not handle tel:' + tel)
      } else {
        return Linking.openURL(tel)
      }
    })
    .catch(error => console.log('tel error', error))
  }

  alertCancelDown = () => {
    console.log('点击了alert取消按钮')
  }

  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]

    return <View>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View style={styles.cardView}>
        <View style={{ width: '18%' }}>
          <Text style={styles.text}>办事处:</Text>
          <Text style={styles.text}>电   话:</Text>
          <Text style={styles.text}>传   真:</Text>
          <Text style={styles.text}>邮   箱:</Text>
          <Text style={styles.text}>地   点:</Text>
        </View>

        <View style={{ width: '82%', paddingRight: 5 }}>
          <Text style={styles.text} numberOfLines={1}>{item.officeName}</Text>
          <Text selectable={true} style={styles.text} onPress={() => {
            this.setState({telnum:item.phone})
            this.refs.AlertView.showAlert()
          }
          }>{item.phone}</Text>
          <Text selectable={true} style={styles.text}>{item.fax}</Text>
          <Text selectable={true} style={styles.text} numberOfLines={1}>{item.mail}</Text>
          <Text selectable={true} style={styles.text} numberOfLines={1}>{item.address}</Text>
        </View>
      </View>
    </View>
  }

  RightTextButton(text) {
    return <TouchableOpacity
      onPress={() => {
        this.rightButtonCLick()
      }}
      style={{
        flexDirection: 'row',
        marginRight: 5,
      }}
    >
      <Text style={{ fontSize: 15, color: '#fff', marginRight: 2 }}>{text}</Text>
      <AntDesign
        name={'retweet'}
        size={15}
        style={{ color: 'white' }} />
    </TouchableOpacity>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'联系我们'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      rightButton={this.RightTextButton(this.state.blockName)}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <LargeList
          ref={ref => (this._largelist = ref)}
          style={{ 
            height: '100%', 
            paddingHorizontal: 10 ,
            paddingBottom:isBottomInset ? 35 : 0
          }}
          data={this.state.largeListData}
          heightForIndexPath={() => 185}
          renderIndexPath={this.renderItem}
          refreshHeader={ChineseWithLastDateHeader}
          onRefresh={() => {
            this.getData(this.state.areaId)
          }}
        />
        <BookSpaceBottomModal
          ref="bookSpaceBottomModal"
          callBack={data => {
            this.setState({
              blockName: data.areaName,
              areaId: data.areaId
            })
            this.getData(data.areaId)
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
          }}
        />
        <AlertView
          ref="AlertView"
          TitleText="是否拨打此电话?"
          CancelText='取消'
          OkText='确定'
          BottomRightFontColor='#BA1B25'
          alertSureDown={this.alertSureDown}
          alertCancelDown={this.alertCancelDown}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}

      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cardView: {
    borderRadius: 10,
    padding: 10,
    paddingRight: 5,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'space-between'
  },
  text: {
    fontSize: 14,
    color: '#555',
    height: 35,
  }
});