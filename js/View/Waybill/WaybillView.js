/**
 * 运单
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import { createMaterialTopTabNavigator, createAppContainer } from "react-navigation";
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view'
import TransBillView from './TransBillView'
import CostBillView from './CostBillView'
import publicDao from '../../dao/publicDao'


export default class WayBillView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabIndex: 0,
      resume: false
    }
  }
  componentWillMount() {

    this.listenner = this.props.navigation.addListener(
      'didFocus',
      (obj) => {
        this.setState({ resume: true })
      })

    this.listenner = this.props.navigation.addListener(
      'didBlur',
      (obj) => {
        this.setState({ resume: false })
      })
  }
  renderTextInput(placeHolder, callBack) {
    return <View style={{ marginLeft: 10, width: (GlobalStyles.screenWidth - 70) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
      {/* <TextInput
        style={{marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: (GlobalStyles.screenWidth - 90) / 2 }}
        fontSize={16}
        placeholder={placeHolder}
        onChangeText={(text) => {
          callBack(text)
        }}
      /> */}
      <TouchableOpacity onPress={() => {
        callBack()
      }}>
        <Text style={{ marginLeft: 10, height: 35, lineHeight: 35, width: (GlobalStyles.screenWidth - 90) / 2, fontSize: 16, color: '#B2B6BF' }}>{placeHolder}</Text>
      </TouchableOpacity>
    </View>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'运单'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
    />;
    let { resume, tabIndex } = this.state
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color} bottomInset={false}>
        {navigationBar}
        <ScrollableTabView
          initialPage={0}
          renderTabBar={() => <ScrollableTabBar
            underlineStyle={{ height: 1.5, backgroundColor: GlobalStyles.nav_bar_color }}
            activeTextColor={GlobalStyles.nav_bar_color}
            inactiveTextColor={'#6D7278'}
            textStyle={{ fontSize: 14, width: GlobalStyles.screenWidth / 2 - 40, textAlign: 'center' }}
            tabStyle={{ height: 35 }}
            style={{ height: 35 }}
          />
          }
          onChangeTab={(i) => this.setState({ tabIndex: i.i })}
        >
          <TransBillView tabLabel="我的运单" resumed={resume} tabIndex={tabIndex} navigation={this.props.navigation} />
          <CostBillView tabLabel="我的订单" resumed={resume} tabIndex={tabIndex} navigation={this.props.navigation} />
        </ScrollableTabView>
      </SafeAreaViewPlus >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: GlobalStyles.backgroundColor,
  }
});