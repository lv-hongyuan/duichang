/**
 * 我的订单
 */
import React, { Component } from 'react';
import { NetInfo, Clipboard, Platform, StyleSheet, Text, View, Dimensions, FlatList, RefreshControl, Image, TouchableOpacity, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import WaybillTopSearchView from './WaybillTopSearchView'
import DashSecondLine from '../../common/DashSecondLine'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_COSTBILL_SEARCH_TYPE, EMITTER_CREATECOSTBILLSUCCESS_TYPE, EMITTER_NETCHANGE_TYPE } from '../../common/EmitterTypes'
import CostBillSearchClass from './CostBillSearchClass';
import LargeListPlus from '../../common/LargeListPlus';
import ActivityIndicatorPlus from '../../common/ActivityIndicatorPlus';
import AlertViewPlus from '../../common/AlertViewPlus'


export default class CostBillView extends Component {
  constructor(props) {
    super(props)
    this.isNetworkFirst = false
    this.startRow = 0
    this.state = {
      dataArr: [],
      searchText: '',         //搜索运单号
      endRefresh: '',       //停止上拉/下拉刷新
      toTop: false,            //置顶事件
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tabIndex == 1 && nextProps.resumed) {
      if (this.state.dataArr && this.state.dataArr.length > 0) {
        this.setState({ toTop: true })
        setTimeout(() => { this.setState({ endRefresh: '', toTop: false }) }, 10)
      }
      if (this.state.searchText != '') {
        this.dealData();
      }
    }
  }


  //一键复制按钮
  copySubmitFunction(text) {
    Clipboard.setString(text);
    this.refs.toast.show('订舱流水号复制成功', 500)
  }

  getData(dic, showLoading = true) {
    showLoading && this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.zt = dic.zt
    params.ddh = dic.ddh
    params.shrqc = dic.shrqc
    params.cm = dic.cm
    params.hc = dic.hc
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('listOrder.json', paramsStr)
      .then(data => {
        !showLoading && this.setState({ endRefresh: 'Refresh', toTop: true })
        this.refs.activityIndicatorPlus.dismiss()
        if (data._backcode == '200') {
          this.setState({ dataArr: data.list, endRefresh: '', toTop: false })
        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backcode == '404') {
          this.setState({ dataArr: [] })
        }
      })
      .catch(error => {
        !showLoading && this.setState({ endRefresh: 'Refresh', toTop: false })
        this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  loadMoreData(dic) {
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.zt = dic.zt
    params.ddh = dic.ddh
    params.shrqc = dic.shrqc
    params.cm = dic.cm
    params.hc = dic.hc
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = ++this.startRow * 30
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('listOrder.json', paramsStr)
      .then(data => {
        this.setState({ endRefresh: 'Loading' })
        if (data._backcode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.list && data.list.length > 0) {
            tempArr.push(...data.list)
            this.setState({ dataArr: tempArr })
          } else {
            this.refs.toast.show('没有更多数据了', 500)
          }

        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backcode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this.setState({ endRefresh: 'Loading', toTop: false })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  orderDetail(id, isGoDetail) {
    this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.id = id
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getOneOrder.json', paramsStr)
      .then(data => {
        this.refs.activityIndicatorPlus.dismiss()
        if (data._backcode == '200') {
          data.orderId = id
          data.isTransBill = false
          data.isTemp = false
          if (isGoDetail) {
            NavigationUtil.goPage(data, 'WaybillDetailVeiw')
          } else {
            this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
            // NavigationUtil.goPage(data, 'CreateOrderAgain')
          }

        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取详情失败', 500)
      })
  }

  dealData() {
    console.log("刷新刷新刷新   ")
    this.setState({ searchText: '' })
    this.startRow = 0
    CostBillSearchClass.zt = ''
    CostBillSearchClass.ddh = ''
    CostBillSearchClass.shrqc = ''
    CostBillSearchClass.cm = ''
    CostBillSearchClass.hc = ''
    CostBillSearchClass.sfg = ''
    CostBillSearchClass.mdg = ''
    CostBillSearchClass.startDate = ''
    CostBillSearchClass.endDate = ''
    this.getData(CostBillSearchClass, false)
  }

  componentDidMount() {
    this.getData(CostBillSearchClass)
    this.searchListener = DeviceEventEmitter.addListener(EMITTER_COSTBILL_SEARCH_TYPE, (para) => {
      this.setState({ searchText: '' })
      this.startRow = 0
      para.ddh = ''
      this.getData(para)
    });

    this.createOrderAgainListener = DeviceEventEmitter.addListener(EMITTER_CREATECOSTBILLSUCCESS_TYPE, () => {
      this.dealData()
    })

    // if (this.props.tabIndex == 1 && this.props.resumed) {
    //   if (this.state.searchText != '') {
    //     this.dealData();
    //   }
    // }

    DeviceEventEmitter.addListener(EMITTER_NETCHANGE_TYPE, () => {
      console.log('订单联网成功')
      this.isNetworkFirst = true
    })

    this.listenerNetwork = this.props.navigation.addListener(
      'didFocus',
      (obj) => {
        if (this.isNetworkFirst) {
          this.getData(CostBillSearchClass)
          this.isNetworkFirst = false
        }
      })
  }

  componentWillUnmount() {
    if (this.searchListener) {
      this.searchListener.remove();
    }
    if (this.createOrderAgainListener) {
      this.createOrderAgainListener.remove();
    }
  }

  //再来一单按钮
  renderBottomBtn(title, callBack) {
    return <TouchableOpacity
      style={{
        marginLeft: 5, width: 70, height: 25, borderRadius: 3,
        borderColor: GlobalStyles.nav_bar_color, borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}
      onPress={() => {
        callBack()
      }}
    >
      <Text style={{ fontSize: 13, color: GlobalStyles.nav_bar_color }}>{title}</Text>
    </TouchableOpacity>
  }

  _renderItem = ({ row: row }) => {
    // let item = data.item
    let item = this.state.dataArr[row]
    let billState = item.zt
    let billName = ''
    let billColor = ''
    if (billState == 0) {
      billName = '草稿'
      billColor = '#0877DE'
    } else if (billState == 1) {
      billName = '已提交/待受理'
      billColor = '#EBB259'
    } else if (billState == 2) {
      billName = '已驳回'
      billColor = '#FF4500'
    } else if (billState == 3) {
      billName = '已生成运单'
      billColor = '#76AE17'
    } else if (billState == 4) {
      billName = '接口提交'
      billColor = '#808000'
    }
    return <TouchableOpacity activeOpacity={1} onPress={() => {
      this.orderDetail(item.id, true)
    }} >
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View style={styles.cellBackView}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ marginTop: 15, fontSize: 13, height: 15, lineHeight: 15, color: '#778087' }}>{'订舱流水号:' + item.ddh}</Text>
            <TouchableOpacity
              style={{
                height: 20, width: 40, backgroundColor: GlobalStyles.nav_bar_color,
                marginTop: 13, justifyContent: 'center', alignItems: 'center',
                borderRadius: 2, marginLeft: 10
              }}
              onPress={() => {
                this.copySubmitFunction(item.ddh)
              }}
            >
              <Text style={{ color: '#fff', fontSize: 13, }}>复制</Text>
            </TouchableOpacity>
          </View>
          <Text style={{ marginTop: 15, fontSize: 13, height: 15, lineHeight: 15, color: billColor }}>{billName}</Text>
        </View>
        <Image style={{ width: GlobalStyles.screenWidth - 30, height: 2, marginLeft: -15, marginTop: 10 }} source={require('../../../resource/xu2.png')} />
        <View style={{ marginTop: 10, height: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 13, color: 'black' }}>{item.hm + ' | ' + item.ystk}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 13, color: '#778087' }}>{'预计装货:'}</Text>
            <Text style={{ fontSize: 13 }}>{item.bookYjzhsj}</Text>
          </View>
        </View>
        <View style={{ marginTop: 10, height: 30, flexDirection: 'row' }}>
          <Image style={{ marginTop: 3, height: 15, width: 24 }}
            source={require('../../../resource/ship.png')}
          />
          <Text style={{ marginLeft: 15, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.sfg}</Text>
          <Image style={{ marginTop: 9, width: 60, height: 5, marginLeft: 20 }} source={require('../../../resource/direction.png')} />
          <Text style={{ marginLeft: 20, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.mdg}</Text>
        </View>
        <View style={{ width: GlobalStyles.screenWidth, height: 0.5, marginTop: 10, marginLeft: -15, backgroundColor: GlobalStyles.backgroundColor }}></View>
        <View style={{ marginTop: 5, height: 25, flexDirection: 'row', justifyContent: 'flex-end' }}>
          {this.renderBottomBtn('再来一单', () => {
            // NavigationUtil.goPage(item, 'CreateOrderAgain')
            this.orderDetail(item.id, false)
          })}
        </View>
        <View style={{ width: GlobalStyles.screenWidth - 30, height: 10, backgroundColor: 'white', marginLeft: -15 }}></View>
      </View>
    </TouchableOpacity>
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          {WaybillTopSearchView.renderSearchView('订舱流水号', this.state.searchText, (text) => {
            // console.log('运单搜索 ', text)
            this.setState({ searchText: text })
          }, (event) => {
            // console.log('键盘搜索按钮点击')
            CostBillSearchClass.ddh = event.nativeEvent.text
            this.getData(CostBillSearchClass)
          }, () => {
            console.log('更多按钮点击事件')
            NavigationUtil.goPage({}, 'CostBillMoreSearchView')
          }, () => {
            CostBillSearchClass.ddh = this.state.searchText
            this.getData(CostBillSearchClass)
          })}
          <LargeListPlus
            toTop={this.state.toTop}
            endRefresh={this.state.endRefresh}
            data={this.state.dataArr}
            onRefresh={() => { this.dealData() }}
            renderItem={this._renderItem}
            NoDataViewheight={200}
            onLoading={() => { this.loadMoreData(CostBillSearchClass) }}
            itemHeight={172.5}
          />
        </View>
        <AlertViewPlus
          ref="AlertView"
          ShowCancelButton={false}
          title='温馨提示'
          message='由于客户端升级，暂时关闭订舱功能。'
          alertSureDown={()=>{}}
        />
        <ActivityIndicatorPlus
          ref={(r) => this.refs.activityIndicatorPlus = r}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />

      </View>
    )

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cellBackView: {
    backgroundColor: 'white',
    borderRadius: 3,
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft: 15,
    width: GlobalStyles.screenWidth - 30,
  }
});