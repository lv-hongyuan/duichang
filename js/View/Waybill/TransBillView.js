/**
 * 我的运单
 */
import React, { Component } from 'react';
import { NetInfo, Clipboard, Platform, StyleSheet, Text, View, Dimensions, FlatList, RefreshControl, Image, TouchableOpacity, ActivityIndicator, DeviceEventEmitter, Keyboard } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import WaybillTopSearchView from './WaybillTopSearchView'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_TRANSBILL_SEARCH_TYPE } from '../../common/EmitterTypes'
import TransBillSearchClass from './TransBillSearchClass'
import { EMITTER_CHANGEORDERAPPLAYSUCCESS_TYPE, EMITTER_EXCRETEORDERSUCESS_TYPE, EMITTER_CREATETRANSBILLSUCESS_TYPE, EMITTER_NETCHANGE_TYPE } from '../../common/EmitterTypes';
import LargeListPlus from '../../common/LargeListPlus'
import ActivityIndicatorPlus from "../../common/ActivityIndicatorPlus";
import AlertViewPlus from '../../common/AlertViewPlus'

export default class TransBillView extends Component {
  constructor(props) {
    super(props)
    this.isNetworkFirst = false
    this.startRow = 0
    this.state = {
      dataArr: [],
      searchText: '',         //搜索运单号
      endRefresh: '',       //停止上拉/下拉刷新
      toTop: false,            //置顶事件
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tabIndex == 0 && nextProps.resumed) {
      if (this.state.dataArr && this.state.dataArr.length > 0) {
        this.setState({ toTop: true })
        setTimeout(() => { this.setState({ endRefresh: '', toTop: false }) }, 10)
      }
    }
  }

  getData(dic, showLoading = true) {
    showLoading && this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getBookingWaybillList.json', paramsStr)
      .then(data => {
        !showLoading && this.setState({ endRefresh: 'Refresh', toTop: true })
        this.refs.activityIndicatorPlus.dismiss()
        if (data._backCode == '200') {
          this.setState({ dataArr: data.entityList, endRefresh: '', toTop: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.setState({ dataArr: [] })
        }
      })
      .catch(error => {
        !showLoading && this.setState({ endRefresh: 'Refresh', toTop: false })
        this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取数据失败', 800)
      })
  }

  //上拉加载更多数据
  loadMoreData(dic) {
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = ++this.startRow * 30
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getBookingWaybillList.json', paramsStr)
      .then(data => {
        this.setState({ endRefresh: 'Loading' })
        if (data._backCode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.entityList && data.entityList.length > 0) {
            tempArr.push(...data.entityList)
            this.setState({ dataArr: tempArr })
          } else {
            this.refs.toast.show('没有更多数据了', 500)
          }
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        } else if (data._backCode == '007') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this.setState({ endRefresh: 'Loading', toTop: false })
        this.refs.toast.show('获取数据失败', 800)
      })
  }

  loadMoreData2(dic) {
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getBookingWaybillList.json', paramsStr)
      .then(data => {
        this.setState({ endRefresh: 'Loading' })
        if (data._backCode == '200') {
          this.setState({ dataArr: data.entityList })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        } else if (data._backCode == '007') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this.setState({ endRefresh: 'Loading', toTop: false })
        this.refs.toast.show('获取数据失败', 800)
      })
  }

  //改单申请和拆单申请
  changeOrderBtnClick(sqlx, id) {
    this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.id = id
    params.sqlx = sqlx
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('updateOrSplitWaybillApply.json', paramsStr)
      .then(data => {
        this.refs.activityIndicatorPlus.dismiss()
        if (data._backcode == '200') {
          data.orderId = id
          if (sqlx == 0) {
            NavigationUtil.goPage(data, 'ChangeOrderApply')
          } else {
            NavigationUtil.goPage(data, 'ExcreteOrderApply')
          }
        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backcode == '404') {
          this.refs.toast.show('数据异常,请联系中谷业务人员', 500)
        } else if (data._backcode == '400') {
          this.refs.toast.show('已有申请正在进行中', 500)

        } else if (data._backcode == '804') {
          this.refs.toast.show('箱量小于两箱不可拆单', 500)
        } else if (data._backcode == '401') {
          this.refs.toast.show('服务器异常,请稍后重试', 500)
        }
      })
      .catch(error => {
        this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  orderDetail(id, isGoDetail) {
    this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.id = id
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getOneWaybill.json', paramsStr)
      .then(data => {
        this.refs.activityIndicatorPlus.dismiss()
        if (data._backcode == '200') {
          data.orderId = id
          data.isTransBill = true
          data.isTemp = false
          if (isGoDetail) {
            NavigationUtil.goPage(data, 'WaybillDetailVeiw')
          } else {
            this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
            // NavigationUtil.goPage(data, 'CreateOrderAgain')
          }

        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取详情失败', 500)
      })
  }

  dealData() {
    console.log("刷新刷新刷新   ")
    this.setState({ searchText: '' })
    this.startRow = 0
    TransBillSearchClass.dcdh = ''
    TransBillSearchClass.shr = ''
    TransBillSearchClass.zwcm = ''
    TransBillSearchClass.hc = ''
    TransBillSearchClass.hm = ''
    TransBillSearchClass.sfg = ''
    TransBillSearchClass.mdg = ''
    TransBillSearchClass.startDate = ''
    TransBillSearchClass.endDate = ''
    this.getData(TransBillSearchClass, false)
  }
  componentDidMount() {
    this.getData(TransBillSearchClass)
    this.searchListener = DeviceEventEmitter.addListener(EMITTER_TRANSBILL_SEARCH_TYPE, (para) => {
      this.setState({ searchText: '' })
      this.startRow = 0
      para.dcdh = ''
      this.getData(para)
    });

    this.changeOrderListener = DeviceEventEmitter.addListener(EMITTER_CHANGEORDERAPPLAYSUCCESS_TYPE, () => {
      this.dealData();
    })

    this.excreteOrderListener = DeviceEventEmitter.addListener(EMITTER_EXCRETEORDERSUCESS_TYPE, () => {
      this.dealData();
    })

    this.createOrderAgainListener = DeviceEventEmitter.addListener(EMITTER_CREATETRANSBILLSUCESS_TYPE, () => {
      this.dealData();
    })

    // if (this.props.tabIndex == 0 && this.props.resumed) {
    //     if (this.state.searchText != '') {
    //         this.dealData();
    //     }
    // }
    DeviceEventEmitter.addListener(EMITTER_NETCHANGE_TYPE, () => {
      console.log('运单联网成功')
      this.isNetworkFirst = true
    })

    this.listenerNetwork = this.props.navigation.addListener(
      'didFocus',
      (obj) => {
        if (this.isNetworkFirst) {
          this.getData(TransBillSearchClass)
          this.isNetworkFirst = false
        }
      })
  }

  componentWillUnmount() {
    if (this.searchListener) {
      this.searchListener.remove();
    }
    if (this.changeOrderListener) {
      this.changeOrderListener.remove();
    }
    if (this.excreteOrderListener) {
      this.excreteOrderListener.remove();
    }
    if (this.createOrderAgainListener) {
      this.createOrderAgainListener.remove();
    }
  }


  //改单申请,货物跟踪等按钮
  renderBottomBtn(title, callBack) {
    return <TouchableOpacity
      style={{
        marginLeft: 5, width: 70, height: 25, borderRadius: 3,
        borderColor: GlobalStyles.nav_bar_color, borderWidth: 1,
        justifyContent: 'center', alignItems: 'center'
      }}
      onPress={() => {
        callBack()
      }}
    >
      <Text style={{ fontSize: 13, color: GlobalStyles.nav_bar_color }}>{title}</Text>
    </TouchableOpacity>
  }
  //一键复制按钮
  copySubmitFunction(text) {
    Clipboard.setString(text);
    this.refs.toast.show('复制运单号成功', 500)
  }
  _renderItem = ({ row: row }) => {
    // let item = data.item
    let item = this.state.dataArr[row]
    let billState = item.ydzt
    let billName = ''
    let billColor = ''
    if (billState == 0) {
      billName = '已受理'
      billColor = '#76AE17'
    } else if (billState == 1) {
      billName = '已离港'
      billColor = '#EBB259'
    } else if (billState == 2) {
      billName = '已到港'
      billColor = '#0877DE'
    } else if (billState == 3) {
      billName = '已完成'
      billColor = '#BC1920'
    }
    let orderNum = item.dcdh
    if (item.dcdhfp && item.dcdhfp.length > 0) {
      orderNum = orderNum + '_' + item.dcdhfp
    }
    return <TouchableOpacity activeOpacity={1} style={styles.container}
      onPress={() => {
        this.orderDetail(item.id, true)
      }}
    >
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View style={styles.cellBackView}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ marginTop: 15, fontSize: 13, height: 15, lineHeight: 15, color: '#778087' }}>{'运单号:' + orderNum}</Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{
                height: 20, width: 40, backgroundColor: GlobalStyles.nav_bar_color,
                marginTop: 13, justifyContent: 'center', alignItems: 'center',
                borderRadius: 2, marginRight: 8
              }}
              onPress={() => {
                this.copySubmitFunction(orderNum)
              }}
            >
              <Text style={{ color: '#fff', fontSize: 13, }}>复制</Text>
            </TouchableOpacity>
            <Text style={{ marginTop: 15, fontSize: 13, height: 15, lineHeight: 15, color: billColor }}>{billName}</Text>
          </View>
        </View>
        <Image style={{ width: GlobalStyles.screenWidth - 30, height: 2, marginLeft: -15, marginTop: 10 }} source={require('../../../resource/xu2.png')} />
        <View style={{ marginTop: 10, height: 28, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 13, color: 'black' }}>{item.zwcm + ' | ' + item.hc + ' | ' + item.ystk}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 13, color: '#778087' }}>{'离港:'}</Text>
            <Text style={{ fontSize: 13 }}>{item.khsj}</Text>
          </View>
        </View>
        <View style={{ height: 30, flexDirection: 'row' }}>
          <Image style={{ marginTop: 3, height: 15, width: 24 }}
            source={require('../../../resource/ship.png')}
          />
          <Text style={{ marginLeft: 15, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.sfg}</Text>
          <Image style={{ marginTop: 9, width: 60, height: 5, marginLeft: 20 }} source={require('../../../resource/direction.png')} />
          <Text style={{ marginLeft: 20, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.mdg}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '59%' }}>货名: {item.hm}</Text>

          <Text style={{ fontSize: 13, color: 'black', lineHeight: 28 }}>箱型 * 箱量: {item.xxxl} * {item.sl}</Text>
        </View>
        <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '95%' }}>收货人: {item.shr}</Text>
        <View style={{ width: GlobalStyles.screenWidth, height: 0.5, marginLeft: -15, backgroundColor: GlobalStyles.backgroundColor }}></View>
        <View style={{ marginTop: 5, height: 25, flexDirection: 'row', justifyContent: 'flex-end' }}>
          {this.renderBottomBtn('改单申请', () => {
            console.log('改单申请点击事件')
            this.changeOrderBtnClick(0, item.id)

          })}
          {this.renderBottomBtn('拆单申请', () => {
            this.changeOrderBtnClick(1, item.id)

          })}
          {this.renderBottomBtn('货物跟踪', () => {
            NavigationUtil.goPage(item, 'TransGoodsTrack')
          })}
          {this.renderBottomBtn('再来一单', () => {
            // NavigationUtil.goPage(item, 'CreateOrderAgain')
            this.orderDetail(item.id, false)
          })}
        </View>
        <View style={{ width: GlobalStyles.screenWidth - 30, height: 10, backgroundColor: 'white', marginLeft: -15 }}></View>
      </View>
    </TouchableOpacity>
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          {WaybillTopSearchView.renderSearchView('运单号', this.state.searchText, (text) => {
            // console.log('运单搜索 ', text)
            this.setState({ searchText: text })
          }, (event) => {
            TransBillSearchClass.dcdh = event.nativeEvent.text
            this.getData(TransBillSearchClass)
          }, () => {
            NavigationUtil.goPage({}, 'TransBillMoreSearchView')
          }, () => {
            TransBillSearchClass.dcdh = this.state.searchText
            this.getData(TransBillSearchClass)
          })}
          <LargeListPlus
            // style={{height:'100%'}}
            toTop={this.state.toTop}
            endRefresh={this.state.endRefresh}
            data={this.state.dataArr}
            onRefresh={() => { this.dealData() }}
            renderItem={this._renderItem}
            NoDataViewheight={200}
            onLoading={() => { this.loadMoreData(TransBillSearchClass) }}
            itemHeight={218}
          />
        </View>
        <AlertViewPlus
          ref="AlertView"
          ShowCancelButton={false}
          title='温馨提示'
          message='由于客户端升级，暂时关闭订舱功能。'
          alertSureDown={()=>{}}
        />
        <ActivityIndicatorPlus
          ref={(r) => this.refs.activityIndicatorPlus = r}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
      </View>
    )

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cellBackView: {
    flex: 1,
    backgroundColor: 'white',
    width: GlobalStyles.screenWidth - 30,
    borderRadius: 3,
    marginLeft: 15,
    paddingLeft: 15,
    paddingRight: 15,
  }
});