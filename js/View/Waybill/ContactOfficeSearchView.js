
//联系我们界面的检索页面

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Keyboard, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import ViewUtil from '../../common/ViewUtil'
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus';
import BookSpaceCellsUtil from '../BookSpaceUtils/BookSpaceCellsUtil';
import BookSpaceBottomModal from '../BookSpaceUtils/BookSpaceBottomModal';
import Toast from 'react-native-easy-toast';
import { EMITTER_CONTACT_SEARCH_TYPE } from '../../common/EmitterTypes'

export default class ContactOfficeSearchView extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params
    this.state = {
      blockArr: this.params[0],      //片区列表
      blockName: '显示所有',   //片区名称
      contactOffice: '',   //办事处
      adressContact: '',   //办事处地点
      phoneNumber: '',    //联系人电话
      faxNumber: '',           //联系人传真
    }
  }

  componentDidMount() {
    console.log( this.props.navigation.state.params);
    
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }


  renderLine() {
    return <View style={{ width: GlobalStyles.screenWidth, height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
  }

  //点击搜索事件
  BottonClick() {
    let searchObject = {}
    searchObject.blockName = this.state.blockName
    searchObject.contactOffice = this.state.contactOffice
    searchObject.adressContact = this.state.adressContact
    searchObject.phoneNumber = this.state.phoneNumber
    searchObject.faxNumber = this.state.faxNumber
    let txt = this.state.phoneNumber
    let isY = this.validatePhone(txt)
    if (!isY) {
      this.refs.toast.show('电话格式不对', 800)
      return
    }
    DeviceEventEmitter.emit(EMITTER_CONTACT_SEARCH_TYPE, searchObject)
    NavigationUtil.goBack(this.props.navigation)
  }

  //电话号码校验
  validatePhone(val) {
    let isPhone = /^1[3|4|5|6|7|8|9][0-9]{9}$|^[0-9]{3}-[0-9]{8}$|^[0-9]{4}-[0-9]{7,8}$|[0-9]{3}[0-9]{8}$|^[0-9]{4}[0-9]{7,8}$/;
    if (isPhone.test(val)) {
      return true
    } if (val == '') {
      return true
    } else {
      return false
    }
  }

  render() {
    let navigationBar = <NavigationBar
      title={'检索'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <TouchableOpacity activeOpacity={1} style={{ height:GlobalStyles.screenHeight, alignItems: 'center' }} onPress={() => { Keyboard.dismiss() }}>
          {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '办事处:', 110, '请输入', true, 'default', this.state.contactOffice, (text) => {

          }, (event) => {
            this.setState({ contactOffice: event.nativeEvent.text })
          })}
          {this.renderLine()}

          {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '办事处地点:', 110, '请输入', true, 'default', this.state.adressContact, (text) => {

          }, (event) => {
            this.setState({ adressContact: event.nativeEvent.text })
          })}
          {this.renderLine()}

          {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '联系人电话:', 110, '请输入', true, 'default', this.state.phoneNumber, (text) => {

          }, (event) => {
            this.setState({ phoneNumber: event.nativeEvent.text })
          })}
          {this.renderLine()}

          {BookSpaceCellsUtil.renderMarginleftInputCell(false, '#FFFBFB', '联系人传真:', 110, '请输入', true, 'default', this.state.faxNumber, (text) => {

          }, (event) => {
            this.setState({ faxNumber: event.nativeEvent.text })
          })}
          {this.renderLine()}

          <View style={{ justifyContent: 'space-between', backgroundColor: '#aaa', width: '100%' }}>
            {BookSpaceCellsUtil.renderTextChooseCell(false, '#FFFBFB', '片区选择', this.state.blockName, '#000', () => {
              Keyboard.dismiss()
              this.refs.bookSpaceBottomModal.show('areaName', this.state.blockArr)
            })}
          </View>

          <TouchableOpacity
            style={{
              width: GlobalStyles.screenWidth - 100,
              height: 45, borderRadius: 5, marginTop: 30,
              backgroundColor: GlobalStyles.nav_bar_color,
              justifyContent: 'center', alignItems: 'center',
            }}
            onPress={() => {
              Keyboard.dismiss()
              this.BottonClick()
            }}
          >
            <Text style={{ fontSize: 16, color: '#fff' }}>搜索</Text>
          </TouchableOpacity>
        </TouchableOpacity>
        <BookSpaceBottomModal
          ref="bookSpaceBottomModal"
          callBack={data => {
            this.setState({ blockName: data.areaName })
          }}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  }
});