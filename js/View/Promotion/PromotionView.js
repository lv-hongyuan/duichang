/**
 * 促销
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import GlobalStyles from '../../common/GlobalStyles'
import NoDataView from '../../common/NoDataView'

export default class PromitionView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isNoData:true
    }
  }

  render() {
    let navigationBar =
      <NavigationBar
        title={'促销'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color} bottomInset={false}>
        {navigationBar}
        {this.state.isNoData?NoDataView.renderContent(40):null}
      </SafeAreaViewPlus>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: GlobalStyles.backgroundColor,
  }
});