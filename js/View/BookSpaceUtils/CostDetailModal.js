/**
 * 费用明细modal
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import Ionicons from 'react-native-vector-icons/Ionicons'


export default class CostDetailModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            dataArr: [],
        }
    }

    show(data) {
        console.log('费用明细:',data)
        this.setState({
            modalVisible: true,
            dataArr: data
        })
    }

    //tableViewHeaderView
    headerView() {
        let totalCost = this.state.dataArr[0]
        let dxhjCost = this.state.dataArr[1]
        return <View>
            <View style={{ width: GlobalStyles.screenWidth, height: 40, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ height: 40, lineHeight: 40, fontSize: 16, fontWeight: '700', color: 'black' }}>{'总费用:'}</Text>
                    <Text style={{ marginLeft: 15, height: 40, lineHeight: 40, fontSize: 16, fontWeight: '700', color: GlobalStyles.nav_bar_color }}>{totalCost.fee}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ height: 40, lineHeight: 40, fontSize: 16, fontWeight: '700', color: 'black' }}>{'单箱费用:'}</Text>
                    <Text style={{ marginLeft: 15, height: 40, lineHeight: 40, fontSize: 16, fontWeight: '700', color: GlobalStyles.nav_bar_color }}>{dxhjCost.fee}</Text>
                </View>
                <TouchableOpacity
                    style={{ height: 40, width: 40, alignItems: 'flex-end' }}
                    activeOpacity={0.7}
                    onPress={() => {
                        this.setState({ modalVisible: false })
                    }}
                >
                    <Ionicons
                        name={'md-close'}
                        size={20}
                        style={{ color: '#B0B0B0', marginRight: 0, marginTop: 10 }}
                    />
                </TouchableOpacity>

            </View>
            <View style={{ height: 1, width: GlobalStyles.screenWidth, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //cell
    renderItem(data) {
        let item = data.item
        return <View style={{ flexDirection: 'row', width: GlobalStyles.screenWidth, height: 40, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}>
            <Text style={{ height: 40, lineHeight: 40, fontSize: 15, color: '#778087' }}>{item.feeName}</Text>
            <Text style={{ height: 40, lineHeight: 40, fontSize: 15, color: 'black' }}>{item.fee}</Text>
        </View>
    }

    //flatlist
    renderFlatList() {
        let listD = []
        let lastStr = ''
        if(this.state.dataArr && this.state.dataArr.length>0){
            let tempD = this.state.dataArr[2]
            lastStr = tempD.fee
            let tempD2 = this.state.dataArr[3]
            listD = tempD2.feeArr
            
        }
        
        return (
            <SafeAreaViewPlus style={styles.backgroundView}>
                <TouchableOpacity style={styles.backgroundView}
                    onPress={() => {
                        this.setState({ modalVisible: false })
                    }}
                >
                </TouchableOpacity >
                <View style={{ backgroundColor: 'white' }}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        // ItemSeparatorComponent={() => this.separatorView()}
                        ListHeaderComponent={() => this.headerView()}
                        data={listD}
                        renderItem={data => this.renderItem(data)}
                        bounces={false}
                    />
                    {lastStr.length>0?<Text style={styles.lastText}>{lastStr}</Text>:null}
                </View>
            </SafeAreaViewPlus>
        )
    }


    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderFlatList()}
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundView: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.3)',
        paddingTop: GlobalStyles.is_iphoneX ? 65 : 0,
    },
    FlatListText: {
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
    },
    lastText:{
        fontSize:15,
        color:'#778087',
        paddingLeft:15,
        paddingRight:15
    }
});