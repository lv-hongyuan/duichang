/**
 * 货名货类联动
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    FlatList,
    SectionList,
    Dimensions,
    TouchableOpacity,
    Image,
    TextInput,
    Platform,
    Keyboard
} from 'react-native';

import ParcelData from '../../../resource/ParcelData2.json'
import NavigationBar from '../../common/NavigationBar'
import GlobalStyles from '../../common/GlobalStyles'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import NavigationUtil from '../../Navigator/NavigationUtil'
import BookInfoDao from '../../dao/BookInfoDao'
import { LargeList } from "react-native-largelist-v3";
import Ionicons from 'react-native-vector-icons/Ionicons'

import flatMap from 'lodash/flatMap'
import filter from 'lodash/filter'
import includes from 'lodash/includes'
import DeviceInfo from 'react-native-device-info'

var { width, height } = Dimensions.get('window');

let ITEM_HEIGHT = 35

export default class BookSpaceGoodsInfo extends Component {

    constructor(props) {
        super(props)
        this.OriData = BookInfoDao.GoodsNameArr     //原数据
        this.ParcelData = this.OriData.concat()     //左边列表的数据
        console.log('===', this.ParcelData)

        this.state = {
            cell: 0,     //选中了左边那个cell
            largeListData: [],
            values: ''
        }
    }

    //右边列表数据
    getRlistData(index) {
        let rData = [{ items: [] }];
        let tempArr = this.ParcelData[index].items
        tempArr.forEach(element => {
            rData[0].items.push(element);
        });

        this.setState({ largeListData: rData })
    }

    componentDidMount() {
        this.getRlistData(0)
    };


    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //搜索按钮点击事件
    searchBtnClick(text) {
        let Chinese = new RegExp('^[\u4e00-\u9fa5]+$');
        let English = new RegExp('^[a-zA-Z]+$');
        let ParcelData = this.ParcelData
        let list = flatMap(ParcelData, (data) => data.items)    //将二维数组转换成一维数组
        let list1 = []
        // if (Chinese.test(text)) {           //匹配goodsName
        //     list1 = filter(list, (item) => {
        //         return includes(item.goodsName, text)
        //     })
        // } else if (English.test(text)) {    //匹配fullSpell
        //     list1 = filter(list, (item) => {
        //         return includes(item.fullSpell, text)
        //     })
        // }

        list1 = filter(list, (item) => {
            return includes(item.goodsName, text)
        })

        let rData = [{ items: [] }];
        list1.forEach(element => {
            rData[0].items.push(element);
        });

        this.setState({ cell: -1, largeListData: rData })
    }

    //左边cell
    renderLRow = (item) => {
        return (
            <TouchableOpacity style={[styles.lItem, { backgroundColor: item.index == this.state.cell ? 'white' : null }]}
                onPress={() => this.cellAction(item)}>
                <Text style={styles.lText}>{item.item.header}</Text>
            </TouchableOpacity>
        )
    };

    //左边flatlist点击事件
    cellAction = (item) => {
        this.getRlistData(item.index)
        this.setState({ cell: item.index });

    };


    //右边cell
    _renderItem = (item) => {

        // console.log('item is :',item)
        // const data = this.ParcelData[this.state.cell].items[item.row];
        let data = this.state.largeListData[0].items[item.row]
        return (
            <TouchableOpacity style={styles.rItem}
                onPress={() => {
                    //console.log('==section:', item.section)
                    //console.log('==index:', item.index)
                    // let tempJson = {}
                    // tempJson.sectionTitle = this.ParcelData[this.state.cell].header
                    // tempJson.data = data
                    // console.log('tempJson:',tempJson)
                    this.props.navigation.state.params.backData(data);
                    this.props.navigation.goBack();
                }}
            >
                <Text style={styles.foodName}>{data.goodsName}</Text>
            </TouchableOpacity>

        )
    };

    //左边cell分割线
    separator = () => {
        return (
            <View style={{ height: 1, backgroundColor: GlobalStyles.backgroundColor }} />
        )
    };

    renderNavBar() {
        let isNotch = DeviceInfo.hasNotch();
        let backButton = <TouchableOpacity
            style={{ marginLeft: 15, marginTop: 20 }}
            onPress={() => {
                this.leftButtonClick()
            }}>
            <Ionicons
                name={'ios-arrow-back'}
                size={26}
                style={{ color: 'white' }} />
        </TouchableOpacity>
        let inputView = <TextInput
            ref="input"
            placeholder={'请输入货名'}

            returnKeyType={'search'}
            onSubmitEditing={(event => {
                let txt = event.nativeEvent.text
                if (txt && txt.length > 0) {
                    this.searchBtnClick(txt)
                } else {
                    this.setState({
                        cell: 0,
                    })
                }

            })}
            onChangeText={(text) => {
                this.setState({ values: text })
            }}
            style={styles.textInput}
        >
        </TextInput>;

        let rightButtom = <TouchableOpacity
            style={{
                marginTop: 20, justifyContent: 'center',
                alignItems: 'center', backgroundColor: '#fff', height: 30,
                width: 50, marginRight: 10, borderRadius: 2
            }}
            onPress={() => {
                Keyboard.dismiss()
                if (this.state.values && this.state.values.length > 0) {
                    this.searchBtnClick(this.state.values)
                } else {
                    this.setState({
                        cell: 0,
                    })
                }

            }}
        >
            <Text style={{ color: GlobalStyles.nav_bar_color }}>搜索</Text>
        </TouchableOpacity>

        return <View style={{
            backgroundColor: GlobalStyles.nav_bar_color,
            flexDirection: 'row',
            alignItems: 'center',
            height: isNotch ? 84 : 64,
        }}>
            {backButton}
            {inputView}
            {rightButtom}
        </View>
    }

    render() {

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {this.renderNavBar()}
                <View style={{ flexDirection: 'row', flex: 1 }}>
                    <FlatList
                        ref='FlatList'
                        style={styles.leftList}
                        data={this.ParcelData}
                        renderItem={(item) => this.renderLRow(item)}
                        ItemSeparatorComponent={() => this.separator()}
                        keyExtractor={(item, index) => index + ''}
                        showsVerticalScrollIndicator={false}
                    />
                    <LargeList
                        ref='sectionList'
                        style={styles.rightList}
                        // heightForSection={() => 30}
                        // renderSection={this._renderSection}
                        heightForIndexPath={() => ITEM_HEIGHT}
                        renderIndexPath={this._renderItem}
                        data={this.state.largeListData}
                        getItemLayout={(data, index) => ({ length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index })}
                    />
                </View>

            </SafeAreaViewPlus>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    leftList: {
        width: 1 * width / 3,
        backgroundColor: GlobalStyles.backgroundColor
    },
    lItem: {
        minHeight: 50,
        justifyContent: 'center',
    },
    lText: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
    },
    rightList: {
        width: 2 * width / 3,
        backgroundColor: 'white'
    },
    rItem: {
        flexDirection: 'row',
        height: ITEM_HEIGHT,

    },
    rItemDetail: {
        flex: 1,
        marginTop: 10,
        marginLeft: 5
    },
    icon: {
        height: 60,
        width: 60,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 8,
        borderWidth: 1,
        borderColor: '#999999'
    },
    foodName: {
        fontSize: 16,
        height: ITEM_HEIGHT,
        lineHeight: ITEM_HEIGHT,
        // textAlign: 'center',
        marginLeft: 15,
        width: 2 * width / 3,
    },
    textInput: {
        flex: 1,
        height: (Platform.OS === 'ios') ? 30 : 36,
        borderWidth: (Platform.OS === 'ios') ? 1 : 0,
        borderColor: "white",
        backgroundColor: 'white',
        alignSelf: 'center',
        paddingLeft: 15,
        marginRight: 10,
        marginLeft: 15,
        borderRadius: 3,
        color: 'black',
        marginTop: 20,
        paddingTop: 0,
        paddingBottom: 0,
    },
});