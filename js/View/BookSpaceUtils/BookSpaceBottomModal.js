/**
 * 订舱底部弹出flatlist
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'

export default class BookSpaceBottomModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            showText: '',    
            dataArr: [],
        }
    }

    show(text, data) {
        this.setState({
            modalVisible: true,
            showText: text,
            dataArr: data
        })
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //cell
    renderItem(data) {
        let showText = this.state.showText
        let item = data.item
        return <TouchableOpacity style={[styles.FlatListText, { backgroundColor: 'white' }]} onPress={() => {
            this.setState({ modalVisible: false })
            this.props.callBack(item)
        }}>
            <Text style={styles.FlatListText}>{item[showText]}</Text>
        </TouchableOpacity>
    }

    //flatlist
    renderFlatList() {
        return (
            <View style={styles.backgroundView}>
                <View style={{ backgroundColor: GlobalStyles.backgroundColor}}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => this.separatorView()}
                        data={this.state.dataArr}
                        renderItem={data => this.renderItem(data)}
                        bounces={false}
                    />
                    <TouchableOpacity style={styles.cancelText} onPress={() => {
                        this.setState({
                            modalVisible: false,
                        })
                    }}>
                        <Text style={styles.FlatListText}>取消</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (
            // <SafeAreaViewPlus style={{ flex: 1, justifyContent: 'flex-end' }} topInset={true}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderFlatList()}
                </Modal>
            // </SafeAreaViewPlus>
        )
    }
}

const styles = StyleSheet.create({
    backgroundView: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingTop: GlobalStyles.is_iphoneX ? 65 : 80,
    },
    FlatListText: {
        height: 50,
        lineHeight: 50,
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
    },
    cancelText: {
        marginTop: 10,
        backgroundColor: 'white',
        height: 50,
    }
});