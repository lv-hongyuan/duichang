/**
 * 货名货类联动(失效)
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    FlatList,
    SectionList,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';

import ParcelData from '../../../resource/ParcelData.json'
import NavigationBar from '../../common/NavigationBar'
import GlobalStyles from '../../common/GlobalStyles'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import NavigationUtil from '../../Navigator/NavigationUtil'
import BookInfoDao from '../../dao/BookInfoDao'

var { width, height } = Dimensions.get('window');

let Headers = [];
let ITEM_HEIGHT = 40

export default class BookSpaceGoodsName extends Component {

    constructor(props) {
        super(props)
        this.ParcelData = BookInfoDao.GoodsNameArr
        // this.ParcelData = ParcelData
        // console.log('===',this.ParcelData)
        this.state = {
            cell: 0     //选中了左边那个cell
        }
    }

    componentDidMount() {
        this.ParcelData.map((item, i) => {
            //console.log('headers',item)
            Headers.push(item.goodsType);
        });
    };

    componentWillUnmount() {
        Headers = [];
    };

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    renderLRow = (item) => {
        return (
            <TouchableOpacity style={[styles.lItem, { backgroundColor: item.index == this.state.cell ? 'white' : null }]}
                onPress={() => this.cellAction(item)}>
                <Text style={styles.lText}>{item.item.goodsType}</Text>
            </TouchableOpacity>
        )
    };

    cellAction = (item) => {
        // console.log('item is :',item)
        if (item.index <= this.ParcelData.length) {
            this.refs.sectionList.scrollToLocation({ animated: false, sectionIndex: item.index, itemIndex: 0, viewPosition: 0 })
            this.setState({
                cell : item.index
              });
        }

    };

    itemChange = (info) => {
        // console.log('info :',info)
        if (info.viewableItems.section) {
            let section = info.viewableItems[0].section.goodsType;
            if (section) {
                let index = Headers.indexOf(section);
                if (index < 0) {
                    index = 0;
                }
                this.setState({ cell: index });
            }
        }

    };

    renderRRow = (item) => {

        //console.log('item is :',item)
        return (
            <TouchableOpacity style={styles.rItem}
                onPress={() => {
                    //console.log('==section:', item.section)
                    //console.log('==index:', item.index)
                    let tempJson = {}
                    tempJson.sectionTitle = item.section.goodsName
                    tempJson.detailTitle = item.section.goodsList[item.index]
                    //console.log('tempJson:',tempJson)
                    this.props.navigation.state.params.backData(tempJson);
                    this.props.navigation.goBack();
                }}
            >
                <Text style={styles.foodName}>{item.item.goodsName}</Text>
                {/* <Text style={styles.foodName}>{'ssss'}</Text> */}
                {/* <Image style={styles.icon} source={{ uri: item.item.img }} /> */}
            </TouchableOpacity>

        )
    };

    sectionComp = (section) => {
        // console.log('section is :',section)
        return (
            <View style={{ height: 30, backgroundColor: '#DEDEDE', justifyContent: 'center', alignItems: 'center' }}>
                <Text >{section.section.goodsType}</Text>
                {/* <Text >{'hhhhh'}</Text> */}
            </View>
        )
    };

    separator = () => {
        return (
            <View style={{ height: 1, backgroundColor: 'gray' }} />
        )
    };

    getItemLayout(data, index) {
        //console.log('data is :',data)
        return {
            index,
            offset: 0,
            length: ITEM_HEIGHT
        }
    }

    render() {
        let navigationBar = <NavigationBar
            title={'订舱信息'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <View style={{ flexDirection: 'row', flex: 1 }}>
                    <FlatList
                        ref='FlatList'
                        style={styles.leftList}
                        data={this.ParcelData}
                        renderItem={(item) => this.renderLRow(item)}
                        ItemSeparatorComponent={() => this.separator()}
                        keyExtractor={(item, index) => index + ''}
                    />
                    <SectionList
                        ref='sectionList'
                        style={styles.rightList}
                        renderSectionHeader={(section) => this.sectionComp(section)}
                        renderItem={(item) => this.renderRRow(item)}
                        sections={this.ParcelData}
                        keyExtractor={(item, index) => index + ''}
                        onViewableItemsChanged={(info) => this.itemChange(info)}
                        getItemLayout={(data, index) => this.getItemLayout(data, index)}
                    />
                </View>

            </SafeAreaViewPlus>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    leftList: {
        width: 1 * width / 3,
        backgroundColor: '#E9E9EF'
    },
    lItem: {
        minHeight: 44,
        justifyContent: 'center',
    },
    lText: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
    },
    rightList: {
        width: 2 * width / 3
    },
    rItem: {
        flexDirection: 'row'
    },
    rItemDetail: {
        flex: 1,
        marginTop: 10,
        marginLeft: 5
    },
    icon: {
        height: 60,
        width: 60,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 8,
        borderWidth: 1,
        borderColor: '#999999'
    },
    foodName: {
        fontSize: 18,
        height: ITEM_HEIGHT,
        lineHeight: ITEM_HEIGHT,
        textAlign: 'center',
        width:2 * width / 3
    }
});