/**
 * 声明条款
 */
import React, { Component } from 'react';
import { WebView,Platform, StyleSheet, Text, View, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
// import { WebView } from "react-native-webview";

export default class DeclareProvision extends Component {

    constructor(props){
        super(props)
        this.state={
            textData:'',
            indicatorAnimating: false, //菊花
        }
    }

    componentDidMount(){
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        let paramStr = JSON.stringify(param)
        this.setState({ indicatorAnimating: true})
        NetworkDao.fetchPostNet('getProtocol.json', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false})
                if (data._backcode == '200') {
                   this.setState({textData:data.fileData})
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } 
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false})
                this.refs.toast.show('获取声明条款数据失败', 800)
            })
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    renderText(){
        return <ScrollView>
            <Text style={styles.text}>{this.state.textData}</Text>
        </ScrollView>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'声明条款'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.renderText()}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { zIndex:1000,position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast" position="center"/>
            </SafeAreaViewPlus>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    text:{
        fontSize:18,
        color:'black',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:5,
        lineHeight:25
    }
});