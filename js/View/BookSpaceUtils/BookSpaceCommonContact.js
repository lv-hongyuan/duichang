//常用联系人
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, TextInput, TouchableOpacity, ActivityIndicator,DeviceEventEmitter } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import NavigationUtil from '../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../common/NoDataView'
import { EMITTER_COMMON_CONTACT_SELECT_TYPE } from '../../common/EmitterTypes'


export default class BookSpaceCommonContact extends Component {


    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
        this.isSend = this.params.isSend
        console.log(this.params)
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],
            isNoData: false,
            searchText: '',
        }
    }

    componentDidMount() {
        this.getContacts()
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    getContacts() {
        this.setState({ indicatorAnimating: true })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        if (this.isSend) {
            params.lxrLx = '20'
        } else {
            params.lxrLx = '10'
        }
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('getAppDcLxr.json', paramsStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backCode == '200') {
                    this.allDataArr = data.entityList   //所有数据
                    this.setState({ dataArr: data.entityList, isNoData: false })
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.setState({ dataArr: [], isNoData: true })
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false ,dataArr: [], isNoData: true})
                this.refs.toast.show('获取常用联系人失败', 800)
            })
    }

    searchContacts(text) {
        let searchDataArr = []
        if(this.allDataArr && this.allDataArr.length > 0){
            this.allDataArr.forEach(element => {
                console.log(element)
                let lxr = element.lxr
                let gsmc = element.gsmc
                let dh = element.dh
                console.log(lxr)
                if ((lxr.indexOf(text) != -1) || (gsmc.indexOf(text) != -1) || (dh.indexOf(text) != -1)) {
                    searchDataArr.push(element)
                }
            });
            if(searchDataArr.length > 0){
                this.setState({ dataArr: searchDataArr,isNoData: false})
            }else{
                this.setState({ dataArr: [], isNoData: true,})
            }
        }else{
            this.setState({ dataArr: [], isNoData: true})
            this.refs.toast.show('联系人不存在',800)
        }
    }
    //搜索的view
    renderSearchView() {
        return <View style={{ marginTop: 10, backgroundColor: 'white', width: GlobalStyles.screenWidth, height: 90, flexDirection: 'row' }}>
            <Image style={{ height: 70, width: 67 }} source={require('../../../resource/rudder.png')} />
            <View style={{ flexDirection: 'row', marginTop: 22.5, marginLeft: -52, height: 45, width: GlobalStyles.screenWidth - 95, borderColor: '#9A9A9A', borderWidth: 1, borderRadius: 3 }}>
                <Image style={{ marginLeft: 10, marginTop: 10, width: 20, height: 20 }} source={require('../../../resource/zoomer-g.png')} />
                <TextInput
                    style={{ marginLeft: 10, height: 40, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 70 }}
                    clearButtonMode={'while-editing'}
                    placeholder={''}
                    fontSize={17}
                    returnKeyType={'search'}
                    onSubmitEditing={(event) => {
                        console.log('搜索的内容:', event.nativeEvent.text)
                        let txt = event.nativeEvent.text
                        if (txt && txt.length > 0) {
                            this.searchContacts(txt)
                        } else {
                            // this.refs.toast.show('请输入联系人', 500)
                            this.setState({ dataArr: this.allDataArr })
                        }
                    }}
                    onChangeText={(text) => {
                        this.setState({ searchText: text })
                    }}
                />
            </View>
            <TouchableOpacity style={styles.searchBtn}
                activeOpacity={0.75}
                onPress={() => {
                    console.log('搜索按钮点击事件')
                    if (this.state.searchText && this.state.searchText.length > 0) {
                        this.searchContacts(this.state.searchText)
                    } else {
                        this.refs.toast.show('请输入联系人', 500)
                    }
                }}
            >
                <Text style={{ color: 'white', fontSize: 17 }}>搜索</Text>
            </TouchableOpacity>
        </View>
    }
    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    renderItem(data) {
        let item = data.item
        return <TouchableOpacity
            onPress={() => {
                //cell点击事件
                console.log('常用联系人cell点击事件')
                let contact = {}
                contact.isSend = this.isSend
                contact.data = item
                DeviceEventEmitter.emit(EMITTER_COMMON_CONTACT_SELECT_TYPE, contact)
                NavigationUtil.goBack(this.props.navigation)
            }}
        >
            <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
            <View style={styles.cardView}>
                <View style={{ width: 100 }}>
                    <Text style={styles.text}>{this.isSend ? '发货人全称:' :'收货人全称:'}</Text>
                    <Text style={styles.text}>{this.isSend ? '发货人联系人:':'收货人联系人:'}</Text>
                    <Text style={styles.text}>联系人电话:</Text>
                    <Text style={styles.text}>门点地址:</Text>
                    <Text style={styles.text}>详细地址:</Text>
                </View>

                <View style={{ width: GlobalStyles.screenWidth - 100 }}>
                    <Text style={styles.text} numberOfLines={1}>{item.gsmc}</Text>
                    <Text style={styles.text}>{item.lxr}</Text>
                    <Text style={styles.text}>{item.dh}</Text>
                    <Text style={styles.text} numberOfLines={1}>{item.md }</Text>
                    <Text style={styles.text} numberOfLines={1}>{item.dz}</Text>
                </View>
            </View>

        </TouchableOpacity>
    }
    //flatlist
    renderFlatList() {
        return <View style={{ flex: 1, marginTop: 10, backgroundColor: GlobalStyles.backgroundColor }}>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => this.separatorView()}
                data={this.state.dataArr}
                renderItem={data => this.renderItem(data)}
            />
        </View>
    }
    render() {
        let navigationBar =
            <NavigationBar
                title={'常用联系人'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
            {navigationBar}
            <Toast ref="toast" position="center" />
            {this.renderSearchView()}
            {this.renderFlatList()}
            {this.state.isNoData ? NoDataView.renderContent(0) : null}
            {this.state.indicatorAnimating && (
                <ActivityIndicator
                    style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                    size={GlobalStyles.isIos ? 'large' : 40}
                    color='gray'
                />)}
        </SafeAreaViewPlus>
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBtn: {
        height: 45,
        width: 60,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: GlobalStyles.nav_bar_color,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        marginTop: 22.5
    },
    cardView: {
        // borderRadius: 10,
        padding: 10,
        paddingRight: 5,
        flexDirection: 'row',
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'space-between'
    },
    text: {
        fontSize: 14,
        color: '#555',
        height: 25,
        width: GlobalStyles.screenWidth - 100
    }
});