import { MapView, Geocode } from 'react-native-baidumap-sdk';
import React, { Component } from 'react';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../common/ViewUtil'
import NavigationBar from '../../common/NavigationBar';
import NavigationUtil from '../../Navigator/NavigationUtil';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image, TextInput, DeviceEventEmitter, Keyboard } from 'react-native';
import Toast, { DURATION } from 'react-native-easy-toast';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { EMITTER_CHOISE_ADDRESS_SUCESS } from '../../common/EmitterTypes'

export default class BaiDuMapDetail extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.zoomLevel = 15  //!默认缩放级别
    this.state = {
      indicatorAnimating: false,
      defaultLoction: { latitude: this.params.lat, longitude: this.params.lon },// !中心点
      location: { latitude: this.params.lat, longitude: this.params.lon },  // !地图标记点
      centreaddress: this.params.address,         // !详细地址
    }
  }

  // !返回按钮
  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }


  render() {
    let navigationBar =
      <NavigationBar
        title={'选择地址'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
        leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <View style={{  // !头部搜索与确定部件
          height: 50, backgroundColor: '#fff', paddingHorizontal: 10, flexDirection: 'row',
          justifyContent: 'space-between', alignItems: 'center'
        }}>
          <View style={{
            height: 35, width: GlobalStyles.screen_width - 80,
            paddingLeft: 10, borderWidth: 1, borderColor: '#eee', flexDirection: 'row', borderRadius: 5,
            justifyContent: 'space-between', overflow: 'hidden'
          }}>
            <TextInput
              style={{ color: '#585858', flex: 1,height:35 }}
              fontSize={14}
              defaultValue={this.state.centreaddress}
              onChangeText={(text) => {
                this.setState({ centreaddress: text })
              }}
            />
            <TouchableOpacity style={{    // !搜索按钮
              backgroundColor: GlobalStyles.nav_bar_color,
              width: 50, height: 33, justifyContent: 'center', alignItems: 'center'
            }}
              onPress={() => {
                this.setState({indicatorAnimating:true})
                Keyboard.dismiss()
                Geocode.search(this.state.centreaddress)
                  .then((data) => {
                    this.setState({
                      location: { latitude: data.latitude, longitude: data.longitude },
                      defaultLoction: { latitude: data.latitude, longitude: data.longitude },
                      indicatorAnimating:false
                    })
                  })
                  .catch((e) => {
                    console.log(e);
                    this.setState({indicatorAnimating:false})
                    this.refs.toast.show('未获取到相关地点,请检查详细地址是否填写正确', 1000)
                  })
              }}
            >
              <Text style={{ color: '#fff', fontSize: 15, }}>搜索</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={{  // !确定按钮
            backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 3,
            width: 50, height: 33, justifyContent: 'center', alignItems: 'center'
          }}
            onPress={() => {
              const item = { key: this.params.key, address: this.state.centreaddress }
              DeviceEventEmitter.emit(EMITTER_CHOISE_ADDRESS_SUCESS, item);
              NavigationUtil.goBack(this.props.navigation)
            }}
          >
            <Text style={{ color: '#fff', fontSize: 15, }}>确定</Text>
          </TouchableOpacity>
        </View>
        <MapView
          ref={ref => (this._MapView = ref)}
          center={this.state.defaultLoction}   // !地图中心点
          locationEnabled={false}   // !是否显示定位图层
          // locationMode='follow'  // !是否跟随至定位点
          style={styles.mapView}
          buildingsDisabled   // !禁用3d建筑
          overlookDisabled    // !禁用倾斜手势
          trafficEnabled={false}  // !是否显示路况
          zoomLevel={this.zoomLevel} // !缩放级别
          minZoomLevel={6}  // !最小缩放级别
          maxZoomLevel={17}  // !最大缩放级别
          zoomControlsDisabled  // !禁用缩放按钮（此按钮仅安卓生效）
          onClick={(data) => {
            // !点击地图事件 
            Geocode.reverse({ latitude: data.latitude, longitude: data.longitude })
              .then((items) => {
                this.setState({
                  centreaddress: items.address,
                  location: { latitude: items.latitude, longitude: items.longitude },
                })
              })
              .catch((e) => {
                console.log(e);
                this.refs.toast.show('未获取到相关地点，请重试')
              })
          }}
        >
          <MapView.Marker  //!地图标记
            coordinate={this.state.location}
            view={() => (
              <View style={{ position: 'relative', right: 17, bottom: 15 }}>
                <Fontisto
                  name={'map-marker-alt'}
                  size={37}
                  style={{ color: 'rgb(70,118,255)' }}
                />
              </View>
            )}
          />

        </MapView>
        <TouchableOpacity
          style={{
            height: 45, width: 45, borderRadius: 3, backgroundColor: 'rgba(255,255,255,.9)',
            position: 'absolute', bottom: 130, right: 15, justifyContent: 'center', alignItems: 'center'
          }}
          // !点击定位到当前位置
          onPress={() => { this._MapView.setStatus(status = { center: this.state.location }, duration = 500) }}
        >
          <Fontisto
            name={'radio-btn-active'}
            size={25}
            style={{ color: 'rgb(70,118,255)' }}
          />
        </TouchableOpacity>
        <View style={{
          backgroundColor: 'rgba(255,255,255,.9)',
          height: 90, width: 45, position: 'absolute',
          right: 15, bottom: 25, borderRadius: 3
        }}>
          <TouchableOpacity
            style={{
              height: 45, justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {   // !点击增加缩放级别
              if (this.zoomLevel < 16) this.zoomLevel++
              this._MapView.setStatus(status = { zoomLevel: this.zoomLevel }, duration = 500)
            }}
          >
            <Text style={{ fontSize: 35, color: 'rgb(70,118,255)' }}> + </Text>
          </TouchableOpacity>
          <View style={{ width: 35, alignSelf: 'center', backgroundColor: '#ddd', height: 1 }} />
          <TouchableOpacity
            style={{
              height: 45, alignItems: 'center',
              justifyContent: 'center'
            }}
            onPress={() => {  // !点击减少缩放级别
              if (this.zoomLevel > 6) this.zoomLevel--
              this._MapView.setStatus(status = { zoomLevel: this.zoomLevel }, duration = 500)
            }}
          >
            <Text style={{ fontSize: 40, color: 'rgb(70,118,255)', lineHeight: 45 }}> - </Text>
          </TouchableOpacity>
        </View>
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={{ position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" />
      </SafeAreaViewPlus>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,

  },
  mapView: {
    flex: 1,
    paddingHorizontal: 15,
    paddingBottom: 15
  }
});