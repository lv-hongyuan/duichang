/**
 * 我的运单和我的订单点击详情cell
 */
import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text, TextInput, Switch,ScrollView } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import GlobalStyles from '../../common/GlobalStyles'
import { Input } from 'react-native-elements';
import  Checkbox  from '@ant-design/react-native/lib/checkbox'


export default class BookSpaceShowCellsUtil {

    /**
     *         左边文字带选择
     * @param {*} title          左侧文字
     * @param {*} isShowDetail   是否显示向下箭头
     * @param {*} callBack       回调
     */
    static renderOutCell(title, isShowDetail, callBack) {
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
            onPress={callBack}
        >
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: 'black' }}>{title}</Text>
            <Ionicons
                name={isShowDetail ? 'ios-arrow-down' : 'ios-arrow-forward'}
                size={20}
                style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5 }}
            />
        </TouchableOpacity>
    }

    /**
     *          左右文字带选择
     * @param {*} cellColor          cell背景色
     * @param {*} leftTitle          左边文字
     * @param {*} rightTitle         右边文字
     * @param {*} rightTitleColor    右边文字颜色
     */
    static renderTextChooseCell(cellColor, leftTitle, rightTitle, rightTitleColor,isSelectable) {
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
        >
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>            
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: rightTitleColor }}
            selectable={isSelectable}
            >{rightTitle}</Text>
                
        </TouchableOpacity>
    }

    /**
     *          右边switch选择
     * @param {*} cellColor         背景颜色
     * @param {*} leftTitle          左边文字
     * @param {*} switchValue        switch的值
     */
    static renderSwitchCell(cellColor, leftTitle, switchValue) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}>
            
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            
            <Switch
                style={{ marginTop: 10, marginRight: 5 }}
                trackColor={{ true: GlobalStyles.nav_bar_color }}
                value={switchValue}
                // thumbColor={'white'}
                thumbColor={switchValue ? 'white' : GlobalStyles.separate_line_color}
                disabled={true}
            />
        </View>
    }

    /**
     *          左边两个文字
     * @param {*} cellColor      背景颜色
     * @param {*} leftTitle       左边文字
     * @param {*} defaultVal      默认值
     */
    static renderDoubleTextCell(cellColor, leftTitle, defaultVal) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }}>
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            <Text style={{ marginLeft: 30, height: 50, paddingRight: 30, lineHeight:50,fontSize:17,color:'#354953'}}>{defaultVal}</Text>    
        </View>
    }

    /**
     *          左边文字固定长度
     * @param {*} cellColor      背景颜色
     * @param {*} leftTitle       左边文字
     * @param {*} defaultVal      默认值
     */
    static renderMarginleftInputCell(cellColor, leftTitle, leftTitleWidth, defaultVal) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }} >
            <Text style={{ height: 50, lineHeight: 50, width: leftTitleWidth, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            {/* <Text style={{ marginLeft: 30, height: 50, paddingRight: 30, lineHeight:50,fontSize:17,color:'black'}}>{defaultVal}</Text>  */}
            <ScrollView
                style={{marginLeft: 30}}
                bounces={false}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
            >
             <Text style={{  height: 50, paddingRight: 30, lineHeight:50,fontSize:17,color:'#354953'}}>{defaultVal}</Text> 
            </ScrollView>
        </View>
    }

    /**
     *      左侧文字颜色可变
     * @param {*} cellColor     背景颜色
     * @param {*} title         左侧文字
     * @param {*} titleColor    左侧文字颜色
     */
    static renderLeftTitleColorChooseCell(cellColor, title, titleColor) {
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
        >    
            <Text style={{ height: 50,lineHeight: 50, fontSize: 17, color: titleColor }}>{title}</Text>
        </TouchableOpacity>
    }

    /**
     *          费用信息总价cell
     * @param {*} totalCost    总价
     * @param {*} calcuBack    计费回调
     * @param {*} detailBack   明细回调
     */
    static renderTotalCostCell(totalCost, isShowCalcu,calcuBack, isShowDetail,detailBack) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: '#FFF5F5', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: 'black' }}>{'总价'}</Text>
                <Text style={{ marginLeft: 20, height: 50, lineHeight: 50, fontSize: 17, color: GlobalStyles.nav_bar_color }}>{totalCost}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                {isShowCalcu?<TouchableOpacity
                    style={{ marginTop: 12.5, width: 50, height: 25, backgroundColor: 'white', borderColor: GlobalStyles.nav_bar_color, borderWidth: 1, borderRadius: 3 }}
                    onPress={calcuBack}
                >
                    <Text style={{ height: 25, lineHeight: 25, width: 50, textAlign: 'center', fontSize: 14, color: GlobalStyles.nav_bar_color }}>{'计费'}</Text>
                </TouchableOpacity>:null}
                {isShowDetail?<TouchableOpacity
                    style={{ marginLeft: 20, marginTop: 10, height: 30, flexDirection: 'row' }}
                    onPress={detailBack}
                >
                    <Text style={{ height: 30, lineHeight: 30, fontSize: 16, color: '#0877DE' }}>{'明细'}</Text>
                    <Ionicons
                        name={'ios-arrow-forward'}
                        size={20}
                        style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5, marginLeft: 5 }}
                    />
                </TouchableOpacity>:null}
            </View>
        </View >
    }

    static renderCheckBoxCell(cellColor, leftTitle, leftTitleWidth, isChecked) {
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
        >
            <Text style={{ height: 50,lineHeight: 50, fontSize: 17, width: leftTitleWidth, color: 'black' }}>{leftTitle}</Text>
            <Checkbox
                checked={isChecked}
                disabled={true}
                style={{ color:'#BC1920', alignSelf: 'center', marginRight: 5, marginLeft: 5 }}
            />

        </TouchableOpacity>
    }

}