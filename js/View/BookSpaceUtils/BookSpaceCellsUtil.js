/**
 * 订舱的各种cell
 */
import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text, TextInput, Switch, Image,  Keyboard } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import GlobalStyles from '../../common/GlobalStyles'
import { Input } from 'react-native-elements';
import Checkbox from '@ant-design/react-native/lib/checkbox'


export default class BookSpaceCellsUtil {

    /**
     *         左边文字带选择
     * @param {*} title          左侧文字
     * @param {*} isShowDetail   是否显示向下箭头
     * @param {*} callBack       回调
     */
    static renderOutCell(title, isShowDetail, callBack) {
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
            onPress={callBack}
        >
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: 'black' }}>{title}</Text>
            <Ionicons
                name={isShowDetail ? 'ios-arrow-down' : 'ios-arrow-forward'}
                size={20}
                style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5 }}
            />
        </TouchableOpacity>
    }

    /**
     *          左右文字带选择
     * @param {*} cellColor          cell背景色
     * @param {*} leftTitle          左边文字
     * @param {*} rightTitle         右边文字
     * @param {*} rightTitleColor    右边文字颜色
     * @param {*} callBack           回调
     */
    static renderTextChooseCell(isStar, cellColor, leftTitle, rightTitle, rightTitleColor, callBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
            onPress={callBack}
        >
            <View style={{ flexDirection: 'row' }}>
                {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
                <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: rightTitleColor }}>{rightTitle}</Text>
                <Ionicons
                    name={'ios-arrow-forward'}
                    size={20}
                    style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5, marginLeft: 10 }}
                />
            </View>
        </TouchableOpacity>
    }

    /**
     *          右边switch选择
     * @param {*} cellColor         背景颜色
     * @param {*} leftTitle          左边文字
     * @param {*} switchValue        switch的值
     * @param {*} callBack           switch的点击回调
     */
    static renderSwitchCell(isStar, cellColor, leftTitle, switchValue, callBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
                {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
                <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            </View>
            <Switch
                style={{ marginTop: 10, marginRight: 5 }}
                trackColor={{ true: GlobalStyles.nav_bar_color }}
                value={switchValue}
                // thumbColor={'white'}
                thumbColor={switchValue ? 'white' : GlobalStyles.separate_line_color}
                onValueChange={(val) => {
                    callBack(val)
                }}
            />
        </View>
    }

    /**
     *          左边文字带输入
     * @param {*} cellColor      背景颜色
     * @param {*} leftTitle       左边文字
     * @param {*} placeHolder     hint
     * @param {*} defaultVal      默认值
     * @param {*} textCallBack    文字回调
     * @param {*} endCallBack     输入完成回调
     */
    static renderTextInputCell(cellColor, leftTitle, placeHolder, defaultVal, textCallBack, endCallBack) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }}>
            <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            <TextInput
                style={{ marginLeft: 30, height: 50, paddingRight: 30, paddingTop: 0, paddingBottom: 0, minWidth: 200, color:'#354953'}}
                placeholder={placeHolder}
                defaultValue={defaultVal}
                placeholderTextColor={'#999999'}
                fontSize={17}
                clearButtonMode={'while-editing'}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
        </View>
    }

    /**
     *          左边文字固定长度,textinput竖直对齐
     * @param {*} cellColor      背景颜色
     * @param {*} leftTitle       左边文字
     * @param {*} placeHolder     hint
     * @param {*} defaultVal      默认值
     * @param {*} textCallBack    文字回调
     * @param {*} endCallBack     输入完成回调
     */
    static renderMarginleftInputCell(isStar, cellColor, leftTitle, leftTitleWidth, placeHolder, editable, inputType, defaultVal, textCallBack, endCallBack, showclear = true) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }} >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
            <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, width: leftTitleWidth, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            <TextInput
                style={{ height: 50, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 150, color:'#354953'}}
                placeholder={placeHolder}
                defaultValue={defaultVal}
                placeholderTextColor={'#999999'}
                clearButtonMode={showclear ? 'while-editing' : 'never'}
                keyboardType={inputType}
                fontSize={17}
                editable={editable}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
        </View>
    }

    /**
     * 
     * @param {*} isStar            星星
     * @param {*} cellColor         背景颜色
     * @param {*} leftTitle         左边文字
     * @param {*} leftTitleWidth    左边文字宽度
     * @param {*} placeHolder       hint
     * @param {*} editable          textinput是否可编辑
     * @param {*} inputType         textinputType
     * @param {*} defaultVal        默认值
     * @param {*} textCallBack      输入回调
     * @param {*} endCallBack       输完回调
     * @param {*} btnCallBack       右侧按钮回调
     */
    static renderMarginleftInputCellRightBtn(isStar, cellColor, leftTitle, leftTitleWidth, placeHolder, editable, inputType, defaultVal, textCallBack, endCallBack, btnCallBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }} >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
            <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, width: leftTitleWidth, fontSize: 17, color: '#666' }}>{leftTitle}</Text>
            <TextInput
                style={{ height: 50, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 190, color:'#354953'}}
                placeholder={placeHolder}
                defaultValue={defaultVal}
                placeholderTextColor={'#999999'}
                clearButtonMode={'while-editing'}
                keyboardType={inputType}
                fontSize={17}
                editable={editable}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
            <TouchableOpacity
                style={{ marginTop: 13, width: 40, height: 30, paddingRight: 10 }}
                activeOpacity={0.75}
                onPress={() => {
                    btnCallBack()
                }}
            >
                <Image style={{ width: 25, height: 25 }} source={require('../../../resource/contactBtn.png')} />
            </TouchableOpacity>
        </View>
    }

    /**
     *      左侧文字颜色可变
     * @param {*} cellColor     背景颜色
     * @param {*} title         左侧文字
     * @param {*} titleColor    左侧文字颜色
     * @param {*} callBack      cell点击回调
     */
    static renderLeftTitleColorChooseCell(isStar, cellColor, title, titleColor, callBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
            onPress={callBack}
        >
            <View style={{ flexDirection: 'row' }}>
                {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
                <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, fontSize: 17, color: titleColor }}>{title}</Text>
            </View>

            <Ionicons
                name={'ios-arrow-forward'}
                size={20}
                style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5 }}
            />
        </TouchableOpacity>
    }

    static renderScreenWidthButton(cellColor, title, btnCallBack) {
        return <TouchableOpacity
            style={{ width: GlobalStyles.screenWidth, height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, alignItems: 'center', justifyContent: 'center' }}
            onPress={btnCallBack}
            activeOpacity={0.75}
        >
            <View style={{ width: '65%', height: 35, backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 3, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ height: 40, fontSize: 16, lineHeight: 40, textAlign: 'center', color: 'white' }}>{title}</Text>
            </View>
        </TouchableOpacity>
    }

    /**
     *        纯输入
     * @param {*} cellColor       背景颜色
     * @param {*} placeHolder     hint
     * @param {*} defaultVal       默认值
     * @param {*} textCallBack      文字回调 
     * @param {*} endCallBack       输入完回调
     */
    static renderPureTextInputCell(isStar, cellColor, placeHolder, defaultVal, textCallBack, endCallBack, rightbutton = false,rightButtonCall) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }} >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
            <TextInput
                style={{ 
                    height: 50, marginLeft: titleMarginLeft, 
                    paddingTop: 0, paddingBottom: 0, 
                    width: rightbutton ? GlobalStyles.screenWidth - 70 - titleMarginLeft : GlobalStyles.screenWidth - 30 - titleMarginLeft,
                    color:'#354953' 
                }}
                placeholder={placeHolder}
                defaultValue={defaultVal}
                placeholderTextColor={'#666'}
                clearButtonMode={'while-editing'}
                fontSize={17}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
            {rightbutton ? <TouchableOpacity
                style={{
                flexDirection: 'row',
                marginRight: 5,
                width:40,height:50,
                justifyContent:'center',
                alignItems:'center',
                paddingRight:5,
                }}
                onPress={() => {
                    Keyboard.dismiss() 
                    rightButtonCall()
                }}
                >
                <Image style={{width:25,height:28}} source={require('../../../resource/MapR.png')}/>
            </TouchableOpacity>: null}
        </View>
    }

    /**
     *          左侧文字带输入和选择
     * @param {*} cellColor           背景颜色
     * @param {*} leftTitle            左边文字
     * @param {*} placeHolder         hint
     * @param {*} defaultVal          默认值
     * @param {*} textCallBack        文字回调
     * @param {*} endCallBack         输入完回调
     * @param {*} chooseCallBack      选择回调
     */
    static renderTextInputChooseCell(cellColor, leftTitle, placeHolder, defaultVal, textCallBack, endCallBack, chooseCallBack) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15 }} >
            <Text style={{ height: 50, lineHeight: 50, width: 120, fontSize: 17, color: 'black' }}>{leftTitle}</Text>
            <TextInput
                style={{ height: 50, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 180 }}
                placeholder={placeHolder}
                defaultValue={defaultVal}
                placeholderTextColor={'#999999'}
                clearButtonMode={'while-editing'}
                fontSize={17}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
            <TouchableOpacity
                style={{ position: 'absolute', right: 0, top: 0, width: 50, height: 50 }}
                onPress={chooseCallBack}
            >
                <Ionicons
                    name={'ios-arrow-forward'}
                    size={20}
                    style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5, marginLeft: 10, marginTop: 15 }}
                />
            </TouchableOpacity>
        </View>
    }

    /**
     *           左右文字带选择
     * @param {*} cellColor           背景颜色           
     * @param {*} leftTitle           左边文字
     * @param {*} rightTitle          右边文字
     * @param {*} rightTitleColor     右边文字颜色
     * @param {*} isChoose            是否有选择的箭头
     * @param {*} callBack            选择箭头的回调
     */
    static renderTextMarginleftChooseCell(isStar, cellColor, leftTitle, leftTitleWidth, rightTitle, rightTitleColor, isChoose, callBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
            onPress={callBack}
        >
            <View style={{ flexDirection: 'row' }}>
                {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
                <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, fontSize: 17, width: leftTitleWidth, color: '#666' }}>{leftTitle}</Text>
                <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: rightTitleColor }}>{rightTitle}</Text>
            </View>
            {isChoose ? <Ionicons
                name={'ios-arrow-forward'}
                size={20}
                style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5, marginLeft: 10 }}
            /> : null}

        </TouchableOpacity>
    }

    /**
     *          费用信息总价cell
     * @param {*} totalCost    总价
     * @param {*} calcuBack    计费回调
     * @param {*} detailBack   明细回调
     */
    static renderTotalCostCell(totalCost, calcuBack, isShowDetail, detailBack) {
        return <View style={{ height: 50, flexDirection: 'row', backgroundColor: '#FFF5F5', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ height: 50, lineHeight: 50, fontSize: 17, color: 'black' }}>{'总价'}</Text>
                <Text style={{ marginLeft: 20, height: 50, lineHeight: 50, fontSize: 17, color: GlobalStyles.nav_bar_color }}>{totalCost}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                    style={{ marginTop: 12.5, width: 50, height: 25, backgroundColor: 'white', borderColor: GlobalStyles.nav_bar_color, borderWidth: 1, borderRadius: 3 }}
                    onPress={calcuBack}
                >
                    <Text style={{ height: 25, lineHeight: 25, width: 50, textAlign: 'center', fontSize: 14, color: GlobalStyles.nav_bar_color }}>{'计费'}</Text>
                </TouchableOpacity>
                {isShowDetail ? <TouchableOpacity
                    style={{ marginLeft: 20, marginTop: 10, height: 30, flexDirection: 'row' }}
                    onPress={detailBack}
                >
                    <Text style={{ height: 30, lineHeight: 30, fontSize: 16, color: '#0877DE' }}>{'明细'}</Text>
                    <Ionicons
                        name={'ios-arrow-forward'}
                        size={20}
                        style={{ color: '#CCCCCC', alignSelf: 'center', marginRight: 5, marginLeft: 5 }}
                    />
                </TouchableOpacity> : null}
            </View>
        </View >
    }

    /**
     * 
     * @param {*} isStar                 星号
     * @param {*} cellColor              背景颜色
     * @param {*} leftTitle              左边文字
     * @param {*} leftTitleWidth         文字宽度
     * @param {*} isChecked                CheckBox是否选中
     * @param {*} callBack               CheckBox回调
     */
    static renderCheckBoxCell(isStar, cellColor, leftTitle, leftTitleWidth, isChecked, callBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: 50, backgroundColor: cellColor, paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            activeOpacity={0.7}
        >
            <View style={{ flexDirection: 'row' }}>
                {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 50 }}>*</Text> : null}
                <Text style={{ height: 50, marginLeft: titleMarginLeft, lineHeight: 50, fontSize: 17, width: leftTitleWidth, color: 'black' }}>{leftTitle}</Text>
            </View>
            <Checkbox
                checked={isChecked}
                style={{ color: '#BC1920', alignSelf: 'center', marginRight: 5, marginLeft: 5 }}
                onChange={(event: any) => {
                    callBack(event.target.checked)
                }}
            />

        </TouchableOpacity>
    }

}