import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image, ImageBackground, ScrollView } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUitl from '../../common/ViewUtil'


export default class TransostViewCell extends Component {

    renderMoneyText(money) {
        return <Text style={{ width: GlobalStyles.screenWidth / 6, height: 20, color: '#BC1920', fontSize: 13, fontWeight: '500', lineHeight: 20, textAlign: 'center' }}>{money}</Text>
    }

    renderWeightText(weight) {
        return <View style={{ width: 30, height: 15, backgroundColor: '#E8E8E8' }}>
            <Text style={{ width: 30, height: 15, lineHeight: 15, color: 'black', textAlign: 'center', fontSize: 10 }} >{weight}</Text>
        </View>
    }


    render() {

        const item = this.props
        let data = item.item.item
        let gp20 = data.gp20 == 0 ? '--' : data.gp20
        let gp40 = data.gp40 == 0 ? '--' : data.gp40
        let hc40 = data.hc40 == 0 ? '--' : data.hc40
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Text style={{ marginLeft: 10, fontSize: 9, color: '#909399' }}>预计航程:</Text>
                    <Text style={{ marginLeft: 3, fontSize: 9, color: 'black' }}>{data.voyageSchedule + 'day'}</Text>
                </View>
                {/* <View style={{marginTop:5,justifyContent:'center',alignItems:'center',height:15}}>
                    <Image style={{width:24,height:15}} source={require('../../../resource/ship.png')}/>
                </View> */}
                <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-around' }}>
                    <Text style={{ fontSize: 15, color: '#0877DE' }}>{data.startPort}</Text>
                    <Image style={{ marginTop: -5, width: 60, height: 17 }} source={require('../../../resource/ship-direction.png')} />
                    <Text style={{ fontSize: 15, color: '#0877DE' }}>{data.endPort}</Text>
                </View>
                <Image style={{ marginTop: 10, width: GlobalStyles.screenWidth / 2, height: 1 }} source={require('../../../resource/xu1.png')} />
                <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                    {this.renderMoneyText('￥' + gp20)}
                    {this.renderMoneyText('￥' + gp40)}
                    {this.renderMoneyText('￥' + hc40)}
                </View>
                <View style={{ marginTop: 5, flexDirection: 'row', width: GlobalStyles.screenWidth / 2, justifyContent: 'space-around' }}>
                    {this.renderWeightText('20GP')}
                    {this.renderWeightText('40GP')}
                    {this.renderWeightText('40HC')}
                </View>
                <TouchableOpacity
                    style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'center' }}
                    onPress={() => {
                        //订舱按钮点击事件
                        this.props.bookSpaceBtnClick()
                    }}>
                    <View style={{ width: 100, height: 25, backgroundColor: '#BC1920', borderRadius: 3 }}>
                        <Text style={{ width: 100, height: 25, lineHeight: 25, textAlign: 'center', fontSize: 13, color: 'white' }}>立即订舱</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ width: GlobalStyles.screenWidth / 2, height: 10 }}></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: GlobalStyles.screenWidth / 2,
        backgroundColor: '#FFFBFB',
        borderColor: GlobalStyles.separate_line_color,
        borderWidth: 0.5,
    }
});