/**
 * 运价
 */

import React, { Component } from 'react';
import { NetInfo, Platform, StyleSheet, Text, View, ScrollView, RefreshControl, Image, TextInput, TouchableOpacity, Keyboard, ActivityIndicator, FlatList, DeviceEventEmitter } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import NavigationUtil from '../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import TransostViewCell from './TransCostViewCell';
import NewHomeViewCell from '../Home/NewHomeViewCell';
import ImmediateBookSpace from '../BookSpaceUtils/ImmediateBookSpace'
import publicDao from '../../dao/publicDao'
import NetworkDao from '../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_TRANSCOSTSEARCHSUCESS_TYPE,EMITTER_NETCHANGE_TYPE } from '../../common/EmitterTypes';
import LargeListPlus from '../../common/LargeListPlus';
import AlertViewPlus from '../../common/AlertViewPlus'

export default class TransCostView extends Component {

  constructor(props) {
    super(props)
    this.isNetworkFirst = false
    this.state = {
      cellDataArr: [],
      startPortText: '',     //起运港
      endPortText: '',        //目的港
      indicatorAnimating: false, //菊花
      hideLoadingMore: true,     //是否显示上拉
    }
  }

  loadData(showLoading) {
    if (showLoading) {
      this.setState({ indicatorAnimating: true })
    }
    let para = {}
    para.token = publicDao.CURRENT_TOKEN
    para.startPort = ''
    para.endPort = ''
    para.defaultFlag = '0'
    let paraStr = JSON.stringify(para)
    NetworkDao.fetchPostNet('getFreightListByPort.json', paraStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        !showLoading && this.setState({ endRefresh: 'Refresh' })
        if (data._backCode == '200') {
          this.setState({ cellDataArr: data.entityList })
        } else if (data._backCode == '205') {
          this.refs.toast.show(data._backMesage, 800)
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        !showLoading && this.setState({ endRefresh: 'Refresh' })
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('获取数据失败', 800)
      })
  }

  componentDidMount() {
    this.loadData(true)
    this.searchListenner = DeviceEventEmitter.addListener(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE, () => {
      this.setState({
        startPortText: '',
        endPortText: ''
      })
    })
    this.netTypeChange = DeviceEventEmitter.addListener(EMITTER_NETCHANGE_TYPE, () => {
      console.log('运价联网成功')
      this.isNetworkFirst = true
    })
    this.listenerNetwork = this.props.navigation.addListener(
      'didFocus',
      (obj) => {
        if (this.isNetworkFirst) {
          this.loadData(true)
          this.isNetworkFirst = false
        }
      })
  }

  componentWillUnmount() {
    if (this.searchListenner) {
      this.searchListenner.remove()
    }
    if (this.netTypeChange) {
      this.netTypeChange.remove()
    }
  }

  // 搜索框
  renderTextInput(placeHolder, value, textCallBack, endCallback, btnCallBack) {
    return <View style={{ flexDirection: 'row', marginLeft: 10, width: (GlobalStyles.screenWidth - 70) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
      <TextInput
        style={{ marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: (GlobalStyles.screenWidth - 140) / 2 }}
        fontSize={16}
        placeholder={placeHolder}
        defaultValue={value}
        onChangeText={(text) => {
          textCallBack(text)
        }}
        returnKeyType={'search'}
        onSubmitEditing={() => {
          endCallback()
        }}
      />
      <TouchableOpacity
        style={{ width: 30, height: 35 }}
        onPress={() => {
          btnCallBack()
        }}>
        <Image style={{ marginTop: 10, width: 15, height: 15 }} source={require('../../../resource/zoomer-g.png')} />
      </TouchableOpacity>
    </View>
  }

  //上方搜索
  renderTopSearchView() {
    return <View style={{ width: GlobalStyles.screenWidth, height: 120 }}>
      <Image style={{ height: 70, width: 67 }} source={require('../../../resource/rudder.png')} />
      <View style={{ marginLeft: 5, marginTop: -55, flexDirection: 'row' }}>
        {this.renderTextInput('起运港', this.state.startPortText, (text) => {
          this.setState({ startPortText: text })
        }, () => {
          let dic = {}
          dic.startPortText = this.state.startPortText
          dic.endPortText = this.state.endPortText
          NavigationUtil.goPage(dic, 'TransCostSearhResultView')
        }, () => {
          //跳转到选择页面
          Keyboard.dismiss()
          this.props.navigation.navigate('TransCostPortSearchView', {
            backData: (data) => {
              this.setState({ startPortText: data })
            }
          })
        })}
        <TouchableOpacity onPress={() => {
          //起运港目的港互换
          let a = this.state.startPortText
          let b = this.state.endPortText
          let c = [a, b]
          b = c[0]
          a = c[1]
          this.setState({ startPortText: a, endPortText: b })

        }}>
          <Image style={{ marginLeft: 10, marginTop: 7.5, width: 20, height: 20 }}
            source={require('../../../resource/direction1.png')}
          />
        </TouchableOpacity>

        {this.renderTextInput('目的港', this.state.endPortText, (text) => {
          this.setState({ endPortText: text })
        }, () => {
          let dic = {}
          dic.startPortText = this.state.startPortText
          dic.endPortText = this.state.endPortText
          NavigationUtil.goPage(dic, 'TransCostSearhResultView')
        }, () => {
          Keyboard.dismiss()
          this.props.navigation.navigate('TransCostPortSearchView', {
            backData: (data) => {
              this.setState({ endPortText: data })
            }
          })
        })}
      </View>
      <TouchableOpacity style={{ marginTop: 15, marginLeft: 15, width: GlobalStyles.screenWidth - 30, height: 35, borderRadius: 17.5, backgroundColor: GlobalStyles.nav_bar_color }}
        onPress={() => {
          console.log('搜索按钮点击事件')
          let dic = {}
          dic.startPortText = this.state.startPortText
          dic.endPortText = this.state.endPortText
          NavigationUtil.goPage(dic, 'TransCostSearhResultView')
        }}
      >
        <Text style={{ width: GlobalStyles.screenWidth - 30, height: 35, lineHeight: 35, textAlign: 'center', color: 'white', fontSize: 16 }}>{'搜  索'}</Text>
      </TouchableOpacity>
    </View>
  }

  //cell
  renderItem = ({ row: row }) => {
    let item = this.state.cellDataArr[row]
    return <View>
      <NewHomeViewCell
        item={item}
        bookSpaceBtnClick={() => {
          // this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
          NavigationUtil.goPage(item, 'ImmediateBookSpace')
        }}
      />
    </View>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'运价'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color} bottomInset={false}>
        {navigationBar}
        <Toast ref='toast' position="center" />
        <View style={{ flex: 1 }}>
          {this.renderTopSearchView()}
          <LargeListPlus
            // style={{height:'100%'}}
            endRefresh={this.state.endRefresh}
            data={this.state.cellDataArr}
            onRefresh={() => { this.loadData(false) }}
            renderItem={this.renderItem}
            NoDataViewheight={212}
            showIndicator={false}
          />
          
        </View>
        <AlertViewPlus
          ref="AlertView"
          ShowCancelButton={false}
          title='温馨提示'
          message='由于客户端升级，暂时关闭订舱功能。'
          alertSureDown={()=>{}}
        />
        {this.state.indicatorAnimating && (
            <ActivityIndicator
              style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 + 120, left: GlobalStyles.screenWidth / 2 - 10 }]}
              size={GlobalStyles.isIos ? 'large' : 40}
              color='gray'
            />)}
      </SafeAreaViewPlus>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  bottomBackView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  bottomCell: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
