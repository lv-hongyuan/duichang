/**
 * 搜索结果页面
 */
import React, { Component } from 'react';
import { BackHandler, Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, FlatList, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import NavigationUtil from '../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import ViewUtil from '../../common/ViewUtil'
// import NoDataModal from '../../common/NoDataModal'
import NoDataView from '../../common/NoDataView'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_TRANSCOSTSEARCHSUCESS_TYPE } from '../../common/EmitterTypes'
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
let countDown = 30

export default class TransCostSearhResultView extends Component {
  constructor(props) {
    super(props)
    this.params = this.props.navigation.state.params;
    this.startRow = 0
    this.state = {
      dataArr: [],
      isNoData: false,
      indicatorAnimating: false, //菊花
      hideLoadingMore: true,     //是否显示上拉
      largeListData: [],           //大列表数据
      numbers: countDown
    }
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  getSearchData(iscountDown) {
    this.setState({ indicatorAnimating: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.startPort = this.params.startPortText
    params.endPort = this.params.endPortText
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getFreightListByPort.json', paramsStr)
      .then(data => {
        if (iscountDown) {
          this.timer && clearInterval(this.timer)
          let aa = countDown
          //开始倒计时
          this.timer = setInterval(() => {
            aa--
            this.setState({ numbers: aa })
            if (aa == 0) {
              this.timer && clearInterval(this.timer)
              this.setState({
                numbers: countDown
              })
              aa = countDown
            }
          }, 100);
        }
        DeviceEventEmitter.emit(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE)
        this.setState({ indicatorAnimating: false })
        if (this._largelist) this._largelist.endRefresh()
        if (data._backCode == '200') {
          let tempA = this.handleLargeListData(data.entityList)
          this.setState({ largeListData: tempA, dataArr: data.entityList, isNoData: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '205') {
          this.setState({ dataArr: [], isNoData: true, largeListData: [] })
        }
      })
      .catch(error => {
        console.log(error);
        this.timer && clearInterval(this.timer)
        if (this._largelist) this._largelist.endRefresh()
        this.setState({ indicatorAnimating: false, dataArr: [], isNoData: true, largeListData: [], numbers: countDown })
        this.refs.toast.show('搜索失败', 800)
      })
  }

  //上拉加载更多数据
  loadMoreData(dic) {
    this.setState({ hideLoadingMore: false })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.startPort = this.params.startPortText
    params.endPort = this.params.endPortText
    params.startRow = ++this.startRow * 30
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getFreightListByPort.json', paramsStr)
      .then(data => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        if (data._backCode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.entityList && data.entityList.length > 0) {
            tempArr.push(...data.entityList)
            let tempA = this.state.largeListData.concat()
            let tempB = data.entityList
            let tempC = tempA[0].items
            tempB.forEach(element => {
              tempC.push(element)
            });
            this.setState({ dataArr: tempArr, largeListData: tempA })
          }

        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '205') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        this.refs.toast.show('搜索失败', 800)
      })
  }

  componentDidMount() {
    this.getSearchData(false)
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      DeviceEventEmitter.emit(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE)
      NavigationUtil.goBack(this.props.navigation)
      return true;
    });
  }
  componentWillUnmount() {
    this.backHandler.remove();
    this.timer && clearInterval(this.timer)
  }

  leftButtonClick() {
    DeviceEventEmitter.emit(EMITTER_TRANSCOSTSEARCHSUCESS_TYPE)
    NavigationUtil.goBack(this.props.navigation)
  }


  //cell之间的分割线
  separatorView() {
    return <View style={{ height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
  }

  // renderItem(data) {
  _renderItem = ({ row: row }) => {
    // let item = data.item
    let item = this.state.dataArr[row]
    let gp20 = item.gp20 == 0 ? '--' : item.gp20
    let gp40 = item.gp40 == 0 ? '--' : item.gp40
    let hc40 = item.hc40 == 0 ? '--' : item.hc40
    return <View style={{
      flexDirection: 'row', height: 95,
      alignItems: 'center', backgroundColor: 'white', padding: 7,
      borderBottomColor: '#ddd', borderBottomWidth: 0.5
    }}>
      <View style={{ flex: 1, justifyContent: 'space-around' }}>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
          <Text numberOfLines={1} style={{ fontSize: 16, color: '#0877DE', width: 82, fontWeight: '500', textAlign: 'center' }}>{item.startPort}</Text>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 0 }}>
            <Image style={{ width: 95, height: 6 }} source={require('../../../resource/direction.png')} />
            {/* <View style={{ flexDirection: 'row', marginTop: 3 }}>
              <Image style={{ marginTop: 1.5, width: 10, height: 10 }} source={require('../../../resource/time.png')} />
              <Text style={{ marginLeft: 5, fontSize: 10, color: '#778087' }}>{item.voyageSchedule + 'day'}</Text>
            </View> */}
          </View>
          <Text numberOfLines={1} style={{ fontSize: 16, color: '#0877DE', width: 82, textAlign: 'center' }}>{item.endPort}</Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5 }}>
          <View>
            <View style={{ flexDirection: 'row', height: 22.5, width: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: GlobalStyles.nav_bar_color, fontSize: 13 }}>￥</Text>
              <Text style={{ fontSize: 18, color: GlobalStyles.nav_bar_color, fontWeight: '500' }}>{gp20}</Text>
            </View>
            <Text style={{ color: '#778087', textAlign: 'center', fontSize: 12, width: 80 }}>{'20GP'}</Text>
          </View>

          <View>
            <View style={{ flexDirection: 'row', height: 22.5, width: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: GlobalStyles.nav_bar_color, fontSize: 13 }}>￥</Text>
              <Text style={{ fontSize: 18, color: GlobalStyles.nav_bar_color, fontWeight: '500' }}>{gp40}</Text>
            </View>
            <Text style={{ color: '#778087', textAlign: 'center', fontSize: 12, width: 80 }}>{'40GP'}</Text>
          </View>

          <View>
            <View style={{ flexDirection: 'row', height: 22.5, width: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: GlobalStyles.nav_bar_color, fontSize: 13 }}>￥</Text>
              <Text style={{ fontSize: 18, color: GlobalStyles.nav_bar_color, fontWeight: '500' }}>{hc40}</Text>
            </View>
            <Text style={{ color: '#778087', textAlign: 'center', fontSize: 12, width: 80 }}>{'40HC'}</Text>
          </View>

        </View>
      </View>
      <TouchableOpacity
        style={{
          width: 78, height: 36, backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 5,
          justifyContent: 'center', alignItems: 'center', marginLeft: 10
        }}
        activeOpacity={0.7}
        onPress={() => {
          //立即订舱按钮点击事件
          NavigationUtil.goPage(item, 'ImmediateBookSpace')
        }}
      >
        <Text style={{ fontSize: 15, color: 'white', fontWeight: '700' }}>{'立即订舱'}</Text>
      </TouchableOpacity>
    </View>
  }

  render() {
    let title = ''
    if (this.params.startPortText && this.params.endPortText) {
      title = this.params.startPortText + ' - ' + this.params.endPortText
    } else if (this.params.startPortText && !this.params.endPortText) {
      title = this.params.startPortText
    } else if (!this.params.startPortText && this.params.endPortText) {
      title = this.params.endPortText
    } else {
      title = '运价'
    }
    let navigationBar = <NavigationBar
      title={title}
      // title={title}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" />
        {this.state.isNoData ? NoDataView.renderContent(40) : null}
        {this.state.isNoData ?
          (this.state.numbers < countDown ? <TouchableOpacity
            style={{ flex: 1 }}
            activeOpacity={1}
            onPressIn={(event) => {
              this.startT = event.nativeEvent.locationY
            }}
            onPressOut={(event) => {
              if ((event.nativeEvent.locationY - this.startT) > 60) {
                this.refs.toast.show('刷新过于频繁,请于' + Math.ceil(this.state.numbers / 10) + '秒后重试', this.state.numbers * 100)
              }
            }}
          /> : <LargeList
              ref={ref => (this._largelist = ref)}
              style={{ flex: 1 }}
              data={this.state.largeListData}
              heightForIndexPath={() => 95}
              renderIndexPath={this._renderItem}
              refreshHeader={ChineseWithLastDateHeader}
              onRefresh={() => {
                this.getSearchData(true)
              }}
            />) :
          (<LargeList
            ref={ref => (this._largelist = ref)}
            style={{ flex: 1 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 95}
            renderIndexPath={this._renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            onRefresh={() => {
              this.getSearchData(true)
            }}
            loadingFooter={ChineseWithLastDateFooter}
            onLoading={() => {
              this.loadMoreData();
            }}
          />)}
      </SafeAreaViewPlus>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  }
});