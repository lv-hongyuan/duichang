/**
 * 运价港口搜索页面
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import NavigationUtil from '../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import Ionicons from 'react-native-vector-icons/Ionicons'
import cityContent from '../../../resource/city.json'
import BookInfoDao from '../../dao/BookInfoDao'
import publicDao from '../../dao/publicDao'
import NetworkDao from '../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../common/NoDataView'
import DeviceInfo from 'react-native-device-info'


const CITYHEIGHT = 35;      //列表数据高度
const TITLEHEIGHT = 40;
const DESCHEIGHT = 40;
const CITYHEIGHT2 = 30;     //常用城市高度

export default class TransCostPortSearchView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            sections: [],
            cityArray: [],
            cityBase: [],
            showFlag: true,
            indicatorAnimating: false, //菊花
            isNoData: false,
            searchText: '',
        }

        // 特殊栏目的数量 （定位，热门，常用）
        descNumber = 0;
        // 和上面对应的item 的数量。三个的总数
        city2number = 0;
        // 每个栏目前面城市的数量
        titleCityArray = [0],
            // 右侧导航中 特殊栏目的数量。
            this.letterDescNumber = 0;

        this._moveAction = this._moveAction.bind(this);
        this._getItemLayout = this._getItemLayout.bind(this);
    }

    leftBtnClick() {
        this.refs.input.blur(); //回收键盘
        NavigationUtil.goBack(this.props.navigation);
    }

    handleData(data, mostData) {
        //初始化数据
        // let cityContent2 = cityContent.data;    //json文件中的data数据
        let cityContent2 = data.list;
        console.log('data is :', cityContent2)
        console.log('mostdata is :', mostData)
        let letterList = [];
        let cityArray = [];
        let sections = [];
        this.city2number = 0;
        this.descNumber = 0;
        this.titleCityArray = [0];
        // sections[sections.length] = {
        //     name: '常用城市',
        //     type: 'desc',                       //desc是左边title
        // };

        // sections[sections.length] = {
        //     name: '大连,广州',
        //     type: 'city2',                      //city2是显示的数据
        // };

        sections[sections.length] = {
            name: '常用港口',
            type: 'desc',                           //desc是左边title
        };

        let len = mostData.length
        for (let i = 0; i < len; i += 3) {
            let tempN = ''
            let tempD = mostData[i]
            tempN = tempD.portName
            if (i + 1 < len) {
                let tempD2 = mostData[i + 1]
                tempN = tempN + ',' + tempD2.portName
            }
            if (i + 2 < len) {
                let tempD3 = mostData[i + 2]
                tempN = tempN + ',' + tempD3.portName
            }

            sections[sections.length] = {
                name: tempN,
                type: 'city2',                          //city2是显示的数据
            };

        }

        // letterList.splice(0, 0, '定位', '常用', '热门');
        letterList.splice(0, 0, '常用');
        this.letterDescNumber = letterList.length;
        sections.forEach(element => {
            if (element.type != 'desc') {
                this.city2number++;
            } else {
                this.descNumber++;
            }
        });
        let i = 0;
        cityContent2.forEach(element => {
            sections[sections.length] = {
                // name: element.title,
                name: element.letter,
                type: 'letter',                 //letter:26个字母,ABCD...
            };

            element.children.forEach(element => {
                sections[sections.length] = {
                    name: element.name,
                    type: 'city',               //city,json文件中的城市,city_child
                }
                i++;
            })
            this.titleCityArray[this.titleCityArray.length] = i;
            letterList[letterList.length] = element.letter;  //添加右边导航(ABCD...)
        });

        // json文件中提出可用数据,查找时使用该数据
        let key = 0;
        cityArray = [];
        cityContent2.forEach(element => {
            element.children.forEach(element => {
                cityArray[cityArray.length] = {
                    'name': element.name,
                    'name_en': element.qc,
                    'key': key++,
                    'jc': element.jc,
                }
            });



        });
        this.setState({
            sections: sections,       //所有的数据
            listData: letterList,     //常用,热门,ABCD..
            cityBase: cityArray,      //JSON文件的数据
            searchArray: cityArray,
        }, () => {
            // console.log('sections==>>', sections)
            // console.log('listData==>>', letterList)
            // console.log('cityBase==>>', cityArray)
        });
    }

    //获取ABCD..的数据
    getData() {
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        console.log('token is ', publicDao.CURRENT_TOKEN)
        let paramStr = JSON.stringify(param)
        this.setState({ indicatorAnimating: true })
        NetworkDao.fetchPostNet('listAllPortInfo.json', paramStr)
            .then(data => {
                // this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    BookInfoDao.PortInfo = data
                    // this.handleData(data)
                    this.getMostData(data)
                } else if (data._backcode == '224') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                this.refs.toast.show('获取港口数据失败', 800, () => {
                    this.leftBtnClick()
                })
            })
    }

    getMostData(listData) {
        if (BookInfoDao.MostCity && BookInfoDao.MostCity.length > 0) {
            this.handleData(listData, BookInfoDao.MostCity)
            return
        }
        let param = {}
        param.token = publicDao.CURRENT_TOKEN
        console.log('token is ', publicDao.CURRENT_TOKEN)
        let paramStr = JSON.stringify(param)
        this.setState({ indicatorAnimating: true })
        NetworkDao.fetchPostNet('getUsedPort.json', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backCode == '200') {
                    BookInfoDao.MostCity = data.entityList
                    this.handleData(listData, data.entityList)
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                this.refs.toast.show('获取港口数据失败', 800, () => {
                    this.leftBtnClick()
                })
            })
    }

    componentDidMount() {
        if (BookInfoDao.PortInfo && BookInfoDao.PortInfo.list) {
            this.handleData(BookInfoDao.PortInfo, BookInfoDao.MostCity)
        } else {
            this.getData()
        }

    }


    //查找
    onChangeText(text) {
        let Chinese = new RegExp('^[\u4e00-\u9fa5]+$');
        let English = new RegExp('^[a-zA-Z]+$');
        if (Chinese.test(text)) {
            let temp = [];

            this.state.cityBase.forEach(city => {
                if (city.name.indexOf(text) == 0) {
                    temp[temp.length] = city;
                }
            });
            if (temp && temp.length > 0) {
                this.setState({
                    searchArray: temp,
                    showFlag: false,
                    isNoData: false
                })
            } else {
                this.setState({
                    searchArray: temp,
                    showFlag: false,
                    isNoData: true
                })
            }

        } else if (English.test(text)) {
            // text = text.toLowerCase();
            text = text.toUpperCase();
            let temp = [];
            this.state.cityBase.forEach(city => {
                if (city.name_en.indexOf(text) == 0) {
                    temp[temp.length] = city;
                } else if (city.jc.indexOf(text) == 0) {
                    temp[temp.length] = city;
                }
            });
            if (temp && temp.length > 0) {
                this.setState({
                    searchArray: temp,
                    showFlag: false,
                    isNoData: false
                })
            } else {
                this.setState({
                    searchArray: temp,
                    showFlag: false,
                    isNoData: true
                })
            }

        } else if (text.length == 0) {
            this.setState({
                searchArray: this.state.cityBase,
                showFlag: true
            });
        } else {
            this.setState({
                searchArray: [],
                showFlag: false
            });
        }
    }

    _flatRenderItem = (info) => {
        // console.log('info is ', info)
        return (
            <TouchableOpacity onPress={() => this._moveAction(info)} style={{ width: CITYHEIGHT, alignItems: 'center', height: 20 }}>
                <Text>{info.item}</Text>
            </TouchableOpacity>
        )
    }

    _searchRenderItem = (info) => {
        return (
            <TouchableOpacity
                style={{ flexDirection: 'row', paddingLeft: 15, width: GlobalStyles.screenWidth, height: CITYHEIGHT, alignItems: 'center' }}
                onPress={() => {
                    this.props.navigation.state.params.backData(info.item.name);
                    this.props.navigation.goBack();
                }}
            >
                <Text style={{ fontSize: 16 }}>{info.item.name}</Text>
            </TouchableOpacity>
        )
    }

    _moveAction(info) {
        let item = info.item.length == 1 ? info.item : info.item + '城市';
        for (let i = 0; i < this.state.sections.length; i++) {
            if (this.state.sections[i].name == item) {
                this.refs.FlatList.scrollToIndex({ animated: false, index: i });
                break;
            }
        }
    }

    _renderItem = (info) => {
        var txt = '  ' + info.item.name;
        switch (info.item.type) {
            case 'city': {             //json文件中的城市数据
                return (
                    <TouchableOpacity
                        style={{ height: CITYHEIGHT, width: GlobalStyles.screenWidth, color: 'black', backgroundColor: '#FFFFFF', paddingLeft: 15, borderBottomColor: '#F8F8F8', borderBottomWidth: 1 }}
                        onPress={() => {
                            console.log('选中的城市:', txt)
                            this.props.navigation.state.params.backData(info.item.name);
                            this.props.navigation.goBack();
                        }}
                    >
                        <Text
                            style={{
                                height: CITYHEIGHT,
                                width: GlobalStyles.screenWidth - 70,
                                // textAlignVertical: 'center',
                                // backgroundColor: "#ffffff", 
                                color: 'black',
                                fontSize: 16,
                                borderBottomColor: 'rgb(161,161,161)',
                                // borderBottomWidth: 1,
                                lineHeight: CITYHEIGHT,
                            }}
                        >
                            {txt}
                        </Text>
                    </TouchableOpacity>


                )
            }
            case 'letter': {         //左边26个字母,ABCD...
                return (
                    <Text
                        style={{
                            height: TITLEHEIGHT,
                            width: GlobalStyles.screenWidth - 70,
                            textAlignVertical: 'center',
                            //backgroundColor: "#0f0", 
                            color: 'black',
                            fontSize: 16,
                            borderBottomColor: 'rgb(161,161,161)',
                            // borderBottomWidth: 1,
                            lineHeight: TITLEHEIGHT,
                            marginLeft: 15

                        }}
                    >
                        {txt}
                    </Text>
                )
            }
            case 'desc': {           //城市描述,定位城市,常用城市,热门城市
                return (
                    <Text
                        style={{
                            height: DESCHEIGHT,
                            lineHeight: DESCHEIGHT,
                            width: GlobalStyles.screenWidth - 50,
                            textAlignVertical: 'center',
                            //backgroundColor: "#0f0", 
                            color: 'black',
                            fontSize: 16,
                        }}
                    >
                        {txt}
                    </Text>
                )
            }
            case 'city2': {                //常用城市和热门城市数据,珠海,广州等
                txt = txt.split(',');
                return (
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 5,
                        backgroundColor: '#F8F8F8',
                        marginLeft: 15
                    }}>
                        {
                            txt.map((element, index) => {
                                return <TouchableOpacity
                                    key={'info' + info.index + 'index' + index}
                                    onPress={() => {
                                        // console.log('常用或热门城市:',element)
                                        element = element.replace(/\s*/g, "")
                                        this.props.navigation.state.params.backData(element);
                                        this.props.navigation.goBack();
                                    }}
                                >
                                    <Text
                                        key={'info' + info.index + 'index' + index}
                                        style={{
                                            textAlignVertical: 'center',
                                            textAlign: 'center',
                                            width: 94.5,
                                            height: CITYHEIGHT2,
                                            borderColor: 'white',
                                            borderWidth: 1,
                                            fontSize: 16,
                                            marginRight: 14,
                                            lineHeight: CITYHEIGHT2,
                                            backgroundColor: '#FFFFFF'
                                        }}>
                                        {element}
                                    </Text>
                                </TouchableOpacity>
                            })
                        }
                    </View>
                )
            }
            case 'location': {       //定位城市中的珠海
                return (
                    <View
                        style={{
                            textAlignVertical: 'center',
                            textAlign: 'center',
                            width: 94.5,
                            height: CITYHEIGHT2,
                            borderColor: 'rgb(220,220,220)',
                            borderWidth: 1,
                            fontSize: 16,
                            marginRight: 14,
                            //marginTop:4
                        }}>
                        {txt}
                    </View>
                )
            }
        }
    }

    _getItemLayout(data, index) {
        if (data[index].type == 'letter' || data[index].type == 'city') {
            let i;
            for (i = index; i > 0; i--) {
                if (data[i].type == 'letter') {
                    break;
                }
            }

            let offset = this.state.listData.indexOf(data[i]['name']) - this.letterDescNumber;
            return {
                index,
                offset: CITYHEIGHT * (this.titleCityArray[offset] + index - i) + (offset) * (TITLEHEIGHT)
                    + this.city2number * CITYHEIGHT2 + this.descNumber * (DESCHEIGHT),
                length: i == index ? TITLEHEIGHT : CITYHEIGHT,
            }
        } else {
            let i;
            for (i = index; i > 0; i--) {
                if (data[i].type == 'desc') {
                    break;
                }
            }

            let offset = this.state.listData.indexOf(data[i]['name'].slice(0, 2));
            return {
                index,
                offset: CITYHEIGHT2 * index + offset * (DESCHEIGHT - CITYHEIGHT2),
                length: i == index ? DESCHEIGHT : CITYHEIGHT2,
            }
        }

    }

    renderNavBar() {
        let isNotch = DeviceInfo.hasNotch();
        let backButton = <TouchableOpacity
            style={{ marginLeft: 15, marginTop: 20 }}
            onPress={() => {
                this.leftBtnClick()
            }}>
            <Ionicons
                name={'ios-arrow-back'}
                size={26}
                style={{ color: 'white' }} />
        </TouchableOpacity>
        let inputView = <TextInput
            ref="input"
            placeholder={'请输入港口名'}
            // onChangeText={(text) => {
            //     console.log('输入的港口名:', text)
            // }}
            onChangeText={(text) => {
                this.setState({ searchText: text })
                this.onChangeText(text)
            }
            }
            style={styles.textInput}
        >
        </TextInput>;

        let searchBtn = <TouchableOpacity style={styles.searchBtn}
            onPress={() => {
                console.log('港口搜索按钮点击事件', this.state.searchText)
            }}
        >
            <Text style={{ fontSize: 15, color: GlobalStyles.nav_bar_color, textAlign: 'center' }}>搜索</Text>
        </TouchableOpacity>

        return <View style={{
            backgroundColor: GlobalStyles.nav_bar_color,
            flexDirection: 'row',
            alignItems: 'center',
            height: isNotch ? 84 : 64,
        }}>
            {backButton}
            {inputView}
            {/* {searchBtn} */}
        </View>
    }

    render() {
        return (
            <SafeAreaViewPlus style={{ flex: 1, backgroundColor: GlobalStyles.backgroundColor }} topColor={GlobalStyles.nav_bar_color}>
                {this.renderNavBar()}
                
                {this.state.showFlag == true ? (        //查找时showFlag为false
                    <View >
                        <FlatList                                                     /**定位,常用,热门的城市数据 */
                            ref={'FlatList'}
                            style={{ width: '100%', backgroundColor: '#F8F8F8' }}
                            data={this.state.sections}
                            contentContainerStyle={styles.list}
                            showsVerticalScrollIndicator={false}
                            renderItem={this._renderItem}
                            initialNumToRender={50}
                            getItemLayout={(data, index) => this._getItemLayout(data, index)}
                            keyExtractor={(item, index) => {
                                return (index.toString());
                            }}
                        >
                        </FlatList>
                        <View
                            style={{
                                position: 'absolute',
                                // height:Utils.size.height-93+Utils.statusBarHeight,
                                alignItems: 'center',
                                justifyContent: 'center',
                                right: 0,
                            }}
                        >
                            <View style={{ marginTop: 50, height: 25 * 20 }}>
                                <FlatList                                        /**定位,常用,热门,ABCD... */
                                    initialNumToRender={25}
                                    data={this.state.listData}
                                    renderItem={this._flatRenderItem}
                                    keyExtractor={(item, index) => { return item + index }}
                                >
                                </FlatList>
                            </View>
                        </View>
                    </View>
                ) : (
                        <FlatList
                            style={{ width: GlobalStyles.screenWidth }}
                            data={this.state.searchArray}
                            renderItem={this._searchRenderItem}
                            keyExtractor={(item, index) => {
                                return (index.toString());
                            }}
                        />
                    )}
                {this.state.isNoData ? NoDataView.renderContent(40) : null}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast" position="center" />
            </SafeAreaViewPlus>
        )
    }
}

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        height: (Platform.OS === 'ios') ? 30 : 36,
        borderWidth: (Platform.OS === 'ios') ? 1 : 0,
        borderColor: "white",
        backgroundColor: 'white',
        alignSelf: 'center',
        paddingLeft: 15,
        marginRight: 15,
        marginLeft: 15,
        borderRadius: 3,
        color: 'black',
        marginTop: 20,
        paddingTop: 0,
        paddingBottom: 0,
    },
    searchBtn: {
        width: 50,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        marginRight: 15,
        marginTop: 20,
        borderRadius: 3,
    }
})