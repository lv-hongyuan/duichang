/**
 * 获取跟踪
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, TextInput, TouchableOpacity, ActivityIndicator,Keyboard } from 'react-native';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import NavigationUtil from '../../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import NetworkDao from '../../../dao/NetworkDao'
import publicDao from '../../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../../common/NoDataView'


export default class GoodsTracking extends Component {
    constructor(props) {
        super(props)
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],
            isNoData: false,
            searchText:''
        }
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    searchGoods(text) {
        this.setState({ indicatorAnimating: true })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.waybillNum = text
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('waybillSearch.json', paramsStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this.setState({ dataArr: data.list, isNoData: false })
                } else if (data._backcode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backcode == '404') {
                    this.setState({ dataArr: [], isNoData: true })
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                this.refs.toast.show('搜索失败', 800)
            })
    }

    //搜索的view
    renderSearchView() {
        return <View style={{ marginTop: 10, backgroundColor: 'white', width: GlobalStyles.screenWidth, height: 90,flexDirection:'row'}}>
            <Image style={{ height: 70, width: 67 }} source={require('../../../../resource/rudder.png')} />
            <View style={{ flexDirection: 'row', marginTop: 22.5, marginLeft: -52, height: 45, width: GlobalStyles.screenWidth - 95, borderColor: '#9A9A9A', borderWidth: 1, borderRadius: 3 }}>
                <Image style={{ marginLeft: 10, marginTop: 10, width: 20, height: 20 }} source={require('../../../../resource/zoomer-g.png')} />
                <TextInput
                    style={{ marginLeft: 10, height: 40, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 70 }}
                    clearButtonMode={'while-editing'}
                    placeholder={'运单号'}
                    fontSize={17}
                    returnKeyType={'search'}
                    onSubmitEditing={(event) => {
                        console.log('搜索的内容:', event.nativeEvent.text)
                        let txt = event.nativeEvent.text
                        if (txt && txt.length > 0) {
                            this.searchGoods(txt)
                        }else{
                            this.refs.toast.show('请输入运单号',500)
                        }
                    }}
                    onChangeText={(text)=>{
                        this.setState({searchText:text})
                    }}
                />
            </View>
            <TouchableOpacity style={styles.searchBtn}
            activeOpacity={0.75}
            onPress={()=>{
                Keyboard.dismiss()
                console.log('货运跟踪搜索按钮点击事件')
                if (this.state.searchText && this.state.searchText.length > 0) {
                    this.searchGoods(this.state.searchText)
                }else{
                    this.refs.toast.show('请输入运单号',500)
                }
            }}
            >
                <Text style={{color:'white',fontSize:17}}>搜索</Text>
            </TouchableOpacity>
        </View>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    renderItem(data) {
        let item = data.item
        return <TouchableOpacity
            style={{ width: GlobalStyles.screenWidth, height: 60, backgroundColor: 'white', flexDirection: 'row', paddingLeft: 15, paddingRight: 15, justifyContent: 'space-between' }}
            onPress={() => {
                //cell点击事件
                NavigationUtil.goPage(item, 'TransBillDetail')
            }}
        >
            <Text style={{ height: 60, lineHeight: 60, fontSize: 17, color: '#0877DE' }}>{item.boxNum}</Text>
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ height: 60, lineHeight: 60, fontSize: 15, color: '#778087' }}>{item.xxxl + item.boxState}</Text>
                <Ionicons
                    name={'ios-arrow-forward'}
                    size={20}
                    style={{
                        marginLeft: 10,
                        alignSelf: 'center',
                        color: '#CCCCCC',
                    }} />
            </View>

        </TouchableOpacity>
    }
    //flatlist
    renderFlatList() {
        return <View style={{ flex: 1, marginTop: 10, backgroundColor: GlobalStyles.backgroundColor }}>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => this.separatorView()}
                data={this.state.dataArr}
                renderItem={data => this.renderItem(data)}
            />
        </View>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'货运跟踪'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast" position="center"/>
                {this.renderSearchView()}
                {this.renderFlatList()}    
                {this.state.isNoData ? NoDataView.renderContent() : null}
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBtn:{
        height:45,
        width:60,
        marginLeft:10,
        marginRight:10,
        backgroundColor:GlobalStyles.nav_bar_color,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:3,
        marginTop:22.5
    }
});

