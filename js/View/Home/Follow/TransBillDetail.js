/**
 * 跟踪信息
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, ImageBackground, TouchableOpacity, ActivityIndicator } from 'react-native';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import NavigationUtil from '../../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import NetworkDao from '../../../dao/NetworkDao'
import publicDao from '../../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'

export default class TransBillDetail extends Component {

  constructor(props) {
    super(props)
    this.params = this.props.navigation.state.params;
    this.state = {
      dataArr: [],
      indicatorAnimating: false, //菊花
      force: '',   //流向
      markerData: []     //地图标记数据
    }
  }

  componentDidMount() {
    this.setState({ indicatorAnimating: true })
    this.getMarkData()
    let para = {}
    para.token = publicDao.CURRENT_TOKEN
    para.waybillNum = this.params.waybillNum
    para.boxId = this.params.boxId
    let paraStr = JSON.stringify(para)
    NetworkDao.fetchPostNet('freightTracking.json', paraStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        if (data._backcode == '200') {
          // data.sfg data.mdg
          let tempForce = data.sfg + '-' + data.mdg
          this.setState({ dataArr: data.waybillList, force: tempForce })
        } else if (data._backcode == '201') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('获取运单信息失败')
      })
  }

  //获取中谷陆运的地图标点数据
  getMarkData() {
    let params = {}
    // params.token = publicDao.CURRENT_TOKEN
    params.dcdh = this.params.waybillNum
    params.contid = this.params.boxId
    let paramsStr = JSON.stringify(params)
    this.getData(paramsStr)
      .then(data => {
        if (data.backcode == '200') {
          this.dataToString(data)
        } else if (data.backcode == '201') {
          this.setState({ mapMarkError: data.backmsg })
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  //调用中谷陆运的接口
  getData(data) {
    var url = 'http://172.160.30.53:8110/app/operation/getTransportRoute.json'
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: '*/*',
          'Content-Type': 'application/json',
        },
        body: data
      }).then((response) => {
        if (response.ok) {
          response.text().then(text => {
            //数据解析
            let jsonData = JSON.parse(text)
            resolve(jsonData)
          })
        }
      }).catch((error) => {
        console.log('error:', error)
        reject(error);
      })
    })
  }

  //将接受到的数据转换成数组
  dataToString(data) {
    let tempArr = [
      { lc: data.lc1, lat: data.lat1, lon: data.lon1 }, { lc: data.lc2, lat: data.lat2, lon: data.lon2 },
      { lc: data.lc3, lat: data.lat3, lon: data.lon3 }, { lc: data.lc4, lat: data.lat4, lon: data.lon4 },
      { lc: data.lc5, lat: data.lat5, lon: data.lon5 }, { lc: data.lc6, lat: data.lat6, lon: data.lon6 },
      { lc: data.lc7, lat: data.lat7, lon: data.lon7 },
    ]
    this.setState({ markerData: tempArr })
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  handleZKL(data) {
    if (data == '100') {
      return '全额'
    } else if (parseInt(data) < 0) {
      return data + '天'
    } else {
      let d = parseInt(data)
      return d / 10 + '折'
    }
  }

  //箱号,超期起始日等
  renderTitleView1(leftTitle, rightTitle, rightColor) {
    return <View style={{ flexDirection: 'row' }}>
      <Text style={{ height: 30, lineHeight: 30, fontSize: 17, color: '#778087' }}>{leftTitle}</Text>
      <Text
        style={{ marginLeft: 10, minHeight: 30, lineHeight: 30, fontSize: 17, color: rightColor }}
        numberOfLines={2}
      >{rightTitle}</Text>
    </View>
  }

  //进口司机,出口司机
  renderTitleView2(title1, title2, title3, title4) {
    return <View style={{ flexDirection: 'row' }}>
      <View style={{ flexDirection: 'row', width: (GlobalStyles.screenWidth - 30) / 2, height: 30 }}>
        <Text style={{ height: 30, lineHeight: 30, fontSize: 17, color: '#778087' }}>{title1}</Text>
        <Text style={{ marginLeft: 10, height: 30, lineHeight: 30, fontSize: 17, color: 'black' }}>{title2}</Text>
      </View>
      <View style={{ flexDirection: 'row', width: (GlobalStyles.screenWidth - 30) / 2, height: 30 }}>
        <Text style={{ height: 30, lineHeight: 30, fontSize: 17, color: '#778087' }}>{title3}</Text>
        <Text style={{ marginLeft: 10, height: 30, lineHeight: 30, fontSize: 17, color: 'black' }}>{title4}</Text>
      </View>
    </View>
  }

  //tableviewHeaderView
  headerView() {
    return <View style={{ padding: 15, backgroundColor: 'white' }}>
      {this.renderTitleView1('流向:', this.state.force, '#187ADB')}
      {this.renderTitleView1('箱号:', this.params.boxNum, '#187ADB')}
      {this.renderTitleView1('超期起始日:', this.params.cqqst, 'black')}
      {this.renderTitleView1('铅封号:', this.params.qfh, 'black')}
      {this.renderTitleView1('超期折扣:', this.handleZKL(this.params.zkl), 'black')}
      {this.renderTitleView1('全额免堆天:', this.handleZKL(this.params.zkl2), 'black')}
      {/* {this.renderTitleView2('出口 司机:', '朱正兵', '车牌号', '苏GS8715')} */}
      {/* {this.renderTitleView2('进口 司机:', '张敬兵', '车牌号', '辽BM2168')} */}
      {this.renderTitleView1('出口:', this.params.ckDriver)}
      {this.renderTitleView1('进口:', this.params.jkDriver)}
    </View>
  }

  RightTextButton() {
    return <TouchableOpacity
    style={{
      flexDirection: 'row',
      marginRight: 5,
      width:40,height:40,
      justifyContent:'center',
      alignItems:'center',
      paddingBottom:5
    }}
      onPress={() => {
        if (this.state.markerData.length > 0) {
          let data = {
            waybillNum: this.params.waybillNum,
            boxId: this.params.boxId,
            markerData: this.state.markerData
          }
          NavigationUtil.goPage(data, 'TrackMapDeail')
        } else {
          if(this.state.mapMarkError){
            this.refs.toast.show(this.state.mapMarkError, 1500)
          }else{
            this.refs.toast.show('网络连接失败', 1500)
          }
          
        }
      }}
    >
      <Image style={{width:25,height:28}} source={require('../../../../resource/Map.png')}/>
    </TouchableOpacity>
  }

  //tableviewFooterView
  fotterView() {
    return <View style={{ height: 20, backgroundColor: GlobalStyles.backgroundColor }}>

    </View>
  }

  renderItem(data) {
    //#778087:里面小圆圈的灰色
    let item = data.item
    let ind = this.state.dataArr.length - data.index
    if (data.index == 0) {
      if (item.describe) {
        return <View style={{ flexDirection: 'row', backgroundColor: GlobalStyles.backgroundColor, marginTop: 20, paddingLeft: 15, paddingRight: 15 }}>
          <View style={{ marginTop: 5, width: 30, height: 30, borderRadius: 15, backgroundColor: 'white' }}>
            <View style={{ marginTop: 7.5, marginLeft: 7.5, width: 15, height: 15, borderRadius: 7.5, backgroundColor: GlobalStyles.nav_bar_color }}>
              <Text style={{ marginTop: 1, alignSelf: 'center', fontSize: 10, color: 'white' }}>{ind}</Text>
            </View>
          </View>
          <ImageBackground
            style={{ marginLeft: 10, width: GlobalStyles.screenWidth - 70, height: 40 }}
            resizeMode={'stretch'}
            source={require('../../../../resource/dialog-box-r.png')}
          >
            <Text style={{ marginLeft: 20, fontSize: 17, height: 40, lineHeight: 40 }} >{item.describe + ':  ' + item.jdsj}</Text>
          </ImageBackground>
        </View >

      } else {
        return <View style={{ flexDirection: 'row', backgroundColor: GlobalStyles.backgroundColor, marginTop: 20, paddingLeft: 15, paddingRight: 15 }}>
          <View style={{ marginTop: 5, width: 30, height: 30, borderRadius: 15, backgroundColor: 'white' }}>
            <View style={{ marginTop: 7.5, marginLeft: 7.5, width: 15, height: 15, borderRadius: 7.5, backgroundColor: GlobalStyles.nav_bar_color }}>
              <Text style={{ marginTop: 1, alignSelf: 'center', fontSize: 10, color: 'white' }}>{ind}</Text>
            </View>
          </View>
          <ImageBackground
            style={{ marginLeft: 10, width: GlobalStyles.screenWidth - 70 }}
            resizeMode={'stretch'}
            // source={require('../../../../resource/dialog-box-r1.png')}
            source={require('../../../../resource/Estimate3.png')}
          >
            <Text style={{ marginLeft: 20, fontSize: 17, marginTop: 10, paddingBottom: 15, lineHeight: 25 }} >{item.noShip + '\n' + '船名/航次: ' + item.zwcm + '/' + item.hc + '\n' + '航程: ' + item.hx + '\n' + item.lgms + '\n' + item.xgms}</Text>
          </ImageBackground>
        </View>
      }

    } else {
      if (item.describe) {
        return <View style={{ flexDirection: 'row', backgroundColor: GlobalStyles.backgroundColor, marginTop: 20, paddingLeft: 15, paddingRight: 15 }}>
          <View style={{ marginTop: 5, width: 30, height: 30, borderRadius: 15, backgroundColor: 'white' }}>
            <View style={{ marginTop: 7.5, marginLeft: 7.5, width: 15, height: 15, borderRadius: 7.5, backgroundColor: '#778087' }}>
              <Text style={{ marginTop: 1, alignSelf: 'center', fontSize: 10, color: 'white' }}>{ind}</Text>
            </View>
          </View>
          <ImageBackground
            style={{ marginLeft: 10, width: GlobalStyles.screenWidth - 70, height: 40 }}
            source={require('../../../../resource/dialog-box.png')}
          >
            <Text style={{ marginLeft: 20, fontSize: 17, height: 40, lineHeight: 40 }} >{item.describe + ':  ' + item.jdsj}</Text>
          </ImageBackground>
        </View >


      } else {
        return <View style={{ flexDirection: 'row', backgroundColor: GlobalStyles.backgroundColor, marginTop: 20, paddingLeft: 15, paddingRight: 15 }}>
          <View style={{ marginTop: 5, width: 30, height: 30, borderRadius: 15, backgroundColor: 'white' }}>
            <View style={{ marginTop: 7.5, marginLeft: 7.5, width: 15, height: 15, borderRadius: 7.5, backgroundColor: '#778087' }}>
              <Text style={{ marginTop: 1, alignSelf: 'center', fontSize: 10, color: 'white' }}>{ind}</Text>
            </View>
          </View>
          <ImageBackground
            style={{ marginLeft: 10, width: GlobalStyles.screenWidth - 70 }}
            resizeMode={'stretch'}
            // source={require('../../../../resource/dialog-box1.png')}
            source={require('../../../../resource/Estimate1.png')}

          >
            <Text style={{ marginLeft: 20, fontSize: 17, marginTop: 10, paddingBottom: 15, lineHeight: 25 }} >{item.noShip + '\n' + '船名/航次: ' + item.zwcm + '/' + item.hc + '\n' + '航程: ' + item.hx + '\n' + item.lgms + '\n' + item.xgms}</Text>
          </ImageBackground>
        </View>
      }
    }

  }

  renderFlatList() {
    return <View style={{ flex: 1 }}>
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={() => this.headerView()}
        ListFooterComponent={() => this.fotterView()}
        data={this.state.dataArr}
        renderItem={data => this.renderItem(data)}
      />
    </View>
  }


  render() {
    let navigationBar =
      <NavigationBar
        title={'跟踪信息'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
        leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        rightButton={this.RightTextButton()}
      />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" />
        {this.renderFlatList()}
      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  }

});