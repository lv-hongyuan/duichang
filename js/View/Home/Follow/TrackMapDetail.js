import { MapView, Location } from 'react-native-baidumap-sdk';
import React, { Component } from 'react';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import GlobalStyles from '../../../common/GlobalStyles';
import ViewUtil from '../../../common/ViewUtil'
import NavigationBar from '../../../common/NavigationBar';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image } from 'react-native';
import Toast, { DURATION } from 'react-native-easy-toast';
import Fontisto from 'react-native-vector-icons/Fontisto';

export default class TrackMapDeail extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.zoomLevel = 9
    this.state = {
      indicatorAnimating: false,
      location: { latitude: 39.8992, longitude: 116.3946 },  //默认中心点
      markerData: this.params.markerData || []     //地图标记数据
    }
  }

  componentDidMount() {
    //监听当前位置信息
    // this.addLocation = Location.addLocationListener(location =>
    //   this.setState({ location })
    // )
    // Location.start()
    let tempArr = this.state.markerData
    //设置地图中心点
    for (var i = 0; i < tempArr.length; i++) {
      if (tempArr[i].lc) {
        this.setState({ location: { latitude: tempArr[i].lat, longitude: tempArr[i].lon } })
        return
      }
    }
  }

  componentWillUnmount() {
    //移除位置监听
    // this.addLocation && this.addLocation.remove()
    // Location.stop()
  }


  //返回按钮
  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  //将高德地图经纬度转换为百度地图经纬度
  GDMapTransBDMap(lng, lat) {
    let x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    let x = lng;
    let y = lat;
    let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
    let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
    let lngs = z * Math.cos(theta) + 0.0065;
    let lats = z * Math.sin(theta) + 0.006;
    return {
      longitude: lngs,
      latitude: lats,
    }


  }

  //展示地图标记
  //传入的参数分别为：1、头部文字  2、经度  3、纬度  4、图片路径  5、显示绿色还是红色（true为绿色） 
  renderMarker(name, lon, lat, img, intocolor) {
    let color = intocolor ? '#76AE17' : '#BC1920'
    if (!name) return null
    return <MapView.Marker
      coordinate={this.GDMapTransBDMap(lon, lat)}
      view={() => (
        <View style={{
          width: 80, justifyContent: 'space-around', alignItems: 'center',
          position: 'relative', right: 40, bottom: 45, height: 90, backgroundColor: 'transparent'
        }}>
          <View style={{
            width: 80, alignItems: 'center', backgroundColor: '#fff',
            borderRadius: 5, marginBottom: 4, borderColor: color, borderWidth: 1
          }}>
            <Text style={{ height: 30, lineHeight: 30, color: color, fontSize: 16 }}>{name}</Text>
          </View>
          <Image style={{ width: 44, height: 54 }} source={img} />
        </View>
      )}
    />
  }


  render() {
    let navigationBar =
      <NavigationBar
        title={'地图详情'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
        leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      />;
    let markdata = this.state.markerData
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <MapView
          // location={{ latitude: 38.8655, longitude: 121.5268 }}   //定位
          ref={ref => (this._MapView = ref)}
          center={this.state.location}   //地图中心点
          locationEnabled={false}   //是否显示定位图层
          // locationMode='follow'  //是否跟随至定位点
          style={styles.mapView}
          buildingsDisabled   //禁用3d建筑
          overlookDisabled    //禁用倾斜手势
          trafficEnabled={false}  //是否显示路况
          zoomLevel={this.zoomLevel} //缩放级别
          minZoomLevel={6}  //最小缩放级别
          maxZoomLevel={17}  //最大缩放级别
          zoomControlsDisabled  //禁用缩放按钮（此按钮仅安卓生效）
        >
          {/* 传入的参数分别为：1、头部文字  2、经度  3、纬度  4、图片路径  5、显示绿色还是红色（true为绿色） */}
          {markdata[0] && this.renderMarker(markdata[0].lc, markdata[0].lon, markdata[0].lat, require('../../../../resource/got_empty_box.png'), true)}
          {markdata[1] && this.renderMarker(markdata[1].lc, markdata[1].lon, markdata[1].lat, require('../../../../resource/start_cargo.png'), true)}
          {markdata[2] && this.renderMarker(markdata[2].lc, markdata[2].lon, markdata[2].lat, require('../../../../resource/box_into_port.png'), true)}
          {markdata[3] && this.renderMarker(markdata[3].lc, markdata[3].lon, markdata[3].lat, require('../../../../resource/box_leave_port.png'), false)}
          {markdata[4] && this.renderMarker(markdata[4].lc, markdata[4].lon, markdata[4].lat, require('../../../../resource/reach_store.png'), false)}
          {markdata[5] && this.renderMarker(markdata[5].lc, markdata[5].lon, markdata[5].lat, require('../../../../resource/received.png'), false)}
          {markdata[6] && this.renderMarker(markdata[6].lc, markdata[6].lon, markdata[6].lat, require('../../../../resource/empty_box.png'), false)}
        </MapView>
        <TouchableOpacity
          style={{
            height: 45, width: 45, borderRadius: 3, backgroundColor: 'rgba(255,255,255,.9)',
            position: 'absolute', bottom: 130, right: 15, justifyContent: 'center', alignItems: 'center'
          }}
          //点击定位到当前位置
          onPress={() => { this._MapView.setStatus(status = { center: this.state.location }, duration = 500) }}
        >
          <Fontisto
            name={'radio-btn-active'}
            size={25}
            style={{ color: 'rgb(70,118,255)' }}
          />
        </TouchableOpacity>
        <View style={{
          backgroundColor: 'rgba(255,255,255,.9)',
          height: 90, width: 45, position: 'absolute',
          right: 15, bottom: 25, borderRadius: 3
        }}>
          <TouchableOpacity
            style={{
              height: 45, justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {   //点击增加缩放级别
              if (this.zoomLevel < 16) this.zoomLevel++
              this._MapView.setStatus(status = { zoomLevel: this.zoomLevel }, duration = 500)
            }}
          >
            <Text style={{ fontSize: 35, color: 'rgb(70,118,255)' }}> + </Text>
          </TouchableOpacity>
          <View style={{ width: 35, alignSelf: 'center', backgroundColor: '#ddd', height: 1 }} />
          <TouchableOpacity
            style={{
              height: 45, alignItems: 'center',
              justifyContent: 'center'
            }}
            onPress={() => {  //点击减少缩放级别
              if (this.zoomLevel > 6) this.zoomLevel--
              this._MapView.setStatus(status = { zoomLevel: this.zoomLevel }, duration = 500)
            }}
          >
            <Text style={{ fontSize: 40, color: 'rgb(70,118,255)', lineHeight: 45 }}> - </Text>
          </TouchableOpacity>
        </View>
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={{ position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" />
      </SafeAreaViewPlus>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,

  },
  mapView: {
    flex: 1,
    paddingHorizontal: 15,
    paddingBottom: 15
  }
});
