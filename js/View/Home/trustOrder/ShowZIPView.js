
//预览Zip

import React, { Component } from 'react';
import { Image, DeviceEventEmitter, StyleSheet, Text, View, Clipboard, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, NetInfo, Modal, TextInput, ShadowPropTypesIOS } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import ViewUtil from '../../../common/ViewUtil';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import RNFetchBlob from 'rn-fetch-blob'


export default class ShowZIPView extends Component {
  constructor(props) {
    super(props)
    this.params = this.props.navigation.state.params
    this.state = {

    }
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  rightButtonClick() {
    if (GlobalStyles.isIos) {
      RNFetchBlob.ios.previewDocument(this.params.path)
    } else {
      RNFetchBlob.android.actionViewIntent(this.params.path, '*/*')
        // RNFetchBlob.android.actionViewIntent(this.params.path, 'application/zip')
        .then((res) => {
          console.log('安卓打开文件', res)
        })
        .catch((error) => {
          console.log('安卓打开文件失败', error)
        })
    }
  }

  renderRightBtn() {
    return <TouchableOpacity
      style={{ alignItems: 'center', width: 30, height: 30, justifyContent: 'center' }}
      onPress={() => {
        this.rightButtonClick()
      }}>
      <Entypo
        name={'share-alternative'}
        size={20}
        color={'#fff'}
        style={{ marginRight: 5 }}>
      </Entypo>
    </TouchableOpacity>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'ZIP预览'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      rightButton={this.renderRightBtn()}
    />;
    const source = { uri: this.params, cache: true };
    console.log(source);

    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <View style={styles.IconView}>
          <FontAwesome
            name={'file-zip-o'}
            size={100}
            color={GlobalStyles.nav_bar_color}
            style={{ marginBottom: 20, marginTop: -100 }}
          >
          </FontAwesome>
          <Text style={{ fontSize: 16 }}>{this.params.fileName}</Text>
        </View>

      </SafeAreaViewPlus>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  IconView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  }
});
