
//单证下载

import React, { Component } from 'react';
import { Image, DeviceEventEmitter, StyleSheet, Text, View, Clipboard, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, NetInfo, Modal, TextInput, ShadowPropTypesIOS, Linking, NativeModules, PermissionsAndroid } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import { EMITTER_TRUSTORDER_SEARCH_TYPE, EMITTER_SOUTH_CHINA_SELECT_DONE } from '../../../common/EmitterTypes'
import TransBillSearchClass from '../../Waybill/TransBillSearchClass';
import Toast from 'react-native-easy-toast'
import ViewUtil from '../../../common/ViewUtil';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
import NetworkDao from '../../../dao/NetworkDao';
import publicDao from '../../../dao/publicDao';
import WaybillTopSearchView from '../../Waybill/WaybillTopSearchView';
import Ionicons from 'react-native-vector-icons/Ionicons'
import BookSpaceBottomModal from '../../BookSpaceUtils/BookSpaceBottomModal'
import RNFetchBlob from 'rn-fetch-blob'
import FormData from '../../../../node_modules/react-native/Libraries/Network/FormData'
import NoDataView from '../../../common/NoDataView';
import AlertView from '../../../common/AlertView'
import ActivityIndicatorPlus from '../../../common/ActivityIndicatorPlus'
import find from 'lodash/find'
import DataDao from '../../../dao/DataDao'
let _this = null

let dataDao = new DataDao();
const pageSize = 30

export default class TrustOrder extends Component {
  constructor(props) {
    super(props)
    _this = this
    this.startRow = 0
    this.state = {
      dataArr: [],
      refreshing: false,
      indicatorAnimating: false, //菊花
      hideLoadingMore: true,     //是否显示上拉
      isNoData: false,
      largeListData: [],           //大列表数据
      searchText: '',
      selectItem: [],                              //多选数据
      downOrderArr: [{ 'name': '华南片区', 'code': 0 }, { 'name': '虎门提重', 'code': 1 }], //放箱单选择
      entrustArr: [{ 'name': '合并PDF下载', 'code': 0 }, { 'name': '批量PDF下载', 'code': 1 }],  //委托书下载
      transArr: [{ 'name': '合并PDF下载', 'code': 0 }, { 'name': '批量PDF下载', 'code': 1 }],  //委托书下载
      modalVisible: false,  //是否显示modal
      textInputValue: '',    //modal上的输入框
    }
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }


  componentDidMount() {
    let dirs = RNFetchBlob.fs.dirs
    let dir = ''
    if (GlobalStyles.isIos) {
      dir = dirs.DocumentDir + '/' + publicDao.COMPANY
    } else {
      dir = dirs.DownloadDir + '/' + publicDao.COMPANY
    }
    publicDao.DOWNLOAD_FILE = dir

    this.cleanSearchmes()
    this.getData(TransBillSearchClass)
    this.searchListener = DeviceEventEmitter.addListener(EMITTER_TRUSTORDER_SEARCH_TYPE, (para) => {
      this.setState({ searchText: '' })
      this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
      this.startRow = 0
      para.dcdh = ''
      this.getData(para)
    });

    this.southListener = DeviceEventEmitter.addListener(EMITTER_SOUTH_CHINA_SELECT_DONE, async (para) => {
      console.log('录入箱量返回数据', para)
      let path = 'fxdpdf.json?waybillIdsfxd=' + para.ids + '&boxnum=' + para.sls
      // let fileName = '华南放箱单.zip'
      let fileName = await this.handleZIPFileName('华南放箱单')
      this.downloadFile(path, false, fileName)
    });

  }

  componentWillUnmount() {
    if (this.searchListener) {
      this.searchListener.remove();
    }
    if (this.southListener) {
      this.southListener.remove()
    }
  }

  //合并PDF下载文件命名
  async handleCombinePDFFileName(fileName) {
    try {
      let res = await RNFetchBlob.fs.ls(publicDao.DOWNLOAD_FILE)
      console.log('所有的文件', res)
      let fileNameArr = []
      for (let i = 0; i < res.length; i++) {
        let scanName = res[i]
        let suffix = scanName.substr(scanName.length - 4, 4)
        // console.log('后缀',suffix)
        // console.log('扫描的文件',scanName)
        if ((scanName.indexOf(fileName) != -1) && (suffix.indexOf('pdf') != -1)) {
          fileNameArr.push(scanName)
        }
      }

      console.log('该名字的文件', fileNameArr)
      let name = fileName + '.pdf'
      if (fileNameArr.length == 0) {
        return name
      } else {
        for (let j = 0; j < fileNameArr.length; j++) {

          let isOriName = find(res, (item) => item === name)
          if (!isOriName) {
            break;
          }

          let isName = find(res, (item) => {
            return (item === (fileName + (j + 1) + '.pdf'))
          })
          if (!isName) {
            name = fileName + (j + 1) + '.pdf'
            break
          }
          name = fileName + fileNameArr.length + '.pdf'
        }

        return name
      }
    } catch (error) {
      console.log('扫描文件夹错误', error)
    }
  }


  //zip文件命名
  async handleZIPFileName(fileName) {
    try {
      let res = await RNFetchBlob.fs.ls(publicDao.DOWNLOAD_FILE)
      console.log('所有的文件', res)
      let fileNameArr = []
      for (let i = 0; i < res.length; i++) {
        let scanName = res[i]
        let suffix = scanName.substr(scanName.length - 4, 4)
        // console.log('后缀',suffix)
        // console.log('扫描的文件',scanName)
        if ((scanName.indexOf(fileName) != -1) && (suffix.indexOf('zip') != -1)) {
          fileNameArr.push(scanName)
        }
      }

      console.log('该名字的文件', fileNameArr)
      let name = fileName + '.zip'
      if (fileNameArr.length == 0) {
        return name
      } else {
        for (let j = 0; j < fileNameArr.length; j++) {

          let isOriName = find(res, (item) => item === name)
          if (!isOriName) {
            break;
          }

          let isName = find(res, (item) => {
            return (item === (fileName + (j + 1) + '.zip'))
          })
          if (!isName) {
            name = fileName + (j + 1) + '.zip'
            break
          }
          name = fileName + fileNameArr.length + '.zip'
        }

        return name
      }
    } catch (error) {
      console.log('扫描文件夹错误', error)
    }
  }


  //清空搜索条件
  cleanSearchmes() {
    TransBillSearchClass.dcdh = ''
    TransBillSearchClass.shr = ''
    TransBillSearchClass.zwcm = ''
    TransBillSearchClass.hc = ''
    TransBillSearchClass.hm = ''
    TransBillSearchClass.sfg = ''
    TransBillSearchClass.mdg = ''
    TransBillSearchClass.startDate = ''
    TransBillSearchClass.endDate = ''
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }
  alertCancelDown = () => {
    console.log('点击了alert取消按钮')
  }

  alertSureDown = () => {

  }

  //文件下载
  downloadFile(path, isPDF, fileName) {
    // let _this = this
    // this.setState({ indicatorAnimating: true })
    _this.refs.activityIndicatorPlus.show()
    NetworkDao.downloadFile(path, isPDF, fileName)
      .then(res => {
        _this.refs.activityIndicatorPlus.dismiss()
        console.log('下载成功', res)
        RNFetchBlob.fs.exists(res.path()).then((info) => {
          console.log('文件是否保存成功:', info)
          if (info) {
            // this.refs.toast.show('下载成功', 800)
            if (GlobalStyles.isIos) {
              let data = {}
              data.path = res.path()
              data.fileName = fileName
              data.timeStamp = new Date().getTime()
              dataDao.saveDownloadFile(fileName, data)
              if (isPDF) {
                NavigationUtil.goPage(data, 'ShowPDFView')
              } else {
                NavigationUtil.goPage(data, 'ShowZIPView')
              }
            } else {
              // this.refs.AlertView.showAlert();
              let data = {}
              data.path = res.path()
              data.fileName = fileName
              data.timeStamp = new Date().getTime()
              dataDao.saveDownloadFile(fileName, data)
              if (isPDF) {
                NavigationUtil.goPage(data, 'ShowPDFView')
              } else {
                NavigationUtil.goPage(data, 'ShowZIPView')
              }
            }

            this.cleanselectAllItem()

          } else {
            this.refs.toast.show('下载失败', 800)
          }
        })
      }).catch(error => {
        _this.refs.activityIndicatorPlus.dismiss()
        console.log('下载失败')
        this.refs.toast.show('下载失败', 800)
      })
  }


  getData(dic) {
    // let _this = this
    this.setState({ refreshing: true })
    _this.refs.activityIndicatorPlus.show()
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = 0
    params.pageSize = pageSize
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getBookingWaybillList.json', paramsStr)
      .then(data => {
        this.setState({ refreshing: false })
        _this.refs.activityIndicatorPlus.dismiss()
        this._largelist.endRefresh();
        if (data._backCode == '200') {
          this.cleanselectAllItem()

          let tempArr = new Array().concat(data.entityList)
          for (var i = 0; i < tempArr.length; i++) {
            tempArr[i].select = false
          }
          let DataAll = tempArr
          let tempA = this.handleLargeListData(data.entityList)
          this.setState({ largeListData: tempA, dataArr: DataAll, isNoData: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.setState({ dataArr: [], isNoData: true, largeListData: [] })
        }
      })
      .catch(error => {
        this._largelist.endRefresh();
        this.setState({ refreshing: false, isNoData: true })
        _this.refs.activityIndicatorPlus.dismiss()
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  //一键复制按钮
  copySubmitFunction(text) {
    Clipboard.setString(text);
    this.refs.toast.show('复制运单号成功', 500)
  }

  loadMoreData(dic) {
    // let _this = this
    this.setState({ hideLoadingMore: false })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = ++this.startRow * pageSize
    params.pageSize = pageSize
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getBookingWaybillList.json', paramsStr)
      .then(data => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        if (data._backCode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.entityList && data.entityList.length > 0) {
            for (var i = 0; i < data.entityList.length; i++) {
              data.entityList[i].select = false
            }
            tempArr.push(...data.entityList)
            let tempA = this.state.largeListData.concat()
            let tempB = data.entityList
            let tempC = tempA[0].items
            tempB.forEach(element => {
              tempC.push(element)
            });
            this.setState({ dataArr: tempArr, largeListData: tempA })
          } else {
            this.refs.toast.show('没有更多数据了', 500)
          }

        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]
    let billState = item.ydzt
    let billName = ''
    let billColor = ''
    if (billState == 0) {
      billName = '已受理'
      billColor = '#76AE17'
    } else if (billState == 1) {
      billName = '已离港'
      billColor = '#EBB259'
    } else if (billState == 2) {
      billName = '已到港'
      billColor = '#0877DE'
    } else if (billState == 3) {
      billName = '已完成'
      billColor = '#BC1920'
    }
    let orderNum = item.dcdh
    if (item.dcdhfp && item.dcdhfp.length > 0) {
      orderNum = orderNum + '_' + item.dcdhfp
    }
    // console.log(item);
    return <View>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <TouchableOpacity
        style={{
          borderRadius: 10,
          padding: 10,
          flex: 1,
          backgroundColor: '#fff',
        }}
        activeOpacity={1}
        onPress={() => {
          this._selectItemPress(item)
        }}
      >
        <View style={styles.cardTopView}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.Ionicons}>
              <Ionicons
                name={item.select ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={24}
                color={GlobalStyles.nav_bar_color} >
              </Ionicons>
            </View>
            <Text style={styles.text}>运单号:{orderNum}</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{
                height: 20, width: 40, backgroundColor: GlobalStyles.nav_bar_color,
                justifyContent: 'center', alignItems: 'center',
                borderRadius: 2, marginRight: 8
              }}
              onPress={() => {
                this.copySubmitFunction(orderNum)
              }}
            >
              <Text style={{ color: '#fff', fontSize: 13, }}>复制</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: 13, height: 20, lineHeight: 20, color: billColor }}>{billName}</Text>
          </View>

        </View>
        <Image style={{ width: GlobalStyles.screenWidth - 30, height: 2, marginLeft: -15, marginTop: 10 }} source={require('../../../../resource/xu2.png')} />
        <View style={{ marginTop: 10, height: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 13, color: 'black' }}>{item.zwcm + ' | ' + item.hc}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 13, color: '#778087' }}>{'离港:'}</Text>
            <Text style={{ fontSize: 13 }}>{item.khsj}</Text>
          </View>
        </View>
        <View style={{ marginTop: 10, height: 30, flexDirection: 'row' }}>
          <Image style={{ marginTop: 3, height: 15, width: 24 }}
            source={require('../../../../resource/ship.png')}
          />
          <Text style={{ marginLeft: 15, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.sfg}</Text>
          <Image style={{ marginTop: 9, width: 60, height: 5, marginLeft: 20 }} source={require('../../../../resource/direction.png')} />
          <Text style={{ marginLeft: 20, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.mdg}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '59%' }}>货名: {item.hm}</Text>

          <Text style={{ fontSize: 13, color: 'black', lineHeight: 28 }}>箱型 * 箱量: {item.xxxl} * {item.sl}</Text>
        </View>
        <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '95%' }}>收货人: {item.shr}</Text>
      </TouchableOpacity>
    </View>
  }


  //下载按钮展示区域
  downloadView() {
    return <View style={{ backgroundColor: '#fff', height: 85, paddingHorizontal: 15, justifyContent: 'space-between', paddingBottom: 10 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        <TouchableOpacity style={styles.downloadBotton}
          activeOpacity={0.75}
          onPress={async () => {
            if (this.state.selectItem && this.state.selectItem.length > 0) {
              let ids = ''
              this.state.selectItem.forEach(element => {
                ids += element.id + ','
              });
              let uploadID = ids.substring(0, ids.length - 1);
              let path = 'containerloadOrder.json?containerLoadPDFName=' + uploadID
              if (this.state.selectItem.length == 1) {
                let fileName = ''
                let tempJson = this.state.selectItem[0]
                if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                  fileName = tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '_' + '装箱单.pdf'
                } else {
                  fileName = tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '_' + '装箱单.pdf'
                }
                this.downloadFile(path, true, fileName)
              } else {
                // let fileName = '装箱单.zip'
                let fileName = await this.handleZIPFileName('装箱单')
                console.log('装箱单名字', fileName)
                this.downloadFile(path, false, fileName)
              }
            } else {
              this.refs.toast.show('请选择运单', 800)
            }
          }}
        >
          <Text style={styles.downloadText}>青岛装箱单下载</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.downloadBotton}
          activeOpacity={0.75}
          onPress={() => {
            this.bottomModalSelect = 'entrustOrder'
            this.refs.bookSpaceBottomModal.show('name', this.state.entrustArr)
          }}
        >
          <Text style={styles.downloadText}>委托书下载</Text>
        </TouchableOpacity>
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        <TouchableOpacity style={styles.downloadBotton}
          activeOpacity={0.75}
          onPress={() => {
            this.bottomModalSelect = 'transOrder'
            this.refs.bookSpaceBottomModal.show('name', this.state.transArr)
          }}
        >
          <Text style={styles.downloadText}>运单下载</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.downloadBotton}
          activeOpacity={0.75}
          onPress={() => {
            this.bottomModalSelect = 'downOrder'
            this.refs.bookSpaceBottomModal.show('name', this.state.downOrderArr)
          }}
        >
          <Text style={styles.downloadText}>放箱单下载</Text>
        </TouchableOpacity>
      </View>
    </View>
  }

  //清除全选
  cleanselectAllItem() {
    let FistList = this.state.dataArr.concat()
    for (var i = 0; i < FistList.length; i++) {
      FistList[i].select = false
    }
    this.setState({ selectItem: [], allsclectitem: false })
  }

  //列表全选
  rightBUttonClick() {
    let a = !this.state.allsclectitem
    let IDList = []
    let FistList = this.state.dataArr.concat()
    if (a == true) {
      for (var i = 0; i < FistList.length; i++) {
        FistList[i].select = true
        // IDList.push(FistList[i].id)
        IDList.push(FistList[i])
      }
    } else if (a == false) {
      for (var i = 0; i < FistList.length; i++) {
        FistList[i].select = false
      }
      IDList = []
    }
    this.setState({ selectItem: IDList, allsclectitem: a })
    console.log('全选数据:', IDList);
  }

  //左侧选择框
  _selectItemPress(item) {
    let IDList = []
    let FistList = this.state.dataArr.concat()
    for (var i = 0; i < FistList.length; i++) {
      // IDList.push(FistList[i].id)
      IDList.push(FistList[i])
    }
    let cellindex = IDList.indexOf(item)
    let data = this.state.selectItem
    if (!item.select) {
      data.push(item)
    } else {
      let aa = data.indexOf(item)
      this.state.selectItem.splice(aa, 1)
    }
    console.log('已选：', this.state.selectItem);
    FistList[cellindex].select = !item.select
    this.setState({ dataArr: FistList, selectItem: data })
  }

  renderTextInputModal() {
    return <Modal
      transparent={true}
      visible={this.state.modalVisible}
    >
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 5, backgroundColor: 'rgba(0,0,0,0.3)' }}>
        <View style={{ width: GlobalStyles.screenWidth - 30, marginLeft: 15, height: 150, backgroundColor: 'white', borderRadius: 5 }}>
          <TextInput
            style={{ height: 40, marginLeft: 15, marginTop: 20, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 60, borderColor: 'gray', borderWidth: 1, borderRadius: 3 }}
            placeholder={'请输入箱量'}
            clearButtonMode={'while-editing'}
            fontSize={17}
            keyboardType={'numeric'}
            onChangeText={(text) => {
              this.setState({ textInputValue: text })
            }} />
          <View style={{ marginTop: 20, height: 50, width: GlobalStyles.screenWidth - 60, flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              style={{ marginLeft: 30, height: 40, width: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: 'gray', borderRadius: 3 }}
              onPress={() => {
                this.setState({ modalVisible: false })
              }}
            >
              <Text style={{ color: 'white' }} >取消</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ marginLeft: 30, height: 40, width: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 3 }}
              onPress={() => {
                let tempJ = this.state.selectItem[0]
                let sl = parseInt(tempJ.sl)
                Keyboard.dismiss()
                if (this.state.textInputValue == '') {
                  this.refs.toast.show('有未输入的箱量,请重新输入', 800)
                  return
                }
                let inputSl = parseInt(this.state.textInputValue)
                if (inputSl < 0) {
                  this.refs.toast.show('箱量不能小于0,请重新输入', 800)
                  return
                }
                if (inputSl > sl) {
                  this.refs.toast2.show('输入箱量大于运单箱量,请重新输入', 800)
                  return
                }

                this.setState({ modalVisible: false })
                let uploadID = tempJ.id
                let path = 'fxdpdf.json?waybillIdsfxd=' + uploadID + '&boxnum=' + this.state.textInputValue
                let fileName = ''
                let tempJson = this.state.selectItem[0]
                if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                  fileName = '华南放箱单_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                } else {
                  fileName = '华南放箱单_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                }
                this.downloadFile(path, true, fileName)

              }}
            >
              <Text style={{ color: 'white' }}>确定</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Toast ref="toast2" position="bottom" style={{ zIndex: 1111 }} />
      </View>
    </Modal>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'单证下载'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      rightButton={ViewUtil.getRightTextButton(this.state.allsclectitem ? '取消选中' : '全选', () => this.rightBUttonClick())}
    />;
    // let _this = this
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {WaybillTopSearchView.renderSearchView('运单号', this.state.searchText,
          (text) => {
            this.setState({ searchText: text })
          },
          (event) => {
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
            TransBillSearchClass.dcdh = event.nativeEvent.text
            this.getData(TransBillSearchClass)
          },
          () => {
            NavigationUtil.goPage({}, 'TrustOrderSearchView')
          },
          () => {
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
            TransBillSearchClass.dcdh = this.state.searchText
            this.getData(TransBillSearchClass)
          }
        )}
        {this.downloadView()}
        {this.state.isNoData ? NoDataView.renderContent() : null}
        {this.state.isNoData ? <LargeList
          ref={ref => (this._largelist = ref)}
          style={{ height: '100%', paddingHorizontal: 10 }}
          data={this.state.largeListData}
          heightForIndexPath={() => 186}
          renderIndexPath={this.renderItem}
          refreshHeader={ChineseWithLastDateHeader}
          onRefresh={() => {
            this.setState({ searchText: '', selectItem: [] })
            this.startRow = 0
            this.cleanSearchmes()
            this.getData(TransBillSearchClass)
          }}
        /> :
          <LargeList
            ref={ref => (this._largelist = ref)}
            style={{ height: '100%', paddingHorizontal: 10 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 186}
            renderIndexPath={this.renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            onRefresh={() => {
              this.setState({ searchText: '', selectItem: [] })
              this.startRow = 0
              this.cleanSearchmes()
              this.getData(TransBillSearchClass)
            }}
            loadingFooter={ChineseWithLastDateFooter}
            onLoading={() => {
              this.loadMoreData(TransBillSearchClass);
            }}
          />}
        <BookSpaceBottomModal
          ref="bookSpaceBottomModal"
          callBack={async data => {
            console.log('bookSpaceBottomModal data :', data)
            if (this.bottomModalSelect == 'downOrder') {        //放箱单下载
              console.log('放箱单下载:', data.name)
              if (this.state.selectItem && this.state.selectItem.length > 0) {
                if (data.name == '华南片区') {
                  for (let i = 0; i < this.state.selectItem.length; i++) {
                    let tempJ = this.state.selectItem[i]
                    if (tempJ.pq == '华南') {
                      //可以下载
                    } else {
                      this.refs.toast.show('所勾选运单不提供放箱单下载,请重新勾选', 800)
                      return
                    }
                  }
                  if (this.state.selectItem.length == 1) {
                    this.setState({ modalVisible: true })
                  } else {
                    let tempData = {}
                    tempData.data = this.state.selectItem
                    NavigationUtil.goPage(tempData, 'SouthChinaSelectView')
                  }

                } else if (data.name == '虎门提重') {
                  for (let i = 0; i < this.state.selectItem.length; i++) {
                    let tempJ = this.state.selectItem[i]
                    if (tempJ.mdg == '虎门') {
                      //可以下载
                    } else {
                      this.refs.toast.show('仅目的港为虎门可以下载提重放箱单,请重新勾选', 800)
                      return
                    }
                  }
                  let ids = ''
                  this.state.selectItem.forEach(element => {
                    ids += element.id + ','
                  });
                  let uploadID = ids.substring(0, ids.length - 1);
                  let path = 'hmtzfxdpdf.json?waybillIdsProxy=' + uploadID
                  if (this.state.selectItem.length == 1) {
                    let fileName = ''
                    let tempJson = this.state.selectItem[0]
                    if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                      fileName = '虎门提重放箱单_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    } else {
                      fileName = '虎门提重放箱单_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    }
                    this.downloadFile(path, true, fileName)
                  } else {
                    // let fileName = '虎门提重放箱单.zip'
                    let fileName = await this.handleZIPFileName('虎门提重放箱单')
                    this.downloadFile(path, false, fileName)
                  }
                }
              } else {
                this.refs.toast.show('请选择运单', 800)
              }
            } else if (this.bottomModalSelect == 'transOrder') { //运单下载
              console.log('运单下载', data.name)
              if (this.state.selectItem && this.state.selectItem.length > 0) {
                for (let i = 0; i < this.state.selectItem.length; i++) {
                  let tempJ = this.state.selectItem[i]
                  if (tempJ.ydsd == 1 || tempJ.ydsd == '1') {
                    //可以下载
                  } else {
                    this.refs.toast.show('所勾选的运单有未锁定的运单,请重新勾选', 800)
                    return
                  }
                }
                let ids = ''
                this.state.selectItem.forEach(element => {
                  ids += element.id + ','
                });
                let uploadID = ids.substring(0, ids.length - 1);
                let path = 'yundanpdfZip.json?wbookid=' + uploadID
                if (this.state.selectItem.length > 1 && data.name == '批量PDF下载') {
                  // let fileName = '运单.zip'
                  let fileName = await this.handleZIPFileName('运单')
                  this.downloadFile(path, false, fileName)
                }
                if (this.state.selectItem.length == 1 && data.name == '批量PDF下载') {
                  let fileName = ''
                  let tempJson = this.state.selectItem[0]
                  if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                    fileName = '运单_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                  } else {
                    fileName = '运单_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                  }
                  this.downloadFile(path, true, fileName)
                }
                if (data.name == '合并PDF下载') {
                  if (this.state.selectItem.length == 1) {
                    path = 'yundanpdf.json?wbookid=' + uploadID
                    let fileName = ''
                    let tempJson = this.state.selectItem[0]
                    if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                      fileName = '运单_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    } else {
                      fileName = '运单_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    }
                    this.downloadFile(path, true, fileName)
                  } else {
                    path = 'yundanpdf.json?wbookid=' + uploadID
                    // let fileName = '运单.pdf'
                    let fileName = await this.handleCombinePDFFileName('运单')
                    this.downloadFile(path, true, fileName)
                  }

                }
              } else {
                this.refs.toast.show('请选择运单', 800)
              }

            } else if (this.bottomModalSelect == 'entrustOrder') { //委托书下载
              if (this.state.selectItem && this.state.selectItem.length > 0) {
                let ids = ''
                this.state.selectItem.forEach(element => {
                  ids += element.id + ','
                });
                let uploadID = ids.substring(0, ids.length - 1);
                if (data.name == '批量PDF下载') {
                  let path = 'wtdpdfZip.json?waybillIds=' + uploadID
                  if (this.state.selectItem.length == 1) {
                    let fileName = ''
                    let tempJson = this.state.selectItem[0]
                    if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                      fileName = '委托书_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    } else {
                      fileName = '委托书_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    }
                    this.downloadFile(path, true, fileName)
                  } else {
                    // let fileName = '委托书.zip'
                    let fileName = await this.handleZIPFileName('委托书')
                    this.downloadFile(path, false, fileName)
                  }
                }
                if (data.name == '合并PDF下载') {
                  // let fileName = '委托书.pdf'
                  if (this.state.selectItem.length == 1) {
                    let fileName = ''
                    let path = 'wtdpdf.json?waybillIdsProxy=' + uploadID
                    let tempJson = this.state.selectItem[0]
                    if (tempJson.dcdhfp && tempJson.dcdhfp.length > 0) {
                      fileName = '委托书_' + tempJson.dcdh + '_' + tempJson.dcdhfp + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'

                    } else {
                      fileName = '委托书_' + tempJson.dcdh + '_' + tempJson.zwcm + '_' + tempJson.hc + '.pdf'
                    }
                    this.downloadFile(path, true, fileName)
                  } else {
                    let fileName = await this.handleCombinePDFFileName('委托书')
                    let path = 'wtdpdf.json?waybillIdsProxy=' + uploadID
                    this.downloadFile(path, true, fileName)
                  }

                }
              } else {
                this.refs.toast.show('请选择运单', 800)
              }
            }
          }}
        />
        {/* {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)} */}
        <ActivityIndicatorPlus
          ref={(r) => _this.refs.activityIndicatorPlus = r}
        />
        <AlertView
          ref="AlertView"
          TitleText={"请到 '文件管理->手机->内部存储->Download->" + publicDao.COMPANY + "' 中查看"}
          // DesText=" "
          CancelText='取消'
          OkText='确定'
          BottomRightFontColor='#BA1B25'
          alertSureDown={this.alertSureDown}
          alertCancelDown={this.alertCancelDown}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
        
        {this.renderTextInputModal()}
      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  downloadBotton: {
    backgroundColor: GlobalStyles.nav_bar_color,
    height: 32, width: '45%', borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  downloadText: {
    color: '#fff',
    fontSize: 15
  },
  cardTopView: {
    height: 25,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    lineHeight: 25,
    fontSize: 13,
    color: '#555',
  },
  Ionicons: {
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8
  },
});