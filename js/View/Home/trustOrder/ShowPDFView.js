//预览PDF
import React, { Component } from 'react';
import { Image, DeviceEventEmitter, StyleSheet, Text, View, Clipboard, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, NetInfo, Modal, TextInput, ShadowPropTypesIOS } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import ViewUtil from '../../../common/ViewUtil';
import Pdf from 'react-native-pdf';
import Entypo from 'react-native-vector-icons/Entypo'
import RNFetchBlob from 'rn-fetch-blob'


export default class ShowPDFView extends Component {
    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
        this.state = {
            totalPage: 1,//pdf总页数
        }
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    rightButtonClick() {
        if (GlobalStyles.isIos) {
            RNFetchBlob.ios.previewDocument(this.params.path)
        } else {
            RNFetchBlob.android.actionViewIntent(this.params.path, '*/*')
            // RNFetchBlob.android.actionViewIntent(this.params.path, 'application/pdf')
                .then((res) => {
                    console.log('安卓打开文件', res)
                })
                .catch((error) => {
                    console.log('安卓打开文件失败', error)
                })
        }
    }

    renderRightBtn() {
        return <TouchableOpacity
            style={{ alignItems: 'center', width: 30, height: 30, justifyContent: 'center' }}
            onPress={() => {
                this.rightButtonClick()
            }}>
            <Entypo
                name={'share-alternative'}
                size={20}
                color={'#fff'}
                style={{ marginRight: 5 }}>
            </Entypo>
        </TouchableOpacity>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'PDF预览'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            rightButton={this.renderRightBtn()}
        />;
        const source = { uri: this.params.path, cache: true };
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <Pdf
                    source={source}
                    horizontal={false}
                    enableRTL={true}
                    onLoadComplete={totalPage => {
                        //加载完成设置pdf总页数
                        this.setState({ totalPage });
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        console.log(`当前页:${page},总页数:${numberOfPages}`);
                    }}
                    onError={(error) => {
                        console.log(error);
                    }}
                    style={styles.pdf} />
            </SafeAreaViewPlus>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    pdf: {
        flex: 1,
        width: GlobalStyles.screenWidth,
    }
});
