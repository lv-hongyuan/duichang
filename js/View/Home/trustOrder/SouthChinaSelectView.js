//华南片区
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity, Keyboard, ActivityIndicator, DeviceEventEmitter, NetInfo, TextInput } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import Toast from 'react-native-easy-toast'
import ViewUtil from '../../../common/ViewUtil';
import NetworkDao from '../../../dao/NetworkDao';
import publicDao from '../../../dao/publicDao';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { each, cloneDeep } from 'lodash';
import { EMITTER_SOUTH_CHINA_SELECT_DONE } from '../../../common/EmitterTypes'


export default class SouthChinaSelectView extends Component {
    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params
            console.log(this.params)
        this.state = {
            inputValue: [],//保存textInput输入的值
        }
    }

    componentWillMount() {
        this.initInputValue();
    }
    

    initInputValue() {
        let arr = []
        each(this.params.data, function (item, index) {
            arr[index] = '' + item.sl
        })
        this.setState({ inputValue: arr })
    }
    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }


    //模拟flatlist
    renderFlatList() {
        let tempArr = this.params.data
        let _this = this
        return (
            // <KeyboardAwareScrollView>
            <View>
                {tempArr.map((item, index) => {
                    return <View style={styles.cellBack} key={item.id}>
                            <View style={{ backgroundColor: GlobalStyles.backgroundColor, height: 10, width: '100%' }}></View>
                            <View style={styles.tansOrderBack}>
                                <Text style={styles.transOrderTitle}>{'运单号: ' + item.dcdh}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: 30 }}>
                                <Text style={{ fontSize: 16, color: 'black', lineHeight: 30, marginLeft: 10 }}>{'箱量: '}</Text>
                                <TextInput style={{ width: '50%', marginLeft: 10,height:30,paddingTop:0,paddingBottom:0 }}
                                    placeholder={'请输入箱量'}
                                    defaultValue={item.sl + ''}
                                    keyboardType={'numeric'}
                                    fontSize={16}
                                    onChangeText={(text) => {
                                        let arr = cloneDeep(_this.state.inputValue)
                                        arr[index] = text
                                        _this.setState({ inputValue: arr }, () => {
                                            console.log('输入完->', _this.state.inputValue)
                                        })
                                    }}
                                />
                            </View>
                        </View>
                    
                })}
                </View>
            // </KeyboardAwareScrollView>
        )
    }
    renderCommitBtn() {
        return <TouchableOpacity style={{ marginLeft: 30, height: 50, width: GlobalStyles.screenWidth - 60, backgroundColor: GlobalStyles.nav_bar_color, alignItems: 'center', justifyContent: 'center', marginTop: 60, borderRadius: 10 }}
            activeOpacity={0.75}
            onPress={() => {
                //提交按钮点击事件
                Keyboard.dismiss()
                let tempArr = this.params.data
                for (let i = 0; i < tempArr.length; i++) {
                    tempJ = tempArr[i]
                    let sl = tempJ.sl
                    let inputSl = this.state.inputValue[i]
                    if (inputSl == '') {
                        this.refs.toast.show('有未输入的箱量,请重新输入', 800)
                        return
                    }
                    if (parseInt(inputSl) < 0) {
                        this.refs.toast.show('箱量不能小于0,请重新输入', 800)
                        return
                    }
                    if (parseInt(inputSl) > parseInt(sl)) {
                        this.refs.toast.show('输入箱量大于运单箱量,请重新输入', 800)
                        return
                    }
                }

                let ids = ''
                tempArr.forEach(element => {
                    ids += element.id + ','
                });
                let uploadID = ids.substring(0, ids.length - 1);
                let sls = ''
                this.state.inputValue.forEach(element => {
                    sls += element + ','
                });
                let uploadSL = sls.substring(0, sls.length - 1);

                let para = {}
                para.ids = uploadID;
                para.sls = uploadSL;
                DeviceEventEmitter.emit(EMITTER_SOUTH_CHINA_SELECT_DONE, para)
                NavigationUtil.goBack(this.props.navigation)

            }}
        >
            <Text style={{ fontSize: 18, fontWeight: '700', lineHeight: 50, color: 'white' }}>提  交</Text>
        </TouchableOpacity>
    }

    render() {
        let navigationBar = <NavigationBar
            title={'录入箱量'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <ScrollView>
                    {this.renderFlatList()}
                    {this.renderCommitBtn()}
                </ScrollView>
                <Toast ref="toast" position="center" />
            </SafeAreaViewPlus>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    cellBack: {
        marginLeft: 15,
        height: 80,
        width: GlobalStyles.screenWidth - 30,
        backgroundColor: 'white',
        borderRadius: 5,
        // flexDirection:'row',
        // justifyContent:'space-between',
    },
    tansOrderBack: {
        marginTop: 10,
        height: 30,
        fontSize: 17,
        // width:'70%',
    },
    transOrderTitle: {
        marginLeft: 10,
        color: 'black',
        fontSize: 17,
    }
});