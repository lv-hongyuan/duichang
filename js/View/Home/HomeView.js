/**
 * 首页
 */

import React, { Component } from 'react';
import { NetInfo, Platform, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image, ImageBackground, ScrollView, FlatList, RefreshControl, NativeModules, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar'
import Entypo from 'react-native-vector-icons/Entypo'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import HomeViewCell from './HomeViewCell'
import NewHomeViewCell from './NewHomeViewCell'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import DeviceInfo from 'react-native-device-info';
import Overlay from 'teaset/components/Overlay/Overlay';
import Label from 'teaset/components/Label/Label';
import { Initializer,Location } from 'react-native-baidumap-sdk'
import { PermissionsAndroid } from 'react-native';
import TrustOrder from './trustOrder/TrustOrder'
import TemplateBookSpace from './templateBookSpace/TemplateBookSpace'
import { EMITTER_LOGIN_SUCESS, EMITTER_LOGOUT_SUCCESS} from '../../common/EmitterTypes'
import NoDismissAlertView from '../../common/NoDismissAlertView';
import LargeListPlus from '../../common/LargeListPlus'
import AlertViewPlus from '../../common/AlertViewPlus';


export default class HomeView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      cellDataArr: [],
      indicatorAnimating: false, //菊花
    }
  }

  //右上角消息按钮点击事件
  msgButtonClick() {

  }

  //alertview确定按钮点击事件
  alertSureDown = () => {
    NativeModules.upgrade.openAPPStore('1480484880');
  }

  //去登陆页面
  goToLoginView() {
    let loginJ = {}
    loginJ.isMine = '0'
    loginJ.routeName = 'Main'
    loginJ.isMineMsg = '0'
    NavigationUtil.goPage(loginJ, 'LoginView')
  }

  //统计访问量
  tivalUser() {
    let param = {}
    param.token = publicDao.CURRENT_TOKEN
    param.uniqueCode = DeviceInfo.getUniqueID()
    console.log('token is ', publicDao.CURRENT_TOKEN)
    let paramStr = JSON.stringify(param)
    NetworkDao.fetchPostNet('setVisitNum.json', paramStr)
      .then(data => {
        if (data._backcode == '200') {
          console.log('统计访问量接口成功')
        }
      })
      .catch(error => {
        console.log('统计访问量接口失败')
      })
  }

  getData(showLoading) {
    let param = {}
    if (publicDao.IS_LOGOUT == '0') {
      param.token = publicDao.CURRENT_TOKEN
    }
    let paramStr = JSON.stringify(param)
    if (showLoading) {
      this.setState({ indicatorAnimating: true })
    }
    NetworkDao.fetchPostNet('getFreightList.json', paramStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        !showLoading && this.setState({ endRefresh: 'Refresh' })
        if (data._backCode == '200') {
          this.setState({ cellDataArr: data.entityList })
        } else {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        !showLoading && this.setState({ endRefresh: 'Refresh' })
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('获取数据失败', 800)
      })
  }

  //检查更新
  checkUpVersion() {
    if (Platform.OS == 'android') {
      let verson = DeviceInfo.getVersion()
      console.log('当前版本号:', verson)
      let param = {}
      param.appVersion = verson
      let paramStr = JSON.stringify(param)
      NetworkDao.fetchPostNet('getVersion.json', paramStr)
        .then(data => {
          if (data._backcode == '401') {
            if (data.appAddress && data.appAddress.length > 0) {
              let overlayView = (
                <Overlay.PopView
                  style={{ alignItems: 'center', justifyContent: 'center' }}
                  modal={true}
                >
                  <View style={{ backgroundColor: '#fff', minWidth: 260, minHeight: 100, borderRadius: 15, justifyContent: 'center', alignItems: 'center' }}>
                    <Label type='title' size='lg' text='正在更新,请稍等' />
                  </View>
                </Overlay.PopView>
              );
              Overlay.show(overlayView);
              NativeModules.upgrade.upgrade(data.appAddress);
            }
          }
        })
        .catch(error => {
          console.log('检查版本失败')
        })
    } else {
      NativeModules.upgrade.upgrade('1480484880', (msg) => {
        if ('YES' == msg) {
          //跳转到APP Stroe  
          // NativeModules.upgrade.openAPPStore('1480484880');
          this.refs.NoDismissAlertView.showAlert();
        } else {
          console.log('当前是最新版本')
        }
      })
    }

  }

  //初始化百度地图
  initBaiduMap() {
    Initializer.init('wlQ4gLiT0jnTcYbBcPXoptBOwYMtPIql').catch(e => console.error(e))
    Location.init()
  }

  getAndroidPermissions() {
    permissions = [
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    ]
    PermissionsAndroid.requestMultiple(permissions)
  }

  componentDidMount() {
    NavigationUtil.navigation = this.props.navigation;
    this.getData(true)
    // this.checkUpVersion()
    this.initBaiduMap()
    this.tivalUser()
    if (GlobalStyles.isIos) {
      // this.upDatelistenner = this.props.navigation.addListener(
      //   'didFocus',
      //   (obj) => {
      //     console.log('viewDidAppear')
      //       this.checkUpVersion()          
      //   })
    } else {
      this.getAndroidPermissions()
    }

    this.loginListener = DeviceEventEmitter.addListener(EMITTER_LOGIN_SUCESS, () => {
      console.log('登陆成功')
      this.getData(true)
    })
    this.logoutListener = DeviceEventEmitter.addListener(EMITTER_LOGOUT_SUCCESS, () => {
      console.log('登陆成功')
      this.getData(true)
    })
  }

  componentWillUnmount(){
    this.loginListener && this.loginListener.remove()
    this.logoutListener && this.logoutListener.remove()
  }

  //Navigation右上角消息按钮
  renderRightButton() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity
          onPress={() => this.msgButtonClick()}>
          <Image style={{ width: 20, height: 20, marginRight: 10 }} source={require('../../../resource/message.png')} />
        </TouchableOpacity>
      </View>
    )
  }

  //订舱,促销,船期,跟踪
  renderTopItem(text, img, callBack) {
    return <TouchableOpacity onPress={() => {
      callBack()
    }}>
      <View style={{ alignItems: 'center' }}>
        <Image
          style={{ width: 55, height: 51 }}
          source={img}
        />
        <Text style={{ fontSize: 13, color: 'black', lineHeight: 14 }} >{text}</Text>
      </View>
    </TouchableOpacity>
  }

  //cell
  renderItem = ({ row: row }) => {
    let item = this.state.cellDataArr[row]
    return <View>
      <NewHomeViewCell
        item={item}
        bookSpaceBtnClick={() => {
          // NavigationUtil.goPage(data.item, 'ImmediateBookSpace')
          if (publicDao.IS_LOGOUT == '1') {
            this.goToLoginView()
          } else {
            this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
            // NavigationUtil.goPage(item, 'ImmediateBookSpace')
          }
        }}
      />
    </View>
  }

  //tableviewHeaderView
  renderTopView =()=> {
    return <View style={{ marginBottom: 8, backgroundColor: '#fff' }}>
      <Image
        style={{ width: GlobalStyles.screenWidth, height: GlobalStyles.screenWidth / 3 }}
        source={require('../../../resource/banner1.png')}
      />
      <View style={[styles.topItemBackView, { paddingTop: 15 }]}>
        {this.renderTopItem('订舱', require('../../../resource/booking-space.png'), () => {
          console.log('点击了订舱')
          // NavigationUtil.goPage({},'BookSpaceInfo')
          if (publicDao.IS_LOGOUT == '1') {
            this.goToLoginView()
          } else {
            this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
            // NavigationUtil.goPage({}, 'ImmediateBookSpace')
          }
        })}
        {this.renderTopItem('促销', require('../../../resource/sales.png'), () => {
          //促销
          NavigationUtil.goPage({}, 'ShipPromotion')
        })}
        {this.renderTopItem('船期', require('../../../resource/sail-schedule.png'), () => {
          //船期
          NavigationUtil.goPage({}, 'ShipDurationDynamic')
        })}
        {this.renderTopItem('跟踪', require('../../../resource/tail-after.png'), () => {
          //跟踪
          NavigationUtil.goPage({}, 'GoodsTracking')
        })}
      </View>
      <View style={styles.topItemBackView}>
        {this.renderTopItem('模板订舱', require('../../../resource/mubandingcang.png'), () => {
          console.log('点击了订舱')
          if (publicDao.IS_LOGOUT == '1') {
            this.goToLoginView()
          } else {
            this.refs.AlertView.showAlert(); //! 由于五级门点异常，暂时关闭订舱入口
            // NavigationUtil.goPage({}, 'TemplateBookSpace')
          }
        })}
        {this.renderTopItem('单证下载', require('../../../resource/danzhengxiazai.png'), () => {
          //单证下载
          if (publicDao.IS_LOGOUT == '1') {
            this.goToLoginView()
          } else {
            NavigationUtil.goPage({}, 'TrustOrder')
          }
        })}
        {this.renderTopItem('录入箱号', require('../../../resource/luruxianghao.png'), () => {
          //录入箱号
          if (publicDao.IS_LOGOUT == '1') {
            this.goToLoginView()
          } else {
            NavigationUtil.goPage({}, 'IntoCartonNoView')
          }
        })}
        <View style={{ width: 55, height: 66 }}>

        </View>
      </View>
    </View>
  }

  render() {

    let navigationBar =
      <NavigationBar
        title={'中谷订舱'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      />;

    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color} bottomInset={false}>
        {navigationBar}
        <Toast ref='toast' position="center" />
        <NoDismissAlertView
          ref="NoDismissAlertView"
          TitleText="有新版本,请更新"
          OkText='确定'
          BottomRightFontColor='#BA1B25'
          alertSureDown={this.alertSureDown}
        />
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 + 40, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <AlertViewPlus
          ref="AlertView"
          ShowCancelButton={false}
          title='温馨提示'
          message='由于客户端升级，暂时关闭订舱功能。'
          alertSureDown={()=>{}}
        />
        <View style={{ flex: 1 }}>
          <LargeListPlus
          // style={{height:'100%',background:'red'}}
            renderHeader={this.renderTopView} 
            endRefresh={this.state.endRefresh}
            data={this.state.cellDataArr}
            onRefresh={() => { this.getData(false) }}
            renderItem={this.renderItem}
            showIndicator={false}
            NoDataViewheight={0}
            showBackgroundImage={false}
          />

        </View>
        
      </SafeAreaViewPlus>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  topItemBackView: {
    width: GlobalStyles.screenWidth,
    paddingBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  bottomBackView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  bottomCell: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
