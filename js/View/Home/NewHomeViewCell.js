import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';


export default class NewHomeViewCell extends Component {

  renderMoneyText(money) {
    return <View style={{
      flexDirection: 'row', height: 22.5, width: 80,
      alignItems: 'center', justifyContent: 'center',
    }}>
      <Text style={{ color: GlobalStyles.nav_bar_color, fontSize: 13 }}>￥</Text>
      <Text style={{ color: GlobalStyles.nav_bar_color, fontSize: 18, fontWeight: '500', }}>{money}</Text>
    </View>

  }

  renderWeightText(weight) {
    return <Text style={{
      color: '#778087', textAlign: 'center', fontSize: 12,width:80
    }} >{weight}</Text>
  }


  render() {

    const item = this.props
    let data = item.item
    let gp20 = data.gp20 == 0 ? '--' : data.gp20
    let gp40 = data.gp40 == 0 ? '--' : data.gp40
    let hc40 = data.hc40 == 0 ? '--' : data.hc40
    return (
      <View style={{ paddingHorizontal: 10, backgroundColor: '#fff' }}>
        <View style={styles.container}>
          <View style={{ flex: 1 }}>
            <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', marginRight: 8 }}>
              {/* <Text style={{ fontSize: 16, color: '#0877DE',width:68,fontWeight:'700' }}>{data.startPort}</Text> */}
              <Text numberOfLines={1} style={{textAlign: 'center', fontSize: 16, color: '#0877DE', width: 82, fontWeight: '700' }}>{data.startPort}</Text>
              <View style={{ flexDirection: 'column', marginTop: 3, alignItems: 'center' }}>
                <Image style={{ width: 97, height: 6, marginTop: 3 }} source={require('../../../resource/direction.png')} />
                {/* <View style={{flexDirection:'row',alignItems:'center',marginTop:3}}>
                  <Image style={{ width: 12, height: 12 }} source={require('../../../resource/time.png')} />
                  <Text style={{ marginLeft: 3, fontSize: 12, color: '#778087' }}>{data.voyageSchedule + 'day'}</Text>
                </View> */}

              </View>
              {/* <Text style={{ fontSize: 16, color: '#0877DE' ,width:68,textAlign:'right'}}>{data.endPort}</Text> */}
              <Text numberOfLines={1} style={{ fontSize: 16, color: '#0877DE', width: 82, textAlign: 'center' }}>{data.endPort}</Text>
            </View>

            <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', marginRight: 8 }}>
              {this.renderMoneyText(gp20)}
              {this.renderMoneyText(gp40)}
              {this.renderMoneyText(hc40)}
            </View>

            <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', marginRight: 8 }}>
              {this.renderWeightText('20GP')}
              {this.renderWeightText('40GP')}
              {this.renderWeightText('40HC')}
            </View>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              //订舱按钮点击事件
              this.props.bookSpaceBtnClick()
            }}>
            <Text style={{ fontSize: 15, color: 'white', fontWeight: '700' }}>立即订舱</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 0.8,
    height: 110,
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  button: {
    height: 36,
    width: 78,
    backgroundColor: GlobalStyles.nav_bar_color,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  }
});