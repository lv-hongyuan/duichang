/**
 * 订舱信息(失效,由ImmediateBookSpace代替)
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity,Keyboard } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import BookSpaceCellsUtil from '../../BookSpaceUtils/BookSpaceCellsUtil'
import ViewUtil from '../../../common/ViewUtil'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BookSpaceBottomModal from '../../BookSpaceUtils/BookSpaceBottomModal'
import DatePicker from 'react-native-datepicker'
import DateUril from '../../../common/DateUtil'
import BookSpaceAreaLinkage from '../../BookSpaceUtils/BookSpaceAreaLinkage'
import area2 from '../../../../resource/area2.json'
import TransCostPortSearchView from '../../TransCost/TransCostPortSearchView'
import CostDetailModal from '../../BookSpaceUtils/CostDetailModal'
import Picker from '@ant-design/react-native/lib/picker';
import find from 'lodash/find'



export default class BookSpaceInfo extends Component {

    constructor(props) {
        super(props)

        let currentDate = DateUril.getCurrentDate()     //获取当前日期
        this.bottomModalSelect = ''                   //bottomModal选了哪一个
        this.portSelect = 0                             //选择起运港还是目的港

        this.state = {
            showBaseInfo: true,               //展开基本信息
            showEntrustInfo: false,           //展开委托信息
            showGoodsInfo: false,             //展开货物信息
            showCostInfo: false,              //展开费用信息
            showDeclareProvision: false,      //展开声明条款

            startPortName: '请选择',           //起运港
            endPortName: '请选择',             //目的港
            advanceLoadDate: '请选择',          //预计装货日期
            payMode: '请选择',                  //付款方式
            transProvision: '请选择',            //运输条款
            collectTicket: '请选择',             //受票方
            boxReceipt: false,                    //箱内签收单
            bookPersonDetention: false,           //订舱人扣货
            baseInfoMark: '',                     //基本信息的备注

            delivePersonName: '',                //发货人全称
            deliverPersonContact: '',           //发货人联系人
            deliverPersonPhone: '',              //发货人电话
            deliverGatePoint: '发货门点',                //发货门点
            deliverDetailAddress: '',            //发货详细地址
            receivePersonName: '',             //收货人全称
            receivePersonContact: '',          //收货人联系人
            receivePersonPhone: '',            //收货人电话
            receiveGatePoint: '收货门点',            //收货门点
            receiveDetailAddress: '',                //收货详细地址

            goodsName: '',                       //货名
            goodsMode: '',                       //货类
            packingMode: '请选择',                    //包装类型
            boxMode: '',                             //箱型
            boxWeight: '',                           //箱量
            singleBoxWeight: '',                     //单箱重量
            boxMark: '',                             //箱备注
            dangerGoods: '',                         //危险品
            selfBox: '',                             //自备柜
            dangerIMDG: '',                          //危险品的IMDG
            dangerMode: '',                          //危险品类别
            dangerUNCode: '',                        //危险品联合国编号
            dangerMark: '',                          //危险品备注

            seaServeCost: '',                        //海运服务费
            uploadCarCost: '',                       //装港拖车服务费
            downloadCarCost: '',                     //卸港拖车服务费
            uploadRailCost: '',                      //装港铁路服务费
            downloadRailCost: '',                    //卸港铁路服务费
            premiumPayMode: '请选择',                      //保险费支付方式
            premiumWorth: '',                        //保险货值
            totalCost: '',                           //总价

            currentDate: currentDate,                //当前日期
            areaArray: area2,                         //省市区门点数据
            isShowAreaModal: false,                    //是否显示省市区门点modal
            collectTicketArr: [{ 'name': '订舱人', 'code': 'ding' }, { 'name': '发货人', 'code': 'fa' }, { 'name': '收货人', 'code': 'shou' }],   //受票方数据

        }
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    commitBtnClick() {
        console.log('提交按钮点击事件')
    }

    renderLine() {
        return <View style={{ width: GlobalStyles.screenWidth, height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //基本信息下面的cell
    renderBaseInfoDetail() {
        let startPortNameColor = this.state.startPortName == '请选择' ? '#999999' : 'black'
        let endPortNameColor = this.state.endPortName == '请选择' ? '#999999' : 'black'
        let advanceLoadDateColor = this.state.advanceLoadDate == '请选择' ? '#999999' : 'black'
        let payModeColor = this.state.payMode == '请选择' ? '#999999' : 'black'
        let transProvisionColor = this.state.transProvision == '请选择' ? '#999999' : 'black'
        let collectTicketColor = this.state.collectTicket == '请选择' ? '#999999' : 'black'
        return <View>
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '起运港', this.state.startPortName, startPortNameColor, () => {
                console.log('起运港点击事件')
                Keyboard.dismiss()
                this.portSelect = 0
                this.props.navigation.navigate('TransCostPortSearchView', {
                    backData: (data) => {
                        this.setState({ startPortName: data })
                    }
                })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '目的港', this.state.endPortName, endPortNameColor, () => {
                console.log('目的港点击事件')
                Keyboard.dismiss()
                this.portSelect = 1
                this.props.navigation.navigate('TransCostPortSearchView', {
                    backData: (data) => {
                        this.setState({ endPortName: data })
                    }
                })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '预计装货日期', this.state.advanceLoadDate, advanceLoadDateColor, () => {
                //预计装货日期
                this.refs.datePicker.onPressDate()
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '付款方式', this.state.payMode, payModeColor, () => {
                //付款方式
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '运输条款', this.state.transProvision, transProvisionColor, () => {
                //运输条款
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextChooseCell('#FFFBFB', '受票方', this.state.collectTicket, collectTicketColor, () => {
                //受票方
                this.bottomModalSelect = 'collectTicket'
                this.refs.bookSpaceBottomModal.show('name', this.state.collectTicketArr)
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell('#FFFBFB', '箱内签收单', this.state.boxReceipt, (val) => {
                console.log('箱内签收单是否选中:', val)
                this.setState({ boxReceipt: val })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell('#FFFBFB', '订舱人扣货', this.state.bookPersonDetention, (val) => {
                this.setState({ bookPersonDetention: val })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextInputCell('#FFFBFB', '备注', '选填', this.state.baseInfoMark, (text) => {
                console.log('输入的文字:', text)
            }, (event) => {
                console.log('备注是:', event.nativeEvent.text)
                this.setState({ baseInfoMark: event.nativeEvent.text })
            })}
        </View>
    }

    //委托信息下面的cell
    renderEnTrustDetail() {

        let deliverGatePointColor = this.state.deliverGatePoint == '发货门点' ? '#999999' : 'black'
        let receiveGatePointColor = this.state.receiveGatePoint == '收货门点' ? '#999999' : 'black'

        return <View>
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人全称', 120, '请输入', 'default', this.state.delivePersonName, (text) => {
                console.log('发货人全称:', text)
            }, (event) => {
                console.log('发货人全称是:', event.nativeEvent.text)
                this.setState({ delivePersonName: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人联系人', 120, '请输入', 'default', this.state.deliverPersonContact, (text) => {

            }, (event) => {
                this.setState({ deliverPersonContact: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '发货人电话', 120, '请输入', 'numeric', this.state.deliverPersonPhone, (text) => {

            }, (event) => {
                this.setState({ deliverPersonPhone: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {/* {BookSpaceCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor, () => {
                //发货门点点击事件
                
            })} */}
            <Picker
                data={this.state.areaArray}
                cols={4}
                onChange={(val) => {
                    console.log('onChnage val is ', val)
                }}
                onOk={(val) => {
                    console.log('onOk val is ', val)
                    let newArr = this.state.areaArray.concat()  //源数据
                    let area1 = find(newArr, (item2) => item2.value == val[0])
                    console.log('省 json :', area1)
                    let area2 = find(area1.children, (item2) => item2.value == val[1])
                    console.log('市 json :', area2)
                    let area3 = find(area2.children, (item2) => item2.value == val[2])
                    console.log('区 json :', area3)
                    let area4 = find(area3.children, (item2) => item2.value == val[3])
                    console.log('门点 json:', area4)
                    let areaFull = area1.label + area2.label + area3.label + area4.label
                    this.setState({ deliverGatePoint: areaFull })
                }}
                okText='确定'
                dismissText='取消'
                cascade={true}
                itemStyle={{fontSize:16}}
            >
                {BookSpaceCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.deliverGatePoint, deliverGatePointColor, () => {

                })}
            </Picker>
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell('#FFFBFB', '发货详细地址', this.state.deliverDetailAddress, (text) => {

            }, (event) => {
                this.setState({ deliverDetailAddress: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人全称', 120, '请输入', 'default', this.state.receivePersonName, (text) => {

            }, (event) => {
                this.setState({ receivePersonName: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人联系人', 120, '请输入', 'default', this.state.receivePersonContact, (text) => {

            }, (event) => {
                this.setState({ receivePersonContact: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '收货人电话', 120, '请输入', 'numeric', this.state.receivePersonPhone, (text) => {

            }, (event) => {
                this.setState({ receivePersonPhone: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {/* {BookSpaceCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor, () => {
                //收货门点点击事件
                
            })} */}
            <Picker
                data={this.state.areaArray}
                cols={4}
                onChange={(val) => {
                    console.log('onChnage val is ', val)
                }}
                onOk={(val) => {
                    console.log('onOk val is ', val)
                    let newArr = this.state.areaArray.concat()  //源数据
                    let area1 = find(newArr, (item2) => item2.value == val[0])
                    console.log('省 json :', area1)
                    let area2 = find(area1.children, (item2) => item2.value == val[1])
                    console.log('市 json :', area2)
                    let area3 = find(area2.children, (item2) => item2.value == val[2])
                    console.log('区 json :', area3)
                    let area4 = find(area3.children, (item2) => item2.value == val[3])
                    console.log('门点 json:', area4)
                    let areaFull = area1.label + area2.label + area3.label + area4.label
                    this.setState({ receiveGatePoint: areaFull })
                }}
                okText='确定'
                dismissText='取消'
                cascade={true}
            >
                {BookSpaceCellsUtil.renderLeftTitleColorChooseCell('#FFFBFB', this.state.receiveGatePoint, receiveGatePointColor, () => {
                    
                })}
            </Picker>
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell('#FFFBFB', '收货详细地址', this.state.receiveDetailAddress, (text) => {

            }, (event) => {
                this.setState({ receiveDetailAddress: event.nativeEvent.text })
            })}

        </View>
    }


    //危险品详情
    renderDanderDetail() {
        return <View>
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFF5F5', 'IMDG', 120, '请输入', 'default', this.state.dangerIMDG, (text) => {

            }, (event) => {
                this.setState({ dangerIMDG: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFF5F5', '类别', 120, '请输入', 'default', this.state.dangerMode, (text) => {

            }, (event) => {
                this.setState({ dangerMode: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFF5F5', '联合国编号', 120, '请输入', 'default', this.state.dangerUNCode, (text) => {

            }, (event) => {
                this.setState({ dangerUNCode: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell('#FFF5F5', '危险品备注', this.state.dangerMark, (text) => {

            }, (event) => {
                this.setState({ dangerMark: event.nativeEvent.text })
            })}
        </View>
    }

    //货物信息的cell
    renderGoodsInfoDetail() {

        let packingModeColor = this.state.packingMode == '请选择' ? '#999999' : 'black'

        return <View>
            {BookSpaceCellsUtil.renderTextInputChooseCell('#FFFBFB', '货名', '请输入', this.state.goodsName, (text) => {

            }, (event) => {

            }, () => {
                this.props.navigation.navigate('BookSpaceGoodsName', {
                    backData: (data) => {
                       console.log('货名回调:',data)
                    }
                })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell('#FFFBFB', '货类', 120, this.state.goodsMode, GlobalStyles.nav_bar_color, false, () => {
                //货类选择,没有选择
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell('#FFFBFB', '包装类型', 120, this.state.packingMode, packingModeColor, true, () => {
                //包装类型选择
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '箱型', 120, '请输入', 'default', this.state.boxMode, (text) => {

            }, (event) => {
                this.setState({ boxMode: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '箱量', 120, '请输入', 'default', this.state.boxWeight, (text) => {

            }, (event) => {
                this.setState({ boxWeight: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '单箱重量(吨)', 120, '请输入', 'default', this.state.singleBoxWeight, (text) => {

            }, (event) => {
                this.setState({ singleBoxWeight: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderPureTextInputCell('#FFFBFB', '箱备注', this.state.boxMark, (text) => {

            }, (event) => {
                this.setState({ boxMark: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell('#FFF5F5', '危险品', this.state.dangerGoods, (val) => {
                this.setState({ dangerGoods: val })
            })}
            {this.state.dangerGoods ? this.renderDanderDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderSwitchCell('#FFFBFB', '自备柜', this.state.selfBox, (val) => {
                this.setState({ selfBox: val })
            })}
        </View>
    }

    //费用信息的cell
    renderCostDetail() {

        let premiumPayModeColor = this.state.premiumPayMode == '请选择' ? '#999999' : 'black'

        return <View>
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '海运服务费', 150, '请输入', 'numeric', this.state.seaServeCost, (text) => {

            }, (event) => {
                this.setState({ seaServeCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港拖车服务费', 150, '请输入', 'numeric', this.state.uploadCarCost, (text) => {

            }, (event) => {
                this.setState({ uploadCarCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港拖车服务费', 150, '请输入', 'numeric', this.state.downloadCarCost, (text) => {

            }, (event) => {
                this.setState({ downloadCarCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '装港铁路服务费', 150, '请输入', 'numeric', this.state.uploadRailCost, (text) => {

            }, (event) => {
                this.setState({ uploadRailCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderMarginleftInputCell('#FFFBFB', '卸港铁路服务费', 150, '请输入', 'numeric', this.state.downloadRailCost, (text) => {

            }, (event) => {
                this.setState({ downloadRailCost: event.nativeEvent.text })
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell('#FFFBFB', '保险费支付方式', 150, this.state.premiumPayMode, premiumPayModeColor, true, () => {
                //保险费支付方式
            })}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderTextMarginleftChooseCell('#FFFBFB', '保险货值', 150, this.state.premiumWorth, 'black', false, () => {
                //保险费支付方式
            })}
            {BookSpaceCellsUtil.renderTotalCostCell(this.state.totalCost, () => {
                console.log('计费回调')
            }, () => {
                console.log('明细回调')
                this.refs.costDetailModal.show([])
            })}
        </View>
    }

    //模拟flatlist
    renderFlatList() {
        return <KeyboardAwareScrollView>
            {BookSpaceCellsUtil.renderOutCell('基本信息', this.state.showBaseInfo, () => {
                console.log('基本信息cell点击事件')
                this.setState({ showBaseInfo: !this.state.showBaseInfo })
            })}
            {this.state.showBaseInfo ? this.renderBaseInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('委托信息', this.state.showEntrustInfo, () => {
                this.setState({ showEntrustInfo: !this.state.showEntrustInfo })
            })}
            {this.state.showEntrustInfo ? this.renderEnTrustDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('货物信息', this.state.showGoodsInfo, () => {
                this.setState({ showGoodsInfo: !this.state.showGoodsInfo })
            })}
            {this.state.showGoodsInfo ? this.renderGoodsInfoDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('费用信息', this.state.showCostInfo, () => {
                this.setState({ showCostInfo: !this.state.showCostInfo })
            })}
            {this.state.showCostInfo ? this.renderCostDetail() : null}
            {this.renderLine()}
            {BookSpaceCellsUtil.renderOutCell('声明条款', this.state.showDeclareProvision, () => {
                this.setState({ showDeclareProvision: !this.state.showDeclareProvision })
            })}
        </KeyboardAwareScrollView>
    }

    renderCommitBtn() {

        return <View style={{ height: 150, width: GlobalStyles.screenWidth }}>
            <TouchableOpacity
                style={{ marginTop: 50, marginLeft: 15, width: GlobalStyles.screenWidth - 30, height: 50, borderRadius: 5, backgroundColor: GlobalStyles.nav_bar_color }}
                activeOpacity={0.6}
                onPress={() => {
                    this.commitBtnClick()
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth - 30, height: 50, lineHeight: 50, color: 'white', textAlign: 'center', fontSize: 18 }}>{'提   交'}</Text>
            </TouchableOpacity>
        </View>


    }

    render() {
        let navigationBar = <NavigationBar
            title={'订舱信息'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <ScrollView>
                    {this.renderFlatList()}
                    {this.renderCommitBtn()}
                </ScrollView>
                <BookSpaceBottomModal
                    ref="bookSpaceBottomModal"
                    callBack={data => {
                        console.log('bookSpaceBottomModal data :', data)
                        if (this.bottomModalSelect == 'collectTicket') {  //受票方
                            this.setState({ collectTicket: data.name })
                        }
                    }}
                />
                <DatePicker
                    ref="datePicker"
                    date={this.state.currentDate}
                    style={{ width: 1, height: 1, position: 'absolute' }}
                    mode="date"
                    placeholder=""
                    format="YYYY-MM-DD"
                    // minDate="2016-05-01"
                    // maxDate="2029-06-01"
                    confirmBtnText="确定"
                    cancelBtnText="取消"
                    customStyles={{
                        btnTextConfirm: {
                            color: GlobalStyles.nav_bar_color
                        }
                    }}
                    hideText={true}
                    showIcon={false}
                    onDateChange={(date) => { this.setState({ advanceLoadDate: date }) }}
                />
                <CostDetailModal
                    ref="costDetailModal"
                />
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    }
});