
//录入箱号

import React, { Component } from 'react';
import { Image, DeviceEventEmitter, StyleSheet, Text, View, Clipboard, TouchableOpacity, ActivityIndicator } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import { EMITTER_INTOCARTONNO_SEARCH_TYPE } from '../../../common/EmitterTypes'
import TransBillSearchClass from '../../Waybill/TransBillSearchClass';
import Toast from 'react-native-easy-toast'
import ViewUtil from '../../../common/ViewUtil';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
import NetworkDao from '../../../dao/NetworkDao';
import publicDao from '../../../dao/publicDao';
import WaybillTopSearchView from '../../Waybill/WaybillTopSearchView';
import NoDataView from '../../../common/NoDataView'



export default class IntoCartonNoView extends Component {
  constructor(props) {
    super(props)
    this.startRow = 0
    this.state = {
      dataArr: [],
      refreshing: false,
      indicatorAnimating: false, //菊花
      hideLoadingMore: true,     //是否显示上拉
      isNoData: false,
      largeListData: [],           //大列表数据
      searchText: '',
    }
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  componentDidMount() {
    this.cleanSearchmes()
    this.getData(TransBillSearchClass)
    this.searchListener = DeviceEventEmitter.addListener(EMITTER_INTOCARTONNO_SEARCH_TYPE, (para) => {
      this.setState({ searchText: '' })
      this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
      this.startRow = 0
      para.dcdh = ''
      this.getData(para)
    });
  }

  componentWillUnmount() {
    if (this.searchListener) {
      this.searchListener.remove();
    }
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }


  getData(dic) {
    this.setState({ indicatorAnimating: true, refreshing: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    console.log('====================================');
    console.log(paramsStr);
    console.log('====================================');
    NetworkDao.fetchPostNet('getLrxhWayBillList.json', paramsStr)
      .then(data => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false })
        if (data._backCode == '200') {
          let tempArr = new Array().concat(data.entityList)
          let tempA = this.handleLargeListData(data.entityList)
          this.setState({ largeListData: tempA, dataArr: tempArr, isNoData: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.setState({ dataArr: [], isNoData: true, largeListData: [] })
        }
      })
      .catch(error => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false, isNoData: false, isNoData: true })
        this.refs.toast.show('获取数据失败', 500)
      })
  }


  //清空搜索条件
  cleanSearchmes() {
    TransBillSearchClass.dcdh = ''
    TransBillSearchClass.shr = ''
    TransBillSearchClass.zwcm = ''
    TransBillSearchClass.hc = ''
    TransBillSearchClass.hm = ''
    TransBillSearchClass.sfg = ''
    TransBillSearchClass.mdg = ''
    TransBillSearchClass.startDate = ''
    TransBillSearchClass.endDate = ''
  }

  //一键复制按钮
  copySubmitFunction(text) {
    Clipboard.setString(text);
    this.refs.toast.show('复制运单号成功', 500)
  }

  loadMoreData(dic) {
    this.setState({ hideLoadingMore: false })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.dcdh = dic.dcdh
    params.shr = dic.shr
    params.zwcm = dic.zwcm
    params.hc = dic.hc
    params.hm = dic.hm
    params.sfg = dic.sfg
    params.mdg = dic.mdg
    params.startDate = dic.startDate
    params.endDate = dic.endDate
    params.startRow = ++this.startRow * 30
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getLrxhWayBillList.json', paramsStr)
      .then(data => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        if (data._backCode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.entityList && data.entityList.length > 0) {
            tempArr.push(...data.entityList)
            let tempA = this.state.largeListData.concat()
            let tempB = data.entityList
            let tempC = tempA[0].items
            tempB.forEach(element => {
              tempC.push(element)
            });
            this.setState({ dataArr: tempArr, largeListData: tempA })
          } else {
            this.refs.toast.show('没有更多数据了', 500)
          }

        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]
    let billState = item.ydzt
    let billName = ''
    let billColor = ''
    if (billState == 0) {
      billName = '已受理'
      billColor = '#76AE17'
    } else if (billState == 1) {
      billName = '已离港'
      billColor = '#EBB259'
    } else if (billState == 2) {
      billName = '已到港'
      billColor = '#0877DE'
    } else if (billState == 3) {
      billName = '已完成'
      billColor = '#BC1920'
    }
    let orderNum = item.dcdh
    if (item.dcdhfp && item.dcdhfp.length > 0) {
      orderNum = orderNum + '_' + item.dcdhfp
    }
    return <View>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View
        style={{
          borderRadius: 10,
          padding: 10,
          flex: 1,
          backgroundColor: '#fff',
        }}
      >
        <View style={styles.cardTopView}>
          <Text style={styles.text}>运单号:{orderNum}</Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{
                height: 20, width: 40, backgroundColor: GlobalStyles.nav_bar_color,
                justifyContent: 'center', alignItems: 'center',
                borderRadius: 2, marginRight: 8
              }}
              onPress={() => {
                this.copySubmitFunction(orderNum)
              }}
            >
              <Text style={{ color: '#fff', fontSize: 13, }}>复制</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: 13, height: 20, lineHeight: 20, color: billColor }}>{billName}</Text>
          </View>

        </View>
        <Image style={{ width: GlobalStyles.screenWidth - 30, height: 2, marginLeft: -15, marginTop: 10 }} source={require('../../../../resource/xu2.png')} />
        <View style={{ marginTop: 10, height: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 13, color: 'black' }}>{item.zwcm + ' | ' + item.hc + ' | ' + item.ystk}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 13, color: '#778087' }}>{'离港:'}</Text>
            <Text style={{ fontSize: 13 }}>{item.khsj}</Text>
          </View>
        </View>

        <View style={{ height: 25, flexDirection: 'row', marginVertical: 5 }}>
          <Image style={{ marginTop: 3, height: 15, width: 24 }}
            source={require('../../../../resource/ship.png')}
          />
          <Text style={{ marginLeft: 15, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.sfg}</Text>
          <Image style={{ marginTop: 9, width: 60, height: 5, marginLeft: 20 }} source={require('../../../../resource/direction.png')} />
          <Text style={{ marginLeft: 20, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.mdg}</Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '59%' }}>货名: {item.hm}</Text>
          {/* <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '59%' }}>收货人: {item.shr}</Text> */}
          <Text style={{ fontSize: 13, color: 'black', lineHeight: 28 }}>箱型 * 箱量: {item.xxxl} * {item.sl}</Text>
        </View>
        <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '95%' }}>收货人: {item.shr}</Text>
        <View style={{ width: GlobalStyles.screenWidth, height: 0.5, marginLeft: -15, backgroundColor: GlobalStyles.backgroundColor }}></View>
        <View style={{ height: 28, flexDirection: 'row', justifyContent: 'flex-end', marginTop: 5 }}>
          <TouchableOpacity style={{ width: 75, height: 28, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, borderWidth: 1 }}
            onPress={() => {
              NavigationUtil.goPage(item, 'IntoCartonNoDetailView')
            }}
          >
            <Text style={{ height: 25, width: 70, lineHeight: 25, textAlign: 'center', fontSize: 13, color: GlobalStyles.nav_bar_color }}>录入箱号</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  }

  render() {
    let navigationBar = <NavigationBar
      title={'录入箱号'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {WaybillTopSearchView.renderSearchView('运单号', this.state.searchText,
          (text) => {
            this.setState({ searchText: text })
          },
          (event) => {
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
            TransBillSearchClass.dcdh = event.nativeEvent.text
            this.getData(TransBillSearchClass)
          },
          () => {
            NavigationUtil.goPage({}, 'IntoCartonNoSeachView')
          },
          () => {
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
            TransBillSearchClass.dcdh = this.state.searchText
            this.getData(TransBillSearchClass)
          }
        )}
        {this.state.isNoData ?
          <LargeList
            ref={ref => (this._largelist = ref)}
            style={{ height: '100%', paddingHorizontal: 10 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 218}
            renderIndexPath={this.renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            onRefresh={() => {
              this.setState({ searchText: '' })
              this.startRow = 0
              this.cleanSearchmes()
              this.getData(TransBillSearchClass)
            }}
          /> :
          <LargeList
            ref={ref => (this._largelist = ref)}
            style={{ height: '100%', paddingHorizontal: 10 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 218}
            renderIndexPath={this.renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            onRefresh={() => {
              this.setState({ searchText: '' })
              this.startRow = 0
              this.cleanSearchmes()
              this.getData(TransBillSearchClass)
            }}
            loadingFooter={ChineseWithLastDateFooter}
            onLoading={() => {
              this.loadMoreData(TransBillSearchClass);
            }}
          />}
        {this.state.isNoData ? NoDataView.renderContent() : null}
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cardTopView: {
    height: 25,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    lineHeight: 25,
    fontSize: 13,
    color: '#555',
  }
});