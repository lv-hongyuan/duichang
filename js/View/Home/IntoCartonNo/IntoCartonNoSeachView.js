/**
 * 录入箱号的订单搜索
 */

import React, { Component } from 'react';
import {  StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Keyboard,DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import { EMITTER_INTOCARTONNO_SEARCH_TYPE } from '../../../common/EmitterTypes'
import TransBillSearchClass from '../../Waybill/TransBillSearchClass'


export default class IntoCartonNoSeachView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            startPortText: '',
            endPortText: '',
            shipName: '',              //船名
            voyage: '',                //航次
            startCalendarText: '创建开始时间',
            endCalendarText: '创建结束时间',
        }
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //搜索框
    renderTextBtnInput(placeHolder, value, textCallBack, endCallback, btnCallBack) {
        return <View style={{ flexDirection: 'row', marginLeft: 10, width: (GlobalStyles.screenWidth - 70) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <TextInput
                style={{ marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: (GlobalStyles.screenWidth - 140) / 2 }}
                fontSize={16}
                placeholder={placeHolder}
                defaultValue={value}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    let txt = event.nativeEvent.text
                    if (txt && txt.length > 0) {
                        // endCallback(txt)
                    }
                }}
            />
            <TouchableOpacity
                style={{ width: 30, height: 35 }}
                onPress={() => {
                    btnCallBack()
                }}>
                <Image style={{ marginTop: 10, width: 15, height: 15 }} source={require('../../../../resource/zoomer-g.png')} />
            </TouchableOpacity>
        </View>
    }
    

    //船名,航次
    renderTextInput(placeHolder, textCallBack, endCallback) {
        return <View style={{ marginLeft: 10, width: (GlobalStyles.screenWidth - 40) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <TextInput
                style={{ marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: (GlobalStyles.screenWidth - 70) / 2 }}
                fontSize={16}
                clearButtonMode={'while-editing'}
                placeholder={placeHolder}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    let txt = event.nativeEvent.text
                    if (txt && txt.length > 0) {
                        endCallback(txt)
                    }
                }}
            />
        </View>
    }

    //开始时间,结束时间
    renderChooseCalendar(text, textColor, callBack) {
        return <View style={{ flexDirection: 'row', marginLeft: 10, width: (GlobalStyles.screenWidth - 40) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <Text style={{ marginLeft: 10, fontSize: 16, color: textColor, height: 35, lineHeight: 35 }}>{text}</Text>
            <TouchableOpacity
                style={{ position: 'absolute', top: 7.5, right: 10 }}
                onPress={() => {
                    callBack()
                }}>
                <Image style={{ width: 20, height: 20 }} source={require('../../../../resource/date.png')} />
            </TouchableOpacity>
        </View>
    }

    renderSearchView() {
        let startCalendarColor = this.state.startCalendarText.length > 6 ? 'black' : '#B2B6BF'
        let endCalendarColor = this.state.endCalendarText.length > 6 ? 'black' : '#B2B6BF'
        return <View style={{ width: GlobalStyles.screenWidth, height: 250, backgroundColor: 'white' }}>
            <Image style={{ height: 70, width: 67 }} source={require('../../../../resource/rudder.png')} />
            <View style={{ marginTop: -45, marginLeft: 5, flexDirection: 'row' }}>
                {this.renderTextBtnInput('起运港', this.state.startPortText, (text) => {
                    console.log('起运港搜索:', text)
                    this.setState({ startPortText: text })
                }, (text) => {
                    this.setState({ startPortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ startPortText: data })
                        }
                    })
                })}
                
                <TouchableOpacity onPress={() => {
                    //起运港目的港互换
                    let a = this.state.startPortText
                    let b = this.state.endPortText
                    let c = [a, b]
                    b = c[0]
                    a = c[1]
                    this.setState({ startPortText: a, endPortText: b })
                }}>
                    <Image style={{ marginLeft: 10, marginTop: 7.5, width: 20, height: 20 }}
                        source={require('../../../../resource/direction1.png')}
                    />
                </TouchableOpacity>

                {this.renderTextBtnInput('目的港', this.state.endPortText, (text) => {
                    this.setState({ endPortText: text })
                }, (text) => {
                    this.setState({ endPortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ endPortText: data })
                        }
                    })
                })}
                
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderTextInput('请输入船名', (text) => {
                    this.setState({ shipName: text })
                }, (text) => {
                    this.setState({ shipName: text })
                })}
                {this.renderTextInput('请输入航次', (text) => {
                    this.setState({ voyage: text })
                }, (text) => {
                    this.setState({ voyage: text })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.startCalendarText, startCalendarColor, () => {
                    //开始时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ startCalendarText: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.endCalendarText, endCalendarColor, () => {
                    //开始时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ endCalendarText: data.dateString}) 
                        }
                    })
                    
                })}
            </View>
            <TouchableOpacity
                style={{ marginLeft: 15, marginTop: 30, width: GlobalStyles.screenWidth - 30, height: 35, backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 17.5 }}
                onPress={() => {
                    //搜索按钮点击事件
                    TransBillSearchClass.sfg = this.state.startPortText
                    TransBillSearchClass.mdg = this.state.endPortText
                    TransBillSearchClass.zwcm = this.state.shipName
                    TransBillSearchClass.hc = this.state.voyage
                    TransBillSearchClass.startDate = this.state.startCalendarText == '创建开始时间' ? '' : this.state.startCalendarText
                    TransBillSearchClass.endDate = this.state.endCalendarText == '创建结束时间' ? '' : this.state.endCalendarText
                    NavigationUtil.goBack(this.props.navigation)
                    DeviceEventEmitter.emit(EMITTER_INTOCARTONNO_SEARCH_TYPE, TransBillSearchClass)
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth - 30, height: 35, lineHeight: 35, textAlign: 'center', color: 'white', fontSize: 17 }}>{'搜  索'}</Text>
            </TouchableOpacity>
        </View>
    }

    render() {

        let navigationBar = <NavigationBar
            title={'订单搜索'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.renderSearchView()}
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    }
});



