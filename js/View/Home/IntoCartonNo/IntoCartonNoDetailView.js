import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, ScrollView, Keyboard } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus';
import Toast from 'react-native-easy-toast'
import ViewUtil from '../../../common/ViewUtil';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
import NetworkDao from '../../../dao/NetworkDao';
import publicDao from '../../../dao/publicDao';
import BookSpaceCellsUtil from '../../BookSpaceUtils/BookSpaceCellsUtil';
import AlertView from '../../../common/AlertView'

export default class IntoCartonNoDetailView extends Component {
  constructor(props) {
    super(props);
    this.upDataArr = []
    this.params = this.props.navigation.state.params
    this.state = {
      indicatorAnimating: false,
      dataArr: [],
      largeListData: [],
      alertmessage: '',
      newdatas: []

    }
  }

  componentDidMount() {
    console.log(this.params);
    this.getData()
  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  //返回202的二次确认
  alertSureDown = () => {
    this.setState({ indicatorAnimating: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.id = this.state.dataArr[0].ydId + ''
    params.checkFlag = '1'
    params.contDTOs = this.state.newdatas
    let paramsStr = JSON.stringify(params)
    console.log(paramsStr);
    NetworkDao.fetchPostNet('updateLrxh.json', paramsStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        if (data._backcode == '200') {
          this.refs.toast.show('录入箱号成功', 800)
          this.timer = setTimeout(() => {
            NavigationUtil.goBack(this.props.navigation)
          }, 1000)
        } else {
          this.refs.toast.show(data._backmes, 500)
        }
      })
      .catch(error => {
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('上传数据失败', 500)
      })

  }

  alertCancelDown = () => {
    console.log('点击了alert取消按钮')
  }

  //点击提交
  upDataClick() {
    let newdata = new Array().concat(this.upDataArr)
    // let presetArr = []
    if (newdata && newdata.length > 0) {
      let zeroChick = this.upDataArr.length
      var reg = /^[A-Za-z0-9]{10}[0-9]{1}$/
      var qfhreg = /^[//a-zA-Z0-9]+$/
      var spkxreg = /^.[0-9]{1}|[0-9]{1}$/
      for (var i = 0; i < this.upDataArr.length; i++) {
        // if (this.state.dataArr[i].xh != "" && this.state.dataArr[i].qfh != "") {
        //   presetArr.push(this.state.dataArr[i].xh)
        // }
        // if ((newdata[i].xh == "" && newdata[i].qfh == "") || (this.state.dataArr[i].xh != "" && this.state.dataArr[i].qfh != "")) {
        if (newdata[i].xh == "" && newdata[i].qfh == "") {
          zeroChick--
        }
        if (this.state.dataArr[i].zwhm != "商品空箱") {
          if (newdata[i].qfh && !qfhreg.test(newdata[i].qfh)) {
            this.refs.toast.show('请填写正确的封号', 500)
            return
          }
          if ((newdata[i].xh != "" && newdata[i].qfh == "") || (newdata[i].qfh != "" && newdata[i].xh == "")) {
            this.refs.toast.show('箱号与封号需同时填写', 500)
            return
          }
          if ((newdata[i].xh.length > 0 && newdata[i].xh.length != 11) || (newdata[i].xh.length > 0 && !reg.test(newdata[i].xh))) {
            this.refs.toast.show('请填写正确的箱号', 500)
            return
          }
        } else {
          if (newdata[i].xh.length > 0 && !spkxreg.test(newdata[i].xh)) {
            this.refs.toast.show('请填写正确的箱号!', 500)
            return
          }
        }

      }
      if (zeroChick == 0) {
        this.refs.toast.show('至少录入一条数据', 500)
        return
      }
      for (var j = 0; j < newdata.length; j++) {
        if (newdata[j].xh == "" && newdata[j].qfh == "") {
          newdata.splice(j, 1)
          j--
        }
      }
      // for (var k = newdata.length - 1; k >= 0; k--) {
      //   if (presetArr.indexOf(newdata[k].xh) != -1) {
      //     newdata.splice(k, 1)
      //   }
      // }
      //console.log(newdata, presetArr);
      console.log(newdata)
      this.setState({ indicatorAnimating: true })
      let params = {}
      params.token = publicDao.CURRENT_TOKEN
      params.id = this.state.dataArr[0].ydId + ''
      params.checkFlag = '0'
      params.contDTOs = newdata
      let paramsStr = JSON.stringify(params)
      console.log(paramsStr);
      NetworkDao.fetchPostNet('updateLrxh.json', paramsStr)
        .then(data => {
          this.setState({ indicatorAnimating: false })
          if (data._backcode == '200') {
            this.refs.toast.show('录入箱号成功', 800)
            this.timer = setTimeout(() => {
              NavigationUtil.goBack(this.props.navigation)
            }, 1000)
          } else if (data._backcode == '202') {
            this.setState({ alertmessage: data._backmes, newdatas: newdata })
            this.refs.AlertView.showAlert();
          } else {
            this.refs.toast.show(data._backmes, 500)
          }
        })
        .catch(error => {
          this.setState({ indicatorAnimating: false })
          this.refs.toast.show('箱号录入失败，请检查网络', 500)
        })
    }
  }


  //数据获取
  getData() {
    this.setState({ indicatorAnimating: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.id = this.params.id
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getLrxhBoxInfo.json', paramsStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        this._largelist.endRefresh();
        if (data._backCode == '200') {
          let tempArr = new Array().concat(data.entityList)
          let tempA = this.handleLargeListData(data.entityList)
          let upDataArr = []
          for (var i = 0; i < tempArr.length; i++) {
            let dataObj = {}
            dataObj.id = tempArr[i].id + ''  //箱id
            dataObj.xh = tempArr[i].xh          //箱号
            dataObj.qfh = tempArr[i].qfh         //封号
            dataObj.xxxl = tempArr[i].xxxl    //箱型
            dataObj.ydId = tempArr[i].ydId + ''    //箱id
            upDataArr.push(dataObj)
          }
          this.setState({ largeListData: tempA, dataArr: tempArr })
          this.upDataArr = upDataArr
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.setState({ dataArr: [], largeListData: [] })
        }
      })
      .catch(error => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  renderLine() {
    return <View style={{ width: GlobalStyles.screenWidth, height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
  }

  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]
    let boxNum = ''   //箱号
    let fenNum = ''   //封号
    if (item.xh && item.xh.length > 0) {
      boxNum = item.xh
    }
    if (item.qfh && item.qfh.length > 0) {
      fenNum = item.qfh
    }
    return <View style={{ backgroundColor: '#fff', borderRadius: 3, }}>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      {BookSpaceCellsUtil.renderMarginleftInputCell(false, boxNum == "" ? '#FFFBFB' : '#e5e5e5', '箱号', 70, '请输入', boxNum == '' ? true : false, 'default', boxNum, (text) => {
        this.upDataArr[row].xh = text
      }, (event) => {
        // this.upDataArr[row].xh = event.nativeEvent.text
      }, false)}
      {this.renderLine()}
      {BookSpaceCellsUtil.renderMarginleftInputCell(false,'#FFFBFB', '封号', 70, '请输入',true , 'default', fenNum, (text) => {
        this.upDataArr[row].qfh = text
      }, (event) => {
        // this.upDataArr[row].qfh = event.nativeEvent.text
      }, false)}
    </View>
  }

  //判断是否显示提交按钮
  // showbotton() {
  //   if (this.state.dataArr && this.state.dataArr.length > 0) {
  //     let a = this.state.dataArr.length
  //     for (var i = 0; i < this.state.dataArr.length; i++) {
  //       if (this.state.dataArr[i].xh != "" && this.state.dataArr[i].qfh != "") {
  //         a--
  //       }
  //     }
  //     if (a == 0) {
  //       return false
  //     } else {
  //       return true
  //     }
  //   }
  // }

  render() {
    let navigationBar = <NavigationBar
      title={'请录入箱号'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
    />;
    let titleText = '为提高玉米类货物运输安全，每柜需加封四个铅封，否则出险将无法保险理赔，请录入4个铅封号，填写格式如( 123***/123***/123***/123***! )'
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        <Text style={{ color: GlobalStyles.nav_bar_color, lineHeight: 20, marginBottom: 5 }}>{titleText}</Text>
        <LargeList
          ref={ref => (this._largelist = ref)}
          style={{ height: '80%', paddingHorizontal: 10 }}
          data={this.state.largeListData}
          heightForIndexPath={() => 112.1}
          renderIndexPath={this.renderItem}
          refreshHeader={ChineseWithLastDateHeader}
          onRefresh={() => {
            this.startRow = 0
            this.getData()
          }}
        />
        <View style={{ height: 60, marginTop: 15, width: GlobalStyles.screenWidth, alignItems: 'center' }}>
          <TouchableOpacity
            style={{
              borderRadius: 5, backgroundColor: GlobalStyles.nav_bar_color, height: 45,
              width: '80%', justifyContent: 'center', alignItems: 'center'
            }}
            onPress={() => {
              Keyboard.dismiss()
              this.upDataClick()
            }}
          >
            <Text style={{ color: '#fff', fontSize: 16 }}>提交</Text>
          </TouchableOpacity>
        </View>

        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
        <AlertView
          ref="AlertView"
          TitleText={this.state.alertmessage}
          DesText="是否继续录入?"
          CancelText='取消'
          OkText='确定'
          BottomRightFontColor='#BA1B25'
          alertSureDown={this.alertSureDown}
          alertCancelDown={this.alertCancelDown}
          TitleFontSize={16}
          HeightModal={220}
        />
      </SafeAreaViewPlus>
    )

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cardTopView: {
    height: 25,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    lineHeight: 25,
    fontSize: 13,
    color: '#555',
    width: '50%'
  }
});