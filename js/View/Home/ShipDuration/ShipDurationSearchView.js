/**
 * 船期搜索
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, FlatList, RefreshControl, Image, TouchableOpacity, TextInput, Keyboard, DeviceEventEmitter, ScrollView} from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import CalendarModal from '../../../common/CalendarModal'
import { EMITTER_SHIPDUIRATION_SEARCH_TYPE } from '../../../common/EmitterTypes'
import ShipDuirationDynamicClass from './ShipDuirationDynamicClass'


export default class ShipDuirationSearchView extends Component {

    constructor(props) {
        super(props)
        this.shipName = ''
        this.voyage = ''
        this.state = {
            startPortText: '',
            endPortText: '',
            shipName: '',                               //船名
            voyage: '',                                  //航次
            startPortCalendarText1: '起始港离泊起',
            startPortCalendarText2: '起始港离泊止',
            firstPortCalendarText1: '起始港靠泊起',
            firstPortCalendarText2: '起始港靠泊止',
            endPortCalendarText1: '目的港靠泊起',
            endPortCalendarText2: '目的港靠泊止',
            trans1PortText:'',      //中转港1
            trans2PortText:'',      //中转港2
            trans1StartPortCalendarText1:'中转港1离泊起',
            trans1StartPortCalendarText2:'中转港1离泊止',
            trans1EndPortCalendarText1:'中转港1靠泊起',
            trans1EndPortCalendarText2:'中转港1靠泊止',
            trans2StartPortCalendarText1:'中转港2离泊起',
            trans2StartPortCalendarText2:'中转港2离泊止',
            trans2EndPortCalendarText1:'中转港2靠泊起',
            trans2EndPortCalendarText2:'中转港2靠泊止'
        }
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //搜索框
    renderTextBtnInput(placeHolder, value, textCallBack, endCallback, btnCallBack,cellWidth,textInputWidth) {
        return <View style={{ flexDirection: 'row', marginLeft: 10, width: cellWidth, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <TextInput
                style={{ marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: textInputWidth }}
                fontSize={16}
                placeholder={placeHolder}
                defaultValue={value}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    let txt = event.nativeEvent.text
                    if (txt && txt.length > 0) {
                        endCallback(txt)
                    }
                }}
            />
            <TouchableOpacity
                style={{ width: 30, height: 35 }}
                onPress={() => {
                    btnCallBack()
                }}>
                <Image style={{ marginTop: 10, width: 15, height: 15 }} source={require('../../../../resource/zoomer-g.png')} />
            </TouchableOpacity>
        </View>
    }
    //船名,航次
    renderTextInput(placeHolder, textCallBack, endCallback) {
        return <View style={{ marginLeft: 10, width: (GlobalStyles.screenWidth - 40) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <TextInput
                style={{ marginLeft: 10, paddingTop: 0, paddingBottom: 0, height: 35, width: (GlobalStyles.screenWidth - 70) / 2 }}
                fontSize={16}
                clearButtonMode={'while-editing'}
                placeholder={placeHolder}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(event) => {
                    let txt = event.nativeEvent.text
                    if (txt && txt.length > 0) {
                        endCallback(txt)
                    }
                }}
            />
        </View>
    }

    //开始时间,结束时间
    renderChooseCalendar(text, textColor, callBack) {
        return <View style={{ flexDirection: 'row', marginLeft: 10, width: (GlobalStyles.screenWidth - 40) / 2, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, height: 35, borderWidth: 1 }}>
            <Text style={{ marginLeft: 10, fontSize: 16, color: textColor, height: 35, lineHeight: 35 }}>{text}</Text>
            <TouchableOpacity
                style={{ position: 'absolute', top: 7.5, right: 10 }}
                onPress={() => {
                    callBack()
                }}>
                <Image style={{ width: 20, height: 20 }} source={require('../../../../resource/date.png')} />
            </TouchableOpacity>
        </View>
    }

    renderSearchView() {
        let startPortCalendarColor1 = this.state.startPortCalendarText1.length > 6 ? 'black' : '#B2B6BF'
        let startPortCalendarColor2 = this.state.startPortCalendarText2.length > 6 ? 'black' : '#B2B6BF'
        let firstPortCalendarColor1 = this.state.firstPortCalendarText1.length > 6 ? 'black' : '#B2B6BF'
        let firstPortCalendarColor2 = this.state.firstPortCalendarText2.length > 6 ? 'black' : '#B2B6BF'
        let endPortCalendarColor1 = this.state.endPortCalendarText1.length > 6 ? 'black' : '#B2B6BF'
        let endPortCalendarColor2 = this.state.endPortCalendarText2.length > 6 ? 'black' : '#B2B6BF'
        let trans1StartPortCalendarColor1 = this.state.trans1StartPortCalendarText1.length >7 ? 'black':'#B2B6BF'
        let trans1StartPortCalendarColor2 = this.state.trans1StartPortCalendarText2.length >7 ? 'black':'#B2B6BF'
        let trans1EndPortCalendarColor1 = this.state.trans1EndPortCalendarText1.length >7 ? 'black':'#B2B6BF'
        let trans1EndPortCalendarColor2 = this.state.trans1EndPortCalendarText2.length >7 ? 'black':'#B2B6BF'
        let trans2StartPortCalendarColor1 = this.state.trans2StartPortCalendarText1.length >7 ? 'black':'#B2B6BF'
        let trans2StartPortCalendarColor2 = this.state.trans2StartPortCalendarText2.length >7 ? 'black':'#B2B6BF'
        let trans2EndPortCalendarColor1 = this.state.trans2EndPortCalendarText1.length >7 ? 'black':'#B2B6BF'
        let trans2EndPortCalendarColor2 = this.state.trans2EndPortCalendarText2.length >7 ? 'black':'#B2B6BF'

        return <ScrollView style={{ width: GlobalStyles.screenWidth, backgroundColor: 'white' }}>

            <Image style={{ height: 70, width: 67 }} source={require('../../../../resource/rudder.png')} />
            <View style={{ marginTop: -45, marginLeft: 5, flexDirection: 'row' }}>
                {this.renderTextBtnInput('起始港', this.state.startPortText, (text) => {
                    console.log('起运港搜索:', text)
                    this.setState({ startPortText: text })
                }, (text) => {
                    this.setState({ startPortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ startPortText: data })
                        }
                    })
                },(GlobalStyles.screenWidth - 70) / 2,(GlobalStyles.screenWidth - 140) / 2)}
                <TouchableOpacity onPress={() => {
                    //起运港目的港互换
                    let a = this.state.startPortText
                    let b = this.state.endPortText
                    let c = [a, b]
                    b = c[0]
                    a = c[1]
                    this.setState({ startPortText: a, endPortText: b })
                }}>
                    <Image style={{ marginLeft: 10, marginTop: 7.5, width: 20, height: 20 }}
                        source={require('../../../../resource/direction1.png')}
                    />
                </TouchableOpacity>

                {this.renderTextBtnInput('目的港', this.state.endPortText, (text) => {
                    this.setState({ endPortText: text })
                }, (text) => {
                    this.setState({ endPortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ endPortText: data })
                        }
                    })
                },(GlobalStyles.screenWidth - 70) / 2,(GlobalStyles.screenWidth - 140) / 2)}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderTextInput('请输入船名', (text) => {
                    // console.log('输入的船名:', text)
                    this.shipName = text
                }, (text) => {
                    this.setState({ shipName: text })
                })}
                {this.renderTextInput('请输入航次', (text) => {
                    // console.log('输入的航次:', text)
                    this.voyage = text
                }, (text) => {
                    this.setState({ voyage: text })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.startPortCalendarText1, startPortCalendarColor1, () => {
                    //起始港开始时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ startPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.startPortCalendarText2, startPortCalendarColor2, () => {
                    //起始港结束时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ startPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.firstPortCalendarText1, firstPortCalendarColor1, () => {
                    //始发港靠泊起
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ firstPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.firstPortCalendarText2, firstPortCalendarColor2, () => {
                    //始发港靠泊止
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ firstPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.endPortCalendarText1, endPortCalendarColor1, () => {
                    //目的港开始时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ endPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.endPortCalendarText2, endPortCalendarColor2, () => {
                    //目的港结束时间选择事件
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ endPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
            {this.renderTextBtnInput('中转港1', this.state.trans1PortText, (text) => {
                    console.log('中转港1搜索:', text)
                    this.setState({ trans1PortText: text })
                }, (text) => {
                    this.setState({ trans1PortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ trans1PortText: data })
                        }
                    })
                },(GlobalStyles.screenWidth - 40) / 2,(GlobalStyles.screenWidth - 115) / 2)}
                {this.renderTextBtnInput('中转港2', this.state.trans2PortText, (text) => {
                    this.setState({ trans2PortText: text })
                }, (text) => {
                    this.setState({ trans2PortText: text })
                }, () => {
                    Keyboard.dismiss()
                    this.props.navigation.navigate('TransCostPortSearchView', {
                        backData: (data) => {
                            this.setState({ trans2PortText: data })
                        }
                    })
                },(GlobalStyles.screenWidth - 40) / 2,(GlobalStyles.screenWidth - 115) / 2)}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.trans1StartPortCalendarText1, trans1StartPortCalendarColor1, () => {
                    //中转港1离泊时间起时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans1StartPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.trans1StartPortCalendarText2, trans1StartPortCalendarColor2, () => {
                    //中转港1离泊止时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans1StartPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.trans1EndPortCalendarText1, trans1EndPortCalendarColor1, () => {
                    //中转港1靠泊时间起时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans1EndPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.trans1EndPortCalendarText2, trans1EndPortCalendarColor2, () => {
                    //中转港1靠泊止时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans1EndPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.trans2StartPortCalendarText1, trans2StartPortCalendarColor1, () => {
                    //中转港2离泊时间起时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans2StartPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.trans2StartPortCalendarText2, trans2StartPortCalendarColor2, () => {
                    //中转港2离泊止时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans2StartPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <View style={{ marginLeft: 5, marginTop: 15, flexDirection: 'row' }}>
                {this.renderChooseCalendar(this.state.trans2EndPortCalendarText1, trans2EndPortCalendarColor1, () => {
                    //中转港2靠泊时间起时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans2EndPortCalendarText1: data.dateString}) 
                        }
                    })
                })}
                {this.renderChooseCalendar(this.state.trans2EndPortCalendarText2, trans2EndPortCalendarColor2, () => {
                    //中转港2靠泊止时间
                    this.props.navigation.navigate('CalendarModal', {
                        backData: (data) => {
                            this.setState({ trans2EndPortCalendarText2: data.dateString}) 
                        }
                    })
                })}
            </View>
            <TouchableOpacity
                style={{ marginLeft: 15, marginTop: 30, width: GlobalStyles.screenWidth - 30, height: 35, backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 17.5 }}
                onPress={() => {
                    //搜索按钮点击事件
                    ShipDuirationDynamicClass.sfg = this.state.startPortText
                    ShipDuirationDynamicClass.mdg = this.state.endPortText
                    ShipDuirationDynamicClass.zwcm = this.shipName
                    ShipDuirationDynamicClass.hc = this.voyage
                    ShipDuirationDynamicClass.lbrq1 = this.state.startPortCalendarText1 == '起始港离泊起' ? '' : this.state.startPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.lbrq2 = this.state.startPortCalendarText2 == '起始港离泊止' ? '' : this.state.startPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.lbrq3 = this.state.endPortCalendarText1 == '目的港靠泊起' ? '' : this.state.endPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.lbrq4 = this.state.endPortCalendarText2 == '目的港靠泊止' ? '' : this.state.endPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.zzg1 = this.state.trans1PortText
                    ShipDuirationDynamicClass.zzg2 = this.state.trans2PortText
                    ShipDuirationDynamicClass.LB_ZZG1 = this.state.trans1StartPortCalendarText1 == '中转港1离泊起' ? '' : this.state.trans1StartPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.LB_ZZG1end = this.state.trans1StartPortCalendarText2 == '中转港1离泊止' ? '' : this.state.trans1StartPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.KB_ZZG1 = this.state.trans1EndPortCalendarText1 == '中转港1靠泊起' ? '' : this.state.trans1EndPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.KB_ZZG1end = this.state.trans1EndPortCalendarText2 == '中转港1靠泊止' ? '' : this.state.trans1EndPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.LB_ZZG2 = this.state.trans2StartPortCalendarText1 == '中转港2离泊起' ? '' : this.state.trans2StartPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.LB_ZZG2end = this.state.trans2StartPortCalendarText2 == '中转港2离泊止' ? '' : this.state.trans2StartPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.KB_ZZG2 = this.state.trans2EndPortCalendarText1 == '中转港2靠泊起' ? '' : this.state.trans2EndPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.KB_ZZG2end = this.state.trans2EndPortCalendarText2 == '中转港2靠泊止' ? '' : this.state.trans2EndPortCalendarText2 + ' 00:00'
                    ShipDuirationDynamicClass.KB_START = this.state.firstPortCalendarText1 == '起始港靠泊起' ? '' : this.state.firstPortCalendarText1 + ' 00:00'
                    ShipDuirationDynamicClass.KB_START_END = this.state.firstPortCalendarText2 == '起始港靠泊止' ? '' : this.state.firstPortCalendarText2 + ' 00:00'
                    console.log('==',ShipDuirationDynamicClass)
                    NavigationUtil.goBack(this.props.navigation)
                    DeviceEventEmitter.emit(EMITTER_SHIPDUIRATION_SEARCH_TYPE, ShipDuirationDynamicClass)
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth - 30, height: 35, lineHeight: 35, textAlign: 'center', color: 'white', fontSize: 17 }}>{'搜  索'}</Text>
            </TouchableOpacity>
        </ScrollView>
    }

    render() {

        let navigationBar = <NavigationBar
            title={'船期搜索'}
            style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
        />;

        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                {this.renderSearchView()}
            </SafeAreaViewPlus>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    }
});
