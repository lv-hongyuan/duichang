/**
 * 船期动态搜索字段类
 */
export default {
    zwcm: '',
    hc:'',
    sfg:'',
    mdg:'',
    lbrq1:'',
    lbrq2:'',
    lbrq3:'',
    lbrq4:'',
    zzg1:'',            //中转港1
    zzg2:'',            //中转港2
    KB_ZZG1:'',         //中转港1靠泊时间起
    KB_ZZG1end:'',      //中转港1靠泊时间止
    KB_ZZG2:'',         //中转港2靠泊时间起
    KB_ZZG2end:'',      //中转港2靠泊时间止
    LB_ZZG1:'',         //中转港1离泊时间起
    LB_ZZG1end:'',      //中转港1离泊时间止
    LB_ZZG2:'',         //中转港2离泊时间起
    LB_ZZG2end:'',      //中转港2离泊时间止
    KB_START_END:'',    //始发港靠泊日期起
    KB_START:''         //始发港靠泊日期止
}