/**
 * 船期动态
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, TextInput, TouchableOpacity, ActivityIndicator, DeviceEventEmitter, RefreshControl } from 'react-native';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import NavigationUtil from '../../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons';
import publicDao from '../../../dao/publicDao'
import NetworkDao from '../../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../../common/NoDataView'
import { EMITTER_SHIPDUIRATION_SEARCH_TYPE } from '../../../common/EmitterTypes'
import ShipDuirationDynamicClass from './ShipDuirationDynamicClass'
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
let countDown = 30


let cellMargin = 25
let cellItemWidth = (GlobalStyles.screenWidth - 10 - cellMargin * 3) / 4 + 3

export default class ShipDurationDynamic extends Component {
    constructor(props) {
        super(props)
        this.startRow = 0
        this.state = {
            dataArr: [],
            indicatorAnimating: false, //菊花
            isNoData: false,
            refreshing: false,
            hideLoadingMore: true,     //是否显示上拉
            largeListData: [],           //大列表数据
            numbers: countDown
        }
    }

    //处理靠泊离泊字符串
    handleKbLbData(b, str) {
        if (str && str.length > 0) {
            let strs = []
            strs = str.split(' ')
            let newStr = strs[0]
            return b + newStr
        } else {
            return '--'
        }
    }

    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    getSearchData(data, iscountDown) {
        console.log('搜索条件:', data)
        this.setState({ indicatorAnimating: true, refreshing: true })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.zwcm = data.zwcm
        params.hc = data.hc
        params.sfg = data.sfg
        params.mdg = data.mdg
        params.lb1 = data.lbrq1
        params.lb2 = data.lbrq2
        params.lb3 = data.lbrq3
        params.lb4 = data.lbrq4
        params.zzg1 = data.zzg1
        params.zzg2 = data.zzg2
        params.KB_ZZG1 = data.KB_ZZG1
        params.KB_ZZG1end = data.KB_ZZG1end
        params.KB_ZZG2 = data.KB_ZZG2
        params.KB_ZZG2end = data.KB_ZZG2end
        params.LB_ZZG1 = data.LB_ZZG1
        params.LB_ZZG1end = data.LB_ZZG1end
        params.LB_ZZG2 = data.LB_ZZG2
        params.LB_ZZG2end = data.LB_ZZG2end
        params.KB_START = data.KB_START
        params.KB_START_END = data.KB_START_END
        params.startRow = 0
        params.pageSize = 30
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('getShipDateList.json', paramsStr)
            .then(data => {
                if (iscountDown) {
                    this.timer && clearInterval(this.timer)
                    let aa = countDown
                    //开始倒计时
                    this.timer = setInterval(() => {
                        aa--
                        this.setState({ numbers: aa })
                        if (aa == 0) {
                            this.timer && clearInterval(this.timer)
                            this.setState({
                                numbers: countDown
                            })
                            aa = countDown
                        }
                    }, 100);
                }
                this.setState({ indicatorAnimating: false, refreshing: false })
                if (this._largelist) this._largelist.endRefresh()
                if (data._backCode == '200') {
                    let tempA = this.handleLargeListData(data.entityList)
                    this.setState({ largeListData: tempA, dataArr: data.entityList, isNoData: false })
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.setState({ dataArr: [], isNoData: true, largeListData: [] })
                }
            })
            .catch(error => {
                console.log(error);
                this.timer && clearInterval(this.timer)
                if (this._largelist) this._largelist.endRefresh()
                this.setState({ indicatorAnimating: false, refreshing: false, numbers: countDown, isNoData: true })
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    loadMoreData(dic) {
        this.setState({ hideLoadingMore: false })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.zwcm = dic.zwcm
        params.hc = dic.hc
        params.sfg = dic.sfg
        params.mdg = dic.mdg
        params.lb1 = dic.lbrq1
        params.lb2 = dic.lbrq2
        params.lb3 = dic.lbrq3
        params.lb4 = dic.lbrq4
        params.zzg1 = dic.zzg1
        params.zzg2 = dic.zzg2
        params.KB_ZZG1 = dic.KB_ZZG1
        params.KB_ZZG1end = dic.KB_ZZG1end
        params.KB_ZZG2 = dic.KB_ZZG2
        params.KB_ZZG2end = dic.KB_ZZG2end
        params.LB_ZZG1 = dic.LB_ZZG1
        params.LB_ZZG1end = dic.LB_ZZG1end
        params.LB_ZZG2 = dic.LB_ZZG2
        params.LB_ZZG2end = dic.LB_ZZG2end
        params.KB_START = dic.KB_START
        params.KB_START_END = dic.KB_START_END
        params.startRow = ++this.startRow * 30
        params.pageSize = 30
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('getShipDateList.json', paramsStr)
            .then(data => {
                this._largelist.endLoading()
                this.setState({ hideLoadingMore: true })
                if (data._backCode == '200') {
                    let tempArr = this.state.dataArr.concat()
                    if (data.entityList && data.entityList.length > 0) {
                        tempArr.push(...data.entityList)
                        let tempA = this.state.largeListData.concat()
                        let tempB = data.entityList
                        let tempC = tempA[0].items
                        tempB.forEach(element => {
                            tempC.push(element)
                        });
                        this.setState({ dataArr: tempArr, largeListData: tempA })
                    } else {
                        this.refs.toast.show('没有更多数据了', 500)
                    }

                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.refs.toast.show('没有更多数据了', 500)
                }
            })
            .catch(error => {
                this._largelist.endLoading()
                this.setState({ hideLoadingMore: true })
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    componentDidMount() {
        this.getSearchData(ShipDuirationDynamicClass, false)

        this.searchListener = DeviceEventEmitter.addListener(EMITTER_SHIPDUIRATION_SEARCH_TYPE, (para) => {
            this._largelist.scrollToIndexPath({ section: 0, row: 0 }, false)
            // console.log('====', para)
            this.getSearchData(para, false)
        });
    }

    componentWillUnmount() {
        if (this.searchListener) {
            this.searchListener.remove();
        }
        this.cleanSearchText()
        this.timer && clearInterval(this.timer)
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //Navigation右上角搜索按钮
    renderRightButton() {
        return <TouchableOpacity
            onPress={() => {
                //搜索按钮点击事件
                NavigationUtil.goPage({}, 'ShipDuirationSearchView')
            }}
        >
            <View style={{ padding: 5, marginRight: 8 }}>
                <Ionicons
                    name={'ios-search'}
                    size={24}
                    style={{
                        marginRight: 8,
                        alignSelf: 'center',
                        color: 'white',
                    }} />
            </View>
        </TouchableOpacity>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //日照,厦门等
    renderCellTtile1(title) {
        return <Text style={{ width: cellItemWidth, height: 20, lineHeight: 20, textAlign: 'center', color: '#0877DE' }}>{title}</Text>
    }

    renderDirectImg() {
        return <Image style={{ marginTop: 7.5, width: cellMargin, height: 5 }} source={require('../../../../resource/direction-s.png')} />
    }

    renderPortView(title, color) {
        return <View style={{ width: cellItemWidth, height: 25, borderRadius: 2, backgroundColor: color }}>
            <Text style={{ height: 25, lineHeight: 25, width: cellItemWidth, textAlign: 'center', fontSize: 9.5 }}>{title}</Text>
        </View>
    }

    //清空搜索内容
    cleanSearchText() {
        ShipDuirationDynamicClass.zwcm = ''
        ShipDuirationDynamicClass.hc = ''
        ShipDuirationDynamicClass.sfg = ''
        ShipDuirationDynamicClass.mdg = ''
        ShipDuirationDynamicClass.lbrq1 = ''
        ShipDuirationDynamicClass.lbrq2 = ''
        ShipDuirationDynamicClass.lbrq3 = ''
        ShipDuirationDynamicClass.lbrq4 = ''
        ShipDuirationDynamicClass.zzg1 = ''
        ShipDuirationDynamicClass.zzg2 = ''
        ShipDuirationDynamicClass.KB_ZZG1 = ''
        ShipDuirationDynamicClass.KB_ZZG1end = ''
        ShipDuirationDynamicClass.KB_ZZG2 = ''
        ShipDuirationDynamicClass.KB_ZZG2end = ''
        ShipDuirationDynamicClass.LB_ZZG1 = ''
        ShipDuirationDynamicClass.LB_ZZG1end = ''
        ShipDuirationDynamicClass.LB_ZZG2 = ''
        ShipDuirationDynamicClass.LB_ZZG2end = ''
        ShipDuirationDynamicClass.KB_START_END = ''
        ShipDuirationDynamicClass.KB_START = ''
    }

    _renderItem = ({ row: row }) => {
        // console.log('row is :', row)
        let item = this.state.dataArr[row]
        let kb1 = this.handleKbLbData('靠:', item.KB_START)
        let kb2 = this.handleKbLbData('靠:', item.KB_ZZG1)
        let kb3 = this.handleKbLbData('靠:', item.KB_ZZG2)
        let kb4 = this.handleKbLbData('靠:', item.KB_END)
        let lb1 = this.handleKbLbData('离:', item.LB_START)
        let lb2 = this.handleKbLbData('离:', item.LB_ZZG1)
        let lb3 = this.handleKbLbData('离:', item.LB_ZZG2)
        let lb4 = this.handleKbLbData('离:', item.LB_END)

        return <View style={{ backgroundColor: 'white', paddingLeft: 5, paddingRight: 5 }}>
            <View style={{ flexDirection: 'row', height: 35, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Image style={{ marginTop: 10, height: 15, width: 24 }} source={require('../../../../resource/ship-r.png')} />
                    <Text style={{ marginLeft: 5, height: 35, lineHeight: 35, fontSize: 16, color: 'black' }}>{item.zwcm}</Text>
                </View>
                <Text style={{ marginRight: 5, height: 35, lineHeight: 35, fontSize: 15, color: 'black' }}>{item.hc}</Text>
            </View>
            <Image style={{ width: GlobalStyles.screenWidth, height: 2, marginTop: 5 }} source={require('../../../../resource/xu2.png')} />
            <View style={{ width: GlobalStyles.screenWidth, backgroundColor: '#FFFBFB', paddingBottom: 10 }}>
                <View style={{ marginTop: 5, width: GlobalStyles.screenWidth - 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    {this.renderCellTtile1((item.sfg && item.sfg.length > 0) ? item.sfg : '--')}
                    {this.renderDirectImg()}
                    {this.renderCellTtile1((item.zzg1 && item.zzg1.length > 0) ? item.zzg1 : '--')}
                    {this.renderDirectImg()}
                    {this.renderCellTtile1((item.zzg2 && item.zzg2.length > 0) ? item.zzg2 : '--')}
                    {this.renderDirectImg()}
                    {this.renderCellTtile1((item.mdg && item.mdg.length > 0) ? item.mdg : '--')}
                </View>
                <View style={{ marginTop: 10, width: GlobalStyles.screenWidth - 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    {this.renderPortView(kb1, '#DEF1BC')}
                    {this.renderPortView(kb2, '#DEF1BC')}
                    {this.renderPortView(kb3, '#DEF1BC')}
                    {this.renderPortView(kb4, '#DEF1BC')}
                </View>
                <View style={{ marginTop: 10, width: GlobalStyles.screenWidth - 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    {this.renderPortView(lb1, '#FEEDC3')}
                    {this.renderPortView(lb2, '#FEEDC3')}
                    {this.renderPortView(lb3, '#FEEDC3')}
                    {this.renderPortView(lb4, '#FEEDC3')}
                </View>
            </View>
        </View>
    }

    renderFlatList() {
        return <View style={{ flex: 1, marginTop: 10, backgroundColor: GlobalStyles.backgroundColor }}>
            {this.state.isNoData ?
                (this.state.numbers < countDown ? <TouchableOpacity
                    style={{ flex: 1 }}
                    activeOpacity={1}
                    onPressIn={(event) => {
                      this.startT = event.nativeEvent.locationY
                    }}
                    onPressOut={(event) => {
                      if ((event.nativeEvent.locationY - this.startT) > 60) {
                        this.refs.toast.show('刷新过于频繁,请于' + Math.ceil(this.state.numbers / 10) + '秒后重试', this.state.numbers * 100)
                      }
                    }}
                  /> : <LargeList
                    ref={ref => (this._largelist = ref)}
                    style={{ flex: 1 }}
                    data={this.state.largeListData}
                    heightForIndexPath={() => 142}
                    renderIndexPath={this._renderItem}
                    refreshHeader={ChineseWithLastDateHeader}
                    onRefresh={() => {
                        this.startRow = 0
                        this.cleanSearchText()
                        this.getSearchData(ShipDuirationDynamicClass, true)
                    }}
                />) :
                (<LargeList
                    ref={ref => (this._largelist = ref)}
                    style={{ flex: 1 }}
                    data={this.state.largeListData}
                    heightForIndexPath={() => 142}
                    renderIndexPath={this._renderItem}
                    refreshHeader={ChineseWithLastDateHeader}
                    onRefresh={() => {
                        this.startRow = 0
                        this.cleanSearchText()
                        this.getSearchData(ShipDuirationDynamicClass, true)
                    }}
                    loadingFooter={ChineseWithLastDateFooter}
                    onLoading={() => {
                        this.loadMoreData(ShipDuirationDynamicClass);
                    }}
                />)
            }
        </View>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'船期动态'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
                rightButton={this.renderRightButton()}
            />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <Toast ref="toast" position="center" />
                {this.renderFlatList()}
                {this.state.isNoData ? NoDataView.renderContent(40) : null}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}

            </SafeAreaViewPlus>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    }
});