//模板订舱
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, Image, TextInput, Keyboard } from 'react-native';
import NavigationUtil from '../../../Navigator/NavigationUtil';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import BookSpaceCellsUtil from '../../BookSpaceUtils/BookSpaceCellsUtil'
import ViewUtil from '../../../common/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import NetworkDao from '../../../dao/NetworkDao'
import publicDao from '../../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import NoDataView from '../../../common/NoDataView'
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";


export default class TemplateBookSpace extends Component {
    constructor(props) {
        super(props)
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],
            isNoData: false,
            searchText: '',
            refreshing: false,
            hideLoadingMore: true,     //是否显示上拉
            largeListData: [],           //大列表数据
        }
    }

    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    searchTemplage(text) {
        this.setState({ indicatorAnimating: true })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.startRow = 0
        params.pageSize = 30
        params.mbmc = text
        let paramsStr = JSON.stringify(params)
        console.log(paramsStr);
        NetworkDao.fetchPostNet('getAllMb.json', paramsStr)
            .then(data => {
                this._largelist.endRefresh();
                this.setState({ indicatorAnimating: false })
                if (data._backCode == '200') {
                    let tempA = this.handleLargeListData(data.entityList)
                    this.setState({ largeListData: tempA, dataArr: data.entityList, isNoData: false })
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.setState({ dataArr: [], isNoData: true, largeListData: [] })
                }
            })
            .catch(error => {
                this._largelist.endRefresh();
                this.setState({ indicatorAnimating: false, isNoData: true })
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    loadMoreData() {
        this.setState({ hideLoadingMore: false })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.startRow = ++this.startRow * 30
        params.pageSize = 30
        params.mbmc = this.state.searchText
        let paramsStr = JSON.stringify(params)
        NetworkDao.fetchPostNet('getAllMb.json', paramsStr)
            .then(data => {
                this._largelist.endLoading()
                this.setState({ hideLoadingMore: true })
                if (data._backCode == '200') {
                    let tempArr = this.state.dataArr.concat()
                    if (data.entityList && data.entityList.length > 0) {
                        tempArr.push(...data.entityList)
                        let tempA = this.state.largeListData.concat()
                        let tempB = data.entityList
                        let tempC = tempA[0].items
                        tempB.forEach(element => {
                            tempC.push(element)
                        });
                        this.setState({ dataArr: tempArr, largeListData: tempA })
                    } else {
                        this.refs.toast.show('没有更多数据了', 500)
                    }
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.setState({ dataArr: [], isNoData: true, largeListData: [] })
                } else if (data._backCode == '007') {
                    this.refs.toast.show('没有更多数据了', 500)
                }
            })
            .catch(error => {
                this._largelist.endLoading()
                this.setState({ hideLoadingMore: true })
                this.refs.toast.show('搜索失败', 800)
            })
    }

    componentDidMount() {
        this.searchTemplage(this.state.searchText)
    }

    //搜索的view
    renderSearchView() {
        return <View style={{ marginTop: 10, backgroundColor: 'white', width: GlobalStyles.screenWidth, height: 90, flexDirection: 'row' }}>
            <Image style={{ height: 70, width: 67 }} source={require('../../../../resource/rudder.png')} />
            <View style={{ flexDirection: 'row', marginTop: 22.5, marginLeft: -52, height: 45, width: GlobalStyles.screenWidth - 95, borderColor: '#9A9A9A', borderWidth: 1, borderRadius: 3 }}>
                <Image style={{ marginLeft: 10, marginTop: 10, width: 20, height: 20 }} source={require('../../../../resource/zoomer-g.png')} />
                <TextInput
                    style={{ marginLeft: 10, height: 40, paddingTop: 0, paddingBottom: 0, width: GlobalStyles.screenWidth - 70 }}
                    clearButtonMode={'while-editing'}
                    placeholder={'模板名称'}
                    fontSize={17}
                    value={this.state.searchText}
                    returnKeyType={'search'}
                    onSubmitEditing={(event) => {
                        console.log('搜索的内容:', event.nativeEvent.text)
                        let txt = event.nativeEvent.text
                        if (txt && txt.length > 0) {
                            this.searchTemplage(txt)
                        } else {
                            this.refs.toast.show('请输入模板名称', 500)
                        }
                    }}
                    onChangeText={(text) => {
                        this.setState({ searchText: text })
                    }}
                />
            </View>
            <TouchableOpacity style={styles.searchBtn}
                activeOpacity={0.75}
                onPress={() => {
                    Keyboard.dismiss()
                    console.log('货运跟踪搜索按钮点击事件')
                    if (this.state.searchText && this.state.searchText.length > 0) {
                        this.searchTemplage(this.state.searchText)
                    } else {
                        this.refs.toast.show('请输入模板名称', 500)
                    }
                }}
            >
                <Text style={{ color: 'white', fontSize: 17 }}>搜索</Text>
            </TouchableOpacity>
        </View>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    _renderItem = ({ row: row }) => {
        let item = this.state.dataArr[row]
        let huoming = item.hm ? item.hm : '  '
        return <View>
            <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
            <View
                style={{
                    borderRadius: 10,
                    padding: 10,
                    flex: 1,
                    backgroundColor: '#fff',
                }}
            >
                <Text style={styles.text}>模版名称:{item.mbmc}</Text>
                <Image style={{ width: GlobalStyles.screenWidth - 30, height: 2, marginLeft: -15, marginTop: 10 }} source={require('../../../../resource/xu2.png')} />
                <Text style={{ fontSize: 13, color: 'black', marginTop: 10, height: 20, lineHeight: 20, marginLeft: 8 }}>{huoming + ' | ' + item.ystk}</Text>
                <View style={{ height: 25, flexDirection: 'row', marginVertical: 5, marginHorizontal: 8 }}>
                    <Image style={{ marginTop: 3, height: 15, width: 24 }}
                        source={require('../../../../resource/ship.png')}
                    />
                    <Text style={{ marginLeft: 15, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.sfg}</Text>
                    <Image style={{ marginTop: 9, width: 60, height: 5, marginLeft: 20 }} source={require('../../../../resource/direction.png')} />
                    <Text style={{ marginLeft: 20, height: 25, lineHeight: 25, fontSize: 16, color: '#0877DE' }}>{item.mdg}</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 8 }}>
                    <Text numberOfLines={1} style={{ fontSize: 13, color: 'black', lineHeight: 28, width: '59%' }}>收货人: {item.shrqc}</Text>
                    <Text style={{ fontSize: 13, color: 'black', lineHeight: 28 }}>箱型 * 箱量: {item.xx} * {item.sl}</Text>
                </View>

                <View style={{ width: GlobalStyles.screenWidth, height: 0.5, marginLeft: -15, backgroundColor: GlobalStyles.backgroundColor }}></View>

                <View style={{ height: 28, flexDirection: 'row', justifyContent: 'flex-end', }}>
                    <TouchableOpacity style={{marginTop:5, width: 75, height: 28, borderRadius: 3, borderColor: GlobalStyles.nav_bar_color, borderWidth: 1 }}
                        onPress={() => {
                            this.TemplateButton(item)
                        }}
                    >
                        <Text style={{ height: 25, width: 70, lineHeight: 25, textAlign: 'center', fontSize: 13, color: GlobalStyles.nav_bar_color }}>模板下单</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    }

    TemplateButton(item) {
        this.setState({ indicatorAnimating: true })
        let params = {}
        params.token = publicDao.CURRENT_TOKEN
        params.startRow = 0
        params.pageSize = 30
        params.mbmc = ""
        params.mbid = item.id
        let paramsStr = JSON.stringify(params)
        console.log(paramsStr);
        NetworkDao.fetchPostNet('getAllMb.json', paramsStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backCode == '200') {
                    let Data = data.entityList[0]
                    Data.orderId = item.id
                    Data.isTemp = true      //是模板下单进入
                    Data.isTransBill = false
                    Data.startPort = data.entityList[0].sfg
                    Data.endPort = data.entityList[0].mdg
                    NavigationUtil.goPage(Data, 'CreateOrderAgain')
                } else if (data._backCode == '224') {
                    this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
                        NavigationUtil.resetToLoginView()
                    });
                } else if (data._backCode == '404') {
                    this.setState({ dataArr: [], isNoData: true, largeListData: [] })
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
            })
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'模板订舱'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (<SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color} bottomInset={false}>
            {navigationBar}

            <Toast ref="toast" position="center" />
            {this.renderSearchView()}
            {this.state.isNoData ? NoDataView.renderContent() : null}
            {this.state.isNoData ?
                <LargeList
                    ref={ref => (this._largelist = ref)}
                    data={this.state.largeListData}
                    heightForIndexPath={() => 190}
                    renderIndexPath={this._renderItem}
                    refreshHeader={ChineseWithLastDateHeader}
                    onRefresh={() => {
                        this.setState({ searchText: '' })
                        this.startRow = 0
                        this.searchTemplage('')
                    }}
                /> :
                <LargeList
                    ref={ref => (this._largelist = ref)}
                    data={this.state.largeListData}
                    heightForIndexPath={() => 190}
                    renderIndexPath={this._renderItem}
                    refreshHeader={ChineseWithLastDateHeader}
                    onRefresh={() => {
                        this.setState({ searchText: '' })
                        this.startRow = 0
                        this.searchTemplage('')
                    }}
                    loadingFooter={ChineseWithLastDateFooter}
                    onLoading={() => {
                        this.loadMoreData();
                    }}
                />
            }

            {this.state.indicatorAnimating && (
                <ActivityIndicator
                    style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                    size={GlobalStyles.isIos ? 'large' : 40}
                    color='gray'
                />)}
        </SafeAreaViewPlus >)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBtn: {
        height: 45,
        width: 60,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: GlobalStyles.nav_bar_color,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        marginTop: 22.5
    },
    cellLeftText: {
        color: "black",
        fontSize: 15,
        width: 80,
        // paddingTop: 5,
        // paddingBottom: 5,
        marginLeft: 15,
        height: 25,
        lineHeight: 25
    },
    cellRightText: {
        color: 'black',
        fontSize: 15,
        // paddingTop: 5,
        // paddingBottom: 5,
        height: 25,
        lineHeight: 25
    },
    cardTopView: {
        height: 25,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    text: {
        lineHeight: 25,
        fontSize: 13,
        color: '#555',
        marginLeft: 8
    }
});