/**
 * 航线促销
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image, TextInput, TouchableOpacity } from 'react-native';
import GlobalStyles from '../../../common/GlobalStyles';
import NavigationBar from '../../../common/NavigationBar'
import NavigationUtil from '../../../Navigator/NavigationUtil';
import SafeAreaViewPlus from '../../../common/SafeAreaViewPlus'
import ViewUtil from '../../../common/ViewUtil'
import LargeListPlus from '../../../common/LargeListPlus'


export default class ShipPromotion extends Component {
    constructor(props) {
        super(props)
        this.state = {
          isNoData:true
        }
      }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'航线促销'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return (
            <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <LargeListPlus
                  data={[]}
                  NoDataViewheight={110}
                />
            </SafeAreaViewPlus>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    //   justifyContent: 'center',
    //   alignItems: 'center',
      backgroundColor: GlobalStyles.backgroundColor,
    }
  });