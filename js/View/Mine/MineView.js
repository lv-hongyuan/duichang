/**
 * 我的
 */

import React, { Component } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, DeviceEventEmitter, ImageBackground, Image } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil'
import NavigationBar from '../../common/NavigationBar'
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus'
import GlobalStyles from '../../common/GlobalStyles'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import DataDao from '../../dao/DataDao'
import AlertView from '../../common/AlertView'
import BookSpaceCellsUtil from '../BookSpaceUtils/BookSpaceCellsUtil'
import { EMITTER_LOGIN_SUCESS, EMITTER_LOGOUT_SUCCESS } from '../../common/EmitterTypes'
import DeviceInfo from 'react-native-device-info'


let dataDao = new DataDao();


export default class MineView extends Component {
  constructor(props) {
    super(props)
    console.log('是否登陆', publicDao.IS_LOGOUT)
    this.state = {
      indicatorAnimating: false, //菊花
      isLogout: publicDao.IS_LOGOUT,
      company: publicDao.COMPANY,
      userName: publicDao.USERNAME,
    }
  }

  componentDidMount() {
    this.loginListener = DeviceEventEmitter.addListener(EMITTER_LOGIN_SUCESS, () => {
      console.log('登陆成功!!!')
      this.setState({ isLogout: '0', company: publicDao.COMPANY, userName: publicDao.USERNAME })
    })
  }


  //点击了确定退出登录
  alertSureDown = () => {
    this.setState({ indicatorAnimating: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('logout.json', paramsStr)
      .then(data => {
        this.setState({ indicatorAnimating: false })
        if (data._backcode == '200') {
          // NavigationUtil.resetToLoginView()
          publicDao.IS_LOGOUT = "1"
          let loginJ = {}
          loginJ.isMine = '1'
          loginJ.routeName = 'MineView'
          loginJ.isMineMsg = '0'
          NavigationUtil.goPage(loginJ, 'LoginView')

          let currentInfo = {}
          currentInfo.uniqueCode = DeviceInfo.getUniqueID()
          currentInfo.userName = publicDao.USERNAME
          currentInfo.companyName = publicDao.COMPANY
          currentInfo.token = ''
          currentInfo.phoneNum = publicDao.PHONE_NUMBER
          currentInfo.phonePWD = publicDao.PHONE_PWD
          currentInfo.isPhoneLogin = publicDao.IS_PHONE_LOGIN
          currentInfo.isIdentifyLogin = publicDao.IS_IDENTIFY_LOGIN
          dataDao.saveCurrentUserInfo(currentInfo)
          DeviceEventEmitter.emit(EMITTER_LOGOUT_SUCCESS);
          this.setState({ isLogout: '1' })
        } else if (data._backcode == '202') {
          this.refs.toast.show('登录信息已失效', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backcode == '201') {
          // this.refs.toast.show('token已失效', 800)
          this.refs.toast.show('token错误', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        }
      })
      .catch(error => {
        this.setState({ indicatorAnimating: false })
        this.refs.toast.show('退出登录失败', 800)
      })
  }

  alertCancelDown = () => {
    console.log('点击了alert取消按钮')
  }

  renderLine() {
    return <View style={{ width: GlobalStyles.screenWidth, height: 5, backgroundColor: GlobalStyles.separate_line_color }}></View>
  }

  renderTopView() {
    return <ImageBackground
      style={{ flexDirection: 'row', width: GlobalStyles.screenWidth, height: 180, alignItems: 'center', paddingTop: 20 }}
      source={require('../../../resource/mineBack.png')}
    >
      <Image
        style={{ marginLeft: 30, width: 80, height: 80, borderRadius: 40, borderWidth: 5, borderColor: 'white' }}
        source={require('../../../resource/admin-photo.png')}
      />
      {this.state.isLogout == '0' ? <View style={{ height: 80, marginLeft: 15 }}>
        <Text style={{ marginTop: 10, height: 30, lineHeight: 30, color: 'white', fontSize: 17, fontWeight: '600' }}>{this.state.company}</Text>
        <Text style={{ height: 30, lineHeight: 30, color: 'white', fontSize: 16 }}>{this.state.userName}</Text>
      </View> : <TouchableOpacity style={{ height: 80, marginLeft: 15 }}
        activeOpacity={0.75}
        onPress={() => {
          let loginJ = {}
          loginJ.isMine = '1'
          loginJ.routeName = 'MineView'
          loginJ.isMineMsg = '0'
          NavigationUtil.goPage(loginJ, 'LoginView')
        }}
      >
          <Text style={{ marginTop: 25, height: 30, lineHeight: 30, color: 'white', fontSize: 18, fontWeight: '600' }}>未登录</Text>
        </TouchableOpacity>}
    </ImageBackground>
  }

  renderMiddleView() {
    return <View style={{ marginTop: 5 }}>
      {BookSpaceCellsUtil.renderOutCell('消息通知', this.state.showBaseInfo, () => {
        console.log('消息通知:', publicDao.IS_LOGOUT)
        if (publicDao.IS_LOGOUT == '1') {
          let loginJ = {}
          loginJ.isMine = '1'
          loginJ.routeName = 'MineView'
          loginJ.isMineMsg = '1'
          NavigationUtil.goPage(loginJ, 'LoginView')
        } else {
          NavigationUtil.goPage({}, 'myMessage')
        }
      })}
      {this.renderLine()}
      {BookSpaceCellsUtil.renderOutCell('联系我们', this.state.showBaseInfo, () => {
        console.log('办公联系方式点击事件')
        NavigationUtil.goPage({}, 'ContactOfficeView')
      })}
      {this.renderLine()}
      {BookSpaceCellsUtil.renderOutCell('我的下载', this.state.showBaseInfo, () => {
        if (publicDao.IS_LOGOUT == '1') {
          let loginJ = {}
          loginJ.isMine = '1'
          loginJ.routeName = 'MineView'
          loginJ.isMineMsg = '1'
          NavigationUtil.goPage(loginJ, 'LoginView')
        } else {
          NavigationUtil.goPage({}, 'DownLoadFireView')
        }
      })}
    </View>
  }

  renderLogoutView() {
    return <TouchableOpacity
      style={{
        position: 'absolute', height: 45, backgroundColor: GlobalStyles.nav_bar_color,
        justifyContent: 'center', alignItems: 'center', bottom: 40, width: '80%',
        borderRadius: 25, marginLeft: '10%'
      }}
      onPress={() => {
        if (publicDao.IS_LOGOUT == '0') {
          this.refs.AlertView.showAlert();
        } else {
          // NavigationUtil.goPage({}, 'LoginView')
          let loginJ = {}
          loginJ.isMine = '1'
          loginJ.routeName = 'MineView'
          loginJ.isMineMsg = '0'
          NavigationUtil.goPage(loginJ, 'LoginView')
        }

      }}
    >
      <Text style={{ height: 45, lineHeight: 45, width: 100, fontSize: 20, color: '#fff', textAlign: 'center' }}>{this.state.isLogout == '0' ? '退出登录' : '前往登录'}</Text>
    </TouchableOpacity>
  }
  render() {
    // let navigationBar =
    //   <NavigationBar
    //     title={'我的'}
    //     style={{ backgroundColor: GlobalStyles.nav_bar_color }}
    //   />;
    return (
      <View style={styles.container} >
        {Platform.OS === 'android' ? <StatusBar barStyle={'light-content'} translucent backgroundColor={'transparent'} /> : null}
        <Toast ref="toast" position="center" />
        {this.renderTopView()}
        {this.renderMiddleView()}
        {this.renderLogoutView()}
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}
        <AlertView
          ref="AlertView"
          TitleText="确定退出登录?"
          // DesText=" "
          CancelText='取消'
          OkText='确定'
          BottomRightFontColor='#BA1B25'
          alertSureDown={this.alertSureDown}
          alertCancelDown={this.alertCancelDown}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: GlobalStyles.backgroundColor,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});