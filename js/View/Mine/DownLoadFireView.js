import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import ViewUtil from '../../common/ViewUtil'
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus';
import publicDao from '../../dao/publicDao';
import NetworkDao from '../../dao/NetworkDao';
import Toast from 'react-native-easy-toast';
import { LargeList } from "react-native-largelist-v3";
import NoDataView from '../../common/NoDataView';
import Ionicons from 'react-native-vector-icons/Ionicons'
import RNFetchBlob from 'rn-fetch-blob'
import DataDao from '../../dao/DataDao'
import AsyncStorage from '@react-native-community/async-storage';
import { orderBy } from 'lodash'
import DateUtil from '../../common/DateUtil'

let dataDao = new DataDao();


export default class downLoadFireView extends Component {
  constructor(props) {
    super(props);
    this.startRow = 0
    this.state = {
      dataArr: [],
      largeListData: [],           //大列表数据
      isNoData: false,
      refreshing: false,
      hideLoadingMore: true,     //是否显示上拉
      indicatorAnimating: false, //菊花
      RightTitleName: false,
      selectItem: [],                              //多选数据
    }
  }

  async componentDidMount() {
    await this.getData()
  }



  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  async getData() {
    // if (publicDao.IS_LOGOUT == '1') {
    //   this.setState({ largeListData: [], dataArr: [], isNoData: true, RightTitleName: false })
    //   return
    // }
    this.setState({ indicatorAnimating: true })
    try {
      let res = await RNFetchBlob.fs.ls(publicDao.DOWNLOAD_FILE)
      console.log('===', res)
      if (res.length == 0) {
        this.setState({ largeListData: [], dataArr: [], isNoData: true, RightTitleName: false, indicatorAnimating: false })
        return
      }
      let tempB = []
      res.forEach(async (ele, index, arr) => {
        let result = await AsyncStorage.getItem(ele)
        if (result === null) {
        } else {
          let jsonData = JSON.parse(result)
          jsonData.select = false
          // console.log('文件信息', jsonData)
          tempB.push(jsonData)
        }
        if (index === res.length - 1) {
          tempB = orderBy(tempB, ['timeStamp'], ['desc'])
          // console.log('文件信息数组===', tempB)
          if (tempB.length == 0) {
            this.setState({ largeListData: [], dataArr: [], isNoData: true, RightTitleName: false, indicatorAnimating: false })
          } else {
            let tempA2 = this.handleLargeListData(tempB)
            this.setState({ largeListData: tempA2, dataArr: tempB, isNoData: false, indicatorAnimating: false })
          }
        }
      });
    } catch (error) {
      console.log('扫描文件夹错误', error)
      this.setState({ largeListData: [], dataArr: [], isNoData: true, RightTitleName: false, indicatorAnimating: false })
    }

  }

  //左侧返回按钮
  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  //左侧选择框
  _selectItemPress(item) {
    let IDList = []
    let FistList = this.state.dataArr.concat()
    for (var i = 0; i < FistList.length; i++) {
      IDList.push(FistList[i])
    }
    let cellindex = IDList.indexOf(item)
    let data = this.state.selectItem
    if (!item.select) {
      data.push(item)
    } else {
      let aa = data.indexOf(item)
      this.state.selectItem.splice(aa, 1)
    }
    console.log('已选：', this.state.selectItem);
    FistList[cellindex].select = !item.select
    this.setState({ dataArr: FistList, selectItem: data })
  }



  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]
    let downloadTime = DateUtil.timestampToTime(item.timeStamp)
    return <TouchableOpacity onPress={() => {
      if (this.state.RightTitleName) {
        this._selectItemPress(item)
      } else {
        // 不进行选择时的列表点击事件  
        if (item.fileName.indexOf('.pdf') != -1) {
          let data = {}
          data.path = publicDao.DOWNLOAD_FILE + '/' + item.fileName
          NavigationUtil.goPage(data, 'ShowPDFView')
        } else if (item.fileName.indexOf('.zip') != -1) {
          let data = {}
          data.path = publicDao.DOWNLOAD_FILE + '/' + item.fileName
          data.fileName = item.fileName
          NavigationUtil.goPage(data, 'ShowZIPView')
        }
      }
    }}>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View style={styles.cardView}>
        <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
          {this.state.RightTitleName ? <Ionicons
            name={item.select ? 'ios-radio-button-on' : 'ios-radio-button-off'}
            size={24}
            color={GlobalStyles.nav_bar_color}
            style={{ marginRight: 10 }}>
          </Ionicons> : null}
          <Text numberOfLines={1} style={styles.text}>{item.fileName}</Text>
        </View>
        <Text style={styles.text1}>{downloadTime}</Text>
      </View>
    </TouchableOpacity>
  }

  //清除全选
  cleanselectAllItem() {
    let FistList = this.state.dataArr.concat()
    for (var i = 0; i < FistList.length; i++) {
      FistList[i].select = false
    }
    this.setState({ selectItem: [], allsclectitem: false })
    console.log('已取消选择');
  }

  //右侧多选按钮
  rightButtonCLick() {
    if (this.state.dataArr.length == 0) {
      this.refs.toast.show('没有可选择的文件', 800)
      return
    }
    this.setState({ RightTitleName: !this.state.RightTitleName })
    this.cleanselectAllItem()
  }

  //列表全选
  allSelectClick() {
    let a = !this.state.allsclectitem
    let IDList = []
    let FistList = this.state.dataArr.concat()
    if (a == true) {
      for (var i = 0; i < FistList.length; i++) {
        FistList[i].select = true
        IDList.push(FistList[i])
      }
    } else if (a == false) {
      for (var i = 0; i < FistList.length; i++) {
        FistList[i].select = false
      }
      IDList = []
    }
    this.setState({ selectItem: IDList, allsclectitem: a })
    console.log('全选数据:', IDList);
  }

  render() {
    let navigationBar = <NavigationBar
      title={'我的下载'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      rightButton={ViewUtil.getRightTextButton(this.state.RightTitleName ? '取消选择' : '选择删除', () => this.rightButtonCLick())}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {this.state.RightTitleName ? <View style={styles.titleView}>
          <TouchableOpacity style={styles.titleTouchableLeft}
            onPress={() => {
              this.allSelectClick()     //点击全选/取消全选事件
            }}
          >
            <Ionicons
              name={this.state.allsclectitem ? 'ios-radio-button-on' : 'ios-radio-button-off'}
              size={24}
              color={GlobalStyles.nav_bar_color}
              style={{ marginRight: 10 }}>
            </Ionicons>
            <Text style={{ color: '#555', fontSize: 16 }}>{this.state.allsclectitem ? '取消选中' : '全选'}</Text>

          </TouchableOpacity>


          <TouchableOpacity style={styles.titleTouchableRight}
            onPress={() => {
              //点击删除事件
              if (this.state.selectItem && this.state.selectItem.length > 0) {
                for (let i = 0; i < this.state.selectItem.length; i++) {
                  let tempJ = this.state.selectItem[i]
                  let fileName = publicDao.DOWNLOAD_FILE + '/' + tempJ.fileName
                  RNFetchBlob.fs.unlink(fileName)
                    .then(async (res) => {
                      dataDao.clearDownloadFileInfo(fileName, (data) => {
                        //删除文件
                      })
                      if (i == this.state.selectItem.length - 1) {
                        await this.getData()
                      }
                    })
                    .catch((error) => {
                      console.log('删除文件失败', error)
                    })
                }
              } else {
                this.refs.toast.show('请选择要删除的文件', 800)
              }

            }}
          ><Text style={{ color: '#fff', fontSize: 16 }}>删除</Text>

          </TouchableOpacity>
        </View> : null}
        {this.state.isNoData ? NoDataView.renderContent(40)  : null}
        <LargeList
          ref={ref => (this._largelist = ref)}
          style={{ height: '100%', paddingHorizontal: 10 }}
          data={this.state.largeListData}
          heightForIndexPath={() => 65}
          renderIndexPath={this.renderItem}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}

      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cardView: {
    borderRadius: 10,
    padding: 10,
    paddingLeft: 15,
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'space-between'
  },
  text: {
    fontSize: 14,
    color: '#555',
  },
  text1: {
    marginTop: 2,
    fontSize: 14,
    color: '#A9A9A9',
    paddingBottom: 5,
  },
  titleView: {
    flexDirection: 'row',
    marginVertical: 5,
    justifyContent: 'space-between',
    marginHorizontal: 10
  },
  titleTouchableRight: {
    backgroundColor: GlobalStyles.nav_bar_color,
    height: 35,
    width: '30%',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleTouchableLeft: {
    flexDirection: 'row',
    // backgroundColor:'red',
    paddingLeft: 15,
    alignItems: 'center',
  }
});