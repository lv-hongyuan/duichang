import React, { Component } from 'react';
import { Modal, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtil';
import ViewUtil from '../../common/ViewUtil'
import GlobalStyles from '../../common/GlobalStyles';
import NavigationBar from '../../common/NavigationBar';
import SafeAreaViewPlus from '../../common/SafeAreaViewPlus';
import publicDao from '../../dao/publicDao';
import NetworkDao from '../../dao/NetworkDao';
import Toast from 'react-native-easy-toast';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader, ChineseWithLastDateFooter } from "react-native-spring-scrollview/Customize";
import BookSpaceBottomModal from '../BookSpaceUtils/BookSpaceBottomModal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NoDataView from '../../common/NoDataView'

export default class myMessage extends Component {
  constructor(props) {
    super(props);
    this.startRow = 0
    this.state = {
      dataArr: [],
      largeListData: [],           //大列表数据
      isNoData: false,
      refreshing: false,
      hideLoadingMore: true,     //是否显示上拉
      indicatorAnimating: false, //菊花
      getAreaListArr: [{ 'name': '全部', 'areaId': null }, { 'name': '已读', 'areaId': '20' }, { 'name': '未读', 'areaId': '10' }],      //片区列表
      areaId: null,             //片区
      blockName: '全部',
      showModal: false,        //是否展示Modal
      showDectail: {}
    }
  }

  componentDidMount() {
    this.getData(this.state.areaId)
  }



  //符合大列表的数据结构
  handleLargeListData(dataArray) {
    let rData = [{ items: [] }];
    dataArray.forEach(element => {
      rData[0].items.push(element);
    });
    return rData
  }

  getData(newsStatus) {
    this.setState({ indicatorAnimating: true, refreshing: true })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.newsStatus = newsStatus
    params.startRow = 0
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getMessageList.json', paramsStr)
      .then(data => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false })
        if (data._backCode == '200') {
          let tempA = this.handleLargeListData(data.entityList)
          this.setState({ largeListData: tempA, dataArr: data.entityList, isNoData: false })
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '802') {
          this.setState({ dataArr: [], isNoData: true, largeListData: [] })
        }
      })
      .catch(error => {
        this._largelist.endRefresh();
        this.setState({ indicatorAnimating: false, refreshing: false, isNoData: false })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  loadMoreData(newsStatus) {
    this.setState({ hideLoadingMore: false })
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.newsStatus = newsStatus
    params.startRow = ++this.startRow * 30
    params.pageSize = 30
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('getMessageList.json', paramsStr)
      .then(data => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        if (data._backCode == '200') {
          let tempArr = this.state.dataArr.concat()
          if (data.entityList && data.entityList.length > 0) {
            tempArr.push(...data.entityList)
            let tempA = this.state.largeListData.concat()
            let tempB = data.entityList
            let tempC = tempA[0].items
            tempB.forEach(element => {
              tempC.push(element)
            });
            this.setState({ dataArr: tempArr, largeListData: tempA })
          } else {
            this.refs.toast.show('没有更多数据了', 500)
          }
        } else if (data._backCode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backCode == '404') {
          this.refs.toast.show('没有更多数据了', 500)
        } else if (data._backCode == '802') {
          this.refs.toast.show('没有更多数据了', 500)
        }
      })
      .catch(error => {
        this._largelist.endLoading()
        this.setState({ hideLoadingMore: true })
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  //左侧返回按钮
  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  //右侧检索按钮
  rightButtonCLick() {
    this.refs.bookSpaceBottomModal.show('name', this.state.getAreaListArr)
  }

  //标记已读
  updateToRead(id) {
    let params = {}
    params.token = publicDao.CURRENT_TOKEN
    params.ids = id
    let paramsStr = JSON.stringify(params)
    NetworkDao.fetchPostNet('updateToRead.json', paramsStr)
      .then(data => {
        if (data._backcode == '200') {
          console.log('12345676543212345');

          this.getData(this.state.areaId)
        } else if (data._backcode == '224') {
          this.refs.toast.show('登录信息已失效,请重新登录', 500, () => {
            NavigationUtil.resetToLoginView()
          });
        } else if (data._backcode == '404') {
          this.setState({ dataArr: [], largeListData: [] })
        }
      })
      .catch(error => {
        this.refs.toast.show('获取数据失败', 500)
      })
  }

  renderItem = ({ row: row }) => {
    let item = this.state.dataArr[row]
    return <TouchableOpacity onPress={() => {
      this.setState({ showModal: true, showDectail: item })
    }}>
      <View style={{ width: GlobalStyles.screenWidth, height: 10, backgroundColor: GlobalStyles.backgroundColor }}></View>
      <View style={styles.cardView}>
        <View style={{ flexDirection: 'row', alignItems: 'center', width: '50%' }}>
          <FontAwesome
            name={item.newsStatus == '10' ? 'envelope-o' : 'envelope-open-o'}
            size={22}
            style={{
              alignSelf: 'center',
              color: item.newsStatus == '10' ? GlobalStyles.nav_bar_color : '#ccc',
              marginRight: 10
            }}
          />
          <Text numberOfLines={1} style={styles.text}>{item.newsMessage}</Text>
        </View>
        <Text style={styles.text}>{item.sendTime}</Text>
      </View>
    </TouchableOpacity>
  }

  //展示详情界面
  showModal(item) {
    return <Modal
      animationType="none"
      transparent={true}
      visible={this.state.showModal}
    >
      <View style={{
        backgroundColor: 'rgba(0,0,0,0.3)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}
      >
        <View style={{
          backgroundColor: '#fff',
          borderRadius: 5,
          width: '80%',
          height: '70%',
          overflow: 'hidden'
        }}>
          <View style={{
            backgroundColor: GlobalStyles.nav_bar_color, height: 40, width: '100%',
            justifyContent: 'center', alignItems: 'center'
          }}>
            <Text style={{ fontSize: 16, color: '#fff' }}>消息详情</Text>
          </View>
          <ScrollView style={{ flex: 1 }}>
            <Text style={[styles.text, { margin: 15, lineHeight: 25 }]}>{item.newsMessage}</Text>
            <Text style={[styles.text, { margin: 15 }]}>{item.sendTime}</Text>
          </ScrollView>
          <TouchableOpacity
            style={{
              backgroundColor: GlobalStyles.nav_bar_color, width: '100%',
              height: 50, justifyContent: 'center', alignItems: 'center',
              position: 'absolute', bottom: 0
            }}
            onPress={() => {
              this.setState({ showModal: false })
              if (item.newsStatus == '10') {
                this.updateToRead(item.id)
              }
            }}
          >
            <Text style={{ color: '#fff', fontSize: 16 }}>关闭</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>

  }

  render() {
    let navigationBar = <NavigationBar
      title={'消息列表'}
      style={{ backgroundColor: GlobalStyles.nav_bar_color }}
      leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      rightButton={ViewUtil.getRightTextButton(this.state.blockName, () => this.rightButtonCLick())}
    />;
    return (
      <SafeAreaViewPlus style={styles.container} topColor={GlobalStyles.nav_bar_color}>
        {navigationBar}
        {this.showModal(this.state.showDectail)}
        {this.state.isNoData ? NoDataView.renderContent(40) : null}
        {this.state.isNoData ?
          <LargeList
            ref={ref => (this._largelist = ref)}
            style={{ height: '100%', paddingHorizontal: 10 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 55}
            renderIndexPath={this.renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            onRefresh={() => {
              this.startRow = 0
              this.getData(this.state.areaId)
            }}
          /> :
          <LargeList
            ref={ref => (this._largelist = ref)}
            style={{ height: '100%', paddingHorizontal: 10 }}
            data={this.state.largeListData}
            heightForIndexPath={() => 55}
            renderIndexPath={this.renderItem}
            refreshHeader={ChineseWithLastDateHeader}
            loadingFooter={ChineseWithLastDateFooter}
            onRefresh={() => {
              this.startRow = 0
              this.getData(this.state.areaId)
            }}
            onLoading={() => {
              this.loadMoreData(this.state.areaId);
            }}
          />}
        <BookSpaceBottomModal
          ref="bookSpaceBottomModal"
          callBack={data => {
            this.setState({
              blockName: data.name,
              areaId: data.areaId
            })
            this.getData(data.areaId)
          }}
        />
        <Toast ref="toast" position="center" style={{ zIndex: 1111 }} />
        {this.state.indicatorAnimating && (
          <ActivityIndicator
            style={[styles.indicatorStyle, { zIndex: 1000, position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
            size={GlobalStyles.isIos ? 'large' : 40}
            color='gray'
          />)}

      </SafeAreaViewPlus>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.backgroundColor,
  },
  cardView: {
    borderRadius: 10,
    padding: 10,
    paddingLeft: 15,
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    fontSize: 14,
    color: '#555',
  }
});