/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity,StatusBar,TextInput,NetInfo,DeviceEventEmitter } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import DataDao from './js/dao/DataDao'
import publicDao from './js/dao/publicDao'
import GlobalStyles from './js/common/GlobalStyles';
import RNFetchBlob from 'rn-fetch-blob';
import { EMITTER_NETCHANGE_TYPE } from './js/common/EmitterTypes'
//禁用字体大小跟随系统
Text.defaultProps = { ...(Text.defaultProps || {}), allowFontScaling: false };
TextInput.defaultProps = { ...(TextInput.defaultProps || {}), allowFontScaling: false };

let ds = new DataDao();

type Props = {};
export default class App extends Component<Props> {

  async getUserInfo() {
    const { navigate } = this.props.navigation
    await ds.getCurrentUserInfo(data => {
      var jsonData = JSON.parse(data)
      publicDao.CURRENT_TOKEN = jsonData.token
      publicDao.COMPANY = jsonData.companyName
      publicDao.USERNAME = jsonData.userName
      publicDao.YG_FLAG = jsonData.yjflag
      publicDao.DOOR_TOKEN = jsonData.doorToken
      publicDao.DOWNLOAD_FILE = jsonData.downloadFile
      if(jsonData.phoneNum && jsonData.phoneNum.length > 0){
        publicDao.PHONE_NUMBER = jsonData.phoneNum
      }
      if (jsonData.phonePWD && jsonData.phonePWD.length > 0) {
        publicDao.PHONE_PWD = jsonData.phonePWD
      }
      publicDao.IS_PHONE_LOGIN = jsonData.isPhoneLogin
      publicDao.IS_IDENTIFY_LOGIN = jsonData.isIdentifyLogin
      SplashScreen.hide();
      if (jsonData.token && jsonData.token.length > 1) {
        publicDao.IS_LOGOUT = '0'
        navigate('Main')
      } else {
        // navigate('Auth')
        publicDao.IS_LOGOUT = '1'
        navigate('Main')
        if(publicDao.COMPANY && publicDao.COMPANY.length > 0){
          if(GlobalStyles.isIos){
            let dirs = RNFetchBlob.fs.dirs
            let dir = dirs.DocumentDir + '/' + publicDao.COMPANY
            this.createDownloadFile(dir)
          }else{
            let dirs = RNFetchBlob.fs.dirs
            let dir = dirs.DownloadDir + '/' + publicDao.COMPANY
            this.createDownloadFile(dir)
          }
        }
      }
    })
  }

  //创建单证下载文件夹
  createDownloadFile(dir) {
    RNFetchBlob.fs.exists(dir)
      .then((res) => {
        if (res) {
          console.log('文件夹已存在')
        } else {
          RNFetchBlob.fs.mkdir(dir)
            .then((res) => {
              if (res) {
                console.log('创建文件夹成功')
                publicDao.DOWNLOAD_FILE = dir
              }
            })
            .catch((error) => {
              console.log('创建文件夹失败', error)
            })
        }
      })
      .catch((error) => {
        console.log('检测文件夹失败')
      })

  }

  componentDidMount() {
    // SplashScreen.hide();
    // this.timer = setTimeout(() => {
    //   SplashScreen.hide();
    // }, 1000);

    //监听网络变化事件
    NetInfo.addEventListener('connectionChange', (networkType) => {
      if ((networkType.type == 'none') || (networkType.type == 'NONE')) {

      } else {
        DeviceEventEmitter.emit(EMITTER_NETCHANGE_TYPE)
      }
    })
    this.getUserInfo()

  }

  render() {
    return (
      <View style={styles.container}>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
